﻿using System;
using Infrastructure.DAL;
using Infrastructure.Models;
using Infrastructure.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test
{
    [TestClass]
    public class TestUserDAL
    {
        private UserDAL dal = null;
        public TestUserDAL()
        {
            DataBaseHelper.InitDataBase();
            dal = new UserDAL();
        }

        [TestMethod]
        public void TestGetAll()
        {
            var list=   dal.GetAll();
            Assert.AreNotEqual(0, list.Count);
        }
        [TestMethod]
        public void TestInsert()
        {
            Users user = new Users();
            user.name = "超管";
            user.account = "admin";
            user.password =MD5Helper.GetMD5("123456");
            user.create_time = DateTime.Now;
            var id = dal.Insert(user);
            Assert.AreEqual(1, id);
        }

        [TestMethod]
        public void TestDelete()
        {
            
            var id = dal.Delete(4);
            Assert.AreEqual(1, id);
        }

        [TestMethod]
        public void TestGetUserByNameAndpwd()
        {
            string pwd = MD5Helper.GetMD5("123456");
            var user = dal.GetByUserNameAndPwd("admin",pwd);
            Assert.AreNotEqual(0, user.id);
        }
    }
}
