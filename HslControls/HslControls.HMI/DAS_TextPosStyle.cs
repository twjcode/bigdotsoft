namespace HslControls.HMI
{
	public enum DAS_TextPosStyle
	{
		TPS_Left,
		TPS_Right,
		TPS_Top,
		TPS_Bottom,
		TPS_Center
	}
}
