namespace HslControls.HMI
{
	public enum DAS_DisplayStyle
	{
		DS_Hex_Style,
		DS_Dec_Style,
		DS_Oct_Style,
		DS_Bin_Style,
		DS_TimeAP_Style,
		DS_Time24_Style,
		DS_Date1_Style,
		DS_Date2_Style,
		DS_TimeAP_Date1_Style,
		DS_TimeAP_Date2_Style,
		DS_Time24_Date1_Style,
		DS_Time24_Date2_Style
	}
}
