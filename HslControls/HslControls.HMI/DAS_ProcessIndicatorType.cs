namespace HslControls.HMI
{
	public enum DAS_ProcessIndicatorType
	{
		PIT_Arrow,
		PIT_Triangle,
		PIT_Squre,
		PIT_Diamond,
		PIT_Circle,
		PIT_Rotation1,
		PIT_Rotation2,
		PIT_Rotation3,
		PIT_Rotation4,
		PIT_Curve,
		PIT_Text,
		PIT_Image,
		PIT_ImageRotation
	}
}
