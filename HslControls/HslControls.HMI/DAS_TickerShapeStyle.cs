namespace HslControls.HMI
{
	public enum DAS_TickerShapeStyle
	{
		Line,
		Rectangle,
		Solid_Rectangle,
		Triangle,
		Solid_Triangle,
		Circle,
		Solid_Circle,
		Trapezoid,
		Solid_Trapezoid
	}
}
