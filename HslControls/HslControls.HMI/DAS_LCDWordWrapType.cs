namespace HslControls.HMI
{
	public enum DAS_LCDWordWrapType
	{
		LWWT_None,
		LWWT_WordWrap1,
		LWWT_WordWrap2,
		LWWT_WordWrap3
	}
}
