namespace HslControls.HMI
{
	public enum DAS_MatrixLEDBordeStyle
	{
		MLBS_None,
		MLBS_Flat,
		MLBS_Raised,
		MLBS_Sunken,
		MLBS_HighRaised,
		MLBS_DeepSunken,
		MLBS_Flat3D,
		MLBS_Ring,
		MLBS_Path,
		MLBS_Linear
	}
}
