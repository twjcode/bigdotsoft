namespace HslControls.HMI
{
	public enum DAS_ProgressImageStyle
	{
		PIS_Img1,
		PIS_Img1_Moving1,
		PIS_Img1_Moving2,
		PIS_Img1_Img2,
		PIS_Img1_Img2_Moving1,
		PIS_Img1_Img2_Moving2,
		PIS_Img1_Img2_Switching1,
		PIS_Img1_Img2_Switching2,
		PIS_Img1_Img2_Img3,
		PIS_Img1_Img2_Img3_Moving1,
		PIS_Img1_Img2_Img3_Moving2,
		PIS_Img1_Img2_Img3_Switching1,
		PIS_Img1_Img2_Img3_Switching2
	}
}
