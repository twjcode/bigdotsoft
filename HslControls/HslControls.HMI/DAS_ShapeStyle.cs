namespace HslControls.HMI
{
	public enum DAS_ShapeStyle
	{
		SS_Rect,
		SS_RoundRect,
		SS_Diamond,
		SS_Circle,
		SS_Left_Triangle,
		SS_Right_Triangle,
		SS_Top_Triangle,
		SS_Bottom_Triangle,
		SS_Left_Arrow,
		SS_Right_Arrow,
		SS_Top_Arrow,
		SS_Bottom_Arrow
	}
}
