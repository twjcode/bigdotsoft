namespace HslControls.HMI
{
	public enum DAS_MatrixLEDShapeStyle
	{
		MLSS_Rect,
		MLSS_RoundRect,
		MLSS_Diamond,
		MLSS_Circle,
		MLSS_Left_Triangle,
		MLSS_Right_Triangle,
		MLSS_Top_Triangle,
		MLSS_Bottom_Triangle
	}
}
