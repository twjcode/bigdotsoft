namespace HslControls.HMI
{
	public enum DAS_ProgressLevelShape
	{
		PLS_Tube,
		PLS_Tube2,
		PLS_Tube3,
		PLS_Funnel,
		PLS_Funnel2,
		PLS_Tank,
		PLS_Tank2,
		PLS_Tank3,
		PLS_Tank4,
		PLS_Tank5,
		PLS_Tank6,
		PLS_Boiler,
		PLS_Boiler2,
		PLS_Cyclinder,
		PLS_Image
	}
}
