namespace HslControls.HMI
{
	public enum DAS_BkGradientStyle
	{
		BKGS_Linear,
		BKGS_Linear2,
		BKGS_Polygon,
		BKGS_Sphere,
		BKGS_Sphere2,
		BKGS_Shine
	}
}
