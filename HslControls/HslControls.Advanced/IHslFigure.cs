using System.Drawing;

namespace HslControls.Advanced
{
	/// <summary>
	/// 图形元素的接口信息，定义了具有什么样子功能和行为
	/// </summary>
	public interface IHslFigure
	{
		/// <summary>
		/// 当前图形的唯一ID信息
		/// </summary>
		string FigureId
		{
			get;
			set;
		}

		/// <summary>
		/// 获取或设置当前图形元素的类型
		/// </summary>
		string FigureType
		{
			get;
			set;
		}

		/// <summary>
		/// 当前的图形的名称
		/// </summary>
		string Name
		{
			get;
			set;
		}

		/// <summary>
		/// 当前图形的位置信息
		/// </summary>
		PointF Location
		{
			get;
			set;
		}

		/// <summary>
		/// 当前元素的宽度信息，默认100
		/// </summary>
		float Width
		{
			get;
			set;
		}

		/// <summary>
		/// 当前元素的高度信息，默认100
		/// </summary>
		float Height
		{
			get;
			set;
		}

		/// <summary>
		/// 当前的图形对象是否处于选择
		/// </summary>
		bool Selected
		{
			get;
			set;
		}

		/// <summary>
		/// 判断光标是否处于当前的图形元素里面
		/// </summary>
		/// <param name="cursor">光标的位置信息，是光标原始的图形坐标的位置</param>
		/// <returns>是否包含当前的位置</returns>
		bool IsMouseIn(PointF cursor);

		/// <summary>
		/// 获取当前图形的中间基准线，需要在子类里进行重写
		/// </summary>
		int GetMiddleDatum();

		/// <summary>
		/// 绘制当前的图形元素的核心方法
		/// </summary>
		/// <param name="g">绘制的画刷</param>
		/// <param name="themeColor">主题颜色</param>
		/// <param name="backColor">主背景色</param>
		/// <param name="font">字体信息</param>
		/// <param name="scale">当前的缩放信息</param>
		void DrawCadFigure(Graphics g, Color themeColor, Color backColor, Font font, float scale);
	}
}
