using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace HslControls.Advanced
{
	/// <summary>
	/// 基于一个封闭的路径的图形元素
	/// </summary>
	public class HslPathFigure : HslFigureBase
	{
		private GraphicsPath[] pathes;

		/// <summary>
		/// 获取或设置图形当前的背景颜色
		/// </summary>
		public Brush FigureBackBrush
		{
			get;
			set;
		}

		/// <summary>
		/// 获取或设置图形当前选中时候的背景颜色
		/// </summary>
		public Brush FigureSelectdBrush
		{
			get;
			set;
		}

		/// <summary>
		/// 获取或设置当前图形元素的边界画笔
		/// </summary>
		public Pen PenBorder
		{
			get;
			set;
		}

		/// <summary>
		/// 实例化一个默认的对象
		/// </summary>
		/// <param name="points">包含的点位信息</param>
		public HslPathFigure(params Point[][] points)
		{
			FigureBackBrush = new SolidBrush(Color.Lavender);
			FigureSelectdBrush = new SolidBrush(Color.LightSkyBlue);
			PenBorder = new Pen(Color.DimGray, 1f);
			pathes = new GraphicsPath[points.Length];
			for (int i = 0; i < pathes.Length; i++)
			{
				pathes[i] = new GraphicsPath(FillMode.Alternate);
				for (int j = 0; j < points[i].Length - 1; j++)
				{
					pathes[i].AddLine(points[i][j], points[i][j + 1]);
				}
				pathes[i].CloseFigure();
			}
		}

		/// <inheritdoc />
		public override bool IsMouseIn(PointF cursor)
		{
			for (int i = 0; i < pathes.Length; i++)
			{
				if (pathes[i].IsVisible(cursor))
				{
					return true;
				}
			}
			return false;
		}

		/// <inheritdoc />
		public override void DrawCadFigure(Graphics g, Color themeColor, Color backColor, Font font, float scale)
		{
			base.DrawCadFigure(g, themeColor, backColor, font, scale);
			if (base.Selected)
			{
				for (int k = 0; k < pathes.Length; k++)
				{
					if (FigureSelectdBrush != null)
					{
						g.FillPath(FigureSelectdBrush, pathes[k]);
					}
				}
			}
			else
			{
				for (int j = 0; j < pathes.Length; j++)
				{
					if (FigureBackBrush != null)
					{
						g.FillPath(FigureBackBrush, pathes[j]);
					}
				}
			}
			for (int i = 0; i < pathes.Length; i++)
			{
				if (PenBorder != null && (double)Math.Abs(PenBorder.Width - 1f / scale) > 0.2)
				{
					Color penColor = PenBorder.Color;
					PenBorder.Dispose();
					PenBorder = new Pen(penColor, 1f / scale);
				}
				if (PenBorder != null)
				{
					g.DrawPath(PenBorder, pathes[i]);
				}
			}
		}
	}
}
