using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Windows.Forms;

namespace HslControls.Charts.Series
{
	/// <summary>
	/// 柱状数据串类
	/// </summary>
	public class ColumnSeries : SeriesBase
	{
		private int _width = 24;

		/// <summary>
		/// 图形宽度
		/// </summary>
		[Category("ZChart")]
		[Description("图形宽度。")]
		[DefaultValue(24)]
		public int Width
		{
			get
			{
				return _width;
			}
			set
			{
				_width = value;
			}
		}

		protected override void PaintLegend(object sender, PaintEventArgs e)
		{
			e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
			using SolidBrush sb = new SolidBrush(base.Color);
			e.Graphics.FillRectangle(sb, 2f, ((float)base.Legend.Height - 16f) / 2f, 12f, 16f);
		}

		internal static void Draw(Graphics g, Rectangle border, int sum, ColumnSeries[] series)
		{
			int len = series.Count();
			float widthSum = 0f;
			for (int j = 0; j < len; j++)
			{
				widthSum += (float)series[j].Width;
			}
			if (!(widthSum > 0f))
			{
				return;
			}
			widthSum += (float)(len - 1) * 3f;
			float axisLenght = border.Width;
			float step = axisLenght / (float)sum;
			float x2 = (float)border.Left + step - widthSum / 2f;
			for (int i = 0; i < len; i++)
			{
				if (!series[i].Visible)
				{
					continue;
				}
				PointF[] pts = series[i].GetLocationArray();
				if (pts != null && pts.Length != 0)
				{
					using SolidBrush sb = new SolidBrush(series[i].Color);
					for (int k = 0; k < pts.Length; k++)
					{
						float x = x2 + step * (float)k;
						float y = pts[k].Y;
						float width = series[i].Width;
						float height = (float)border.Bottom - pts[k].Y - 1f;
						RectangleF rect = new RectangleF(x, y, width, height);
						g.FillRectangle(sb, rect);
					}
				}
				x2 += (float)series[i].Width + 3f;
			}
		}
	}
}
