using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Windows.Forms;

namespace HslControls.Charts.Series
{
	/// <summary>
	/// 折线数据串类
	/// </summary>
	public class LineSeries : SeriesBase
	{
		private float _thickness = 1.8f;

		private float _curveTension = 0f;

		private bool _fillGraph = true;

		private bool _pointVisible = false;

		private bool _valueVisible = false;

		private Font _font = new Font("微软雅黑", 10f);

		/// <summary>
		/// 图形宽度
		/// </summary>
		[Category("ZChart")]
		[Description("图形宽度。")]
		[DefaultValue(1.8f)]
		public float Thickness
		{
			get
			{
				return _thickness;
			}
			set
			{
				_thickness = value;
			}
		}

		/// <summary>
		/// 曲线张力
		/// </summary>
		/// <remarks>数据量大时，设置张力后会降低绘制效率。</remarks>
		[Category("ZChart")]
		[Description("曲线张力。")]
		[DefaultValue(0f)]
		public float CurveTension
		{
			get
			{
				return _curveTension;
			}
			set
			{
				_curveTension = value;
			}
		}

		/// <summary>
		/// 是否填充图形
		/// </summary>
		[Category("ZChart")]
		[Description("是否填充图形。")]
		[DefaultValue(true)]
		public bool FillGraph
		{
			get
			{
				return _fillGraph;
			}
			set
			{
				_fillGraph = value;
			}
		}

		/// <summary>
		/// 显示/隐藏点状图形
		/// </summary>
		[Category("ZChart")]
		[Description("显示/隐藏点状图形。")]
		[DefaultValue(false)]
		public bool PointVisible
		{
			get
			{
				return _pointVisible;
			}
			set
			{
				_pointVisible = value;
			}
		}

		/// <summary>
		/// 显示/隐藏当前值
		/// </summary>
		[Category("ZChart")]
		[Description("显示/隐藏当前值。")]
		[DefaultValue(false)]
		public bool ValueVisible
		{
			get
			{
				return _valueVisible;
			}
			set
			{
				_valueVisible = value;
			}
		}

		/// <summary>
		/// 字体
		/// </summary>
		[Category("ZChart")]
		[Description("字体。")]
		public Font Font
		{
			get
			{
				return _font;
			}
			set
			{
				_font = value;
			}
		}

		internal override void Draw(Graphics g)
		{
			PointF[] pts = GetLocationArray();
			if (pts != null && pts.Length > 1)
			{
				using (Pen p = new Pen(base.Color, Thickness))
				{
					if (CurveTension > 0f)
					{
						g.DrawCurve(p, pts, CurveTension);
					}
					else
					{
						g.DrawLines(p, pts);
					}
				}
				if (FillGraph && base.ParentAxisY != null)
				{
					double x = base.ParentAxisY.IsReverse ? base.ParentAxisY.MaxValue : base.ParentAxisY.MinValue;
					int y2 = (int)Math.Round(CountTop(x));
					PointF[] pts2 = new PointF[pts.Length + 2];
					pts.CopyTo(pts2, 0);
					pts2[pts2.Length - 1] = new PointF(pts[0].X, y2);
					pts2[pts2.Length - 2] = new PointF(pts[pts.Length - 1].X, y2);
					using SolidBrush brush = new SolidBrush(Color.FromArgb(50, base.Color));
					g.FillPolygon(brush, pts2);
				}
			}
			if (base.Points.Count <= 0 || (!_pointVisible && !_valueVisible))
			{
				return;
			}
			using SolidBrush sb = new SolidBrush(base.Color);
			if (_pointVisible)
			{
				float size = 4f * _thickness;
				foreach (ChartPoint cp in base.Points)
				{
					int x2 = (int)Math.Round((float)cp.Left - 2f * _thickness);
					int y = (int)Math.Round((float)cp.Top - 2f * _thickness);
					g.FillEllipse(sb, x2, y, size, size);
				}
			}
			if (_valueVisible)
			{
				ChartPoint lastPoint = base.Points.Last();
				string valStr = lastPoint.Y.ToString();
				if (!string.IsNullOrEmpty(valStr))
				{
					g.DrawString(valStr, _font, sb, new Point(lastPoint.Left, lastPoint.Top), SeriesFormat.LabelFormat);
				}
			}
		}

		protected override void PaintLegend(object sender, PaintEventArgs e)
		{
			e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
			float y = ((float)base.Legend.Height - _thickness) / 2f;
			using Pen p = new Pen(base.Color, _thickness);
			e.Graphics.DrawLine(p, 0f, y, 12f, y);
		}
	}
}
