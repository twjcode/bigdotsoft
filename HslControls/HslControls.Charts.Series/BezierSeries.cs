using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Windows.Forms;

namespace HslControls.Charts.Series
{
	/// <summary>
	/// 贝塞尔数据串类
	/// </summary>
	public class BezierSeries : SeriesBase
	{
		private float _thickness = 1.8f;

		/// <summary>
		/// 图形宽度
		/// </summary>
		[Category("ZChart")]
		[Description("图形宽度。")]
		[DefaultValue(1.8f)]
		public float Thickness
		{
			get
			{
				return _thickness;
			}
			set
			{
				_thickness = value;
			}
		}

		internal override void Draw(Graphics g)
		{
			PointF[] pts = GetLocationArray();
			if (pts != null && pts.Length > 3)
			{
				int i = pts.Length % 3;
				int j = pts.Length / 3;
				int b = (i != 0) ? 1 : (-2);
				PointF[] bezierPts = pts.Take(3 * j + b).ToArray();
				using Pen p = new Pen(base.Color, Thickness);
				g.DrawBeziers(p, bezierPts);
			}
		}

		protected override void PaintLegend(object sender, PaintEventArgs e)
		{
			e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
			PointF[] pts = new PointF[4]
			{
				new PointF(0f, (float)base.Legend.Height / 2f),
				new PointF(3f, 0f),
				new PointF(9f, base.Legend.Height),
				new PointF(12f, (float)base.Legend.Height / 2f)
			};
			using Pen p = new Pen(base.Color, _thickness);
			e.Graphics.DrawBeziers(p, pts);
		}
	}
}
