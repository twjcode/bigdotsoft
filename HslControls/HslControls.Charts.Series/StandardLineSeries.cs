using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace HslControls.Charts.Series
{
	/// <summary>
	/// 基准线数据串类
	/// </summary>
	public class StandardLineSeries : SeriesBase
	{
		private float _thickness = 1f;

		private bool _horizontatLineVisible = true;

		private bool _verticalLineVisible = true;

		/// <summary>
		/// 图形宽度
		/// </summary>
		[Category("ZChart")]
		[Description("图形宽度。")]
		[DefaultValue(1f)]
		public float Thickness
		{
			get
			{
				return _thickness;
			}
			set
			{
				_thickness = value;
			}
		}

		/// <summary>
		/// 显示/隐藏横向基准线
		/// </summary>
		[Category("ZChart")]
		[Description("显示/隐藏横向基准线。")]
		[DefaultValue(true)]
		public bool HorizontatLineVisible
		{
			get
			{
				return _horizontatLineVisible;
			}
			set
			{
				_horizontatLineVisible = value;
			}
		}

		/// <summary>
		/// 显示/隐藏纵向基准线
		/// </summary>
		[Category("ZChart")]
		[Description("显示/隐藏纵向基准线。")]
		[DefaultValue(true)]
		public bool VerticalLineVisible
		{
			get
			{
				return _verticalLineVisible;
			}
			set
			{
				_verticalLineVisible = value;
			}
		}

		internal override void Draw(Graphics g)
		{
			PointF[] pts = GetLocationArray();
			if (pts == null || pts.Length == 0)
			{
				return;
			}
			using Pen p = new Pen(base.Color, Thickness);
			if (_horizontatLineVisible && base.ParentAxisX != null)
			{
				float x1 = CountLeft(base.ParentAxisX.MinValue);
				float x2 = CountLeft(base.ParentAxisX.MaxValue);
				PointF[] array = pts;
				for (int i = 0; i < array.Length; i++)
				{
					PointF pt2 = array[i];
					g.DrawLine(p, x1, pt2.Y, x2, pt2.Y);
				}
			}
			if (_verticalLineVisible && base.ParentAxisY != null)
			{
				float y1 = CountTop(base.ParentAxisY.MinValue);
				float y2 = CountTop(base.ParentAxisY.MaxValue);
				PointF[] array2 = pts;
				for (int j = 0; j < array2.Length; j++)
				{
					PointF pt = array2[j];
					g.DrawLine(p, pt.X, y1, pt.X, y2);
				}
			}
		}

		protected override void PaintLegend(object sender, PaintEventArgs e)
		{
			e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
			float y = ((float)base.Legend.Height - _thickness) / 2f;
			using Pen p = new Pen(base.Color, _thickness);
			e.Graphics.DrawLine(p, 0f, y, 12f, y);
		}
	}
}
