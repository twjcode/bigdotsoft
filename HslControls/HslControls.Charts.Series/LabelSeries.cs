using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace HslControls.Charts.Series
{
	/// <summary>
	/// 标签数据串类
	/// </summary>
	public class LabelSeries : SeriesBase
	{
		private int _width = 12;

		private int _height = 12;

		private Font _font = new Font("微软雅黑", 10f);

		/// <summary>
		/// 图形宽度
		/// </summary>
		[Category("ZChart")]
		[Description("图形宽度。")]
		[DefaultValue(12)]
		public int Width
		{
			get
			{
				return _width;
			}
			set
			{
				_width = value;
			}
		}

		/// <summary>
		/// 图形宽度
		/// </summary>
		[Category("ZChart")]
		[Description("图形高度。")]
		[DefaultValue(12)]
		public int Height
		{
			get
			{
				return _height;
			}
			set
			{
				_height = value;
			}
		}

		/// <summary>
		/// 字体
		/// </summary>
		[Category("ZChart")]
		[Description("字体。")]
		public Font Font
		{
			get
			{
				return _font;
			}
			set
			{
				_font = value;
			}
		}

		internal override void Draw(Graphics g)
		{
			if (base.Points.Count <= 0)
			{
				return;
			}
			using SolidBrush sb = new SolidBrush(base.Color);
			foreach (ChartPoint cp in base.Points)
			{
				int x = cp.Left;
				int y = cp.Top;
				Point[] iconPs = new Point[3]
				{
					new Point(x, y),
					new Point(x - _width / 2, y - _height),
					new Point(x + _width / 2, y - _height)
				};
				g.FillPolygon(sb, iconPs);
				if (!string.IsNullOrEmpty(cp.Label))
				{
					g.DrawString(point: new Point(x, y - _height), s: cp.Label, font: _font, brush: sb, format: SeriesFormat.LabelFormat);
				}
			}
		}

		protected override void PaintLegend(object sender, PaintEventArgs e)
		{
			e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
			float x1 = ((float)base.Legend.Height - 12f) / 2f;
			PointF[] pts = new PointF[3]
			{
				new PointF(0f, x1),
				new PointF(12f, x1),
				new PointF(6f, x1 + 12f)
			};
			using SolidBrush sb = new SolidBrush(base.Color);
			e.Graphics.FillPolygon(sb, pts);
		}
	}
}
