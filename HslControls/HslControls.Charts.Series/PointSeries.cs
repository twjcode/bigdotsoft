using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace HslControls.Charts.Series
{
	/// <summary>
	/// 点数据串类
	/// </summary>
	public class PointSeries : SeriesBase
	{
		private int _width = 8;

		private int _height = 8;

		/// <summary>
		/// 图形宽度
		/// </summary>
		[Category("ZChart")]
		[Description("图形宽度。")]
		[DefaultValue(8)]
		public int Width
		{
			get
			{
				return _width;
			}
			set
			{
				_width = value;
			}
		}

		/// <summary>
		/// 图形宽度
		/// </summary>
		[Category("ZChart")]
		[Description("图形高度。")]
		[DefaultValue(8)]
		public int Height
		{
			get
			{
				return _height;
			}
			set
			{
				_height = value;
			}
		}

		internal override void Draw(Graphics g)
		{
			if (base.Points.Count <= 0)
			{
				return;
			}
			using SolidBrush sb = new SolidBrush(base.Color);
			foreach (ChartPoint cp in base.Points)
			{
				float x = (float)cp.Left - (float)_width / 2f;
				float y = (float)cp.Top - (float)_height / 2f;
				g.FillEllipse(sb, x, y, _width, _height);
			}
		}

		protected override void PaintLegend(object sender, PaintEventArgs e)
		{
			e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
			using SolidBrush sb = new SolidBrush(base.Color);
			e.Graphics.FillEllipse(sb, 2f, (float)(base.Legend.Height - 8) / 2f, 8f, 8f);
		}
	}
}
