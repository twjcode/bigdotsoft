using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace HslControls.Forms
{
	public class FormHslDigitalInput : Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private IContainer components = null;

		private HslDigitalInput hslDigitalInput1;

		/// <summary>
		/// 当按ok键触发的信息
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public Action<string> OnOk
		{
			get
			{
				return hslDigitalInput1.OnOk;
			}
			set
			{
				hslDigitalInput1.OnOk = value;
			}
		}

		/// <summary>
		/// 当前的显示控件，可以用来更改一些属性设计。
		/// </summary>
		public HslDigitalInput DigitalInput => hslDigitalInput1;

		public FormHslDigitalInput(Action<string> onOk = null)
		{
			InitializeComponent();
			OnOk = onOk;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			hslDigitalInput1 = new HslControls.HslDigitalInput();
			SuspendLayout();
			hslDigitalInput1.BackColor = System.Drawing.Color.Linen;
			hslDigitalInput1.ButtonColor = System.Drawing.Color.Beige;
			hslDigitalInput1.DisplayBackColor = System.Drawing.Color.FromArgb(210, 225, 213);
			hslDigitalInput1.Dock = System.Windows.Forms.DockStyle.Fill;
			hslDigitalInput1.EnableNegative = true;
			hslDigitalInput1.EnableSpot = true;
			hslDigitalInput1.Font = new System.Drawing.Font("微软雅黑", 18f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 134);
			hslDigitalInput1.ForeColor = System.Drawing.Color.Black;
			hslDigitalInput1.LedBackColor = System.Drawing.Color.FromArgb(210, 225, 213);
			hslDigitalInput1.LedForeColor = System.Drawing.Color.FromArgb(64, 64, 64);
			hslDigitalInput1.Location = new System.Drawing.Point(0, 0);
			hslDigitalInput1.Name = "hslDigitalInput1";
			hslDigitalInput1.Size = new System.Drawing.Size(276, 308);
			hslDigitalInput1.TabIndex = 0;
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			base.ClientSize = new System.Drawing.Size(276, 308);
			base.Controls.Add(hslDigitalInput1);
			Font = new System.Drawing.Font("微软雅黑", 15f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 134);
			base.MaximizeBox = false;
			MaximumSize = new System.Drawing.Size(292, 347);
			base.MinimizeBox = false;
			MinimumSize = new System.Drawing.Size(292, 347);
			base.Name = "FormHslDigitalInput";
			base.ShowIcon = false;
			base.ShowInTaskbar = false;
			base.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			Text = "数字键盘";
			ResumeLayout(false);
		}
	}
}
