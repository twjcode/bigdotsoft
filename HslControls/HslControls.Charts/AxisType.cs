namespace HslControls.Charts
{
	/// <summary>
	/// 坐标轴类型
	/// </summary>
	public enum AxisType
	{
		X,
		Y
	}
}
