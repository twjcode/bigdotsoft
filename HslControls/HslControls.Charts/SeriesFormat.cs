using System.Drawing;

namespace HslControls.Charts
{
	/// <summary>
	/// 数据串格式类，定义了数据的布局信息。
	/// </summary>
	public class SeriesFormat
	{
		public static StringFormat LabelFormat
		{
			get;
			set;
		} = new StringFormat
		{
			Alignment = StringAlignment.Center,
			LineAlignment = StringAlignment.Far
		};

	}
}
