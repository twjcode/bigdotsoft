using System.ComponentModel;

namespace HslControls.Charts
{
	/// <summary>
	/// 坐标轴标签类
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class AxisLabel
	{
		private string _content;

		private double _value;

		/// <summary>
		/// 内容
		/// </summary>
		public string Content
		{
			get
			{
				return _content;
			}
			set
			{
				_content = value;
			}
		}

		/// <summary>
		/// 值
		/// </summary>
		public double Value
		{
			get
			{
				return _value;
			}
			set
			{
				_value = value;
			}
		}

		/// <summary>
		/// 实例化一个AxisLabel类
		/// </summary>
		/// <param name="value">值</param>
		public AxisLabel(double value)
		{
			_value = value;
			_content = _value.ToString();
		}

		/// <summary>
		/// 实例化一个AxisLabel类
		/// </summary>
		public AxisLabel()
			: this(0.0)
		{
		}
	}
}
