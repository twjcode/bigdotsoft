using System;
using System.ComponentModel.Design;
using HslControls.Charts.Series;

namespace HslControls.Charts
{
	public class SeriesCollectionEditor : CollectionEditor
	{
		public SeriesCollectionEditor(Type type)
			: base(type)
		{
		}

		protected override bool CanSelectMultipleInstances()
		{
			return true;
		}

		protected override Type[] CreateNewItemTypes()
		{
			return new Type[6]
			{
				typeof(BezierSeries),
				typeof(ColumnSeries),
				typeof(LabelSeries),
				typeof(LineSeries),
				typeof(PointSeries),
				typeof(StandardLineSeries)
			};
		}

		protected override object CreateInstance(Type itemType)
		{
			if ((object)itemType == typeof(BezierSeries))
			{
				return new BezierSeries();
			}
			if ((object)itemType == typeof(ColumnSeries))
			{
				return new ColumnSeries();
			}
			if ((object)itemType == typeof(LabelSeries))
			{
				return new LabelSeries();
			}
			if ((object)itemType == typeof(LineSeries))
			{
				return new LineSeries();
			}
			if ((object)itemType == typeof(PointSeries))
			{
				return new PointSeries();
			}
			if ((object)itemType == typeof(StandardLineSeries))
			{
				return new StandardLineSeries();
			}
			return null;
		}
	}
}
