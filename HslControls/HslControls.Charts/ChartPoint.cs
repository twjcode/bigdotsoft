using System;
using System.ComponentModel;
using System.Drawing;

namespace HslControls.Charts
{
	/// <summary>
	/// 数据点类，描述数据点的值与所在图表的坐标。
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class ChartPoint
	{
		private HslChart _chart;

		private SeriesBase _series;

		private double _x;

		private double _y;

		private string _label;

		/// <summary>
		/// 图表对象
		/// </summary>
		[Browsable(false)]
		public HslChart Chart
		{
			get
			{
				return _chart;
			}
			internal set
			{
				_chart = value;
			}
		}

		/// <summary>
		/// 数据串对象
		/// </summary>
		[Browsable(false)]
		public SeriesBase Series
		{
			get
			{
				return _series;
			}
			internal set
			{
				_series = value;
			}
		}

		/// <summary>
		/// 附着的X轴
		/// </summary>
		[Browsable(false)]
		public Axis ParentAxisX
		{
			get
			{
				if (_series != null)
				{
					return _series.ParentAxisX;
				}
				return null;
			}
		}

		/// <summary>
		/// 附着的Y轴
		/// </summary>
		[Browsable(false)]
		public Axis ParentAxisY
		{
			get
			{
				if (_series != null)
				{
					return _series.ParentAxisY;
				}
				return null;
			}
		}

		/// <summary>
		/// 数据值X
		/// </summary>
		public double X
		{
			get
			{
				return _x;
			}
			set
			{
				if (_x != value)
				{
					_x = value;
					_chart?.Invalidate();
					OnValueChanged(new EventArgs());
				}
			}
		}

		/// <summary>
		/// 数据值Y
		/// </summary>
		public double Y
		{
			get
			{
				return _y;
			}
			set
			{
				if (_y != value)
				{
					_y = value;
					_chart?.Invalidate();
					OnValueChanged(new EventArgs());
				}
			}
		}

		/// <summary>
		/// 标签
		/// </summary>
		public string Label
		{
			get
			{
				return _label;
			}
			set
			{
				if (_label != value)
				{
					_label = value;
					_chart?.Invalidate();
					OnLabelChanged(new EventArgs());
				}
			}
		}

		/// <summary>
		/// 与图表左边缘的距离
		/// </summary>
		[Browsable(false)]
		public int Left
		{
			get
			{
				int offset = (_chart != null) ? _chart.OffsetX : 0;
				return (int)(Series.CountLeft(X) + (float)offset);
			}
		}

		/// <summary>
		/// 与图表上边缘的距离
		/// </summary>
		[Browsable(false)]
		public int Top
		{
			get
			{
				int offset = (_chart != null) ? _chart.OffsetY : 0;
				return (int)(Series.CountTop(Y) + (float)offset);
			}
		}

		/// <summary>
		/// 在图表上的坐标
		/// </summary>
		[Browsable(false)]
		public Point Location => new Point(Left, Top);

		public event EventHandler ValueChanged;

		public event EventHandler LabelChanged;

		/// <summary>
		/// 实例化一个ChartPoint对象
		/// </summary>
		/// <param name="x">数据值X</param>
		/// <param name="y">数据值Y</param>
		public ChartPoint(double x, double y)
		{
			_x = x;
			_y = y;
		}

		/// <summary>
		/// 实例化一个ChartPoint对象
		/// </summary>
		public ChartPoint()
			: this(0.0, 0.0)
		{
		}

		protected void OnValueChanged(EventArgs e)
		{
			this.ValueChanged?.Invoke(this, e);
		}

		protected void OnLabelChanged(EventArgs e)
		{
			this.LabelChanged?.Invoke(this, e);
		}

		public string GetLabelFromAxisX()
		{
			if (ParentAxisX != null)
			{
				return ParentAxisX.GetLabel(_x);
			}
			return null;
		}

		public string GetLabelFromAxisY()
		{
			if (ParentAxisY != null)
			{
				return ParentAxisY.GetLabel(_y);
			}
			return null;
		}
	}
}
