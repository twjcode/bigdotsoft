namespace HslControls.Charts
{
	/// <summary>
	/// 缩放类型
	/// </summary>
	public enum ZoomType
	{
		None,
		Enlarge,
		Reduce
	}
}
