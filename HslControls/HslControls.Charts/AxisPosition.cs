namespace HslControls.Charts
{
	/// <summary>
	/// 坐标轴位置
	/// </summary>
	public enum AxisPosition
	{
		LeftBottom,
		RightTop
	}
}
