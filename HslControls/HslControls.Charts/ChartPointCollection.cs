using System.Collections.ObjectModel;

namespace HslControls.Charts
{
	/// <summary>
	/// 数据点集合类
	/// </summary>
	public class ChartPointCollection : Collection<ChartPoint>
	{
		/// <summary>
		/// 图表对象
		/// </summary>
		public HslChart Chart
		{
			get;
			internal set;
		}

		/// <summary>
		/// 数据串对象
		/// </summary>
		public SeriesBase Series
		{
			get;
			internal set;
		}

		protected override void InsertItem(int index, ChartPoint item)
		{
			base.InsertItem(index, item);
			item.Chart = Chart;
			item.Series = Series;
			Chart?.Invalidate();
		}

		protected override void SetItem(int index, ChartPoint item)
		{
			base.SetItem(index, item);
			item.Chart = Chart;
			item.Series = Series;
			Chart?.Invalidate();
		}

		protected override void ClearItems()
		{
			base.ClearItems();
			Chart?.Invalidate();
		}

		protected override void RemoveItem(int index)
		{
			base.RemoveItem(index);
			Chart?.Invalidate();
		}
	}
}
