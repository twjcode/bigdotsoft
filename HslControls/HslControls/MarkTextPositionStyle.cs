namespace HslControls
{
	/// <summary>
	/// 标记点的样式信息
	/// </summary>
	public enum MarkTextPositionStyle
	{
		/// <summary>
		/// 在标记点的上方
		/// </summary>
		Up = 1,
		/// <summary>
		/// 在标记点的右侧
		/// </summary>
		Right = 2,
		/// <summary>
		/// 在标记点的下方
		/// </summary>
		Down = 3,
		/// <summary>
		/// 在标记点的左侧
		/// </summary>
		Left = 4,
		/// <summary>
		/// 自动选择位置
		/// </summary>
		Auto = 10
	}
}
