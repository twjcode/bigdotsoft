namespace HslControls
{
	/// <summary>
	/// 所有标记信息的基类，支持区间的表示
	/// </summary>
	public class HslMarkSectionBase
	{
		/// <summary>
		/// 开始的数据位置
		/// </summary>
		public int StartIndex
		{
			get;
			set;
		}

		/// <summary>
		/// 结束的数据位置
		/// </summary>
		public int EndIndex
		{
			get;
			set;
		}
	}
}
