using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslControls
{
	/// <summary>
	/// 一个开关操作的控件，可用于两种状态的切换
	/// </summary>
	[DefaultEvent("OnSwitchChanged")]
	[Description("一个开关控件，可以在开关两种状态之间进行选择")]
	public class HslSwitch : UserControl
	{
		private Color textForeColor = Color.Black;

		private Brush textForeBrush = new SolidBrush(Color.Black);

		private Color color_switch_background = Color.DimGray;

		private Brush brush_switch_background = new SolidBrush(Color.DimGray);

		private Pen pen_switch_background = new Pen(Color.DimGray, 2f);

		private bool switch_status = false;

		private Color color_switch_foreground = Color.FromArgb(36, 36, 36);

		private Brush brush_switch_foreground = new SolidBrush(Color.FromArgb(36, 36, 36));

		private StringFormat sf = null;

		private string description = "Off;On";

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置控件的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Transparent")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// 获取或设置开关按钮的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置开关按钮的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "DimGray")]
		public Color SwitchBackground
		{
			get
			{
				return color_switch_background;
			}
			set
			{
				color_switch_background = value;
				brush_switch_background?.Dispose();
				pen_switch_background?.Dispose();
				brush_switch_background = new SolidBrush(color_switch_background);
				pen_switch_background = new Pen(color_switch_background, 2f);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置开关按钮的前景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置开关按钮的前景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "[36, 36, 36]")]
		public Color SwitchForeground
		{
			get
			{
				return color_switch_foreground;
			}
			set
			{
				color_switch_foreground = value;
				brush_switch_foreground = new SolidBrush(color_switch_foreground);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置开关按钮的开合状态
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置开关按钮的开合状态")]
		[Category("HslControls")]
		[DefaultValue(false)]
		public bool SwitchStatus
		{
			get
			{
				return switch_status;
			}
			set
			{
				if (value != switch_status)
				{
					switch_status = value;
					Invalidate();
					this.OnSwitchChanged?.Invoke(this, switch_status);
				}
			}
		}

		/// <summary>
		/// 获取或设置两种开关状态的文本描述，例如："Off;On"
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置两种开关状态的文本描述，例如：Off;On")]
		[Category("HslControls")]
		[DefaultValue("Off;On")]
		public string SwitchStatusDescription
		{
			get
			{
				return description;
			}
			set
			{
				description = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置当前控件的文本
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前控件的文本")]
		[Category("HslControls")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Bindable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置当前控件的文本的颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前控件的文本的颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Black")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color ForeColor
		{
			get
			{
				return textForeColor;
			}
			set
			{
				textForeColor = value;
				textForeBrush.Dispose();
				textForeBrush = new SolidBrush(value);
				Invalidate();
			}
		}

		/// <summary>
		/// 开关按钮发生变化的事件
		/// </summary>
		[Category("Action")]
		[Description("点击了按钮开发后触发")]
		public event Action<object, bool> OnSwitchChanged;

		/// <summary>
		/// 实例化一个默认的对象
		/// </summary>
		public HslSwitch()
		{
			InitializeComponent();
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
		}

		private Point GetCenterPoint()
		{
			if (base.Height > base.Width)
			{
				return new Point(base.Width / 2, base.Width / 2);
			}
			return new Point(base.Height / 2, base.Height / 2);
		}

		/// <summary>
		/// 重绘控件的界面信息
		/// </summary>
		/// <param name="e">重绘事件</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			if (!Authorization.iashdiadasbdnajsdhjaf())
			{
				return;
			}
			string[] tmp = description.Split(new char[1]
			{
				';'
			}, StringSplitOptions.RemoveEmptyEntries);
			string tmp2 = string.Empty;
			string tmp3 = string.Empty;
			if (tmp.Length != 0)
			{
				tmp2 = tmp[0];
			}
			if (tmp.Length > 1)
			{
				tmp3 = tmp[1];
			}
			e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
			e.Graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
			Point center = GetCenterPoint();
			e.Graphics.TranslateTransform(center.X, center.Y);
			int radius = 45 * (center.X * 2 - 30) / 130;
			if (radius >= 5)
			{
				Rectangle rectangle_larger = new Rectangle(-radius - 4, -radius - 4, 2 * radius + 8, 2 * radius + 8);
				Rectangle rectangle = new Rectangle(-radius, -radius, 2 * radius, 2 * radius);
				e.Graphics.DrawEllipse(pen_switch_background, rectangle_larger);
				e.Graphics.FillEllipse(brush_switch_background, rectangle);
				float angle = -36f;
				if (switch_status)
				{
					angle = 36f;
				}
				e.Graphics.RotateTransform(angle);
				int temp = 20 * (center.X * 2 - 30) / 130;
				Rectangle rect_switch = new Rectangle(-center.X / 8, -radius - temp, center.X / 4, radius * 2 + temp * 2);
				e.Graphics.FillRectangle(brush_switch_foreground, rect_switch);
				Rectangle rect_mini = new Rectangle(-center.X / 16, -radius - 10, center.X / 8, center.X * 3 / 8);
				e.Graphics.FillEllipse(switch_status ? Brushes.LimeGreen : Brushes.Tomato, rect_mini);
				Rectangle rect_text = new Rectangle(-50, -radius - temp - 15, 100, 15);
				e.Graphics.DrawString(switch_status ? tmp3 : tmp2, Font, switch_status ? Brushes.LimeGreen : Brushes.Tomato, rect_text, sf);
				e.Graphics.ResetTransform();
				if (base.Height - Math.Min(base.Width, base.Height) > 0 && !string.IsNullOrEmpty(Text))
				{
					e.Graphics.DrawString(Text, Font, textForeBrush, new Rectangle(0, Math.Min(base.Width, base.Height) - 15, Math.Min(base.Width, base.Height), base.Height - Math.Min(base.Width, base.Height) + 15), sf);
				}
				base.OnPaint(e);
			}
		}

		/// <summary>
		/// 鼠标的点击事件
		/// </summary>
		/// <param name="e">鼠标的点击事件</param>
		protected override void OnMouseClick(MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				SwitchStatus = !SwitchStatus;
			}
			base.OnClick(e);
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			BackColor = System.Drawing.Color.Transparent;
			Cursor = System.Windows.Forms.Cursors.Hand;
			base.Name = "HslSwitch";
			ResumeLayout(false);
		}
	}
}
