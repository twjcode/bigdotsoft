namespace HslControls
{
	/// <summary>
	/// 皮带链的样式，选择水平还是下坡，还是上坡
	/// </summary>
	public enum HslConveyerStyle
	{
		/// <summary>
		/// 水平
		/// </summary>
		Horizontal = 1,
		/// <summary>
		/// 下坡
		/// </summary>
		Downslope,
		/// <summary>
		/// 上坡
		/// </summary>
		Upslope
	}
}
