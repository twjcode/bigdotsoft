using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslControls
{
	/// <summary>
	/// 一个阀门控件，支持颜色的设置
	/// </summary>
	[Description("一个阀门控件，支持开关两种状态，支持颜色的设置")]
	public class HslValves : UserControl
	{
		private StringFormat sf = null;

		private Color edgeColor = Color.Gray;

		private Pen edgePen = new Pen(Color.Gray, 1f);

		private HslDirectionStyle hslValvesStyle = HslDirectionStyle.Horizontal;

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置控件的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Transparent")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// 获取或设置当前控件的文本
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前控件的文本")]
		[Category("HslControls")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Bindable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置阀门控件的边缘颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置阀门控件的边缘颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "DimGray")]
		public Color EdgeColor
		{
			get
			{
				return edgeColor;
			}
			set
			{
				edgeColor = value;
				edgePen = new Pen(value, 1f);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置阀门控件是否是横向的还是纵向的
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置阀门控件是否是横向的还是纵向的")]
		[Category("HslControls")]
		[DefaultValue(typeof(HslDirectionStyle), "Horizontal")]
		public HslDirectionStyle PipeLineStyle
		{
			get
			{
				return hslValvesStyle;
			}
			set
			{
				hslValvesStyle = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 实例化一个阀门控件
		/// </summary>
		public HslValves()
		{
			InitializeComponent();
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
		}

		/// <summary>
		/// 重绘控件的
		/// </summary>
		/// <param name="e">重绘事件</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			if (Authorization.iashdiadasbdnajsdhjaf())
			{
				Graphics g = e.Graphics;
				g.SmoothingMode = SmoothingMode.HighQuality;
				g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
				if (hslValvesStyle == HslDirectionStyle.Horizontal)
				{
					PaintMain(g, base.Width, base.Height);
				}
				else
				{
					g.TranslateTransform(base.Width, 0f);
					g.RotateTransform(90f);
					PaintMain(g, base.Height, base.Width);
					g.ResetTransform();
				}
				base.OnPaint(e);
			}
		}

		private void PaintMain(Graphics g, float width, float height)
		{
			g.TranslateTransform(width / 2f, 0f);
			ColorBlend colorBlend = new ColorBlend();
			colorBlend.Positions = new float[3]
			{
				0f,
				0.25f,
				1f
			};
			colorBlend.Colors = new Color[3]
			{
				edgeColor,
				Color.WhiteSmoke,
				edgeColor
			};
			LinearGradientBrush brush = new LinearGradientBrush(new PointF(0f, height * 0.34f), new PointF(0f, height * 0.93f), Color.FromArgb(247, 252, 253), Color.WhiteSmoke);
			brush.InterpolationColors = colorBlend;
			g.FillRectangle(brush, (0f - width) * 0.5f - 1f, height * 0.38f, width + 1f, height * 0.55f);
			brush.Dispose();
			brush = new LinearGradientBrush(new PointF(0f, height * 0.25f), new PointF(0f, height * 1.1f), Color.FromArgb(247, 252, 253), Color.WhiteSmoke);
			brush.InterpolationColors = colorBlend;
			g.FillRectangle(brush, (0f - width) * 0.45f, height * 0.31f, width * 0.18f, height * 0.69f);
			g.FillRectangle(brush, width * 0.27f, height * 0.31f, width * 0.18f, height * 0.69f);
			brush.Dispose();
			colorBlend.Positions = new float[3]
			{
				0f,
				0.31f,
				1f
			};
			colorBlend.Colors = new Color[3]
			{
				Color.FromArgb(144, 162, 167),
				Color.WhiteSmoke,
				Color.FromArgb(144, 162, 167)
			};
			brush = new LinearGradientBrush(new PointF((0f - width) * 0.07f, 0f), new PointF(width * 0.07f, 0f), Color.FromArgb(247, 252, 253), Color.WhiteSmoke);
			brush.InterpolationColors = colorBlend;
			g.FillRectangle(brush, (0f - width) * 0.05f, height * 0.1f, width * 0.1f, height * 0.13f);
			brush.Dispose();
			colorBlend.Positions = new float[3]
			{
				0f,
				0.21f,
				1f
			};
			colorBlend.Colors = new Color[3]
			{
				Color.FromArgb(190, 190, 190),
				Color.WhiteSmoke,
				Color.FromArgb(170, 175, 175)
			};
			brush = new LinearGradientBrush(new PointF((0f - width) * 0.1f, 0f), new PointF(width * 0.1f, 0f), Color.FromArgb(247, 252, 253), Color.WhiteSmoke);
			brush.InterpolationColors = colorBlend;
			g.FillEllipse(brush, (0f - width) * 0.1f, height * 0.34f, width * 0.2f, height * 0.08f);
			GraphicsPath path = new GraphicsPath();
			path.AddLines(new PointF[4]
			{
				new PointF((0f - width) * 0.1f, height * 0.38f),
				new PointF((0f - width) * 0.07f, height * 0.22f),
				new PointF(width * 0.07f, height * 0.22f),
				new PointF(width * 0.1f, height * 0.38f)
			});
			path.CloseFigure();
			g.FillPath(brush, path);
			brush.Dispose();
			colorBlend.Positions = new float[3]
			{
				0f,
				0.78f,
				1f
			};
			colorBlend.Colors = new Color[3]
			{
				Color.FromArgb(172, 77, 80),
				Color.FromArgb(244, 188, 189),
				Color.FromArgb(172, 77, 80)
			};
			brush = new LinearGradientBrush(new PointF((0f - width) * 0.23f, 0f), new PointF(width * 0.23f, 0f), Color.FromArgb(247, 252, 253), Color.WhiteSmoke);
			brush.InterpolationColors = colorBlend;
			g.FillRectangle(brush, (0f - width) * 0.23f, 0f, width * 0.46f, height * 0.15f);
			brush.Dispose();
			colorBlend.Positions = new float[3]
			{
				0f,
				0.7f,
				1f
			};
			colorBlend.Colors = new Color[3]
			{
				Color.FromArgb(165, 74, 77),
				Color.FromArgb(244, 188, 189),
				Color.FromArgb(184, 87, 90)
			};
			brush = new LinearGradientBrush(new PointF(0f, 0f), new PointF(width * 0.06f, 0f), Color.FromArgb(247, 252, 253), Color.WhiteSmoke);
			brush.InterpolationColors = colorBlend;
			g.TranslateTransform((0f - width) * 0.03f, 0f);
			g.FillRectangle(brush, 0f, 0f, width * 0.06f, height * 0.15f);
			g.TranslateTransform((0f - width) * 0.2f, 0f);
			g.FillRectangle(brush, 0f, 0f, width * 0.06f, height * 0.15f);
			g.TranslateTransform(width * 0.4f, 0f);
			g.FillRectangle(brush, 0f, 0f, width * 0.06f, height * 0.15f);
			g.TranslateTransform((0f - width) * 0.17f, 0f);
			brush.Dispose();
			using (Brush foreBrush = new SolidBrush(ForeColor))
			{
				g.DrawString(Text, Font, foreBrush, new RectangleF((0f - width) / 2f, height * 0.38f, width, height * 0.55f), sf);
			}
			g.TranslateTransform((0f - width) / 2f, 0f);
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			BackColor = System.Drawing.Color.Transparent;
			base.Name = "HslValves";
			base.Size = new System.Drawing.Size(177, 90);
			ResumeLayout(false);
		}
	}
}
