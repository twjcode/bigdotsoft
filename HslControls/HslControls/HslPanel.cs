using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace HslControls
{
	/// <inheritdoc cref="T:System.Windows.Forms.Panel" />
	[Description("Panel控件，从系统继承而来，支持背景的渐变色颜色")]
	public class HslPanel : Panel
	{
		private Color borderColor = Color.Gray;

		private Color edgeColor = Color.DimGray;

		private Color centerColor = Color.Gray;

		private Pen boderPen = new Pen(Color.Gray, 1f);

		private StringFormat sf = null;

		/// <summary>
		/// 获取或设置当前控件的文本
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前控件的文本")]
		[Category("HslControls")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Bindable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置当前panel的边框颜色
		/// </summary>
		[Description("获取或设置当前panel的边框颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Gray")]
		public Color BorderColor
		{
			get
			{
				return borderColor;
			}
			set
			{
				borderColor = value;
				boderPen?.Dispose();
				boderPen = new Pen(borderColor, 1f);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置当前panel的渐变色的边缘颜色
		/// </summary>
		[Description("获取或设置当前panel的渐变色的边缘颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "DimGray")]
		public Color EdgeColor
		{
			get
			{
				return edgeColor;
			}
			set
			{
				edgeColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置当前panel的渐变色的中间颜色
		/// </summary>
		[Description("获取或设置当前panel的渐变色的中间颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Gray")]
		public Color CenterColor
		{
			get
			{
				return centerColor;
			}
			set
			{
				centerColor = value;
				Invalidate();
			}
		}

		/// <inheritdoc cref="M:System.Windows.Forms.Panel.#ctor" />
		public HslPanel()
		{
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
		}

		/// <inheritdoc />
		protected override void OnPaintBackground(PaintEventArgs e)
		{
			if (Authorization.iashdiadasbdnajsdhjaf())
			{
				Graphics g = e.Graphics;
				ColorBlend colorBlend = new ColorBlend();
				colorBlend.Positions = new float[3]
				{
					0f,
					0.5f,
					1f
				};
				colorBlend.Colors = new Color[3]
				{
					edgeColor,
					centerColor,
					edgeColor
				};
				using (LinearGradientBrush linear = new LinearGradientBrush(new Point(0, 0), new Point(0, base.Height - 1), centerColor, borderColor))
				{
					linear.InterpolationColors = colorBlend;
					g.FillRectangle(linear, e.ClipRectangle);
				}
				g.DrawRectangle(boderPen, new Rectangle(0, 0, base.Width - 1, base.Height - 1));
			}
		}
	}
}
