namespace HslControls
{
	/// <summary>
	/// 指示控件的样子
	/// </summary>
	public enum HslDirectionStyle
	{
		/// <summary>
		/// 控件是横向摆放的
		/// </summary>
		Horizontal = 1,
		/// <summary>
		/// 控件是纵向摆放的
		/// </summary>
		Vertical
	}
}
