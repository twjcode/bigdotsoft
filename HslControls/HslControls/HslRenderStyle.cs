namespace HslControls
{
	/// <summary>
	/// 某些特殊图形的表现形式
	/// </summary>
	public enum HslRenderStyle
	{
		/// <summary>
		/// 椭圆
		/// </summary>
		Ellipse = 1,
		/// <summary>
		/// 矩形
		/// </summary>
		Rectangle,
		/// <summary>
		/// 菱形
		/// </summary>
		Rhombus
	}
}
