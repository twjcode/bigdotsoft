using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslControls
{
	/// <summary>
	/// 实例化一个时钟的控件，支持时分秒的显示，
	/// </summary>
	[Description("时钟控件，支持时针，分针，秒针显示，支持按秒跳格或是连续运行，支持显示附加文本显示")]
	public class HslClock : UserControl
	{
		private StringFormat sf = null;

		private Timer time1s = null;

		private Brush foreBrush = new SolidBrush(Color.Chocolate);

		private Color clockBackColor = Color.White;

		private Brush brushClockBack = new SolidBrush(Color.White);

		private Color hourColor = Color.Chocolate;

		private Pen penHour = new Pen(Color.Chocolate, 2f);

		private Color minuteColor = Color.Coral;

		private Pen penMinute = new Pen(Color.Coral, 2f);

		private Color secondColor = Color.Green;

		private Pen penSecond = new Pen(Color.Green, 2f);

		private bool isSecondStep = false;

		private float needleWidth = 2f;

		private int secondCurrent = -1;

		private bool isShowCalendar = false;

		private bool isShowWeek = true;

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置控件的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Transparent")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// 获取或设置控件的字体信息
		/// </summary>
		[Browsable(true)]
		[Category("HslControls")]
		[Description("获取或设置控件的字体信息")]
		public override Font Font
		{
			get
			{
				return base.Font;
			}
			set
			{
				base.Font = value;
				BackgroundImage = getBackGround();
			}
		}

		/// <summary>
		/// 用于组件的背景图像布局
		/// </summary>
		[Browsable(true)]
		[Category("HslControls")]
		[DefaultValue(ImageLayout.None)]
		[Description("用于组件的背景图像布局")]
		public override ImageLayout BackgroundImageLayout
		{
			get
			{
				return base.BackgroundImageLayout;
			}
			set
			{
				base.BackgroundImageLayout = value;
			}
		}

		/// <summary>
		/// 获取或设置时钟指针的颜色
		/// </summary>
		[Browsable(true)]
		[Category("HslControls")]
		[Description("获取或设置时钟指针的颜色")]
		[DefaultValue(typeof(Color), "Chocolate")]
		public Color HourColor
		{
			get
			{
				return hourColor;
			}
			set
			{
				hourColor = value;
				penHour.Dispose();
				penHour = new Pen(hourColor, needleWidth);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置分钟指针的颜色
		/// </summary>
		[Browsable(true)]
		[Category("HslControls")]
		[Description("获取或设置分钟指针的颜色")]
		[DefaultValue(typeof(Color), "Coral")]
		public Color MinuteColor
		{
			get
			{
				return minuteColor;
			}
			set
			{
				minuteColor = value;
				penMinute.Dispose();
				penMinute = new Pen(minuteColor, needleWidth);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置秒钟指针的颜色
		/// </summary>
		[Browsable(true)]
		[Category("HslControls")]
		[Description("获取或设置秒钟指针的颜色")]
		[DefaultValue(typeof(Color), "Green")]
		public Color SecondColor
		{
			get
			{
				return secondColor;
			}
			set
			{
				secondColor = value;
				penSecond.Dispose();
				penSecond = new Pen(secondColor, needleWidth);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置当前控件的文本
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前控件的文本")]
		[Category("HslControls")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Bindable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置秒针是否按照每秒前进还是连续变化
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置秒针是否按照每秒前进还是连续变化")]
		[Category("HslControls")]
		[DefaultValue(false)]
		public bool IsSecondStep
		{
			get
			{
				return isSecondStep;
			}
			set
			{
				isSecondStep = value;
			}
		}

		/// <summary>
		/// 获取或设置时钟的背景颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置时钟的背景颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "White")]
		public Color ClockBackColor
		{
			get
			{
				return clockBackColor;
			}
			set
			{
				clockBackColor = value;
				brushClockBack.Dispose();
				brushClockBack = new SolidBrush(value);
				BackgroundImage = getBackGround();
			}
		}

		/// <summary>
		/// 获取或设置文本的颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置文本的颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Chocolate")]
		public override Color ForeColor
		{
			get
			{
				return base.ForeColor;
			}
			set
			{
				base.ForeColor = value;
				foreBrush.Dispose();
				foreBrush = new SolidBrush(value);
				BackgroundImage = getBackGround();
			}
		}

		/// <summary>
		/// 获取或设置控件是否显示日期信息
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件是否显示日期信息")]
		[Category("HslControls")]
		[DefaultValue(true)]
		public bool IsShowCalendar
		{
			get
			{
				return isShowCalendar;
			}
			set
			{
				isShowCalendar = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置控件是否显示星期的信息
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件是否显示星期的信息")]
		[Category("HslControls")]
		[DefaultValue(true)]
		public bool IsShowWeek
		{
			get
			{
				return isShowWeek;
			}
			set
			{
				isShowWeek = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 实例化一个默认的对象
		/// </summary>
		public HslClock()
		{
			InitializeComponent();
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
			time1s = new Timer();
			time1s.Interval = 50;
			time1s.Tick += Time1s_Tick;
			BackgroundImageLayout = ImageLayout.None;
			ForeColor = Color.Chocolate;
		}

		/// <summary>
		/// 重写窗体加载的方法
		/// </summary>
		/// <param name="e">事件</param>
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			BackgroundImage = getBackGround();
			time1s.Start();
		}

		/// <summary>
		///  重写控件界面绘制的方法
		/// </summary>
		/// <param name="e">绘制事件</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			if (Authorization.iashdiadasbdnajsdhjaf())
			{
				Graphics g = e.Graphics;
				g.SmoothingMode = SmoothingMode.AntiAlias;
				g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
				DateTime dateTime = DateTime.Now;
				int offect = 10;
				int width = Math.Min(base.Width, base.Height);
				float radius = (float)(width - offect * 2) / 2f;
				float hour = (float)dateTime.Hour + (float)dateTime.Minute / 60f;
				float minite = (float)dateTime.Minute + (float)dateTime.Second / 60f;
				float seconds = (float)dateTime.Second + (float)dateTime.Millisecond / 1000f;
				if (isSecondStep)
				{
					seconds = dateTime.Second;
				}
				float _ArcHour = hour * 30f + 270f;
				float _ArcMinite = minite * 6f + 270f;
				float _ArcSeconds = seconds * 6f + 270f;
				g.TranslateTransform(width / 2, width / 2);
				if (width > 130 && isShowWeek)
				{
					g.DrawString(dateTime.DayOfWeek.ToString(), Font, foreBrush, new RectangleF(-100f, radius / 4f, 200f, Font.Height), sf);
				}
				g.DrawString(Text, Font, foreBrush, new RectangleF(-100f, (0f - radius) / 3f, 200f, Font.Height), sf);
				if (width > 180 && isShowCalendar)
				{
					RectangleF rectangle = new RectangleF(radius / 2f - 30f, -Font.Height / 2 - 2, 40f, Font.Height + 4);
					g.FillRectangle(Brushes.LimeGreen, rectangle);
					g.DrawString(dateTime.Day.ToString(), Font, Brushes.White, rectangle, sf);
				}
				g.RotateTransform(_ArcHour, MatrixOrder.Prepend);
				g.DrawLine(penHour, new Point(4, 0), new Point(9, 0));
				g.DrawClosedCurve(penHour, new PointF[6]
				{
					new PointF(12f, 2f),
					new PointF(10f, 0f),
					new PointF(12f, -2f),
					new PointF(radius / 2f, -2f),
					new PointF(radius / 2f + 6f, 0f),
					new PointF(radius / 2f, 2f)
				});
				g.RotateTransform(0f - _ArcHour);
				g.RotateTransform(_ArcMinite, MatrixOrder.Prepend);
				g.DrawLine(penMinute, new Point(4, 0), new Point(9, 0));
				g.DrawClosedCurve(penMinute, new PointF[6]
				{
					new PointF(14f, 2f),
					new PointF(10f, 0f),
					new PointF(14f, -2f),
					new PointF(radius - 17f, -2f),
					new PointF(radius - 10f, 0f),
					new PointF(radius - 17f, 2f)
				});
				g.RotateTransform(0f - _ArcMinite);
				g.RotateTransform(_ArcSeconds, MatrixOrder.Prepend);
				g.DrawLine(penSecond, new PointF(-13f, 0f), new PointF(radius - 6f, 0f));
				g.ResetTransform();
				base.OnPaint(e);
			}
		}

		/// <summary>
		/// 重写当控件大小变化的的方法
		/// </summary>
		/// <param name="e">大小改变的事件</param>
		protected override void OnSizeChanged(EventArgs e)
		{
			BackgroundImage = getBackGround();
			base.OnSizeChanged(e);
		}

		private void Time1s_Tick(object sender, EventArgs e)
		{
			if (isSecondStep)
			{
				if (DateTime.Now.Second != secondCurrent)
				{
					secondCurrent = DateTime.Now.Second;
					Invalidate();
				}
			}
			else
			{
				Invalidate();
			}
		}

		private Bitmap getBackGround()
		{
			int width = Math.Min(base.Width, base.Height);
			Bitmap temp = new Bitmap(width, width);
			Graphics g = Graphics.FromImage(temp);
			Pen pen1 = new Pen(Color.Chocolate, 2f);
			g.SmoothingMode = SmoothingMode.AntiAlias;
			g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
			int offect = 10;
			g.TranslateTransform((float)temp.Width / 2f, (float)temp.Height / 2f);
			float radius = (float)(temp.Width - offect * 2) / 2f;
			g.FillEllipse(Brushes.DarkGray, new RectangleF(0f - radius, 0f - radius, 2f * radius, 2f * radius));
			g.FillEllipse(brushClockBack, new RectangleF(0f - radius + 2f, 0f - radius + 2f, 2f * radius - 4f, 2f * radius - 4f));
			g.DrawEllipse(Pens.Black, new RectangleF(-4f, -4f, 8f, 8f));
			for (int k = 0; k < 60; k++)
			{
				g.RotateTransform(6f);
				g.DrawLine(Pens.DarkGray, new PointF(radius - 3f, 0f), new PointF(radius - 1f, 0f));
			}
			for (int j = 0; j < 12; j++)
			{
				g.RotateTransform(30f);
				g.DrawLine(pen1, new PointF(radius - 6f, 0f), new PointF(radius - 1f, 0f));
			}
			for (int i = 0; i < 12; i++)
			{
				float x = (float)((double)(radius - 5f - ((width > 100) ? ((float)Font.Height) : 7f)) * Math.Cos((double)(30 * i - 60) / 360.0 * 2.0 * Math.PI));
				float y = (float)((double)(radius - 5f - ((width > 100) ? ((float)Font.Height) : 7f)) * Math.Sin((double)(30 * i - 60) / 360.0 * 2.0 * Math.PI));
				g.DrawString(layoutRectangle: new RectangleF(x - 30f, y - (float)(Font.Height / 2), 60f, Font.Height), s: (i + 1).ToString(), font: Font, brush: foreBrush, format: sf);
			}
			pen1.Dispose();
			g.ResetTransform();
			g.Dispose();
			return temp;
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			BackColor = System.Drawing.Color.Transparent;
			base.Name = "HslClock";
			base.Size = new System.Drawing.Size(182, 179);
			ResumeLayout(false);
		}
	}
}
