using System.Collections.Generic;
using System.Drawing;

namespace HslControls
{
	/// <summary>
	/// 用于前景色标记的区段信息
	/// </summary>
	public class HslMarkForeSection : HslMarkSectionBase
	{
		/// <summary>
		/// 绘制的起始高度
		/// </summary>
		public float StartHeight
		{
			get;
			set;
		}

		/// <summary>
		/// 绘制是高度信息
		/// </summary>
		public float Height
		{
			get;
			set;
		}

		/// <summary>
		/// 是否显示开始和结束的信息
		/// </summary>
		public bool IsRenderTimeText
		{
			get;
			set;
		}

		/// <summary>
		/// 特殊标记的区间的背景色
		/// </summary>
		public Pen LinePen
		{
			get;
			set;
		}

		/// <summary>
		/// 字体的颜色
		/// </summary>
		public Brush FontBrush
		{
			get;
			set;
		}

		/// <summary>
		/// 特殊标记的文本，如果不为空，则显示出来
		/// </summary>
		public string MarkText
		{
			get;
			set;
		}

		/// <summary>
		/// 光标处显示的信息
		/// </summary>
		public Dictionary<string, string> CursorTexts
		{
			get;
			set;
		}

		/// <summary>
		/// 实例化一个默认的对象
		/// </summary>
		public HslMarkForeSection()
		{
			LinePen = Pens.Cyan;
			FontBrush = Brushes.Yellow;
			IsRenderTimeText = true;
			CursorTexts = new Dictionary<string, string>();
		}
	}
}
