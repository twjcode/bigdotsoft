namespace HslControls
{
	/// <summary>
	/// 系统的主题样式，将会改变一些控件的外观
	/// </summary>
	public enum HslThemeStyle
	{
		/// <summary>
		/// 浅色的主题
		/// </summary>
		Light = 1,
		/// <summary>
		/// 深色的主题
		/// </summary>
		Dark
	}
}
