using System.Drawing;

namespace HslControls
{
	/// <summary>
	/// 在曲线控件中额外标记的区间显示，可以设置不同的背景颜色
	/// </summary>
	public class HslMarkBackSection : HslMarkSectionBase
	{
		/// <summary>
		/// 特殊标记的区间的背景色
		/// </summary>
		public Color BackColor
		{
			get;
			set;
		}

		/// <summary>
		/// 标记文本
		/// </summary>
		public string MarkText
		{
			get;
			set;
		}

		/// <summary>
		/// 实例化一个默认的对象
		/// </summary>
		public HslMarkBackSection()
		{
			BackColor = Color.FromArgb(52, 52, 52);
		}
	}
}
