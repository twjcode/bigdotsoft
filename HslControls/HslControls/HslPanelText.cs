using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslControls
{
	/// <summary>
	/// 带有一个文本的扩展的Panel容器
	/// </summary>
	[Description("Panel控件，左上角带一个文本的标题，该标题没有背景")]
	public class HslPanelText : Panel
	{
		private int textOffect = 20;

		private Color themeColor = Color.DodgerBlue;

		private StringFormat sf = null;

		/// <summary>
		/// 获取或设置当前控件的文本
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前控件的文本")]
		[Category("HslControls")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Bindable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置当前控件的主题色
		/// </summary>
		[Description("获取或设置当前控件的主题色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "DodgerBlue")]
		public Color ThemeColor
		{
			get
			{
				return themeColor;
			}
			set
			{
				themeColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置控件顶部文本的高度
		/// </summary>
		[Description("获取或设置控件顶部文本的高度")]
		[Category("HslControls")]
		[DefaultValue(30)]
		public int TextOffect
		{
			get
			{
				return textOffect;
			}
			set
			{
				textOffect = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 实例化一个默认的对象
		/// </summary>
		public HslPanelText()
		{
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
		}

		/// <inheritdoc />
		protected override void OnPaintBackground(PaintEventArgs e)
		{
			base.OnPaintBackground(e);
			if (Authorization.iashdiadasbdnajsdhjaf())
			{
				e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
				e.Graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
				SizeF size = e.Graphics.MeasureString(Text, Font);
				size = new SizeF(size.Width + 8f, size.Height + 4f);
				using (Brush brush = new SolidBrush(ForeColor))
				{
					e.Graphics.DrawString(Text, Font, brush, new RectangleF(textOffect, 0f, size.Width, size.Height), sf);
				}
				using Pen pen = new Pen(themeColor);
				PointF[] points = new PointF[6]
				{
					new PointF(textOffect, (int)(size.Height / 2f)),
					new PointF(0f, (int)(size.Height / 2f)),
					new PointF(0f, base.Height - 1),
					new PointF(base.Width - 1, base.Height - 1),
					new PointF(base.Width - 1, (int)(size.Height / 2f)),
					new PointF((float)textOffect + size.Width, (int)(size.Height / 2f))
				};
				e.Graphics.DrawLines(pen, points);
			}
		}
	}
}
