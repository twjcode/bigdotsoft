using System;
using System.Drawing;

namespace HslControls
{
	/// <summary>
	/// 辅助线对象
	/// </summary>
	internal class AuxiliaryLine : IDisposable
	{
		private bool disposedValue = false;

		/// <summary>
		/// 实际的数据值
		/// </summary>
		public float Value
		{
			get;
			set;
		}

		/// <summary>
		/// 实际的数据绘制的Y轴位置
		/// </summary>
		public float PaintValue
		{
			get;
			set;
		}

		/// <summary>
		/// 实际的数据绘制的Y轴位置，备份使用的
		/// </summary>
		public float PaintValueBackUp
		{
			get;
			set;
		}

		/// <summary>
		/// 辅助线的颜色
		/// </summary>
		public Color LineColor
		{
			get;
			set;
		}

		/// <summary>
		/// 辅助线的虚线画笔资源
		/// </summary>
		public Pen PenDash
		{
			get;
			set;
		}

		/// <summary>
		/// 辅助线的实线画笔资源
		/// </summary>
		public Pen PenSolid
		{
			get;
			set;
		}

		/// <summary>
		/// 辅助线的宽度
		/// </summary>
		public float LineThickness
		{
			get;
			set;
		}

		/// <summary>
		/// 辅助线文本的画刷
		/// </summary>
		public Brush LineTextBrush
		{
			get;
			set;
		}

		/// <summary>
		/// 是否左侧参考系，True为左侧，False为右侧
		/// </summary>
		public bool IsLeftFrame
		{
			get;
			set;
		}

		/// <summary>
		/// 辅助线是否是虚线，默认为虚线
		/// </summary>
		public bool IsDashStyle
		{
			get;
			set;
		} = true;


		/// <summary>
		/// 获取真实的画笔
		/// </summary>
		/// <returns></returns>
		public Pen GetPen()
		{
			return IsDashStyle ? PenDash : PenSolid;
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					PenDash?.Dispose();
					PenSolid?.Dispose();
					LineTextBrush?.Dispose();
				}
				disposedValue = true;
			}
		}

		/// <summary>
		/// 释放内存信息
		/// </summary>
		public void Dispose()
		{
			Dispose(disposing: true);
		}
	}
}
