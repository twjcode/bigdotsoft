using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslControls
{
	/// <summary>
	/// 按钮控件，和windows自带的功能基本一致
	/// </summary>
	[DefaultEvent("Click")]
	[DefaultBindingProperty("Text")]
	[DefaultProperty("Text")]
	[Description("按钮控件，带点小圆角，当点击按钮的时候，文本会偏移一点点")]
	public class HslButton : UserControl
	{
		private Color textForeColor = Color.DimGray;

		private Brush textForeBrush = new SolidBrush(Color.DimGray);

		private int roundCorner = 4;

		private bool selected = false;

		private Color backColor = Color.Lavender;

		private Color enablecolor = Color.FromArgb(190, 190, 190);

		private Color activeColor = Color.AliceBlue;

		private bool borderVisiable = true;

		private bool isMouseOn = false;

		private bool isLeftMouseDown = false;

		private StringFormat sf = null;

		private bool useGradient = false;

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置控件的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Transparent")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// 获取或设置当前控件的文本的颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前控件的文本的颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "DimGray")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color ForeColor
		{
			get
			{
				return textForeColor;
			}
			set
			{
				textForeColor = value;
				textForeBrush.Dispose();
				textForeBrush = new SolidBrush(value);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置当前控件的文本
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前控件的文本")]
		[Category("HslControls")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Bindable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 设置按钮的圆角
		/// </summary>
		[Category("HslControls")]
		[DefaultValue(4)]
		[Description("按钮框的圆角属性")]
		public int CornerRadius
		{
			get
			{
				return roundCorner;
			}
			set
			{
				roundCorner = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 用来设置按钮的选中状态
		/// </summary>
		[Category("HslControls")]
		[DefaultValue(false)]
		[Description("指示按钮的选中状态")]
		public bool Selected
		{
			get
			{
				return selected;
			}
			set
			{
				selected = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 按钮的背景色
		/// </summary>
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Lavender")]
		[Description("按钮的背景色")]
		public Color OriginalColor
		{
			get
			{
				return backColor;
			}
			set
			{
				backColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 按钮的背景色
		/// </summary>
		[Category("HslControls")]
		[Description("按钮的活动色")]
		[DefaultValue(typeof(Color), "0xBEBEBE")]
		public Color EnableColor
		{
			get
			{
				return enablecolor;
			}
			set
			{
				enablecolor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 鼠标挪动时的活动颜色
		/// </summary>
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "AliceBlue")]
		[Description("按钮的活动色")]
		public Color ActiveColor
		{
			get
			{
				return activeColor;
			}
			set
			{
				activeColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 设置按钮的边框是否可见
		/// </summary>
		[Category("HslControls")]
		[Browsable(true)]
		[DefaultValue(true)]
		[Description("指示按钮是否存在边框")]
		public bool BorderVisiable
		{
			get
			{
				return borderVisiable;
			}
			set
			{
				borderVisiable = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置本按钮控件是否使用渐进色
		/// </summary>
		[Category("HslControls")]
		[Description("获取或设置按钮控件是否使用渐进色")]
		[Browsable(true)]
		[DefaultValue(false)]
		public bool UseGradient
		{
			get
			{
				return useGradient;
			}
			set
			{
				useGradient = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 存放用户需要保存的一些额外的信息
		/// </summary>
		[Browsable(false)]
		public string CustomerInformation
		{
			get;
			set;
		}

		/// <summary>
		/// 实例化一个对象
		/// </summary>
		public HslButton()
		{
			InitializeComponent();
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
		}

		/// <summary>
		/// 重绘界面的逻辑功能
		/// </summary>
		/// <param name="e">重绘的事件</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			if (!Authorization.iashdiadasbdnajsdhjaf())
			{
				return;
			}
			e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
			e.Graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
			GraphicsPath path = new GraphicsPath();
			path.AddLine(roundCorner, 0, base.Width - roundCorner - 1, 0);
			path.AddArc(base.Width - roundCorner * 2 - 1, 0, roundCorner * 2, roundCorner * 2, 270f, 90f);
			path.AddLine(base.Width - 1, roundCorner, base.Width - 1, base.Height - roundCorner - 1);
			path.AddArc(base.Width - roundCorner * 2 - 1, base.Height - roundCorner * 2 - 1, roundCorner * 2, roundCorner * 2, 0f, 90f);
			path.AddLine(base.Width - roundCorner - 1, base.Height - 1, roundCorner, base.Height - 1);
			path.AddArc(0, base.Height - roundCorner * 2 - 1, roundCorner * 2, roundCorner * 2, 90f, 90f);
			path.AddLine(0, base.Height - roundCorner - 1, 0, roundCorner);
			path.AddArc(0, 0, roundCorner * 2, roundCorner * 2, 180f, 90f);
			Brush brush_fore_text = null;
			Brush brush_back_text = null;
			Rectangle rect_text = new Rectangle(base.ClientRectangle.X, base.ClientRectangle.Y, base.ClientRectangle.Width, base.ClientRectangle.Height);
			if (base.Enabled)
			{
				brush_fore_text = new SolidBrush(ForeColor);
				if (!useGradient)
				{
					brush_back_text = (Selected ? new SolidBrush(Color.DodgerBlue) : ((!isMouseOn) ? new SolidBrush(backColor) : new SolidBrush(ActiveColor)));
				}
				else
				{
					ColorBlend colorBlend = new ColorBlend();
					colorBlend.Positions = new float[3]
					{
						0f,
						0.5f,
						1f
					};
					if (Selected)
					{
						colorBlend.Colors = new Color[3]
						{
							Color.DodgerBlue,
							HslHelper.GetColorLight(Color.DodgerBlue),
							Color.DodgerBlue
						};
					}
					else if (isMouseOn)
					{
						colorBlend.Colors = new Color[3]
						{
							ActiveColor,
							HslHelper.GetColorLight(ActiveColor),
							ActiveColor
						};
					}
					else
					{
						colorBlend.Colors = new Color[3]
						{
							backColor,
							HslHelper.GetColorLight(backColor),
							backColor
						};
					}
					LinearGradientBrush linear = new LinearGradientBrush(new PointF(0f, 0f), new PointF(0f, base.Height - 1), Color.FromArgb(247, 252, 253), Color.WhiteSmoke);
					linear.InterpolationColors = colorBlend;
					brush_back_text = linear;
				}
				if (isLeftMouseDown)
				{
					rect_text.Offset(1, 1);
				}
			}
			else
			{
				brush_fore_text = new SolidBrush(Color.Gray);
				brush_back_text = new SolidBrush(EnableColor);
			}
			e.Graphics.FillPath(brush_back_text, path);
			Pen pen_border = new Pen(Color.FromArgb(170, 170, 170));
			if (BorderVisiable)
			{
				e.Graphics.DrawPath(pen_border, path);
			}
			e.Graphics.DrawString(Text, Font, brush_fore_text, rect_text, sf);
			brush_fore_text.Dispose();
			brush_back_text.Dispose();
			pen_border.Dispose();
			path.Dispose();
			base.OnPaint(e);
		}

		/// <summary>
		/// 引发MouseEnter事件
		/// </summary>
		/// <param name="e">事件</param>
		protected override void OnMouseEnter(EventArgs e)
		{
			base.OnMouseEnter(e);
			isMouseOn = true;
			Invalidate();
		}

		/// <summary>
		/// 引发MouseLeave事件
		/// </summary>
		/// <param name="e">事件</param>
		protected override void OnMouseLeave(EventArgs e)
		{
			base.OnMouseLeave(e);
			isMouseOn = false;
			Invalidate();
		}

		/// <summary>
		/// 引发MouseDown事件
		/// </summary>
		/// <param name="e">事件</param>
		protected override void OnMouseDown(MouseEventArgs e)
		{
			base.OnMouseDown(e);
			if (e.Button == MouseButtons.Left)
			{
				isLeftMouseDown = true;
				Invalidate();
			}
		}

		/// <summary>
		/// 引发MouseUp事件
		/// </summary>
		/// <param name="e"></param>
		protected override void OnMouseUp(MouseEventArgs e)
		{
			base.OnMouseUp(e);
			if (e.Button == MouseButtons.Left)
			{
				isLeftMouseDown = false;
				Invalidate();
			}
		}

		/// <summary>
		/// 触发一次点击的事件
		/// </summary>
		public void PerformClick()
		{
			OnClick(new EventArgs());
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			BackColor = System.Drawing.Color.Transparent;
			base.Name = "HslButton";
			base.Size = new System.Drawing.Size(114, 46);
			ResumeLayout(false);
		}
	}
}
