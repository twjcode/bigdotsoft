namespace HslControls
{
	/// <summary>
	/// 进度条的样子
	/// </summary>
	public enum HslProgressStyle
	{
		/// <summary>
		/// 进度条是横向摆放的
		/// </summary>
		Horizontal = 1,
		/// <summary>
		/// 纵向的进度条
		/// </summary>
		Vertical,
		/// <summary>
		/// 圆形的进度条
		/// </summary>
		Circular
	}
}
