using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslControls
{
	[Description("一个真空泵控件，支持两个方向的旋转操作，支持颜色设置")]
	public class HslVacuumPump : UserControl
	{
		private StringFormat sf = null;

		private HslDirectionStyle hslValvesStyle = HslDirectionStyle.Vertical;

		private int entrance = 1;

		private int export = 4;

		private Color color1 = Color.FromArgb(0, 151, 0);

		private Brush brush1 = new SolidBrush(Color.FromArgb(0, 151, 0));

		private Color color2 = Color.FromArgb(76, 76, 76);

		private Brush brush2 = new SolidBrush(Color.FromArgb(76, 76, 76));

		private Color color3 = Color.FromArgb(255, 255, 255);

		private Brush brush3 = new SolidBrush(Color.FromArgb(255, 255, 255));

		private Color color4 = Color.FromArgb(151, 151, 151);

		private Brush brush4 = new SolidBrush(Color.FromArgb(151, 151, 151));

		private float moveSpeed = 0.3f;

		private float startAngle = -47f;

		private Timer timer = null;

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置控件的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Transparent")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// 获取或设置当前控件的文本
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前控件的文本")]
		[Category("HslControls")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Bindable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置泵控件是否是横向的还是纵向的
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置泵控件是否是横向的还是纵向的")]
		[Category("HslControls")]
		[DefaultValue(typeof(HslDirectionStyle), "Vertical")]
		public HslDirectionStyle PumpStyle
		{
			get
			{
				return hslValvesStyle;
			}
			set
			{
				hslValvesStyle = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置泵的动画速度，0为静止，正数为正向流动，负数为反向流动
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置传送带流动的速度，0为静止，正数为正向流动，负数为反向流动")]
		[Category("HslControls")]
		[DefaultValue(0.3f)]
		public float MoveSpeed
		{
			get
			{
				return moveSpeed;
			}
			set
			{
				moveSpeed = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置颜色1
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置颜色1")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "[0, 151, 0]")]
		public Color Color1
		{
			get
			{
				return color1;
			}
			set
			{
				color1 = value;
				brush1.Dispose();
				brush1 = new SolidBrush(value);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置颜色2
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置颜色2")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "[76, 76, 76]")]
		public Color Color2
		{
			get
			{
				return color2;
			}
			set
			{
				color2 = value;
				brush2.Dispose();
				brush2 = new SolidBrush(value);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置颜色3
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置颜色3")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "[255, 255, 255]")]
		public Color Color3
		{
			get
			{
				return color3;
			}
			set
			{
				color3 = value;
				brush3.Dispose();
				brush3 = new SolidBrush(value);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置颜色4
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置颜色4")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "[151, 151, 151]")]
		public Color Color4
		{
			get
			{
				return color4;
			}
			set
			{
				color4 = value;
				brush4.Dispose();
				brush4 = new SolidBrush(value);
				Invalidate();
			}
		}

		public HslVacuumPump()
		{
			InitializeComponent();
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
			timer = new Timer();
			timer.Interval = 50;
			timer.Tick += Timer_Tick;
			timer.Start();
		}

		/// <summary>
		/// 重绘控件的
		/// </summary>
		/// <param name="e">重绘事件</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			if (!Authorization.iashdiadasbdnajsdhjaf())
			{
				return;
			}
			Graphics g = e.Graphics;
			g.SmoothingMode = SmoothingMode.HighQuality;
			g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
			float width = 0f;
			float height = 0f;
			if (hslValvesStyle == HslDirectionStyle.Vertical)
			{
				if (base.Height >= base.Width * 112 / 80)
				{
					width = base.Width - 1;
					height = base.Width * 112 / 80 - 1;
				}
				else
				{
					height = base.Height - 1;
					width = base.Height * 80 / 112 - 1;
				}
				PaintMain(g, width, height);
			}
			else
			{
				if (base.Width >= base.Height * 112 / 80)
				{
					height = base.Height - 1;
					width = base.Height * 112 / 80 - 1;
				}
				else
				{
					width = base.Width - 1;
					height = base.Width * 80 / 112 - 1;
				}
				g.TranslateTransform(width, 0f);
				g.RotateTransform(90f);
				PaintMain(g, height, width);
				g.ResetTransform();
			}
			base.OnPaint(e);
		}

		private void PaintMain(Graphics g, float width, float height)
		{
			ColorBlend colorBlend = new ColorBlend();
			colorBlend.Positions = new float[3]
			{
				0f,
				0.51f,
				1f
			};
			colorBlend.Colors = new Color[3]
			{
				color1,
				Color.WhiteSmoke,
				color1
			};
			LinearGradientBrush brush = new LinearGradientBrush(new PointF(0f, 0f), new PointF(width, 0f), Color.Wheat, Color.White);
			brush.InterpolationColors = colorBlend;
			g.FillEllipse(brush, new RectangleF(0f, 0f, width, width));
			g.FillEllipse(brush, new RectangleF(0f, height - width, width, width));
			if (height - width > 0f)
			{
				g.FillRectangle(brush, new RectangleF(0f, width / 2f, width, height - width));
			}
			g.FillEllipse(brush2, new RectangleF(width * 0.15f, width * 0.15f, width * 0.7f, width * 0.7f));
			g.FillEllipse(brush2, new RectangleF(width * 0.15f, height - width + width * 0.15f, width * 0.7f, width * 0.7f));
			if (height - width > 0f)
			{
				g.FillRectangle(brush2, new RectangleF(0f, width / 2f, width, height - width));
			}
			g.TranslateTransform(width / 2f, width / 2f);
			g.RotateTransform(startAngle);
			PointF[] points = new PointF[20]
			{
				new PointF(0f, (0f - width) * 0.08f),
				new PointF((0f - width) * 0.1f, (0f - width) * 0.12f),
				new PointF((0f - width) * 0.2f, (0f - width) * 0.13f),
				new PointF((0f - width) * 0.25f, (0f - width) * 0.11f),
				new PointF((0f - width) * 0.28f, (0f - width) * 0.07f),
				new PointF((0f - width) * 0.3f, 0f),
				new PointF((0f - width) * 0.28f, width * 0.07f),
				new PointF((0f - width) * 0.25f, width * 0.11f),
				new PointF((0f - width) * 0.2f, width * 0.13f),
				new PointF((0f - width) * 0.1f, width * 0.12f),
				new PointF(0f, width * 0.08f),
				new PointF(width * 0.1f, width * 0.12f),
				new PointF(width * 0.2f, width * 0.13f),
				new PointF(width * 0.25f, width * 0.11f),
				new PointF(width * 0.28f, width * 0.07f),
				new PointF(width * 0.3f, 0f),
				new PointF(width * 0.28f, (0f - width) * 0.07f),
				new PointF(width * 0.25f, (0f - width) * 0.11f),
				new PointF(width * 0.2f, (0f - width) * 0.13f),
				new PointF(width * 0.1f, (0f - width) * 0.12f)
			};
			brush.Dispose();
			colorBlend = new ColorBlend();
			colorBlend.Positions = new float[3]
			{
				0f,
				0.5f,
				1f
			};
			colorBlend.Colors = new Color[3]
			{
				color4,
				color3,
				color4
			};
			brush = new LinearGradientBrush(new PointF((0f - width) * 0.3f, 0f), new PointF(width * 0.3f, 0f), Color.Wheat, Color.White);
			brush.InterpolationColors = colorBlend;
			g.FillClosedCurve(brush, points);
			g.FillEllipse(Brushes.Black, new RectangleF((0f - width) * 0.04f, (0f - width) * 0.04f, width * 0.08f, width * 0.08f));
			g.RotateTransform(0f - startAngle);
			g.TranslateTransform((0f - width) / 2f, (0f - width) / 2f);
			g.TranslateTransform(width / 2f, height - width / 2f);
			g.RotateTransform(startAngle * -1f - 83f);
			g.FillClosedCurve(brush, points);
			g.FillEllipse(Brushes.Black, new RectangleF((0f - width) * 0.04f, (0f - width) * 0.04f, width * 0.08f, width * 0.08f));
			g.RotateTransform(0f - (startAngle * -1f - 83f));
			g.TranslateTransform((0f - width) / 2f, 0f - height + width / 2f);
			brush.Dispose();
		}

		private PointF[] GetPointsFrom(string points, float width, float height, float dx = 0f, float dy = 0f)
		{
			return HslHelper.GetPointsFrom(points, 80.7f, 112.5f, width, height, dx, dy);
		}

		private void Timer_Tick(object sender, EventArgs e)
		{
			if (moveSpeed != 0f)
			{
				startAngle = (float)((double)startAngle + (double)(moveSpeed * 180f) / Math.PI / 10.0);
				if (startAngle <= -360f)
				{
					startAngle += 360f;
				}
				else if (startAngle >= 360f)
				{
					startAngle -= 360f;
				}
				Invalidate();
			}
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			BackColor = System.Drawing.Color.Transparent;
			base.Name = "HslVacuumPump";
			base.Size = new System.Drawing.Size(109, 151);
			ResumeLayout(false);
		}
	}
}
