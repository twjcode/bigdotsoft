using System.Drawing;

namespace HslControls
{
	/// <summary>
	/// 辅助标签的信息
	/// </summary>
	public class AuxiliaryLable
	{
		/// <summary>
		/// 标签的文本信息
		/// </summary>
		public string Text
		{
			get;
			set;
		}

		/// <summary>
		/// 标签的文本颜色
		/// </summary>
		public Brush TextBrush
		{
			get;
			set;
		}

		/// <summary>
		/// 标签的文本背景
		/// </summary>
		public Brush TextBack
		{
			get;
			set;
		}

		/// <summary>
		/// 标签的横轴位置，小于1则为百分比，大于1则为绝对值
		/// </summary>
		public float LocationX
		{
			get;
			set;
		}

		/// <summary>
		/// 实例化一个对象
		/// </summary>
		public AuxiliaryLable()
		{
			TextBrush = Brushes.Black;
			TextBack = Brushes.Transparent;
			LocationX = 0.5f;
		}
	}
}
