using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslControls
{
	/// <summary>
	/// 工厂控件信息
	/// </summary>
	[Description("一个工厂厂房的控件，支持设置颜色")]
	public class HslFactory : UserControl
	{
		private StringFormat sf = null;

		private HslDirectionStyle hslValvesStyle = HslDirectionStyle.Horizontal;

		private Color color1 = Color.FromArgb(160, 216, 239);

		private Brush brush1 = new SolidBrush(Color.FromArgb(160, 216, 239));

		private Color color2 = Color.FromArgb(149, 202, 224);

		private Brush brush2 = new SolidBrush(Color.FromArgb(149, 202, 224));

		private Color color3 = Color.FromArgb(248, 248, 248);

		private Brush brush3 = new SolidBrush(Color.FromArgb(248, 248, 248));

		private Color color4 = Color.FromArgb(181, 181, 181);

		private Brush brush4 = new SolidBrush(Color.FromArgb(181, 181, 181));

		private Color color5 = Color.FromArgb(255, 222, 173);

		private Brush brush5 = new SolidBrush(Color.FromArgb(255, 222, 173));

		private Color color6 = Color.FromArgb(233, 201, 142);

		private Brush brush6 = new SolidBrush(Color.FromArgb(233, 201, 142));

		private Color color7 = Color.FromArgb(158, 158, 158);

		private Brush brush7 = new SolidBrush(Color.FromArgb(158, 158, 158));

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置控件的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Transparent")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// 获取或设置当前控件的文本
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前控件的文本")]
		[Category("HslControls")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Bindable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置自定义颜色1
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置自定义颜色1")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "[160, 216, 239]")]
		public Color Color1
		{
			get
			{
				return color1;
			}
			set
			{
				color1 = value;
				brush1?.Dispose();
				brush1 = new SolidBrush(color1);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置自定义颜色2
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置自定义颜色2")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "[149, 202, 224]")]
		public Color Color2
		{
			get
			{
				return color2;
			}
			set
			{
				color2 = value;
				brush2?.Dispose();
				brush2 = new SolidBrush(color2);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置自定义颜色3
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置自定义颜色3")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "[248, 248, 248]")]
		public Color Color3
		{
			get
			{
				return color3;
			}
			set
			{
				color3 = value;
				brush3?.Dispose();
				brush3 = new SolidBrush(color3);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置自定义颜色4
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置自定义颜色4")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "[181, 181, 181]")]
		public Color Color4
		{
			get
			{
				return color4;
			}
			set
			{
				color4 = value;
				brush4?.Dispose();
				brush4 = new SolidBrush(color4);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置自定义颜色5
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置自定义颜色5")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "[255, 222, 173]")]
		public Color Color5
		{
			get
			{
				return color5;
			}
			set
			{
				color5 = value;
				brush5?.Dispose();
				brush5 = new SolidBrush(color5);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置自定义颜色6
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置自定义颜色6")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "[233, 201, 142]")]
		public Color Color6
		{
			get
			{
				return color6;
			}
			set
			{
				color6 = value;
				brush6?.Dispose();
				brush6 = new SolidBrush(color6);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置自定义颜色7
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置自定义颜色7")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "[158, 158, 158]")]
		public Color Color7
		{
			get
			{
				return color7;
			}
			set
			{
				color7 = value;
				brush7?.Dispose();
				brush7 = new SolidBrush(color7);
				Invalidate();
			}
		}

		/// <summary>
		/// 实例化一个默认的对象
		/// </summary>
		public HslFactory()
		{
			InitializeComponent();
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
		}

		/// <summary>
		/// 重绘控件的
		/// </summary>
		/// <param name="e">重绘事件</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			if (Authorization.iashdiadasbdnajsdhjaf())
			{
				Graphics g = e.Graphics;
				g.SmoothingMode = SmoothingMode.HighQuality;
				g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
				if (hslValvesStyle == HslDirectionStyle.Horizontal)
				{
					PaintMain(g, base.Width, base.Height);
				}
				else
				{
					g.TranslateTransform(base.Width, 0f);
					g.RotateTransform(90f);
					PaintMain(g, base.Height, base.Width);
					g.ResetTransform();
				}
				base.OnPaint(e);
			}
		}

		private void PaintMain(Graphics g, float width, float height)
		{
			PointF point1 = new PointF(width * 0.2f, 0f);
			PointF point9 = new PointF(width * 0.4f, 0f);
			PointF point10 = new PointF(width - 1f, height * 0.35f);
			PointF point11 = new PointF(width * 0.8f, height * 0.35f);
			PointF point12 = new PointF(0f, height * 0.25f);
			PointF point13 = new PointF(width * 0.6f, height * 0.6f);
			PointF point14 = new PointF(point12.X, point12.Y + height * 0.03f);
			PointF point15 = new PointF(point13.X, point13.Y + height * 0.03f);
			PointF point16 = new PointF(point10.X, point10.Y + height * 0.03f);
			PointF point2 = new PointF(point11.X, point11.Y + height * 0.03f);
			PointF point3 = new PointF(width * 0.03f, point14.Y);
			PointF point4 = new PointF(point13.X + width * 0.03f, point10.Y);
			PointF point5 = new PointF(point13.X + width * 0.03f, height - 1f);
			PointF point6 = new PointF(width * 0.03f, height * 0.65f);
			PointF point7 = new PointF(width * 0.97f, height * 0.35f);
			PointF point8 = new PointF(width * 0.97f, height * 0.75f);
			g.FillPolygon(brush5, new PointF[4]
			{
				point3,
				point4,
				point5,
				point6
			});
			g.FillPolygon(brush6, new PointF[4]
			{
				point4,
				point5,
				point8,
				point7
			});
			g.FillPolygon(brush2, new PointF[4]
			{
				point1,
				point9,
				point10,
				point11
			});
			g.FillPolygon(brush1, new PointF[4]
			{
				point1,
				point11,
				point13,
				point12
			});
			g.FillPolygon(brush3, new PointF[4]
			{
				point12,
				point13,
				point15,
				point14
			});
			g.FillPolygon(brush4, new PointF[6]
			{
				point13,
				point11,
				point10,
				point16,
				point2,
				point15
			});
			PointF[] points = new PointF[4]
			{
				new PointF(width * 0.12f, height * 0.46f),
				new PointF(width * 0.12f, height * 0.54f),
				new PointF(width * 0.22f, height * 0.6f),
				new PointF(width * 0.22f, height * 0.52f)
			};
			g.FillPolygon(brush1, points);
			AddOffect(points, width * 0.14f, height * 0.08f);
			g.FillPolygon(brush1, points);
			AddOffect(points, width * 0.14f, height * 0.08f);
			g.FillPolygon(brush1, points);
			points = new PointF[4]
			{
				new PointF(width * 0.72f, height * 0.934f),
				new PointF(width * 0.91f, height * 0.8f),
				new PointF(width * 0.91f, height * 0.6f),
				new PointF(width * 0.72f, height * 0.734f)
			};
			g.FillPolygon(brush7, points);
		}

		private void AddOffect(PointF[] points, float x, float y)
		{
			for (int i = 0; i < points.Length; i++)
			{
				points[i] = new PointF(points[i].X + x, points[i].Y + y);
			}
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			BackColor = System.Drawing.Color.Transparent;
			base.Name = "HslFactory";
			base.Size = new System.Drawing.Size(325, 163);
			ResumeLayout(false);
		}
	}
}
