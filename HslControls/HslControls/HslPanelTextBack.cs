using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslControls
{
	/// <summary>
	/// 带有一个文本的扩展的Panel容器
	/// </summary>
	[Description("Panel控件，继承自系统控件，左上角带一个文本的标题，可设置背景颜色")]
	public class HslPanelTextBack : Panel
	{
		private int textOffect = 20;

		private Color borderColor = Color.DodgerBlue;

		private Color panelBackColor = Color.Lavender;

		private Color panelTextBackColor = Color.Red;

		private StringFormat sf = null;

		/// <summary>
		/// 获取或设置当前控件的文本
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前控件的文本")]
		[Category("HslControls")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Bindable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置当前控件的主题色
		/// </summary>
		[Description("获取或设置当前控件的主题色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "DodgerBlue")]
		public Color BorderColor
		{
			get
			{
				return borderColor;
			}
			set
			{
				borderColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置当前控件的重要背景色
		/// </summary>
		[Description("获取或设置当前控件的主题色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Lavender")]
		public Color PanelBackColor
		{
			get
			{
				return panelBackColor;
			}
			set
			{
				panelBackColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置当前控件的标题文本的背景色
		/// </summary>
		[Description("获取或设置当前控件的主题色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Red")]
		public Color PanelTextBackColor
		{
			get
			{
				return panelTextBackColor;
			}
			set
			{
				panelTextBackColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置控件顶部文本的高度
		/// </summary>
		[Description("获取或设置控件顶部文本的高度")]
		[Category("HslControls")]
		[DefaultValue(30)]
		public int TextOffect
		{
			get
			{
				return textOffect;
			}
			set
			{
				textOffect = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 实例化一个默认的对象
		/// </summary>
		public HslPanelTextBack()
		{
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);
			if (Authorization.iashdiadasbdnajsdhjaf())
			{
				e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
				e.Graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
				SizeF size = e.Graphics.MeasureString(Text, Font);
				size = new SizeF(size.Width + 8f, size.Height + 4f);
				using (SolidBrush brush2 = new SolidBrush(panelBackColor))
				{
					e.Graphics.FillRectangle(brush2, new RectangleF(0f, size.Height / 2f, base.Width - 1, (float)base.Height - size.Height / 2f));
				}
				using (SolidBrush brush3 = new SolidBrush(panelTextBackColor))
				{
					e.Graphics.FillRectangle(brush3, new RectangleF(textOffect, 0f, size.Width, size.Height));
				}
				using (Brush brush = new SolidBrush(ForeColor))
				{
					e.Graphics.DrawString(Text, Font, brush, new RectangleF(textOffect, 0f, size.Width, size.Height), sf);
				}
				using Pen pen = new Pen(borderColor);
				PointF[] points = new PointF[6]
				{
					new PointF(textOffect, (int)(size.Height / 2f)),
					new PointF(0f, (int)(size.Height / 2f)),
					new PointF(0f, base.Height - 1),
					new PointF(base.Width - 1, base.Height - 1),
					new PointF(base.Width - 1, (int)(size.Height / 2f)),
					new PointF((float)textOffect + size.Width, (int)(size.Height / 2f))
				};
				e.Graphics.DrawLines(pen, points);
			}
		}
	}
}
