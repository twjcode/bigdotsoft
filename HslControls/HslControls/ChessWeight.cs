using System;

namespace HslControls
{
	/// <summary>
	/// 棋子的权重类
	/// </summary>
	public class ChessWeight
	{
		private GobangPlayer ValuePlayer = GobangPlayer.Player1;

		private int ValueOpponent = 0;

		/// <summary>
		/// user large weight
		/// 对方的最大权重的落子
		/// </summary>
		public int WeightOpponent
		{
			get
			{
				return ValueOpponent;
			}
			set
			{
				if (ValuePlayer == GobangPlayer.Player1)
				{
					ValueOpponent = value;
					return;
				}
				ValueOpponent = value;
				if (value == 12 || value == 13)
				{
					WeightMax += 5;
				}
			}
		}

		/// <summary>
		/// 最大权重值
		/// </summary>
		public int WeightMax
		{
			get;
			set;
		} = 0;


		/// <summary>
		/// 第二大的权重值
		/// </summary>
		public int WeightLarge
		{
			get;
			set;
		} = 0;


		/// <summary>
		/// 第三大的权重值
		/// </summary>
		public int WeightSmall
		{
			get;
			set;
		} = 0;


		/// <summary>
		/// 最小权重值
		/// </summary>
		public int WeightMin
		{
			get;
			set;
		} = 0;


		/// <summary>
		/// 综合权重值
		/// </summary>
		public int TotleWeight
		{
			get
			{
				int totle = 0;
				if (WeightMax > 4)
				{
					totle += WeightMax;
				}
				if (WeightLarge > 4)
				{
					totle += WeightLarge;
				}
				if (WeightSmall > 4)
				{
					totle += WeightSmall;
				}
				if (WeightMin > 4)
				{
					totle += WeightMin;
				}
				if (totle < 7)
				{
					totle = WeightMax + WeightLarge + WeightSmall + WeightMin;
				}
				return totle;
			}
		}

		/// <summary>
		/// 总得分
		/// </summary>
		public int TotleScore => WeightMax + WeightLarge + WeightSmall + WeightMin;

		/// <summary>
		/// 实例化一个默认的对象
		/// </summary>
		public ChessWeight()
		{
		}

		/// <summary>
		/// 通过指定玩家和数据进行实例化
		/// </summary>
		/// <param name="data">数据值</param>
		/// <param name="m_play">玩家</param>
		public ChessWeight(int[] data, GobangPlayer m_play)
		{
			ValuePlayer = m_play;
			Array.Sort(data);
			Array.Reverse(data);
			WeightMax = data[0];
			WeightLarge = data[1];
			WeightSmall = data[2];
			WeightMin = data[3];
			if ((WeightMax == 11 || WeightMax == 12) && (WeightLarge == 11 || WeightLarge == 12))
			{
				WeightMax = 40;
			}
			if ((WeightMax == 11 || WeightMax == 12) && (WeightLarge == 9 || WeightLarge == 10))
			{
				WeightMax = 30;
			}
			if (ValuePlayer == GobangPlayer.Player2 && (WeightMax == 11 || WeightMax == 12) && WeightLarge < 7)
			{
				WeightMax = 7;
			}
		}

		public static bool operator >(ChessWeight cw1, ChessWeight cw2)
		{
			if (cw1.WeightMax > cw2.WeightMax)
			{
				return true;
			}
			if (cw1.WeightMax < cw2.WeightMax)
			{
				return false;
			}
			if (cw1.WeightLarge > cw2.WeightLarge)
			{
				return true;
			}
			if (cw1.WeightLarge < cw2.WeightLarge)
			{
				return false;
			}
			if (cw1.WeightSmall > cw2.WeightSmall)
			{
				return true;
			}
			if (cw1.WeightSmall < cw2.WeightSmall)
			{
				return false;
			}
			if (cw1.WeightMin > cw2.WeightMin)
			{
				return true;
			}
			return false;
		}

		public static bool operator <(ChessWeight cw1, ChessWeight cw2)
		{
			if (cw1.WeightMax < cw2.WeightMax)
			{
				return true;
			}
			if (cw1.WeightMax > cw2.WeightMax)
			{
				return false;
			}
			if (cw1.WeightLarge < cw2.WeightLarge)
			{
				return true;
			}
			if (cw1.WeightLarge > cw2.WeightLarge)
			{
				return false;
			}
			if (cw1.WeightSmall < cw2.WeightSmall)
			{
				return true;
			}
			if (cw1.WeightSmall > cw2.WeightSmall)
			{
				return false;
			}
			if (cw1.WeightMin < cw2.WeightMin)
			{
				return true;
			}
			return false;
		}

		public static bool operator >=(ChessWeight cw1, ChessWeight cw2)
		{
			if (cw1.WeightMax > cw2.WeightMax)
			{
				return true;
			}
			if (cw1.WeightMax < cw2.WeightMax)
			{
				return false;
			}
			if (cw1.WeightLarge > cw2.WeightLarge)
			{
				return true;
			}
			if (cw1.WeightLarge < cw2.WeightLarge)
			{
				return false;
			}
			if (cw1.WeightSmall > cw2.WeightSmall)
			{
				return true;
			}
			if (cw1.WeightSmall < cw2.WeightSmall)
			{
				return false;
			}
			if (cw1.WeightMin > cw2.WeightMin)
			{
				return true;
			}
			if (cw1.WeightMin < cw2.WeightMin)
			{
				return false;
			}
			return true;
		}

		public static bool operator <=(ChessWeight cw1, ChessWeight cw2)
		{
			if (cw1.WeightMax < cw2.WeightMax)
			{
				return true;
			}
			if (cw1.WeightMax > cw2.WeightMax)
			{
				return false;
			}
			if (cw1.WeightLarge < cw2.WeightLarge)
			{
				return true;
			}
			if (cw1.WeightLarge > cw2.WeightLarge)
			{
				return false;
			}
			if (cw1.WeightSmall < cw2.WeightSmall)
			{
				return true;
			}
			if (cw1.WeightSmall > cw2.WeightSmall)
			{
				return false;
			}
			if (cw1.WeightMin < cw2.WeightMin)
			{
				return true;
			}
			if (cw1.WeightMin > cw2.WeightMin)
			{
				return false;
			}
			return true;
		}

		public static bool operator ==(ChessWeight cw1, ChessWeight cw2)
		{
			if (cw1.WeightLarge == cw2.WeightLarge && cw1.WeightMax == cw2.WeightMax && cw1.WeightSmall == cw2.WeightSmall && cw1.WeightMin == cw2.WeightMin)
			{
				return true;
			}
			return false;
		}

		public static bool operator !=(ChessWeight cw1, ChessWeight cw2)
		{
			if (cw1.WeightLarge == cw2.WeightLarge && cw1.WeightMax == cw2.WeightMax && cw1.WeightSmall == cw2.WeightSmall && cw1.WeightMin == cw2.WeightMin)
			{
				return false;
			}
			return true;
		}
	}
}
