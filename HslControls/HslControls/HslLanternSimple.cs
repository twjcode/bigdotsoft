using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslControls
{
	/// <summary>
	/// 一个简约的灯控件，支持纯色和渐变色的设置
	/// </summary>
	[Description("一个圆形的信号灯，支持设置颜色，是否渐变")]
	public class HslLanternSimple : UserControl
	{
		private Color colorBackground = Color.LimeGreen;

		private Brush brushBackground = new SolidBrush(Color.LimeGreen);

		private StringFormat sf = null;

		private Color centerColor = Color.White;

		private bool isUseGradientColor = false;

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置控件的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Transparent")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// 获取或设置灯信号的前景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置信号灯的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "LimeGreen")]
		public Color LanternBackground
		{
			get
			{
				return colorBackground;
			}
			set
			{
				colorBackground = value;
				brushBackground?.Dispose();
				brushBackground = new SolidBrush(colorBackground);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置中心点的颜色，当且仅当UseGradientColor属性为True时生效
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置中心点的颜色，当且仅当UseGradientColor属性为True时生效")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "White")]
		public Color CenterBackground
		{
			get
			{
				return centerColor;
			}
			set
			{
				centerColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置当前的灯信号是否启用渐变色的画刷
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前的灯信号是否启用渐变色的画刷")]
		[Category("HslControls")]
		[DefaultValue(false)]
		public bool UseGradientColor
		{
			get
			{
				return isUseGradientColor;
			}
			set
			{
				isUseGradientColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置当前控件的文本
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前控件的文本")]
		[Category("HslControls")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Bindable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 实例化一个灯控件信息
		/// </summary>
		public HslLanternSimple()
		{
			InitializeComponent();
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
		}

		/// <summary>
		/// 重绘控件的界面信息
		/// </summary>
		/// <param name="e">重绘事件</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			if (!Authorization.iashdiadasbdnajsdhjaf())
			{
				return;
			}
			e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
			e.Graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
			e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
			e.Graphics.CompositingQuality = CompositingQuality.HighQuality;
			int circle_width = Math.Min(base.Width, base.Height);
			circle_width--;
			Point center = new Point(circle_width / 2, circle_width / 2);
			e.Graphics.TranslateTransform(center.X, center.Y);
			float radius = (float)(center.X * 17) / 20f;
			float borderWidth = (float)center.X / 20f;
			if (radius < 2f)
			{
				return;
			}
			RectangleF rectangle_larger = new RectangleF(0f - radius - 2f * borderWidth, 0f - radius - 2f * borderWidth, 2f * radius + 4f * borderWidth, 2f * radius + 4f * borderWidth);
			RectangleF rectangle = new RectangleF(0f - radius, 0f - radius, 2f * radius, 2f * radius);
			using (Pen pen2 = new Pen(colorBackground, borderWidth))
			{
				e.Graphics.DrawEllipse(pen2, rectangle_larger);
			}
			if (!UseGradientColor)
			{
				e.Graphics.FillEllipse(brushBackground, rectangle);
			}
			else
			{
				GraphicsPath path = new GraphicsPath();
				path.AddEllipse(0f - radius, 0f - radius, 2f * radius, 2f * radius);
				PathGradientBrush brush = new PathGradientBrush(path);
				brush.CenterPoint = new Point(0, 0);
				ColorBlend colorBlend = new ColorBlend();
				colorBlend.Positions = new float[2]
				{
					0f,
					1f
				};
				colorBlend.Colors = new Color[2]
				{
					colorBackground,
					centerColor
				};
				brush.InterpolationColors = colorBlend;
				e.Graphics.FillEllipse(brush, rectangle);
				using (Pen pen = new Pen(colorBackground))
				{
					e.Graphics.DrawEllipse(pen, 0f - radius, 0f - radius, 2f * radius, 2f * radius);
				}
				brush.Dispose();
				path.Dispose();
			}
			e.Graphics.ResetTransform();
			if (base.Height - circle_width > 0 && !string.IsNullOrEmpty(Text))
			{
				using Brush forebrush = new SolidBrush(ForeColor);
				e.Graphics.DrawString(Text, Font, forebrush, new Rectangle(0, circle_width, circle_width, base.Height - circle_width), sf);
			}
			base.OnPaint(e);
		}

		/// <summary>
		/// 触发一次鼠标点击的事件
		/// </summary>
		public void PerformClick()
		{
			OnClick(new EventArgs());
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			BackColor = System.Drawing.Color.Transparent;
			base.Name = "HslLanternSimple";
			base.Size = new System.Drawing.Size(78, 94);
			ResumeLayout(false);
		}
	}
}
