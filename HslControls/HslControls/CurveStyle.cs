namespace HslControls
{
	/// <summary>
	/// 曲线的样式
	/// </summary>
	public enum CurveStyle
	{
		/// <summary>
		/// 线段，原始的多个点之间的直线连接
		/// </summary>
		LineSegment,
		/// <summary>
		/// 曲线，相对于线段比较平滑
		/// </summary>
		Curve,
		/// <summary>
		/// 阶梯线段，采用阶梯的样式表示
		/// </summary>
		StepLine,
		/// <summary>
		/// 阶梯线段，没有竖向的信息
		/// </summary>
		StepLineWithoutVertical
	}
}
