using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Threading;
using System.Windows.Forms;

namespace HslControls
{
	[Description("进度条控件，支持横向，竖向两种方向，支持颜色渐变色")]
	public class HslProgressColorful : UserControl
	{
		private StringFormat sf = null;

		private Color colorBorder = Color.Silver;

		private Color colorCenter = Color.WhiteSmoke;

		private Color colorTmp = Color.Tomato;

		private Brush backBrush = new SolidBrush(Color.DimGray);

		private int max = 100;

		private int m_value = 50;

		private int m_actual = 50;

		private int m_speed = 1;

		private bool useAnimation = false;

		private int m_version = 0;

		private HslProgressStyle m_progressStyle = HslProgressStyle.Vertical;

		private bool isTextRender = true;

		private Action m_UpdateAction;

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置进度条的背景色
		/// </summary>
		[Description("获取或设置进度条的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "DimGray")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Browsable(true)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
				backBrush?.Dispose();
				backBrush = new SolidBrush(value);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置进度的颜色
		/// </summary>
		[Description("获取或设置进度条的前景色")]
		[Category("HslControls")]
		[Browsable(true)]
		[DefaultValue(typeof(Color), "Tomato")]
		public Color ProgressColor
		{
			get
			{
				return colorTmp;
			}
			set
			{
				colorTmp = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置进度条的最大值，默认为100
		/// </summary>
		[Description("获取或设置进度条的最大值，默认为100")]
		[Category("HslControls")]
		[Browsable(true)]
		[DefaultValue(100)]
		public int Max
		{
			get
			{
				return max;
			}
			set
			{
				if (value > 1)
				{
					max = value;
				}
				if (m_value > max)
				{
					m_value = max;
				}
				Invalidate();
			}
		}

		/// <summary>
		/// 当前进度条的值，不能大于最大值或小于0
		/// </summary>
		[Description("获取或设置当前进度条的值")]
		[Category("HslControls")]
		[Browsable(true)]
		[DefaultValue(50)]
		public int Value
		{
			get
			{
				return m_value;
			}
			set
			{
				if (value >= 0 && value <= max && value != m_value)
				{
					m_value = value;
					if (UseAnimation)
					{
						int version = Interlocked.Increment(ref m_version);
						ThreadPool.QueueUserWorkItem(ThreadPoolUpdateProgress, version);
					}
					else
					{
						m_actual = value;
						Invalidate();
					}
				}
			}
		}

		/// <summary>
		/// 是否显示进度
		/// </summary>
		[Description("获取或设置是否显示进度文本")]
		[Category("HslControls")]
		[Browsable(true)]
		[DefaultValue(true)]
		public bool IsTextRender
		{
			get
			{
				return isTextRender;
			}
			set
			{
				isTextRender = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 设置进度条的边框颜色
		/// </summary>
		[Description("获取或设置进度条的边框颜色")]
		[Category("HslControls")]
		[Browsable(true)]
		[DefaultValue(typeof(Color), "Silver")]
		public Color BorderColor
		{
			get
			{
				return colorBorder;
			}
			set
			{
				colorBorder = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置进度条中间的过渡色
		/// </summary>
		[Description("获取或设置进度条中间的过渡色")]
		[Category("HslControls")]
		[Browsable(true)]
		[DefaultValue(typeof(Color), "WhiteSmoke")]
		public Color CenterColor
		{
			get
			{
				return colorCenter;
			}
			set
			{
				colorCenter = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 设置进度变更的速度
		/// </summary>
		[Description("获取或设置进度条的变化进度")]
		[Category("HslControls")]
		[Browsable(true)]
		[DefaultValue(1)]
		public int ValueChangeSpeed
		{
			get
			{
				return m_speed;
			}
			set
			{
				if (value >= 1)
				{
					m_speed = value;
				}
			}
		}

		/// <summary>
		/// 获取或设置进度条变化的时候是否采用动画效果
		/// </summary>
		[Description("获取或设置进度条变化的时候是否采用动画效果")]
		[Category("HslControls")]
		[Browsable(true)]
		[DefaultValue(false)]
		public bool UseAnimation
		{
			get
			{
				return useAnimation;
			}
			set
			{
				useAnimation = value;
			}
		}

		/// <summary>
		/// 进度条的样式
		/// </summary>
		[Description("获取或设置进度条的样式")]
		[Category("HslControls")]
		[Browsable(true)]
		[DefaultValue(typeof(HslProgressStyle), "Vertical")]
		public HslProgressStyle ProgressStyle
		{
			get
			{
				return m_progressStyle;
			}
			set
			{
				m_progressStyle = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 实例化一个对象
		/// </summary>
		public HslProgressColorful()
		{
			InitializeComponent();
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
			m_UpdateAction = UpdateRender;
		}

		/// <summary>
		/// 重绘整个控件的界面
		/// </summary>
		/// <param name="e">重绘事件</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			if (!Authorization.iashdiadasbdnajsdhjaf())
			{
				return;
			}
			try
			{
				Graphics g = e.Graphics;
				g.FillRectangle(rect: new Rectangle(0, 0, base.Width - 1, base.Height - 1), brush: backBrush);
				g.SmoothingMode = SmoothingMode.HighQuality;
				g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
				ColorBlend colorBlend = new ColorBlend();
				colorBlend.Positions = new float[3]
				{
					0f,
					0.8f,
					1f
				};
				colorBlend.Colors = new Color[3]
				{
					colorBorder,
					colorCenter,
					colorBorder
				};
				switch (m_progressStyle)
				{
				case HslProgressStyle.Vertical:
				{
					LinearGradientBrush brush3 = new LinearGradientBrush(new PointF(0f, 0f), new PointF(base.Width - 1, 0f), colorBorder, colorCenter);
					brush3.InterpolationColors = colorBlend;
					g.FillEllipse(brush3, 0, 0, base.Width - 1, base.Width - 1);
					g.FillRectangle(brush3, 0f, (float)base.Width / 2f, base.Width - 1, base.Height - base.Width);
					colorBlend.Colors = new Color[3]
					{
						colorTmp,
						HslHelper.GetColorLightFive(colorTmp),
						colorTmp
					};
					brush3.InterpolationColors = colorBlend;
					float height2 = (int)((long)m_actual * (long)(base.Height - base.Width) / max);
					g.FillRectangle(brush3, 0f, (float)base.Height - height2 - 1f - (float)(base.Width / 2), base.Width - 1, height2);
					g.FillEllipse(brush3, 0f, (float)base.Height - height2 - (float)base.Width, base.Width - 1, base.Width - 1);
					g.FillEllipse(brush3, 0, base.Height - base.Width, base.Width - 1, base.Width - 1);
					brush3.Dispose();
					break;
				}
				case HslProgressStyle.Horizontal:
				{
					LinearGradientBrush brush2 = new LinearGradientBrush(new PointF(0f, 0f), new PointF(0f, base.Height - 1), colorBorder, colorCenter);
					brush2.InterpolationColors = colorBlend;
					g.FillEllipse(brush2, base.Width - base.Height, 0, base.Height - 1, base.Height - 1);
					g.FillRectangle(brush2, base.Height / 2, 0, base.Width - base.Height - 1, base.Height - 1);
					colorBlend.Colors = new Color[3]
					{
						colorTmp,
						HslHelper.GetColorLightFive(colorTmp),
						colorTmp
					};
					brush2.InterpolationColors = colorBlend;
					float height = (int)((long)m_actual * (long)(base.Width - base.Height) / max);
					g.FillRectangle(brush2, base.Height / 2, 0f, height, base.Height - 1);
					g.FillEllipse(brush2, height, 0f, base.Height - 1, base.Height - 1);
					g.FillEllipse(brush2, 0, 0, base.Height - 1, base.Height - 1);
					brush2.Dispose();
					break;
				}
				}
				Rectangle rectangle = new Rectangle(0, 0, base.Width - 1, base.Height - 1);
				if (isTextRender)
				{
					string str = (long)m_actual * 100L / max + "%";
					using Brush brush = new SolidBrush(ForeColor);
					if (m_progressStyle != HslProgressStyle.Circular)
					{
						g.DrawString(str, Font, brush, rectangle, sf);
					}
					else
					{
						g.DrawString("Not supported", Font, brush, rectangle, sf);
					}
				}
			}
			catch (Exception)
			{
			}
			base.OnPaint(e);
		}

		private void ThreadPoolUpdateProgress(object obj)
		{
			try
			{
				int version = (int)obj;
				if (m_speed < 1)
				{
					m_speed = 1;
				}
				while (m_actual != m_value)
				{
					Thread.Sleep(17);
					if (version != m_version)
					{
						break;
					}
					int newActual = 0;
					if (m_actual > m_value)
					{
						int offect2 = m_actual - m_value;
						if (offect2 > m_speed)
						{
							offect2 = m_speed;
						}
						newActual = m_actual - offect2;
					}
					else
					{
						int offect = m_value - m_actual;
						if (offect > m_speed)
						{
							offect = m_speed;
						}
						newActual = m_actual + offect;
					}
					m_actual = newActual;
					if (version == m_version)
					{
						if (base.IsHandleCreated)
						{
							Invoke(m_UpdateAction);
						}
						continue;
					}
					break;
				}
			}
			catch (Exception)
			{
			}
		}

		private void UpdateRender()
		{
			Invalidate();
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			BackColor = System.Drawing.Color.Transparent;
			base.Name = "HslProgressColorful";
			base.Size = new System.Drawing.Size(421, 17);
			ResumeLayout(false);
		}
	}
}
