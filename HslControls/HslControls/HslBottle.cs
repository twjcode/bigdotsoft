using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslControls
{
	/// <summary>
	/// 一个类似瓶子的控件对象模型
	/// </summary>
	[Description("一个类似瓶子，罐子的控件，支持液位显示，支持设置渐变的颜色及文本标题")]
	public class HslBottle : UserControl
	{
		private Color foreColorTop = Color.FromArgb(243, 245, 139);

		private Color foreColorEdge = Color.FromArgb(194, 190, 77);

		private Color foreColorCenter = Color.FromArgb(226, 221, 98);

		private Color backColorTop = Color.FromArgb(151, 232, 244);

		private Color backColorEdge = Color.FromArgb(142, 196, 216);

		private Color backColorCenter = Color.FromArgb(240, 240, 240);

		private double value = 50.0;

		private bool isOpen = false;

		private string bottleTag = "";

		private StringFormat sf;

		private string headTag = "原料1";

		private float dockHeight = 30f;

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置控件的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Transparent")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// 获取或设置瓶子的液位值。
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置瓶子的液位值。")]
		[DefaultValue(typeof(double), "60")]
		[Category("HslControls")]
		public double Value
		{
			get
			{
				return value;
			}
			set
			{
				if (value >= 0.0 && value <= 100.0)
				{
					this.value = value;
					Invalidate();
				}
			}
		}

		/// <summary>
		/// 获取或设置瓶子是否处于打开的状态。
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置瓶子是否处于打开的状态。")]
		[DefaultValue(typeof(bool), "false")]
		[Category("HslControls")]
		public bool IsOpen
		{
			get
			{
				return isOpen;
			}
			set
			{
				isOpen = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置瓶子的标签信息，用于绘制在瓶子上的信息。
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置瓶子的标签信息，用于绘制在瓶子上的信息。")]
		[DefaultValue(typeof(string), "")]
		[Category("HslControls")]
		public string BottleTag
		{
			get
			{
				return bottleTag;
			}
			set
			{
				bottleTag = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置瓶子的备注信息，用于绘制在瓶子顶部的信息。
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置瓶子的备注信息，用于绘制在瓶子顶部的信息。")]
		[DefaultValue(typeof(string), "原料1")]
		[Category("HslControls")]
		public string HeadTag
		{
			get
			{
				return headTag;
			}
			set
			{
				headTag = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置控件底座的高度
		/// </summary>
		[Browsable(true)]
		[DefaultValue(30f)]
		[Description("获取或设置控件底座的高度")]
		[Category("HslControls")]
		public float DockHeight
		{
			get
			{
				return dockHeight;
			}
			set
			{
				dockHeight = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置前景色的边缘颜色内容
		/// </summary>
		[Browsable(true)]
		[DefaultValue(typeof(Color), "[194, 190, 77]")]
		[Description("获取或设置前景色的边缘颜色内容")]
		[Category("HslControls")]
		public Color ForeColorEdge
		{
			get
			{
				return foreColorEdge;
			}
			set
			{
				foreColorEdge = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置前景色的中心颜色内容
		/// </summary>
		[Browsable(true)]
		[DefaultValue(typeof(Color), "[226, 221, 98]")]
		[Description("获取或设置前景色的中心颜色内容")]
		[Category("HslControls")]
		public Color ForeColorCenter
		{
			get
			{
				return foreColorCenter;
			}
			set
			{
				foreColorCenter = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置前景色的顶部颜色内容
		/// </summary>
		[Browsable(true)]
		[DefaultValue(typeof(Color), "[243, 245, 139]")]
		[Description("获取或设置前景色的顶部颜色内容")]
		[Category("HslControls")]
		public Color ForeColorTop
		{
			get
			{
				return foreColorTop;
			}
			set
			{
				foreColorTop = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置背景色的边缘颜色内容
		/// </summary>
		[Browsable(true)]
		[DefaultValue(typeof(Color), "[142, 196, 216]")]
		[Description("获取或设置背景色的边缘颜色内容")]
		[Category("HslControls")]
		public Color BackColorEdge
		{
			get
			{
				return backColorEdge;
			}
			set
			{
				backColorEdge = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置背景色的中心颜色内容
		/// </summary>
		[Browsable(true)]
		[DefaultValue(typeof(Color), "[240, 240, 240]")]
		[Description("获取或设置背景色的中心颜色内容")]
		[Category("HslControls")]
		public Color BackColorCenter
		{
			get
			{
				return backColorCenter;
			}
			set
			{
				backColorCenter = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置背景色的顶部颜色内容
		/// </summary>
		[Browsable(true)]
		[DefaultValue(typeof(Color), "[151, 232, 244]")]
		[Description("获取或设置背景色的顶部颜色内容")]
		[Category("HslControls")]
		public Color BackColorTop
		{
			get
			{
				return backColorTop;
			}
			set
			{
				backColorTop = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 实例化一个新的控件对象
		/// </summary>
		public HslBottle()
		{
			InitializeComponent();
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
		}

		/// <summary>
		/// 重绘控件的外观
		/// </summary>
		/// <param name="e">事件</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			if (!Authorization.iashdiadasbdnajsdhjaf())
			{
				return;
			}
			Graphics g = e.Graphics;
			g.SmoothingMode = SmoothingMode.HighQuality;
			g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
			if (base.Width < 15 || base.Height < 15)
			{
				return;
			}
			float middle = (float)base.Width / 2f;
			float value_Y = (float)base.Height - dockHeight - ((float)base.Height - dockHeight - 20f) * Convert.ToSingle(value) / 100f;
			int ellipseHelght = base.Width / 50 + 3;
			GraphicsPath graphicsPath = new GraphicsPath();
			graphicsPath.AddPolygon(new PointF[6]
			{
				new PointF(0f, 20f),
				new PointF(0f, (float)base.Height - dockHeight),
				new PointF(middle, base.Height - 1),
				new PointF(base.Width - 1, (float)base.Height - dockHeight),
				new PointF(base.Width - 1, 20f),
				new PointF(0f, 20f)
			});
			LinearGradientBrush b = new LinearGradientBrush(new Point(0, 20), new Point(base.Width - 1, 20), Color.FromArgb(142, 196, 216), Color.FromArgb(240, 240, 240));
			ColorBlend colorBlend = new ColorBlend();
			colorBlend.Positions = new float[3]
			{
				0f,
				0.5f,
				1f
			};
			colorBlend.Colors = new Color[3]
			{
				backColorEdge,
				backColorCenter,
				backColorEdge
			};
			b.InterpolationColors = colorBlend;
			g.FillPath(b, graphicsPath);
			using (Brush brush2 = new SolidBrush(backColorTop))
			{
				g.FillEllipse(brush2, 1, 20 - ellipseHelght, base.Width - 2, ellipseHelght * 2);
			}
			graphicsPath.Reset();
			graphicsPath.AddPolygon(new PointF[6]
			{
				new PointF(0f, value_Y),
				new PointF(0f, (float)base.Height - dockHeight),
				new PointF(middle, base.Height - 1),
				new PointF(base.Width - 1, (float)base.Height - dockHeight),
				new PointF(base.Width - 1, value_Y),
				new PointF(0f, value_Y)
			});
			colorBlend.Colors = new Color[3]
			{
				foreColorEdge,
				foreColorCenter,
				foreColorEdge
			};
			b.InterpolationColors = colorBlend;
			g.FillPath(b, graphicsPath);
			b.Dispose();
			using (Brush brush3 = new SolidBrush(foreColorTop))
			{
				g.FillEllipse(brush3, 1f, value_Y - (float)ellipseHelght, base.Width - 2, ellipseHelght * 2);
			}
			graphicsPath.Reset();
			graphicsPath.AddPolygon(new PointF[3]
			{
				new PointF(0f, (float)base.Height - dockHeight),
				new PointF(middle, base.Height - 1),
				new PointF(base.Width - 1, (float)base.Height - dockHeight)
			});
			graphicsPath.AddArc(0f, (float)base.Height - dockHeight - (float)ellipseHelght, base.Width, ellipseHelght * 2, 0f, 180f);
			using (Brush solidBrush = new SolidBrush(foreColorEdge))
			{
				g.FillPath(solidBrush, graphicsPath);
			}
			graphicsPath.Reset();
			graphicsPath.AddLines(new PointF[4]
			{
				new PointF((float)base.Width / 4f, (float)base.Height - dockHeight / 2f),
				new PointF((float)base.Width / 4f, base.Height - 1),
				new PointF((float)base.Width * 3f / 4f, base.Height - 1),
				new PointF((float)base.Width * 3f / 4f, (float)base.Height - dockHeight / 2f)
			});
			graphicsPath.AddArc((float)base.Width / 4f, (float)base.Height - dockHeight / 2f - (float)ellipseHelght - 1f, (float)base.Width / 2f, ellipseHelght * 2, 0f, 180f);
			g.FillPath(Brushes.DimGray, graphicsPath);
			using (Brush brush = new SolidBrush(ForeColor))
			{
				if (!string.IsNullOrEmpty(bottleTag))
				{
					g.DrawString(bottleTag, Font, brush, new Rectangle(-10, 26, base.Width + 20, 20), sf);
				}
				if (!string.IsNullOrEmpty(headTag))
				{
					g.DrawString(headTag, Font, brush, new Rectangle(-10, 0, base.Width + 20, 20), sf);
				}
			}
			graphicsPath.Dispose();
			base.OnPaint(e);
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			BackColor = System.Drawing.Color.Transparent;
			base.Name = "HslBottle";
			ResumeLayout(false);
		}
	}
}
