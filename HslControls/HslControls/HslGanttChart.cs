using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslControls
{
	/// <summary>
	/// 甘特图控件，可以用来表示一天的数据使用情况
	/// </summary>
	[Description("一个简单的甘特图控件，可以用来表示一天或是一个月里面各个环节占用的时间信息")]
	public class HslGanttChart : UserControl
	{
		private int[] data = new int[0];

		private Color[] colors = new Color[0];

		private StringFormat sf = null;

		private int timeStart = 0;

		private int timeMax = 24;

		private int timeCount = 86400;

		private int timeSegment = 6;

		private string timeFormate = "{0}H";

		private Color ganttBackColor = Color.LightGray;

		private Brush ganttBrush = new SolidBrush(Color.LightGray);

		private int leftRightOffect = 20;

		private HslDirectionStyle hslValvesStyle = HslDirectionStyle.Horizontal;

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置控件的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Transparent")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// 获取或设置甘特图的背景颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置甘特图的背景颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "LightGray")]
		public Color GanttBackColor
		{
			get
			{
				return ganttBackColor;
			}
			set
			{
				ganttBackColor = value;
				ganttBrush.Dispose();
				ganttBrush = new SolidBrush(ganttBackColor);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置甘特图的时间的分段信息
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置甘特图的时间的分段信息")]
		[Category("HslControls")]
		[DefaultValue(6)]
		public int TimeSegment
		{
			get
			{
				return timeSegment;
			}
			set
			{
				timeSegment = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置时间的起始信息，通常一天的起始为0，一个月的起始为1，年份的起始为自然年，比如2018
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置时间的起始信息，通常一天的起始为0，一个月的起始为1，年份的起始为自然年，比如2018")]
		[Category("HslControls")]
		[DefaultValue(0)]
		public int TimeStart
		{
			get
			{
				return timeStart;
			}
			set
			{
				timeStart = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置时间的结束信息，通常一天的结束为24，一个月的结束为29,30,31，年份的结束为自然年，比如2019
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置时间的结束信息，通常一天的结束为24，一个月的结束为29,30,31，年份的结束为自然年，比如2019")]
		[Category("HslControls")]
		[DefaultValue(24)]
		public int TimeMax
		{
			get
			{
				return timeMax;
			}
			set
			{
				timeMax = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置时间的总数信息，比如是按照天计算的甘特图，设置了1440，就是按照分钟计数，如果是86400，就是按照秒计数，这个传入的值密切相关
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置时间的总数信息，比如是按照天计算的甘特图，设置了1440，就是按照分钟计数，如果是86400，就是按照秒计数，这个传入的值密切相关")]
		[Category("HslControls")]
		[DefaultValue(86400)]
		public int TimeCount
		{
			get
			{
				return timeCount;
			}
			set
			{
				timeCount = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置时间的显示格式，可以设置为显示小时，分钟，天等等信息
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置时间的显示格式，可以设置为显示小时，分钟，天等等信息")]
		[Category("HslControls")]
		[DefaultValue("{0}H")]
		public string TimeFormate
		{
			get
			{
				return timeFormate;
			}
			set
			{
				timeFormate = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置两侧的距离宽度，当你的数字范围特别长的时候就有用，默认为20
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置两侧的距离宽度，当你的数字范围特别长的时候就有用，默认为20")]
		[Category("HslControls")]
		[DefaultValue(20)]
		public int LeftRightOffect
		{
			get
			{
				return leftRightOffect;
			}
			set
			{
				leftRightOffect = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 实例化一个默认的甘特图控件
		/// </summary>
		public HslGanttChart()
		{
			InitializeComponent();
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
		}

		/// <summary>
		/// 重绘控件的
		/// </summary>
		/// <param name="e">重绘事件</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			if (Authorization.iashdiadasbdnajsdhjaf())
			{
				Graphics g = e.Graphics;
				g.SmoothingMode = SmoothingMode.HighQuality;
				g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
				if (hslValvesStyle == HslDirectionStyle.Horizontal)
				{
					PaintMain(g, base.Width, base.Height);
				}
				else
				{
					g.TranslateTransform(base.Width, 0f);
					g.RotateTransform(90f);
					PaintMain(g, base.Height, base.Width);
					g.ResetTransform();
				}
				base.OnPaint(e);
			}
		}

		private void PaintMain(Graphics g, float width, float height)
		{
			int offect = leftRightOffect;
			int top = Font.Height + 2;
			Brush fontBrush = new SolidBrush(ForeColor);
			for (int j = 0; j <= timeSegment; j++)
			{
				int hourValue = (timeMax - timeStart) * j / timeSegment + timeStart;
				float paint_x = (width - (float)(2 * offect)) * (float)j / (float)timeSegment + (float)offect;
				g.DrawString(string.Format(timeFormate, hourValue), Font, fontBrush, new Rectangle((int)paint_x - 50, 0, 100, top), sf);
			}
			g.SmoothingMode = SmoothingMode.None;
			RectangleF rectangleMain = new RectangleF(offect, top, width - (float)(2 * offect), height - (float)top);
			g.FillRectangle(ganttBrush, rectangleMain);
			int secondsCount = 0;
			for (int i = 0; i < data.Length; i++)
			{
				float paint_start = (float)secondsCount * (width - (float)(2 * offect)) / (float)timeCount + (float)offect;
				float paint_width = (float)data[i] * (width - (float)(2 * offect)) / (float)timeCount;
				if (paint_width > rectangleMain.Width - paint_start + (float)offect)
				{
					paint_width = rectangleMain.Width - paint_start + (float)offect;
				}
				if (i < colors.Length)
				{
					using Brush brush = new SolidBrush(colors[i]);
					g.FillRectangle(brush, new RectangleF(paint_start, top, paint_width, rectangleMain.Height));
				}
				secondsCount += data[i];
				if (secondsCount > timeCount)
				{
					break;
				}
			}
			fontBrush.Dispose();
		}

		/// <summary>
		/// 设置甘特图的数据信息，和颜色信息
		/// </summary>
		/// <param name="data">数据</param>
		/// <param name="colors">颜色</param>
		public void SetGanttChart(int[] data, Color[] colors)
		{
			if (data != null && colors != null)
			{
				this.data = data;
				this.colors = colors;
				Invalidate();
			}
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			BackColor = System.Drawing.Color.Transparent;
			base.Name = "HslGanttChart";
			base.Size = new System.Drawing.Size(333, 65);
			ResumeLayout(false);
		}
	}
}
