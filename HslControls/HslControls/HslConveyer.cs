using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslControls
{
	/// <summary>
	/// 一个传送带的控件
	/// </summary>
	[Description("传送带控件，支持正反方向设置，支持转速设置，颜色设置")]
	public class HslConveyer : UserControl
	{
		private StringFormat sf = null;

		private float moveSpeed = 0.3f;

		private float startAngle = 0f;

		private float startOffect = 0f;

		private Timer timer = null;

		private bool isConveyerActive = false;

		private float circularRadius = 20f;

		private HslConveyerStyle conveyerStyle = HslConveyerStyle.Horizontal;

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置控件的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Transparent")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// 获取或设置当前控件的文本
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前控件的文本")]
		[Category("HslControls")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Bindable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置控件的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的前景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "[142, 196, 216]")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color ForeColor
		{
			get
			{
				return base.ForeColor;
			}
			set
			{
				base.ForeColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置传送带流动的速度，0为静止，正数为正向流动，负数为反向流动
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置传送带流动的速度，0为静止，正数为正向流动，负数为反向流动")]
		[Category("HslControls")]
		[DefaultValue(0.3f)]
		public float MoveSpeed
		{
			get
			{
				return moveSpeed;
			}
			set
			{
				moveSpeed = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置管道线是否激活液体显示
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置管道线是否激活液体显示")]
		[Category("HslControls")]
		[DefaultValue(false)]
		public bool ConveyerActive
		{
			get
			{
				return isConveyerActive;
			}
			set
			{
				isConveyerActive = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置两侧的圆圈的半径，当且仅当ConveyerStyle属性不为Horizontal枚举时成立
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置两侧的圆圈的半径，当且仅当ConveyerStyle属性不为Horizontal枚举时成立")]
		[Category("HslControls")]
		[DefaultValue(HslConveyerStyle.Horizontal)]
		public float CircularRadius
		{
			get
			{
				return circularRadius;
			}
			set
			{
				if (!(value > (float)(base.Width / 2)) && !(value > (float)(base.Height / 2)))
				{
					circularRadius = value;
					Invalidate();
				}
			}
		}

		/// <summary>
		/// 获取或设置传送带的样式，水平，还是上坡，还是下坡
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置传送带的样式，水平，还是上坡，还是下坡")]
		[Category("HslControls")]
		[DefaultValue(HslConveyerStyle.Horizontal)]
		public HslConveyerStyle ConveyerStyle
		{
			get
			{
				return conveyerStyle;
			}
			set
			{
				conveyerStyle = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 实例化一个传送带的控件对象
		/// </summary>
		public HslConveyer()
		{
			InitializeComponent();
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
			base.ForeColor = Color.FromArgb(142, 196, 216);
			timer = new Timer();
			timer.Interval = 50;
			timer.Tick += Timer_Tick;
			timer.Start();
		}

		private void Timer_Tick(object sender, EventArgs e)
		{
			if (moveSpeed == 0f)
			{
				return;
			}
			if (isConveyerActive)
			{
				startOffect -= moveSpeed;
				if (startOffect <= -10f || startOffect >= 10f)
				{
					startOffect = 0f;
				}
			}
			startAngle = (float)((double)startAngle + (double)(moveSpeed * 180f) / Math.PI / 10.0);
			if (startAngle <= -360f)
			{
				startAngle += 360f;
			}
			else if (startAngle >= 360f)
			{
				startAngle -= 360f;
			}
			Invalidate();
		}

		/// <summary>
		/// 重绘控件的界面信息
		/// </summary>
		/// <param name="e">事件</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			if (Authorization.iashdiadasbdnajsdhjaf())
			{
				e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
				e.Graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
				int margin = 5;
				int offect = 5;
				if (conveyerStyle == HslConveyerStyle.Horizontal)
				{
					PaintMain(e.Graphics, base.Width, base.Height, margin, offect);
				}
				else if (conveyerStyle == HslConveyerStyle.Upslope)
				{
					PointF left2 = new PointF((float)(margin + offect) + circularRadius, (float)(base.Height - 1 - margin - offect) - circularRadius);
					PointF right2 = new PointF((float)(base.Width - margin - offect - 1) - circularRadius, (float)(margin + offect) + circularRadius);
					float beveling2 = (float)Math.Sqrt(Math.Pow(right2.Y - left2.Y, 2.0) + Math.Pow(right2.X - left2.X, 2.0));
					float angle2 = (float)(Math.Acos((right2.X - left2.X) / beveling2) * 180.0 / Math.PI);
					e.Graphics.TranslateTransform(left2.X, left2.Y);
					e.Graphics.RotateTransform(0f - angle2);
					e.Graphics.TranslateTransform((float)(-margin - offect) - circularRadius, (float)(-margin - offect) - circularRadius);
					PaintMain(e.Graphics, beveling2 + (float)(2 * margin) + (float)(2 * offect) + 2f * circularRadius, (float)(2 * margin + 2 * offect) + 2f * circularRadius, margin, offect);
					e.Graphics.ResetTransform();
				}
				else
				{
					PointF left = new PointF((float)(margin + offect) + circularRadius, (float)(margin + offect) + circularRadius);
					PointF right = new PointF((float)(base.Width - margin - offect - 1) - circularRadius, (float)(base.Height - 1 - margin - offect) - circularRadius);
					float beveling = (float)Math.Sqrt(Math.Pow(right.Y - left.Y, 2.0) + Math.Pow(right.X - left.X, 2.0));
					float angle = (float)(Math.Acos((right.X - left.X) / beveling) * 180.0 / Math.PI);
					e.Graphics.TranslateTransform(left.X, left.Y);
					e.Graphics.RotateTransform(angle);
					e.Graphics.TranslateTransform((float)(-margin - offect) - circularRadius, (float)(-margin - offect) - circularRadius);
					PaintMain(e.Graphics, beveling + (float)(2 * margin) + (float)(2 * offect) + 2f * circularRadius, (float)(2 * margin + 2 * offect) + 2f * circularRadius, margin, offect);
					e.Graphics.ResetTransform();
				}
				base.OnPaint(e);
			}
		}

		private void PaintMain(Graphics g, float width, float height, float margin, float offect)
		{
			float radius = (height - margin * 2f - offect * 2f) / 2f;
			Pen pen = new Pen(ForeColor, 1f);
			Pen penCross = new Pen(ForeColor, (radius > 100f) ? 7 : ((radius > 20f) ? 5 : ((!(radius > 5f)) ? 1 : 3)));
			penCross.StartCap = LineCap.Round;
			penCross.EndCap = LineCap.Round;
			Brush brush = new SolidBrush(ForeColor);
			GraphicsPath graphicsPath = new GraphicsPath();
			graphicsPath.AddArc(margin, margin, radius * 2f + offect * 2f, radius * 2f + offect * 2f, 90f, 180f);
			graphicsPath.AddLine(margin + offect + radius, margin, width - margin - offect - radius - 1f, margin);
			graphicsPath.AddArc(width - margin - radius * 2f - offect * 2f, margin, radius * 2f + offect * 2f, radius * 2f + offect * 2f, -90f, 180f);
			graphicsPath.CloseFigure();
			g.DrawPath(pen, graphicsPath);
			g.DrawEllipse(pen, margin + offect, margin + offect, radius * 2f, radius * 2f);
			g.DrawEllipse(pen, width - margin - radius * 2f - offect, margin + offect, radius * 2f, radius * 2f);
			float spec_width = (float)Math.Sqrt(2f * offect * radius + offect * offect);
			float angle = (float)(Math.Asin(radius / (radius + offect)) * 180.0 / Math.PI);
			graphicsPath.Reset();
			graphicsPath.AddArc(margin, margin, radius * 2f + offect * 2f, radius * 2f + offect * 2f, 0f - angle, angle * 2f);
			graphicsPath.AddLine(margin + offect + radius + spec_width, height - margin - offect, width - margin - offect - radius - 1f - spec_width, height - margin - offect);
			graphicsPath.AddArc(width - margin - radius * 2f - offect * 2f, margin, radius * 2f + offect * 2f, radius * 2f + offect * 2f, 180f - angle, angle * 2f);
			graphicsPath.CloseFigure();
			g.DrawPath(pen, graphicsPath);
			g.TranslateTransform(margin + offect + radius, margin + offect + radius);
			g.RotateTransform(startAngle);
			g.DrawLine(penCross, (0f - radius) / 2f, 0f, radius / 2f, 0f);
			g.DrawLine(penCross, 0f, (0f - radius) / 2f, 0f, radius / 2f);
			g.RotateTransform(0f - startAngle);
			g.TranslateTransform(0f - margin - offect - radius, 0f - margin - offect - radius);
			g.TranslateTransform(width - margin - radius - offect, margin + offect + radius);
			g.RotateTransform(startAngle);
			g.DrawLine(penCross, (0f - radius) / 2f, 0f, radius / 2f, 0f);
			g.DrawLine(penCross, 0f, (0f - radius) / 2f, 0f, radius / 2f);
			g.RotateTransform(0f - startAngle);
			g.TranslateTransform(0f - width + margin + radius + offect, 0f - margin - offect - radius);
			if (isConveyerActive)
			{
				using Pen pen_dash = new Pen(ForeColor, 5f);
				pen_dash.DashStyle = DashStyle.Custom;
				pen_dash.DashPattern = new float[2]
				{
					5f,
					5f
				};
				pen_dash.DashOffset = startOffect;
				graphicsPath.Reset();
				graphicsPath.AddArc(margin + offect / 2f, margin + offect / 2f, radius * 2f + offect, radius * 2f + offect, 90f, 180f);
				graphicsPath.AddLine(margin + offect + radius + 1f, margin + offect / 2f, width - margin - offect - radius - 2f, margin + offect / 2f);
				graphicsPath.AddArc(width - margin - radius * 2f - offect * 2f + offect / 2f, margin + offect / 2f, radius * 2f + offect, radius * 2f + offect, -90f, 180f);
				graphicsPath.CloseFigure();
				g.DrawPath(pen_dash, graphicsPath);
			}
			g.DrawString(Text, Font, brush, new RectangleF(0f, 0f, width, height), sf);
			graphicsPath.Dispose();
			pen.Dispose();
			penCross.Dispose();
			brush.Dispose();
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			BackColor = System.Drawing.Color.Transparent;
			base.Name = "HslConveyer";
			base.Size = new System.Drawing.Size(461, 61);
			ResumeLayout(false);
		}
	}
}
