using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Linq;
using System.Windows.Forms;

namespace HslControls
{
	[Description("一个新的仪表盘控件，方便的用来显示流量计等情况")]
	public class HslDialPlate : UserControl
	{
		private StringFormat sf = null;

		private Color boderColor = Color.FromArgb(0, 0, 0);

		private Color boderColor2 = Color.FromArgb(228, 229, 229);

		private Pen borderPen = new Pen(Color.FromArgb(0, 0, 0));

		private Color centerColor = Color.Gray;

		private Color dialPlateBackgroundColor = Color.White;

		private bool isRenderSegmentText = true;

		private int segment = 8;

		private float maxValue = 100f;

		private float minValue = 0f;

		private float value = 0f;

		private string unitText = "";

		private MarkValueCollection markValues;

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置控件的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Transparent")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// 获取或设置仪表控件的中心背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置仪表控件的中心背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Gray")]
		public Color CenterColor
		{
			get
			{
				return centerColor;
			}
			set
			{
				centerColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置控件的边缘颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置分类器控件的边缘颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "[0,0,0]")]
		public Color BorderColor
		{
			get
			{
				return boderColor;
			}
			set
			{
				boderColor = value;
				borderPen.Dispose();
				borderPen = new Pen(boderColor);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置控件内边缘的背景颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件内边缘的背景颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "[228, 229, 229]")]
		public Color BoderColor2
		{
			get
			{
				return boderColor2;
			}
			set
			{
				boderColor2 = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置当前仪表的主背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前仪表的主背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "White")]
		public Color DialPlateBackgroundColor
		{
			get
			{
				return dialPlateBackgroundColor;
			}
			set
			{
				dialPlateBackgroundColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置当前仪表是否显示文本信息
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前仪表是否显示文本信息")]
		[Category("HslControls")]
		[DefaultValue(true)]
		public bool IsRenderSegmentText
		{
			get
			{
				return isRenderSegmentText;
			}
			set
			{
				isRenderSegmentText = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置当前仪表的分割数量
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前仪表的分割数量")]
		[Category("HslControls")]
		[DefaultValue(8)]
		public int Segment
		{
			get
			{
				return segment;
			}
			set
			{
				if (segment > 0)
				{
					segment = value;
					Invalidate();
				}
			}
		}

		/// <summary>
		/// 获取或设置当前仪表控件的最小值
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前仪表控件的最小值")]
		[Category("HslControls")]
		[DefaultValue(0f)]
		public float MinValue
		{
			get
			{
				return minValue;
			}
			set
			{
				minValue = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置当前仪表控件的最大值
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前仪表控件的最大值")]
		[Category("HslControls")]
		[DefaultValue(100f)]
		public float MaxValue
		{
			get
			{
				return maxValue;
			}
			set
			{
				maxValue = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置仪表控件的当前值
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置仪表控件的当前值")]
		[Category("HslControls")]
		[DefaultValue(0f)]
		public float Value
		{
			get
			{
				return value;
			}
			set
			{
				this.value = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置显示的单位文本信息
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置显示的单位文本信息")]
		[Category("HslControls")]
		[DefaultValue("")]
		public string UnitText
		{
			get
			{
				return unitText;
			}
			set
			{
				unitText = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置当前辅助标记的集合
		/// </summary>
		[Category("HslControls")]
		[Description("获取或设置当前辅助标记的集合。")]
		[TypeConverter(typeof(CollectionConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public MarkValueCollection MarkValues => markValues;

		public HslDialPlate()
		{
			InitializeComponent();
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
			markValues = new MarkValueCollection();
			markValues.control = this;
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			if (Authorization.iashdiadasbdnajsdhjaf())
			{
				Graphics g = e.Graphics;
				g.SmoothingMode = SmoothingMode.HighQuality;
				g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
				if (base.Width > 15 && base.Height > 15)
				{
					PaintHslControls(g, Math.Min(base.Width, base.Height), Math.Min(base.Width, base.Height));
				}
				base.OnPaint(e);
			}
		}

		/// <inheritdoc cref="M:HslControls.HslArrow.PaintHslControls(System.Drawing.Graphics,System.Single,System.Single)" />
		public void PaintHslControls(Graphics g, float width, float height)
		{
			if (maxValue <= minValue)
			{
				return;
			}
			g.TranslateTransform(width / 2f, width / 2f);
			Font font = new Font(Font.FontFamily, width / 30f);
			Brush fontBrush = new SolidBrush(ForeColor);
			using (Brush brush2 = new SolidBrush(dialPlateBackgroundColor))
			{
				g.FillEllipse(brush2, new RectangleF((0f - width) / 2f + 1f, (0f - width) / 2f + 1f, width - 3f, width - 3f));
			}
			borderPen.Width = width / 14f;
			borderPen.Color = BoderColor2;
			g.DrawEllipse(borderPen, new RectangleF((0f - width) / 2f + borderPen.Width / 2f, (0f - width) / 2f + borderPen.Width / 2f, width - borderPen.Width - 1f, width - borderPen.Width - 1f));
			borderPen.Color = ForeColor;
			borderPen.Width = 1f;
			if (segment > 0)
			{
				float angleOffset = 360f / (float)segment;
				if (isRenderSegmentText)
				{
					for (int l = 0; l < segment; l++)
					{
						float currentValue = (maxValue - minValue) * (float)l / (float)segment + minValue;
						if (markValues.FirstOrDefault((MarkValue m) => m.Value == currentValue) == null)
						{
							float offset = width * 0.36f - (float)font.Height;
							float angleTmp = 360f / (float)segment * (float)l;
							g.RotateTransform(angleTmp);
							g.DrawLine(borderPen, 0f, width * 0.365f, 0f, width * 0.4f);
							g.RotateTransform(0f - angleTmp);
							float x = (float)((double)offset * Math.Cos((double)(angleTmp + 90f) / 360.0 * 2.0 * Math.PI));
							float y = (float)((double)offset * Math.Sin((double)(angleTmp + 90f) / 360.0 * 2.0 * Math.PI));
							g.DrawString(layoutRectangle: new RectangleF(x - 100f, y - (float)(font.Height / 2), 200f, font.Height), s: currentValue.ToString(), font: font, brush: fontBrush, format: sf);
						}
					}
					for (int k = 0; k < markValues.Count; k++)
					{
						angleOffset = (markValues[k].Value - minValue) / (maxValue - minValue) * 360f;
						g.RotateTransform(angleOffset);
						using (Pen pen = new Pen(markValues[k].Color, markValues[k].LineWidth))
						{
							g.DrawLine(pen, 0f, width * 0.365f, 0f, width * 0.4f);
						}
						g.RotateTransform(0f - angleOffset);
						float offset2 = width * 0.36f - (float)font.Height;
						float x2 = (float)((double)offset2 * Math.Cos((double)(angleOffset + 90f) / 360.0 * 2.0 * Math.PI));
						float y2 = (float)((double)offset2 * Math.Sin((double)(angleOffset + 90f) / 360.0 * 2.0 * Math.PI));
						RectangleF rectangle = new RectangleF(x2 - 100f, y2 - (float)(font.Height / 2), 200f, font.Height);
						using Brush brush3 = new SolidBrush(markValues[k].Color);
						g.DrawString(markValues[k].Value.ToString(), font, brush3, rectangle, sf);
					}
				}
				else
				{
					for (int j = 0; j < segment; j++)
					{
						g.DrawLine(borderPen, 0f, width * 0.33f, 0f, width * 0.4f);
						g.RotateTransform(angleOffset);
					}
				}
			}
			GraphicsPath path2 = new GraphicsPath();
			path2.AddEllipse(new RectangleF((0f - width) / 2f + 1f, (0f - width) / 2f + 1f, width - 3f, width - 3f));
			GraphicsPath path3 = new GraphicsPath();
			path3.AddEllipse(new RectangleF((0f - width) / 2f + 1f - width * 0.2f, (0f - width) / 2f + 1f - width * 0.2f, width - 3f, width - 3f));
			Region region = new Region(path2);
			region.Intersect(path3);
			using (Brush brush4 = new SolidBrush(Color.FromArgb(60, centerColor)))
			{
				g.FillRegion(brush4, region);
			}
			region.Dispose();
			path3.Dispose();
			path2.Dispose();
			borderPen.Width = width / 20f;
			borderPen.Color = boderColor;
			g.DrawEllipse(borderPen, new RectangleF((0f - width) / 2f + borderPen.Width / 2f, (0f - width) / 2f + borderPen.Width / 2f, width - borderPen.Width - 1f, width - borderPen.Width - 1f));
			float angle = (value - minValue) / (maxValue - minValue) * 360f;
			g.RotateTransform(angle);
			using (Brush brush = new SolidBrush(boderColor))
			{
				g.FillEllipse(brush, new RectangleF((0f - width) * 0.04f, (0f - width) * 0.04f, width * 0.08f, width * 0.08f));
				for (int i = 0; i < 2; i++)
				{
					GraphicsPath path = new GraphicsPath();
					path.AddLine(0f, width * 0.41f, width * 0.02f, 0f);
					path.AddBezier(new PointF(width * 0.02f, 0f), new PointF(width * 0.03f, (0f - width) * 0.1f), new PointF(width * 0.04f, (0f - width) * 0.11f), new PointF(width * 0.05f, (0f - width) * 0.13f));
					path.AddBezier(new PointF(width * 0.05f, (0f - width) * 0.13f), new PointF(width * 0.06f, (0f - width) * 0.15f), new PointF(width * 0.06f, (0f - width) * 0.15f), new PointF(width * 0.04f, (0f - width) * 0.15f));
					path.AddLine(new PointF(width * 0.04f, (0f - width) * 0.15f), new PointF(-1f, (0f - width) * 0.15f));
					path.CloseFigure();
					g.FillPath(brush, path);
					g.ScaleTransform(-1f, 1f);
				}
			}
			g.RotateTransform(0f - angle);
			LinearGradientBrush b = new LinearGradientBrush(new PointF(width * 0.022f, width * 0.022f), new PointF((0f - width) * 0.022f, (0f - width) * 0.022f), Color.FromArgb(161, 141, 108), Color.FromArgb(255, 232, 190));
			b.InterpolationColors = new ColorBlend
			{
				Colors = new Color[3]
				{
					Color.FromArgb(161, 141, 108),
					Color.FromArgb(255, 232, 190),
					Color.FromArgb(161, 141, 108)
				},
				Positions = new float[3]
				{
					0f,
					0.3f,
					1f
				}
			};
			g.FillEllipse(b, new RectangleF((0f - width) * 0.022f, (0f - width) * 0.022f, width * 0.044f, width * 0.044f));
			b.Dispose();
			if (!string.IsNullOrEmpty(unitText))
			{
				g.DrawString(unitText, font, fontBrush, new RectangleF(width * 0.12f - 100f, width * (width / 5000f + 0.01f), 200f, Font.Height), sf);
			}
			font.Dispose();
			fontBrush.Dispose();
			g.TranslateTransform((0f - width) / 2f, (0f - width) / 2f);
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			BackColor = System.Drawing.Color.Transparent;
			Font = new System.Drawing.Font("微软雅黑", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 134);
			base.Name = "HslDialPlate";
			base.Size = new System.Drawing.Size(241, 218);
			ResumeLayout(false);
		}
	}
}
