using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslControls
{
	/// <summary>
	/// 一个分类器的控件
	/// </summary>
	[DefaultBindingProperty("Text")]
	[DefaultProperty("Text")]
	public class HslClassifier : UserControl
	{
		private StringFormat sf = null;

		private Color boderColor = Color.DimGray;

		private Pen borderPen = new Pen(Color.DimGray);

		private Color edgeColor = Color.Gray;

		private Color centerColor = Color.WhiteSmoke;

		private Color textBackColor = Color.FromArgb(46, 46, 46);

		private Brush textBackBrush = new SolidBrush(Color.FromArgb(46, 46, 46));

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置控件的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Transparent")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// 获取或设置当前控件的文本
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前控件的文本")]
		[Category("HslControls")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Bindable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置控件的前景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Cyan")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color ForeColor
		{
			get
			{
				return base.ForeColor;
			}
			set
			{
				base.ForeColor = value;
			}
		}

		/// <summary>
		/// 获取或设置分类器控件的边缘颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置分类器控件的边缘颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Gray")]
		public Color EdgeColor
		{
			get
			{
				return edgeColor;
			}
			set
			{
				edgeColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置分类器控件的中心颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置分类器控件的中心颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "WhiteSmoke")]
		public Color CenterColor
		{
			get
			{
				return centerColor;
			}
			set
			{
				centerColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置分类器控件的边缘颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置分类器控件的边缘颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "DimGray")]
		public Color BorderColor
		{
			get
			{
				return boderColor;
			}
			set
			{
				boderColor = value;
				borderPen.Dispose();
				borderPen = new Pen(boderColor);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置分类器控件文本的背景颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置分类器控件文本的背景颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "[46, 46, 46]")]
		public Color TextBackColor
		{
			get
			{
				return textBackColor;
			}
			set
			{
				textBackColor = value;
				textBackBrush.Dispose();
				textBackBrush = new SolidBrush(textBackColor);
				Invalidate();
			}
		}

		/// <summary>
		/// 实例化一个分类器的对象
		/// </summary>
		public HslClassifier()
		{
			InitializeComponent();
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
			ForeColor = Color.Cyan;
		}

		/// <summary>
		/// 控件的重绘事件
		/// </summary>
		/// <param name="e">事件</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			if (Authorization.iashdiadasbdnajsdhjaf())
			{
				Graphics g = e.Graphics;
				g.SmoothingMode = SmoothingMode.HighQuality;
				g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
				PaintMain(g, base.Width, base.Height);
				base.OnPaint(e);
			}
		}

		private void PaintMain(Graphics g, float width, float height)
		{
			LinearGradientBrush b = new LinearGradientBrush(new Point(0, 20), new Point(base.Width - 1, 20), Color.FromArgb(142, 196, 216), Color.FromArgb(240, 240, 240));
			ColorBlend colorBlend = new ColorBlend();
			colorBlend.Positions = new float[3]
			{
				0f,
				0.45f,
				1f
			};
			colorBlend.Colors = new Color[3]
			{
				edgeColor,
				centerColor,
				edgeColor
			};
			b.InterpolationColors = colorBlend;
			g.FillRectangle(b, width * 0.02f, -1f, width * 0.96f, height * 0.2f);
			g.DrawRectangle(borderPen, width * 0.02f, 0f, width * 0.96f, height * 0.2f);
			g.FillRectangle(b, 0f, height * 0.02f, width, height * 0.52f);
			PointF[] points = new PointF[4]
			{
				new PointF(0f, height * 0.54f),
				new PointF(width - 1f, height * 0.54f),
				new PointF(width * 0.65f, height * 0.8f),
				new PointF(width * 0.35f, height * 0.8f)
			};
			g.FillPolygon(b, points);
			g.DrawPolygon(borderPen, points);
			b.Dispose();
			g.DrawRectangle(borderPen, 0f, height * 0.02f, width - 1f, height * 0.52f);
			g.DrawRectangle(borderPen, width * 0.35f, height * 0.8f, width * 0.3f, height * 0.17f);
			b = new LinearGradientBrush(new PointF(width * 0.35f, 20f), new PointF((float)base.Width * 0.65f, 20f), Color.FromArgb(142, 196, 216), Color.FromArgb(240, 240, 240));
			colorBlend.Positions = new float[3]
			{
				0f,
				0.333333343f,
				1f
			};
			b.InterpolationColors = colorBlend;
			g.FillEllipse(b, width * 0.35f, height * 0.94f - 1f, width * 0.3f, height * 0.06f);
			g.DrawEllipse(borderPen, width * 0.35f, height * 0.94f - 1f, width * 0.3f, height * 0.06f);
			b.InterpolationColors = colorBlend;
			g.FillRectangle(b, width * 0.35f, height * 0.8f, width * 0.3f, height * 0.17f);
			b.Dispose();
			if (!string.IsNullOrEmpty(Text))
			{
				float font_height = g.MeasureString(Text, Font, (int)(width * 0.5f)).Height + 10f;
				RectangleF rectangle = new RectangleF(width * 0.25f, height * 0.26f - font_height / 2f, width * 0.5f, font_height);
				g.FillRectangle(textBackBrush, rectangle);
				g.DrawRectangle(borderPen, rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
				using SolidBrush brush = new SolidBrush(ForeColor);
				g.DrawString(Text, Font, brush, rectangle, sf);
			}
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			BackColor = System.Drawing.Color.Transparent;
			base.Name = "HslClassifier";
			base.Size = new System.Drawing.Size(161, 159);
			ResumeLayout(false);
		}
	}
}
