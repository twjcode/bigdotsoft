using System.Drawing;

namespace HslControls
{
	/// <summary>
	/// 曲线数据对象
	/// </summary>
	public class HslCurveItem
	{
		/// <summary>
		/// 数据
		/// </summary>
		public float[] Data = null;

		/// <summary>
		/// 标记文本
		/// </summary>
		public string[] MarkText = null;

		/// <summary>
		/// 线条的宽度
		/// </summary>
		public float LineThickness
		{
			get;
			set;
		}

		/// <summary>
		/// 是否平滑的曲线显示，默认为False
		/// </summary>
		public CurveStyle Style
		{
			get;
			set;
		}

		/// <summary>
		/// 曲线颜色
		/// </summary>
		public Color LineColor
		{
			get;
			set;
		}

		/// <summary>
		/// 是否左侧参考系，True为左侧，False为右侧
		/// </summary>
		public bool IsLeftFrame
		{
			get;
			set;
		}

		/// <summary>
		/// 本曲线是否显示出来，默认为显示
		/// </summary>
		public bool Visible
		{
			get;
			set;
		}

		/// <summary>
		/// 用于曲线自身选择是否显示的情况的判断
		/// </summary>
		public bool LineRenderVisiable
		{
			get;
			set;
		}

		/// <summary>
		/// 标题实现的基本区域
		/// </summary>
		public RectangleF TitleRegion
		{
			get;
			set;
		}

		/// <summary>
		/// 本曲线在图形上显示的格式化信息，对历史数据有效
		/// </summary>
		public string RenderFormat
		{
			get;
			set;
		} = "{0}";


		/// <summary>
		/// 实例化一个对象
		/// </summary>
		public HslCurveItem()
		{
			LineThickness = 1f;
			IsLeftFrame = true;
			Visible = true;
			LineRenderVisiable = true;
			TitleRegion = new RectangleF(0f, 0f, 0f, 0f);
			Style = CurveStyle.LineSegment;
		}
	}
}
