namespace HslControls
{
	/// <summary>
	/// 棋子的三种状态
	/// </summary>
	public enum GobangPlayer
	{
		/// <summary>
		/// 没有玩家
		/// </summary>
		NonePlayer = 1,
		/// <summary>
		/// 玩家一
		/// </summary>
		Player1,
		/// <summary>
		/// 玩家二
		/// </summary>
		Player2
	}
}
