using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslControls
{
	[Description("一个卡车控件，支持两种样式，支持车体升降操作")]
	public class HslTruck : UserControl
	{
		private HslPipeTurnDirection directionStyle = HslPipeTurnDirection.Right;

		private float widthDesign = 530f;

		private float heightDesign = 325f;

		private StringFormat sf = null;

		private float truckAngle = 0f;

		private int styleMode = 1;

		private Color themeColor = Color.FromArgb(235, 209, 159);

		private Color boderColor = Color.Silver;

		private Pen borderPen = new Pen(Color.FromArgb(150, 150, 150));

		private Color edgeColor = Color.FromArgb(176, 176, 176);

		private Color centerColor = Color.FromArgb(229, 229, 229);

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置控件的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Transparent")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// 获取或设置分类器控件的边缘颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置分类器控件的边缘颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "[176, 176, 176]")]
		public Color EdgeColor
		{
			get
			{
				return edgeColor;
			}
			set
			{
				edgeColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置当前的卡车的倾斜角度，合适范围为0-21
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前的卡车的倾斜角度，合适范围为0-21")]
		[Category("HslControls")]
		[DefaultValue(0f)]
		public float TruckAngle
		{
			get
			{
				return truckAngle;
			}
			set
			{
				truckAngle = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置分类器控件的中心颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置分类器控件的中心颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "[229, 229, 229]")]
		public Color CenterColor
		{
			get
			{
				return centerColor;
			}
			set
			{
				centerColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置分类器控件的边缘颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置分类器控件的边缘颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Silver")]
		public Color BorderColor
		{
			get
			{
				return boderColor;
			}
			set
			{
				boderColor = value;
				borderPen.Dispose();
				borderPen = new Pen(boderColor);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置卡车的主题颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置卡车的主题颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "[235, 209, 159]")]
		public Color ThemeColor
		{
			get
			{
				return themeColor;
			}
			set
			{
				themeColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置卡车的状态，有两个选择，1，2
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置卡车的状态，有两个选择，1，2")]
		[Category("HslControls")]
		[DefaultValue(1)]
		public int TruckStyle
		{
			get
			{
				return styleMode;
			}
			set
			{
				styleMode = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置卡车的朝向，仅支持朝右边或是左边
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置卡车的朝向，仅支持朝右边或是左边")]
		[Category("HslControls")]
		[DefaultValue(typeof(HslPipeTurnDirection), "Right")]
		public HslPipeTurnDirection TurnDirection
		{
			get
			{
				return directionStyle;
			}
			set
			{
				directionStyle = value;
				Invalidate();
			}
		}

		public HslTruck()
		{
			InitializeComponent();
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
		}

		/// <inheritdoc />
		protected override void OnPaint(PaintEventArgs e)
		{
			if (!Authorization.iashdiadasbdnajsdhjaf())
			{
				return;
			}
			Graphics g = e.Graphics;
			g.SmoothingMode = SmoothingMode.HighQuality;
			g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
			if (directionStyle == HslPipeTurnDirection.Left)
			{
				g.ScaleTransform(-1f, 1f);
				g.TranslateTransform(-base.Width, 0f);
			}
			if (base.Width > 15 && base.Height > 15)
			{
				if ((float)base.Width > widthDesign / heightDesign * (float)base.Height)
				{
					PaintMain(g, widthDesign / heightDesign * (float)base.Height, base.Height);
				}
				else
				{
					PaintMain(g, base.Width, heightDesign / widthDesign * (float)base.Width);
				}
			}
			if (directionStyle == HslPipeTurnDirection.Left)
			{
				g.TranslateTransform(base.Width, 0f);
				g.ScaleTransform(-1f, 1f);
			}
			base.OnPaint(e);
		}

		private RectangleF TransRectangleF(RectangleF rectangle)
		{
			return new RectangleF(rectangle.X * (float)base.Width / widthDesign, rectangle.Y * (float)base.Height / heightDesign, rectangle.Width * (float)base.Width / widthDesign, rectangle.Height * (float)base.Height / heightDesign);
		}

		private PointF[] TransPoints(PointF[] points)
		{
			PointF[] buffer = new PointF[points.Length];
			for (int i = 0; i < points.Length; i++)
			{
				buffer[i] = new PointF(points[i].X * (float)base.Width / widthDesign, points[i].Y * (float)base.Height / heightDesign);
			}
			return buffer;
		}

		private void PaintRectangle(Graphics g, RectangleF rectangle, ColorBlend colorBlend)
		{
			rectangle = TransRectangleF(rectangle);
			LinearGradientBrush b = new LinearGradientBrush(new PointF(rectangle.X, 0f), new PointF(rectangle.X + rectangle.Width, 0f), Color.FromArgb(142, 196, 216), Color.FromArgb(240, 240, 240));
			b.InterpolationColors = colorBlend;
			g.FillRectangle(b, rectangle);
			g.DrawRectangle(borderPen, rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
			b.Dispose();
		}

		private void PaintPolygon(Graphics g, string points, Color color)
		{
			PointF[] p = HslHelper.GetPointsFrom(points, widthDesign, heightDesign, base.Width, base.Height);
			using (SolidBrush brush = new SolidBrush(color))
			{
				g.FillPolygon(brush, p);
			}
			g.DrawPolygon(borderPen, p);
		}

		private void PaintPolygon(Graphics g, PointF[] points, ColorBlend colorBlend)
		{
			points = TransPoints(points);
			LinearGradientBrush b = new LinearGradientBrush(new PointF(points[0].X, points[0].Y), new PointF(points[1].X, points[0].Y), Color.FromArgb(142, 196, 216), Color.FromArgb(240, 240, 240));
			b.InterpolationColors = colorBlend;
			g.FillPolygon(b, points);
			g.DrawPolygon(borderPen, points);
			b.Dispose();
		}

		private void PaintMain(Graphics g, float width, float height)
		{
			ColorBlend colorBlend = new ColorBlend();
			colorBlend.Positions = new float[3]
			{
				0f,
				0.35f,
				1f
			};
			colorBlend.Colors = new Color[3]
			{
				edgeColor,
				centerColor,
				edgeColor
			};
			PaintPolygon(g, "244,274 410,274 410,283 244,283", themeColor);
			PaintPolygon(g, "410,125 485,125 529,188 529,283 410,283 410,125", themeColor);
			using (SolidBrush brush2 = new SolidBrush(Color.FromArgb(251, 253, 252)))
			{
				g.FillPolygon(brush2, HslHelper.GetPointsFrom("430,137 482,137 509,180 509,195 430,195 430,137", widthDesign, heightDesign, base.Width, base.Height));
			}
			using (SolidBrush brush3 = new SolidBrush(Color.FromArgb(232, 244, 237)))
			{
				g.FillPolygon(brush3, HslHelper.GetPointsFrom("430,137 482,137 454,195 430,195 430,137", widthDesign, heightDesign, base.Width, base.Height));
			}
			using (Pen pen2 = new Pen(Color.FromArgb(81, 81, 81), 3f))
			{
				g.DrawPolygon(pen2, HslHelper.GetPointsFrom("430,137 482,137 509,180 509,195 430,195 430,137", widthDesign, heightDesign, base.Width, base.Height));
			}
			using (SolidBrush brush = new SolidBrush(Color.FromArgb(81, 81, 81)))
			{
				g.FillEllipse(brush, TransRectangleF(new Rectangle(511, 200, 6, 6)));
			}
			using (Pen pen3 = new Pen(Color.FromArgb(212, 186, 137), 5f))
			{
				g.DrawLines(pen3, HslHelper.GetPointsFrom("413,209 527,209 527,280 413,280 413,209", widthDesign, heightDesign, base.Width, base.Height));
				g.DrawArc(pen3, TransRectangleF(new RectangleF(430f, 258f, 58f, 52f)), 180f, 180f);
				g.DrawPolygon(pen3, HslHelper.GetPointsFrom($"350,268 {Math.Cos((double)(truckAngle * 2f) * Math.PI / 360.0) * 298.0 + 52.0},{258.0 - Math.Sin((double)(truckAngle * 2f) * Math.PI / 360.0) * 298.0}", widthDesign, heightDesign, base.Width, base.Height));
			}
			PaintPolygon(g, "510,179 516,179 516,194 510,194 510,179", Color.FromArgb(81, 81, 81));
			PaintPolygon(g, "513,203 515,203 515,194 513,194 513,203", HslHelper.GetColorLight(Color.FromArgb(81, 81, 81)));
			PaintPolygon(g, "52,260 396,260 396,275 52,275 52,260", Color.FromArgb(81, 81, 81));
			PaintTair(g, 460f, 293f);
			PaintTair(g, 102f, 293f);
			PaintTair(g, 162f, 293f);
			g.TranslateTransform((float)(52 * base.Width) / widthDesign, (float)(259 * base.Height) / heightDesign);
			if (truckAngle != 0f)
			{
				g.RotateTransform(0f - truckAngle);
			}
			if (styleMode == 1)
			{
				PaintPolygon(g, "0,-139 348,-139 348,0 0,0 0,-139", Color.FromArgb(233, 233, 233));
				PaintPolygon(g, "8,-131 340,-131 340,-8 8,-8 8,-131", themeColor);
				float[] stripe = new float[15]
				{
					30f,
					40f,
					50f,
					95f,
					105f,
					115f,
					160f,
					170f,
					180f,
					225f,
					235f,
					245f,
					290f,
					300f,
					310f
				};
				for (int i = 0; i < stripe.Length; i++)
				{
					PaintPolygon(g, $"{stripe[i]},-131 {stripe[i] + 5f},-131 {stripe[i] + 5f},-8 {stripe[i]},-8 {stripe[i]},-131", HslHelper.GetColorLight(themeColor));
				}
			}
			else if (styleMode == 2)
			{
				RectangleF rect = TransRectangleF(new RectangleF(-10f, -130f, 20f, 121f));
				LinearGradientBrush b = new LinearGradientBrush(new PointF(rect.X, rect.Y), new PointF(rect.X, rect.Y + rect.Height), Color.FromArgb(142, 196, 216), Color.FromArgb(240, 240, 240));
				b.InterpolationColors = colorBlend;
				g.FillEllipse(b, rect);
				using (Pen pen = new Pen(edgeColor, 1f))
				{
					g.DrawEllipse(pen, rect);
				}
				g.FillPolygon(b, HslHelper.GetPointsFrom("348,-130 355,-120 355,-10 348,0 348,-130", widthDesign, heightDesign, base.Width, base.Height));
				g.DrawPolygon(borderPen, HslHelper.GetPointsFrom("348,-130 355,-120 355,-10 348,0 348,-130", widthDesign, heightDesign, base.Width, base.Height));
				b.Dispose();
				rect = TransRectangleF(new RectangleF(0f, -139f, 348f, 140f));
				b = new LinearGradientBrush(new PointF(rect.X, rect.Y), new PointF(rect.X, rect.Y + rect.Height), Color.FromArgb(142, 196, 216), Color.FromArgb(240, 240, 240));
				b.InterpolationColors = colorBlend;
				g.FillRectangle(b, rect);
				g.DrawRectangle(borderPen, rect.X, rect.Y, rect.Width, rect.Height);
				PaintPolygon(g, "0,-139 348,-139 348,-134 0,-134 0,-139", BorderColor);
				PaintPolygon(g, "0,-75 348,-75 348,-65 0,-65 0,-75", themeColor);
				PaintPolygon(g, "0,-50 348,-50 348,-40 0,-40 0,-50", themeColor);
			}
			if (truckAngle != 0f)
			{
				g.RotateTransform(truckAngle);
			}
			g.TranslateTransform((float)(-52 * base.Width) / widthDesign, (float)(-259 * base.Height) / heightDesign);
		}

		private void PaintTair(Graphics g, float x, float y)
		{
			SolidBrush brush = new SolidBrush(Color.FromArgb(55, 55, 55));
			RectangleF rectangleF = new RectangleF(x - 30f, y - 30f, 60f, 60f);
			g.FillEllipse(brush, TransRectangleF(rectangleF));
			rectangleF = new RectangleF(x - 20f, y - 20f, 40f, 40f);
			ColorBlend colorBlend = new ColorBlend();
			colorBlend.Positions = new float[3]
			{
				0f,
				0.45f,
				1f
			};
			colorBlend.Colors = new Color[3]
			{
				Color.FromArgb(150, 150, 150),
				Color.FromArgb(220, 220, 220),
				Color.FromArgb(150, 150, 150)
			};
			PointF[] points = HslHelper.GetPointsFrom($"{x + 15f},{y - 20f} {x - 15f},{y + 20f}", widthDesign, heightDesign, base.Width, base.Height);
			LinearGradientBrush b = new LinearGradientBrush(points[0], points[1], Color.FromArgb(142, 196, 216), Color.FromArgb(240, 240, 240));
			b.InterpolationColors = colorBlend;
			g.FillEllipse(b, TransRectangleF(rectangleF));
			b.Dispose();
			rectangleF = new RectangleF(x - 7f, y - 7f, 14f, 14f);
			using (Pen pen = new Pen(brush, 2f))
			{
				g.DrawEllipse(pen, TransRectangleF(rectangleF));
			}
			rectangleF = new RectangleF(x - 11f, y - 15f, 8f, 8f);
			g.FillEllipse(brush, TransRectangleF(rectangleF));
			rectangleF = new RectangleF(x - 16f, y + 1f, 8f, 8f);
			g.FillEllipse(brush, TransRectangleF(rectangleF));
			rectangleF = new RectangleF(x - 3f, y + 10f, 8f, 8f);
			g.FillEllipse(brush, TransRectangleF(rectangleF));
			rectangleF = new RectangleF(x + 8f, y + 2f, 8f, 8f);
			g.FillEllipse(brush, TransRectangleF(rectangleF));
			rectangleF = new RectangleF(x + 5f, y - 14f, 8f, 8f);
			g.FillEllipse(brush, TransRectangleF(rectangleF));
			brush.Dispose();
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			BackColor = System.Drawing.Color.Transparent;
			base.Name = "HslTruck";
			base.Size = new System.Drawing.Size(329, 185);
			ResumeLayout(false);
		}
	}
}
