using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslControls
{
	/// <summary>
	/// 一个播放样式的按钮，支持开始和结束的两种操作方法
	/// </summary>
	[DefaultEvent("OnPlayChanged")]
	[DefaultBindingProperty("Text")]
	[DefaultProperty("Text")]
	[Description("一个按钮控件，样式是播放停止")]
	public class HslPlay : UserControl
	{
		private Color textForeColor = Color.OrangeRed;

		private Brush textForeBrush = new SolidBrush(Color.OrangeRed);

		private bool played = false;

		private Color activeColor = Color.Green;

		private Brush activeBrush = new SolidBrush(Color.Green);

		private Color borderColor = Color.DodgerBlue;

		private StringFormat sf = null;

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置控件的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Transparent")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// 获取或设置当前控件的文本的颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前控件的文本的颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "OrangeRed")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color ForeColor
		{
			get
			{
				return textForeColor;
			}
			set
			{
				textForeColor = value;
				textForeBrush.Dispose();
				textForeBrush = new SolidBrush(value);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置当前控件的文本
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前控件的文本")]
		[Category("HslControls")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Bindable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 当点击了开始播放的时候的前景色
		/// </summary>
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Green")]
		[Description("当点击了开始播放的时候的前景色")]
		public Color ActiveColor
		{
			get
			{
				return activeColor;
			}
			set
			{
				activeColor = value;
				activeBrush.Dispose();
				activeBrush = new SolidBrush(activeColor);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置外层圈圈的颜色
		/// </summary>
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "DodgerBlue")]
		[Description("获取或设置外层圈圈的颜色")]
		public Color BorderColor
		{
			get
			{
				return borderColor;
			}
			set
			{
				borderColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置是否处于播放中的状态
		/// </summary>
		[Category("HslControls")]
		[DefaultValue(false)]
		[Description("获取或设置是否处于播放中的状态")]
		public bool Played
		{
			get
			{
				return played;
			}
			set
			{
				if (value != played)
				{
					played = value;
					Invalidate();
					this.OnPlayChanged?.Invoke(this, played);
				}
			}
		}

		/// <summary>
		/// 开关按钮发生变化的事件
		/// </summary>
		[Category("Action")]
		[Description("点击了按钮开发后触发")]
		public event Action<object, bool> OnPlayChanged;

		/// <summary>
		/// 实例化一个对象
		/// </summary>
		public HslPlay()
		{
			InitializeComponent();
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
		}

		/// <summary>
		/// 当鼠标点击的事件触发的时候
		/// </summary>
		/// <param name="e">点击事件</param>
		protected override void OnMouseClick(MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				Played = !Played;
			}
			base.OnMouseClick(e);
		}

		/// <summary>
		/// 控件的重绘界面
		/// </summary>
		/// <param name="e">重绘的消息事件</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			if (!Authorization.iashdiadasbdnajsdhjaf())
			{
				return;
			}
			e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
			e.Graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
			if (base.Width >= 3 && base.Height >= 3)
			{
				int circle_width = Math.Min(base.Width, base.Height);
				int lineWidth = (circle_width / 15 == 0) ? 1 : (circle_width / 15);
				using (Pen pen = new Pen(borderColor, lineWidth))
				{
					e.Graphics.DrawEllipse(pen, lineWidth, lineWidth, circle_width - 2 * lineWidth, circle_width - 2 * lineWidth);
				}
				if (played)
				{
					e.Graphics.FillRectangle(activeBrush, circle_width * 32 / 100, circle_width * 27 / 100, circle_width * 12 / 100, circle_width * 46 / 100);
					e.Graphics.FillRectangle(activeBrush, circle_width * 56 / 100, circle_width * 27 / 100, circle_width * 12 / 100, circle_width * 46 / 100);
				}
				else
				{
					Point[] points = new Point[4]
					{
						new Point(circle_width * 32 / 100, circle_width * 27 / 100),
						new Point(circle_width * 32 / 100, circle_width * 73 / 100),
						new Point(circle_width * 78 / 100, circle_width * 50 / 100),
						new Point(circle_width * 32 / 100, circle_width * 27 / 100)
					};
					e.Graphics.FillPolygon(textForeBrush, points);
				}
				if (base.Height - circle_width > 0 && !string.IsNullOrEmpty(Text))
				{
					e.Graphics.DrawString(Text, Font, textForeBrush, new Rectangle(0, circle_width, circle_width, base.Height - circle_width), sf);
				}
				base.OnPaint(e);
			}
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			BackColor = System.Drawing.Color.Transparent;
			Cursor = System.Windows.Forms.Cursors.Hand;
			base.Name = "HslPlay";
			base.Size = new System.Drawing.Size(97, 139);
			ResumeLayout(false);
		}
	}
}
