using System.Drawing;

namespace HslControls
{
	/// <summary>
	/// 饼图的基本元素
	/// </summary>
	public class HslPieItem
	{
		/// <summary>
		/// 名称
		/// </summary>
		public string Name
		{
			get;
			set;
		}

		/// <summary>
		/// 值
		/// </summary>
		public int Value
		{
			get;
			set;
		}

		/// <summary>
		/// 饼图背景的颜色信息
		/// </summary>
		public Color PieColor
		{
			get;
			set;
		}

		/// <summary>
		/// 文字的颜色，如果没有指定，就选择和 <seealso cref="P:HslControls.HslPieItem.PieColor" /> 一样。
		/// </summary>
		public Color FontColor
		{
			get;
			set;
		}

		/// <summary>
		/// 线条的颜色信息
		/// </summary>
		public Color LineColor
		{
			get;
			set;
		}

		/// <summary>
		/// 实例化一个饼图基本元素的对象
		/// </summary>
		public HslPieItem()
		{
			PieColor = Color.DodgerBlue;
			LineColor = Color.DimGray;
			FontColor = Color.DimGray;
		}
	}
}
