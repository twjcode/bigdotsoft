using System.ComponentModel;
using System.Drawing;

namespace HslControls
{
	/// <summary>
	/// 刻度标记对象
	/// </summary>
	public class MarkValue
	{
		/// <summary>
		/// 当前的数值信息
		/// </summary>
		[Description("获取或设置当前的值数据")]
		public float Value
		{
			get;
			set;
		}

		/// <summary>
		/// 当前的颜色信息
		/// </summary>
		[Description("获取或设置当前的颜色信息")]
		public Color Color
		{
			get;
			set;
		}

		/// <summary>
		/// 当前线的宽度信息
		/// </summary>
		[Description("获取或设置当前的刻度宽度信息")]
		public float LineWidth
		{
			get;
			set;
		}

		/// <summary>
		/// 实例化一个默认的对象
		/// </summary>
		public MarkValue()
		{
			Value = 0f;
			Color = Color.DodgerBlue;
			LineWidth = 1f;
		}
	}
}
