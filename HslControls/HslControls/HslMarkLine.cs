using System.Drawing;

namespace HslControls
{
	/// <summary>
	/// 曲线图里的直线标记信息
	/// </summary>
	public class HslMarkLine
	{
		/// <summary>
		/// 文本的颜色
		/// </summary>
		public Brush TextBrush
		{
			get;
			set;
		}

		/// <summary>
		/// 圆圈的画刷
		/// </summary>
		public Brush CircleBrush
		{
			get;
			set;
		}

		/// <summary>
		/// 线段颜色的标记
		/// </summary>
		public Pen LinePen
		{
			get;
			set;
		}

		/// <summary>
		/// 所有的点的集合
		/// </summary>
		public PointF[] Points
		{
			get;
			set;
		}

		/// <summary>
		/// 所有的标记点的集合
		/// </summary>
		public string[] Marks
		{
			get;
			set;
		}

		/// <summary>
		/// 线段是否闭合
		/// </summary>
		public bool IsLineClosed
		{
			get;
			set;
		}

		/// <summary>
		/// 是否参照左侧的坐标轴
		/// </summary>
		public bool IsLeftFrame
		{
			get;
			set;
		}

		/// <summary>
		/// 实例化一个默认的对象，默认参考左坐标轴，颜色为亮蓝色
		/// </summary>
		public HslMarkLine()
		{
			TextBrush = Brushes.DodgerBlue;
			CircleBrush = Brushes.DodgerBlue;
			LinePen = Pens.DodgerBlue;
			IsLeftFrame = true;
		}
	}
}
