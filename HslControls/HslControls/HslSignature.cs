using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslControls
{
	/// <summary>
	/// 一个用于签名的控件，方便实现用户的签名，保存
	/// </summary>
	[Description("一个用于签名的控件，方便实现用户签名，并对签名内容进行保存")]
	public class HslSignature : UserControl
	{
		private Color boderColor = Color.FromArgb(205, 205, 205);

		private Pen penBorder = new Pen(Color.FromArgb(205, 205, 205));

		private StringFormat sf = null;

		private HslDirectionStyle hslValvesStyle = HslDirectionStyle.Horizontal;

		private List<List<PointF>> totalPoints = new List<List<PointF>>();

		private List<PointF> currPoints = new List<PointF>();

		private bool isEnableSign = true;

		private bool isMouseDown = false;

		private float signWidth = 1f;

		private Color signColor = Color.Black;

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置控件的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置控件的背景色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "White")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		/// 获取或设置当前控件的文本
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前控件的文本")]
		[Category("HslControls")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Bindable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置当前控件的是否允许签名，默认是允许签名的
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前控件的是否允许签名，默认是允许签名的")]
		[Category("HslControls")]
		[DefaultValue(true)]
		public bool EnableSign
		{
			get
			{
				return isEnableSign;
			}
			set
			{
				isEnableSign = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置当前签名控件的宽度信息
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前签名控件的宽度信息")]
		[Category("HslControls")]
		[DefaultValue(1f)]
		public float SignWidth
		{
			get
			{
				return signWidth;
			}
			set
			{
				signWidth = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置当前签名的颜色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置当前签名的颜色")]
		[Category("HslControls")]
		[DefaultValue(typeof(Color), "Black")]
		public Color SignColor
		{
			get
			{
				return signColor;
			}
			set
			{
				signColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 实例化一个默认的对象
		/// </summary>
		public HslSignature()
		{
			InitializeComponent();
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, value: true);
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
		}

		private void HslSignature_Load(object sender, EventArgs e)
		{
			base.MouseDown += HslSignature_MouseDown;
			base.MouseUp += HslSignature_MouseUp;
			base.MouseMove += HslSignature_MouseMove;
		}

		private void HslSignature_MouseMove(object sender, MouseEventArgs e)
		{
			if (isMouseDown && isEnableSign)
			{
				currPoints.Add(new PointF(e.X, e.Y));
				Invalidate();
			}
		}

		private void HslSignature_MouseUp(object sender, MouseEventArgs e)
		{
			if (isEnableSign && e.Button == MouseButtons.Left)
			{
				isMouseDown = false;
				currPoints.Add(new PointF(e.X, e.Y));
				totalPoints.Add(currPoints);
				currPoints = new List<PointF>();
				Invalidate();
			}
		}

		private void HslSignature_MouseDown(object sender, MouseEventArgs e)
		{
			if (isEnableSign)
			{
				if (e.Button == MouseButtons.Left)
				{
					isMouseDown = true;
					currPoints.Add(new PointF(e.X, e.Y));
				}
				else if (e.Button == MouseButtons.Right)
				{
					ClearSign();
				}
			}
		}

		/// <summary>
		/// 清除当前所有的签名内容
		/// </summary>
		public void ClearSign()
		{
			currPoints.Clear();
			totalPoints.Clear();
			Invalidate();
		}

		/// <summary>
		/// 获取签名的图像
		/// </summary>
		/// <returns>图片信息</returns>
		public Image SaveBitmap()
		{
			Bitmap image = new Bitmap(base.Width, base.Height);
			Graphics g = Graphics.FromImage(image);
			g.SmoothingMode = SmoothingMode.HighQuality;
			g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
			g.Clear(BackColor);
			PaintMain(g, image.Width, image.Height);
			g.Dispose();
			return image;
		}

		/// <summary>
		/// 获取签名的图像信息，并存储到文件
		/// </summary>
		/// <returns>图片信息</returns>
		public void SaveBitmap(string fileName)
		{
			SaveBitmap().Save(fileName);
		}

		/// <summary>
		/// 重绘控件的
		/// </summary>
		/// <param name="e">重绘事件</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			if (Authorization.iashdiadasbdnajsdhjaf())
			{
				Graphics g = e.Graphics;
				g.SmoothingMode = SmoothingMode.HighQuality;
				g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
				if (hslValvesStyle == HslDirectionStyle.Horizontal)
				{
					PaintMain(g, base.Width, base.Height);
				}
				else
				{
					g.TranslateTransform(base.Width, 0f);
					g.RotateTransform(90f);
					PaintMain(g, base.Height, base.Width);
					g.ResetTransform();
				}
				base.OnPaint(e);
			}
		}

		private void PaintMain(Graphics g, float width, float height)
		{
			using Pen pen = new Pen(signColor, signWidth);
			foreach (List<PointF> item in totalPoints)
			{
				PointF[] datas = item.ToArray();
				if (datas.Length > 1)
				{
					g.DrawCurve(pen, datas);
				}
			}
			PointF[] currDatas = currPoints.ToArray();
			if (currDatas.Length > 1)
			{
				g.DrawCurve(pen, currDatas);
			}
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 12f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			BackColor = System.Drawing.Color.White;
			base.Name = "HslSignature";
			base.Size = new System.Drawing.Size(487, 177);
			base.Load += new System.EventHandler(HslSignature_Load);
			ResumeLayout(false);
		}
	}
}
