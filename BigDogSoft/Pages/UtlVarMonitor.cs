﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HZH_Controls.Controls;
using Infrastructure.Models;
using Infrastructure.DAL;
using Infrastructure;

namespace BigDogSoft.Pages
{
    public partial class UtlVarMonitor : UCControlBase
    {
        BindingList<DeviceVar> bindList = new BindingList<DeviceVar>();

        public UtlVarMonitor()
        {
            InitializeComponent();
            InitData();
        }

        private void InitData()
        {
            List<Device> list = DeviceVarDAL.GetDeviceList();
            List<KeyValuePair<string, string>> cmbSource = new List<KeyValuePair<string, string>>();

            foreach(var item in list)
            {
                cmbSource.Add(new KeyValuePair<string, string>(item.id.ToString(),item.device_name));
            }

            this.cmbDevices.Source = cmbSource;

      
        }

        private void UtlVarMonitor_Load(object sender, EventArgs e)
        {
            //添加列
            List<DataGridViewColumnEntity> lstCulumns = new List<DataGridViewColumnEntity>();
            lstCulumns.Add(new DataGridViewColumnEntity() { DataField = "device_name", HeadText = "设备", Width = 70, WidthType = SizeType.Absolute });
            lstCulumns.Add(new DataGridViewColumnEntity() { DataField = "var_name", HeadText = "变量名称", Width = 200, WidthType = SizeType.Absolute });
            lstCulumns.Add(new DataGridViewColumnEntity() { DataField = "var_address", HeadText = "寄存器地址", Width = 150, WidthType = SizeType.Absolute });
            lstCulumns.Add(new DataGridViewColumnEntity() { DataField = "var_val", HeadText = "值", Width = 100, WidthType = SizeType.Absolute});

            this.ucDataGridView1.Columns = lstCulumns;
            //订阅变量监控情况
            EventBus.SubScribe("monitor",doUpdateData);

        }

        private void doUpdateData(object o)
        {
                var dv = o as DeviceVar;
                if (dv != null)
                {
                    foreach (var item in bindList)
                    {
                        if (item.id == dv.id)
                        {
                            item.var_val = dv.var_val;

                        }
                    }
                    //将任务发送到UI主线程
                    FrmMain.WindowUIContext.Post(state => {
                        this.ucDataGridView1.DataSource = bindList;
                    }, null);
                }
            
        }

        /// <summary>
        /// 查询方法
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_BtnClick(object sender, EventArgs e)
        {
            string deviceId = this.cmbDevices.SelectedValue;
            if(string.IsNullOrEmpty(deviceId))
            {
                MessageBox.Show("请选择设备！");
                return;
            }
            string varName = this.txtVarName.Text.Trim();
            
            List<DeviceVar> varList = DeviceVarDAL.GetVarListIncludeDeviceInfoBy(int.Parse(deviceId),varName);
            bindList.Clear();
            foreach (var item in varList)
            {
                bindList.Add(item);
            }
            this.ucDataGridView1.DataSource = bindList;
        }

        private void UtlVarMonitor_VisibleChanged(object sender, EventArgs e)
        {
            //bool isShow = this.Visible;
        }
    }
}
