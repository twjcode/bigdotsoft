﻿namespace BigDogSoft.Pages
{
    partial class UtlVarMonitor
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSearch = new HZH_Controls.Controls.UCBtnExt();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbDevices = new HZH_Controls.Controls.UCCombox();
            this.txtVarName = new HZH_Controls.Controls.TextBoxEx();
            this.ucDataGridView1 = new HZH_Controls.Controls.UCDataGridView();
            this.SuspendLayout();
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.White;
            this.btnSearch.BtnBackColor = System.Drawing.Color.White;
            this.btnSearch.BtnFont = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSearch.BtnForeColor = System.Drawing.Color.White;
            this.btnSearch.BtnText = "查询";
            this.btnSearch.ConerRadius = 5;
            this.btnSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSearch.EnabledMouseEffect = false;
            this.btnSearch.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(77)))), ((int)(((byte)(59)))));
            this.btnSearch.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnSearch.IsRadius = true;
            this.btnSearch.IsShowRect = true;
            this.btnSearch.IsShowTips = false;
            this.btnSearch.Location = new System.Drawing.Point(661, 20);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(0);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(77)))), ((int)(((byte)(58)))));
            this.btnSearch.RectWidth = 1;
            this.btnSearch.Size = new System.Drawing.Size(85, 38);
            this.btnSearch.TabIndex = 0;
            this.btnSearch.TabStop = false;
            this.btnSearch.TipsColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(30)))), ((int)(((byte)(99)))));
            this.btnSearch.TipsText = "";
            this.btnSearch.BtnClick += new System.EventHandler(this.btnSearch_BtnClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(22, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = "设备";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(277, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "变量名称";
            // 
            // cmbDevices
            // 
            this.cmbDevices.BackColor = System.Drawing.Color.Transparent;
            this.cmbDevices.BackColorExt = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.cmbDevices.BoxStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.cmbDevices.ConerRadius = 5;
            this.cmbDevices.DropPanelHeight = -1;
            this.cmbDevices.FillColor = System.Drawing.Color.White;
            this.cmbDevices.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbDevices.IsRadius = true;
            this.cmbDevices.IsShowRect = true;
            this.cmbDevices.ItemWidth = 70;
            this.cmbDevices.Location = new System.Drawing.Point(92, 20);
            this.cmbDevices.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbDevices.Name = "cmbDevices";
            this.cmbDevices.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.cmbDevices.RectWidth = 1;
            this.cmbDevices.SelectedIndex = -1;
            this.cmbDevices.SelectedValue = "";
            this.cmbDevices.Size = new System.Drawing.Size(162, 35);
            this.cmbDevices.Source = null;
            this.cmbDevices.TabIndex = 2;
            this.cmbDevices.TextValue = null;
            this.cmbDevices.TriangleColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(77)))), ((int)(((byte)(59)))));
            // 
            // txtVarName
            // 
            this.txtVarName.DecLength = 2;
            this.txtVarName.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtVarName.InputType = HZH_Controls.TextInputType.NotControl;
            this.txtVarName.Location = new System.Drawing.Point(376, 24);
            this.txtVarName.MaxValue = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.txtVarName.MinValue = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.txtVarName.MyRectangle = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.txtVarName.Name = "txtVarName";
            this.txtVarName.OldText = null;
            this.txtVarName.PromptColor = System.Drawing.Color.Gray;
            this.txtVarName.PromptFont = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtVarName.PromptText = "";
            this.txtVarName.RegexPattern = "";
            this.txtVarName.Size = new System.Drawing.Size(240, 31);
            this.txtVarName.TabIndex = 3;
            // 
            // ucDataGridView1
            // 
            this.ucDataGridView1.BackColor = System.Drawing.Color.White;
            this.ucDataGridView1.Columns = null;
            this.ucDataGridView1.DataSource = null;
            this.ucDataGridView1.HeadFont = new System.Drawing.Font("微软雅黑", 12F);
            this.ucDataGridView1.HeadHeight = 40;
            this.ucDataGridView1.HeadPadingLeft = 0;
            this.ucDataGridView1.HeadTextColor = System.Drawing.Color.Black;
            this.ucDataGridView1.IsShowCheckBox = false;
            this.ucDataGridView1.IsShowHead = true;
            this.ucDataGridView1.Location = new System.Drawing.Point(0, 90);
            this.ucDataGridView1.Name = "ucDataGridView1";
            this.ucDataGridView1.Padding = new System.Windows.Forms.Padding(0, 40, 0, 0);
            this.ucDataGridView1.RowHeight = 40;
            this.ucDataGridView1.RowType = typeof(HZH_Controls.Controls.UCDataGridViewRow);
            this.ucDataGridView1.Size = new System.Drawing.Size(857, 539);
            this.ucDataGridView1.TabIndex = 4;
            // 
            // UtlVarMonitor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.ucDataGridView1);
            this.Controls.Add(this.txtVarName);
            this.Controls.Add(this.cmbDevices);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSearch);
            this.Name = "UtlVarMonitor";
            this.Size = new System.Drawing.Size(864, 655);
            this.Load += new System.EventHandler(this.UtlVarMonitor_Load);
            this.VisibleChanged += new System.EventHandler(this.UtlVarMonitor_VisibleChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private HZH_Controls.Controls.UCBtnExt btnSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private HZH_Controls.Controls.UCCombox cmbDevices;
        private HZH_Controls.Controls.TextBoxEx txtVarName;
        private HZH_Controls.Controls.UCDataGridView ucDataGridView1;
    }
}
