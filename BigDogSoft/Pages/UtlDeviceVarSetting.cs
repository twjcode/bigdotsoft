﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HZH_Controls.Controls;
using BigDogSoft.Pages.forms;
using Infrastructure.Models;
using Infrastructure.DAL;

namespace BigDogSoft.Pages
{
    public partial class UtlDeviceVarSetting : UCControlBase
    {
        

        public UtlDeviceVarSetting()
        {
            this.DoubleBuffered = true;
            InitializeComponent();

            this.ucListView1.SizeChanged += ucListView1_SizeChanged;
        }

        private void ucListView1_SizeChanged(object sender, EventArgs e)
        {
            if (this.ucListView1.Page != null)
            {
                this.ucListView1.DataSource = this.ucListView1.Page.GetCurrentSource();
            }
        }

        private void UtlDeviceVarSetting_Load(object sender, EventArgs e)
        {
            InitData();
        }

        private void InitData()
        {
            List<Device> list = DeviceVarDAL.GetDeviceList();


            #region 使用分页控件    English:Using Paging Control
            var page = new UCPagerControl2();
            //this.ucListView1.Page = page;
            //page.DataSource = list;
            #endregion

            #region 不使用分页控件    English:Do not use paging controls
            this.ucListView1.DataSource = list;
            #endregion
        }

        private void ucBtnExt1_BtnClick(object sender, EventArgs e)
        {
            FrmDeviceAdd frm = new FrmDeviceAdd();
            frm.ShowDialog();
            InitData();
        }
        /// <summary>
        /// 删除方法
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ucBtnExt3_BtnClick(object sender, EventArgs e)
        {
            List<object> list= this.ucListView1.SelectedSource;
            if (list.Count > 0)
            {
              if(DialogResult.Yes==  MessageBox.Show("您确认要删除吗？", "警告", MessageBoxButtons.YesNo)){
                    Device obj = list[0] as Device;
                    int rows= DeviceVarDAL.RemoveDeviceById(obj.id);
                    if (rows > 0)
                    {
                        InitData();
                    }
                }
            }
            else
            {
                MessageBox.Show("请选择要删除的设备！");
            }
        }
        /// <summary>
        /// 编辑方法
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ucBtnExt2_BtnClick(object sender, EventArgs e)
        {
            List<object> list = this.ucListView1.SelectedSource;
            if (list.Count > 0)
            {
               
                Device obj = list[0] as Device;
                FrmDeviceAdd frm = new FrmDeviceAdd(obj);
                frm.ShowDialog();

            }
            else
            {
                MessageBox.Show("请选择要编辑的设备！");
            }
          
        }
    }
}
