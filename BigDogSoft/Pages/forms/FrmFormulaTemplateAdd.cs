﻿using Infrastructure.DAL;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BigDogSoft.Pages.forms
{
    public partial class FrmFormulaTemplateAdd : Form
    {
        private BindingList<FormulaTplParam> bindingList = new BindingList<FormulaTplParam>();
        private FormulaTpl editObj = null;
        public FrmFormulaTemplateAdd()
        {
            InitializeComponent();
            InitData();
        }

        public FrmFormulaTemplateAdd(FormulaTpl obj):this()
        {
            //初始化需要编辑的参数
            editObj = obj;
            this.cmbDevices.SelectedValue = obj.device_id;
            this.cmbDevices.Enabled = false;
            this.txtTplName.Text = obj.template_name;
            List<FormulaTplParam> paramList = FormulaDAL.GetFormulaTplParamsBy(obj.id);
            bindingList.Clear();
            paramList.ForEach(p => { bindingList.Add(p); });
            this.dataGridView1.Refresh();
        }
        /// <summary>
        /// 初始化
        /// </summary>
        private void InitData()
        {
            var devices= DeviceVarDAL.GetDeviceList();
            this.cmbDevices.DataSource = devices;
            this.cmbDevices.DisplayMember = "device_name";
            this.cmbDevices.ValueMember = "id";
            this.dataGridView1.AutoGenerateColumns = false;
            string[] varTypes = Enum.GetNames(typeof(VarType));
            DataGridViewColumn col = this.dataGridView1.Columns["ColParamType"];
            DataGridViewComboBoxColumn cmbCol = col as DataGridViewComboBoxColumn;
            if (cmbCol != null)
            {
                cmbCol.DataSource = varTypes;
            }
            this.dataGridView1.DataSource = bindingList;

        }

        private void btnCancel_BtnClick(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_BtnClick(object sender, EventArgs e)
        {
            Device device = this.cmbDevices.SelectedItem as Device;
            if(device==null)
            {
                MessageBox.Show("请选择设备！");
                return;
            }
            string tplName = this.txtTplName.Text.Trim();
            if (string.IsNullOrEmpty(tplName))
            {
                MessageBox.Show("请输入配方模板名称！");
                return;
            }

            FormulaTpl tpl =editObj==null? new FormulaTpl():editObj;
            tpl.device_id = device.id;
            tpl.template_name = tplName;
            tpl.create_time = DateTime.Now;
            tpl.update_time = DateTime.Now;
            tpl.update_user = SessionContext.CurrentUser.account;
            tpl.create_user = SessionContext.CurrentUser.account;
            tpl.ParamList = new List<FormulaTplParam>();
            bindingList.ToList().ForEach(p =>
            {
                if (!string.IsNullOrEmpty(p.param_name))
                {
                    p.tpl_id = device.id;
                    p.create_time = device.create_time;
                    p.create_user = device.create_user;
                    p.update_time = device.update_time;
                    p.update_user = device.update_user;
                    tpl.ParamList.Add(p);
                }
            });
            int rows= FormulaDAL.AddOrUpdate(tpl);
            if (rows > 0)
            {
                this.Close();
            }
            else
            {
                MessageBox.Show("保存失败！");
            }
        }
    }
}
