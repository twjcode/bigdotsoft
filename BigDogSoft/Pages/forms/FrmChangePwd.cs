﻿using Infrastructure.DAL;
using Infrastructure.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BigDogSoft.Pages.forms
{
    public partial class FrmChangePwd : Form
    {
        private UserDAL dal = new UserDAL();
        public FrmChangePwd()
        {
            InitializeComponent();
        }

        private void ucBtnExt1_BtnClick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ucBtnExt2_BtnClick(object sender, EventArgs e)
        {
            //保存
            string oldPwd = this.txtOldPwd.Text.Trim();
            string newPwd = this.txtNewPwd.Text.Trim();
            string confirmPwd = this.txtConfirmPwd.Text.Trim();
            string error = string.Empty;
            //校验输入的参数合不合法
            if (string.IsNullOrEmpty(oldPwd))
            {
                error = "请输入旧密码！";
            }
            else if (string.IsNullOrEmpty(newPwd))
            {
                error = "请输入新密码！";
            }
            else if (!newPwd.Equals(confirmPwd))
            {
                error = "确认密码与新密码输入不一致！";
            }
            if (error.Length > 0)
            {
                MessageBox.Show(error);
                return;
            }
            //业务
            if (SessionContext.CurrentUser
                .password.Equals(MD5Helper.GetMD5(oldPwd)))
            {
                SessionContext.CurrentUser.password = MD5Helper.GetMD5(newPwd);
                //修改
                int row=  dal.UpdatePwd(SessionContext.CurrentUser);
                if (row > 0)
                {
                    MessageBox.Show("修改成功！");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("修改失败！");
                }
            }
            else
            {
                MessageBox.Show("旧密码输入错误！");
            }
        }
    }
}
