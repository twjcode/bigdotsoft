﻿namespace BigDogSoft.Pages
{
    partial class CtrlUserManager
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.ucBtnExt7 = new HZH_Controls.Controls.UCBtnExt();
            this.ucBtnExt2 = new HZH_Controls.Controls.UCBtnExt();
            this.ucDataGridView1 = new HZH_Controls.Controls.UCDataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ucBtnExt7
            // 
            this.ucBtnExt7.BackColor = System.Drawing.Color.Transparent;
            this.ucBtnExt7.BtnBackColor = System.Drawing.Color.Transparent;
            this.ucBtnExt7.BtnFont = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ucBtnExt7.BtnForeColor = System.Drawing.Color.White;
            this.ucBtnExt7.BtnText = "新增";
            this.ucBtnExt7.ConerRadius = 10;
            this.ucBtnExt7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ucBtnExt7.EnabledMouseEffect = true;
            this.ucBtnExt7.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(159)))), ((int)(((byte)(255)))));
            this.ucBtnExt7.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.ucBtnExt7.ForeColor = System.Drawing.Color.White;
            this.ucBtnExt7.IsRadius = true;
            this.ucBtnExt7.IsShowRect = false;
            this.ucBtnExt7.IsShowTips = false;
            this.ucBtnExt7.Location = new System.Drawing.Point(18, 16);
            this.ucBtnExt7.Margin = new System.Windows.Forms.Padding(0);
            this.ucBtnExt7.Name = "ucBtnExt7";
            this.ucBtnExt7.RectColor = System.Drawing.Color.Gainsboro;
            this.ucBtnExt7.RectWidth = 1;
            this.ucBtnExt7.Size = new System.Drawing.Size(97, 34);
            this.ucBtnExt7.TabIndex = 16;
            this.ucBtnExt7.TabStop = false;
            this.ucBtnExt7.TipsColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(30)))), ((int)(((byte)(99)))));
            this.ucBtnExt7.TipsText = "";
            this.ucBtnExt7.BtnClick += new System.EventHandler(this.ucBtnExt7_BtnClick);
            // 
            // ucBtnExt2
            // 
            this.ucBtnExt2.BackColor = System.Drawing.Color.Transparent;
            this.ucBtnExt2.BtnBackColor = System.Drawing.Color.Transparent;
            this.ucBtnExt2.BtnFont = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ucBtnExt2.BtnForeColor = System.Drawing.Color.White;
            this.ucBtnExt2.BtnText = "删除";
            this.ucBtnExt2.ConerRadius = 10;
            this.ucBtnExt2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ucBtnExt2.EnabledMouseEffect = true;
            this.ucBtnExt2.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(87)))), ((int)(((byte)(34)))));
            this.ucBtnExt2.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.ucBtnExt2.ForeColor = System.Drawing.Color.White;
            this.ucBtnExt2.IsRadius = true;
            this.ucBtnExt2.IsShowRect = false;
            this.ucBtnExt2.IsShowTips = false;
            this.ucBtnExt2.Location = new System.Drawing.Point(139, 16);
            this.ucBtnExt2.Margin = new System.Windows.Forms.Padding(0);
            this.ucBtnExt2.Name = "ucBtnExt2";
            this.ucBtnExt2.RectColor = System.Drawing.Color.Gainsboro;
            this.ucBtnExt2.RectWidth = 1;
            this.ucBtnExt2.Size = new System.Drawing.Size(97, 34);
            this.ucBtnExt2.TabIndex = 16;
            this.ucBtnExt2.TabStop = false;
            this.ucBtnExt2.TipsColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(30)))), ((int)(((byte)(99)))));
            this.ucBtnExt2.TipsText = "";
            this.ucBtnExt2.BtnClick += new System.EventHandler(this.ucBtnExt2_BtnClick);
            // 
            // ucDataGridView1
            // 
            this.ucDataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.ucDataGridView1.BackColor = System.Drawing.Color.White;
            this.ucDataGridView1.Columns = null;
            this.ucDataGridView1.DataSource = null;
            this.ucDataGridView1.HeadFont = new System.Drawing.Font("微软雅黑", 12F);
            this.ucDataGridView1.HeadHeight = 40;
            this.ucDataGridView1.HeadPadingLeft = 0;
            this.ucDataGridView1.HeadTextColor = System.Drawing.Color.Black;
            this.ucDataGridView1.IsShowCheckBox = false;
            this.ucDataGridView1.IsShowHead = true;
            this.ucDataGridView1.Location = new System.Drawing.Point(26, 80);
            this.ucDataGridView1.Name = "ucDataGridView1";
            this.ucDataGridView1.Padding = new System.Windows.Forms.Padding(0, 40, 0, 0);
            this.ucDataGridView1.RowHeight = 40;
            this.ucDataGridView1.RowType = typeof(HZH_Controls.Controls.UCDataGridViewRow);
            this.ucDataGridView1.Size = new System.Drawing.Size(763, 520);
            this.ucDataGridView1.TabIndex = 17;
            this.ucDataGridView1.ItemClick += new HZH_Controls.Controls.DataGridViewEventHandler(this.ucDataGridView1_ItemClick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ucBtnExt7);
            this.panel1.Controls.Add(this.ucBtnExt2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(813, 74);
            this.panel1.TabIndex = 18;
            // 
            // CtrlUserManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.ucDataGridView1);
            this.Name = "CtrlUserManager";
            this.Size = new System.Drawing.Size(813, 609);
            this.Load += new System.EventHandler(this.CtrlUserManager_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private HZH_Controls.Controls.UCBtnExt ucBtnExt7;
        private HZH_Controls.Controls.UCBtnExt ucBtnExt2;
        private HZH_Controls.Controls.UCDataGridView ucDataGridView1;
        private System.Windows.Forms.Panel panel1;
    }
}
