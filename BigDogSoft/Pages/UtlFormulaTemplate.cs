﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HZH_Controls.Controls;
using BigDogSoft.Pages.forms;
using Infrastructure.DAL;
using Infrastructure.Models;

namespace BigDogSoft.Pages
{
    public partial class UtlFormulaTemplate : UCControlBase
    {
        private int currentPage = 1;
        private int pageSize = 20;
        public UtlFormulaTemplate()
        {
            InitializeComponent();
           
        }

        private void InitData()
        {
            string tplName = this.txtTplName.Text.Trim();
            int total = 0;
            List<FormulaTpl> tpls=  FormulaDAL.GetFormulaTpls(tplName,out total, currentPage, pageSize);
            this.ucDataGridView1.DataSource = tpls;
            this.ucPagerControl22.PageCount = total%pageSize==0?total/pageSize:total/pageSize+1;
            this.ucPagerControl22.PageIndex = currentPage;
        }

        private void UtlFormulaTemplate_Load(object sender, EventArgs e)
        {
            List<DataGridViewColumnEntity> lstCulumns = new List<DataGridViewColumnEntity>();
            lstCulumns.Add(new DataGridViewColumnEntity() { DataField = "template_name", HeadText = "配方名称", Width = 150, WidthType = SizeType.Absolute });
            lstCulumns.Add(new DataGridViewColumnEntity() { DataField = "device_name", HeadText = "设备", Width = 200, WidthType = SizeType.Absolute });
            lstCulumns.Add(new DataGridViewColumnEntity() { DataField = "create_time", HeadText = "创建时间", Width = 300, WidthType = SizeType.Absolute, Format = (a) => { return ((DateTime)a).ToString("yyyy-MM-dd"); } });
            lstCulumns.Add(new DataGridViewColumnEntity() { DataField = "create_user", HeadText = "创建人", Width = 100, WidthType = SizeType.Absolute });
            this.ucDataGridView1.Columns = lstCulumns;
            InitData();
        }

        private void ucBtnExt1_BtnClick(object sender, EventArgs e)
        {
            FrmFormulaTemplateAdd frm = new FrmFormulaTemplateAdd();
            frm.ShowDialog();
            InitData();
        }
        /// <summary>
        /// 查询方法
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ucBtnExt4_BtnClick(object sender, EventArgs e)
        {
            InitData();
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ucBtnExt2_BtnClick(object sender, EventArgs e)
        {
            var row =
               this.ucDataGridView1.SelectRow.DataSource as FormulaTpl;

            if (row != null)
            {
                FrmFormulaTemplateAdd frm = new FrmFormulaTemplateAdd(row);
                frm.ShowDialog();
                InitData();
            }
            else
            {
                MessageBox.Show("请选择要修改的数据！");
            }
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ucBtnExt3_BtnClick(object sender, EventArgs e)
        {
            var row =
                this.ucDataGridView1.SelectRow.DataSource as FormulaTpl;

            if (row != null)
            {
                if (DialogResult.Yes == MessageBox.Show("您确定要删除吗！", "警告", MessageBoxButtons.YesNo))
                {
                    int rowNumber = FormulaDAL.DeleteTpl(row.id);
                    if (rowNumber > 0)
                    {
                        InitData();
                    }
                    else
                    {
                        MessageBox.Show("删除失败！");
                    }
                }
            }
            else
            {
                MessageBox.Show("请选择要删除的数据！");
            }
        }

        /// <summary>
        /// 更改页数发生
        /// </summary>
        /// <param name="currentSource"></param>
        private void ucPagerControl22_ShowSourceChanged(object currentSource)
        {
            currentPage = this.ucPagerControl22.PageIndex;
            InitData();
            //MessageBox.Show(this.ucPagerControl22.PageIndex.ToString());
        }
    }
}
