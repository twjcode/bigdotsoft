﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HZH_Controls.Controls;
using Infrastructure.DAL;
using BigDogSoft.Pages.forms;
using Infrastructure.Models;

namespace BigDogSoft.Pages
{
    public partial class CtrlUserManager : UCControlBase
    {
        /// <summary>
        /// 用户数据库操作
        /// </summary>
        private UserDAL dal = new UserDAL();
        public CtrlUserManager()
        {
            InitializeComponent();
            
        }

        private void CtrlUserManager_Load(object sender, EventArgs e)
        {
            List<DataGridViewColumnEntity> lstCulumns = new List<DataGridViewColumnEntity>();
            lstCulumns.Add(new DataGridViewColumnEntity() { DataField = "id", HeadText = "编号", Width = 70, WidthType = SizeType.Absolute });
            lstCulumns.Add(new DataGridViewColumnEntity() { DataField = "name", HeadText = "姓名", Width = 200, WidthType = SizeType.Absolute });
            lstCulumns.Add(new DataGridViewColumnEntity() { DataField = "account", HeadText = "登录账号", Width = 200, WidthType = SizeType.Absolute });
            lstCulumns.Add(new DataGridViewColumnEntity() { DataField = "create_time", HeadText = "创建时间", Width = 200, WidthType = SizeType.Absolute, Format = (a) => { return ((DateTime)a).ToString("yyyy-MM-dd"); } });

            this.ucDataGridView1.Columns = lstCulumns;
            LoadFromDB();
        }
        /// <summary>
        /// 从数据库查所有用户
        /// </summary>
        private void LoadFromDB()
        {
            var list = dal.GetAll();
            this.ucDataGridView1.DataSource = list;
        }

        private void ucBtnExt7_BtnClick(object sender, EventArgs e)
        {
            FrmUserAdd frm = new FrmUserAdd();
            frm.ShowDialog();
            LoadFromDB();
        }

        private void ucDataGridView1_ItemClick(object sender, DataGridViewEventArgs e)
        {
           
        }
        /// <summary>
        /// 删除事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ucBtnExt2_BtnClick(object sender, EventArgs e)
        {
            var row =
                this.ucDataGridView1.SelectRow.DataSource as Users;

            if (row != null)
            {
                if (row.account.Equals("admin",
                    StringComparison.CurrentCultureIgnoreCase))
                {
                    MessageBox.Show("超管账号不能删除！");
                    return;
                }
                else
                {
                  int rowNumber=  dal.Delete(row.id);
                    if (rowNumber > 0)
                    {
                        LoadFromDB();
                    }
                    else
                    {
                        MessageBox.Show("删除失败！");
                    }
                }
            }
            else
            {
                MessageBox.Show("请选择要删除的数据！");
            }
        }
    }
}
