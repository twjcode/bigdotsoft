﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HZH_Controls.Controls;
using Infrastructure.DAL;
using Infrastructure.Models;
using LiveCharts.Wpf;
using LiveCharts;
using Infrastructure.Util;

namespace BigDogSoft.Pages
{
    public partial class UtlHistoryQuery : UCControlBase
    {
        public UtlHistoryQuery()
        {
            InitializeComponent();
            InitData();
        }

        private void InitData()
        {
            List<Device> list = DeviceVarDAL.GetDeviceList();
            List<KeyValuePair<string, string>> cmbSource = new List<KeyValuePair<string, string>>();

            foreach (var item in list)
            {
                cmbSource.Add(new KeyValuePair<string, string>(item.id.ToString(), item.device_name));
            }

            this.cmbDevice.Source = cmbSource;
            this.dateEnd.CurrentTime = DateTime.Now;
            this.dateBegin.CurrentTime = DateTime.Now.AddHours(-2);
            
        }

        private void UtlHistoryQuery_Load(object sender, EventArgs e)
        {
            this.dataGridView1.AutoGenerateColumns = false;
            MariaSeries = new LineSeries
            {
                Values = new ChartValues<double> { 1,2,3,4,5,6,7,8}
            };
            this.cartesianChart1.Series = new SeriesCollection()
            {
                MariaSeries
            };
            this.cartesianChart1.Refresh();
        }
        public LineSeries MariaSeries { get; set; }
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_BtnClick(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.cmbDevice.SelectedValue))
            {
                MessageBox.Show("请选择设备！");
                return;
            }
            if (string.IsNullOrEmpty(this.cmbVarName.SelectedValue))
            {
                MessageBox.Show("请选择变量！");
                return;
            }
            int deviceId =int.Parse( this.cmbDevice.SelectedValue);
            int varId = int.Parse(this.cmbVarName.SelectedValue);
            DateTime begin= this.dateBegin.CurrentTime;
            DateTime end = this.dateEnd.CurrentTime;
            if (end < begin)
            {
                MessageBox.Show("结束时间必须大于开始时间！");
                return;
            }
            currentList= DeviceVarDAL.GetDeviceVarValBy(deviceId, varId,begin,end);
            this.dataGridView1.DataSource = currentList;
            MariaSeries = new LineSeries
            {
                Values = new ChartValues<double>()
            };
            var arry= currentList.GroupBy(p => p.create_time.ToString("yyyy-MM-dd HH:mm"));
            foreach (var item in arry)
            {
                foreach (var val in item)
                {
                    MariaSeries.Values.Add(double.Parse(val.var_val));
                    break;
                }
            }
            //foreach (var item in list)
            //{
            //    MariaSeries.Values.Add(double.Parse(item.var_val));
            //}
            this.cartesianChart1.Series = new SeriesCollection()
            {
                MariaSeries
            };
        }
        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExportExcel_BtnClick(object sender, EventArgs e)
        {
            if (currentList != null)
            {
                saveFileDialog1.DefaultExt = "xlsx";
                saveFileDialog1.Filter = "Excel 2007文件|*.xlsx|Excel 99-03文件|*.xls";
                if (DialogResult.OK== saveFileDialog1.ShowDialog())
                {
                    string fileName= saveFileDialog1.FileName;

                    ExcelHelper.ExportToExcelFile<DeviceVarVal>(fileName, new Dictionary<string, string>() {
                        {"device_name","设备名称" },
                        {"var_name","变量名称" },
                        {"var_address","变量地址" },
                        {"var_val","值" },
                        {"create_time","时间" }

                    }, currentList);
                }
                
            }
        }
        private List<DeviceVarVal> currentList = null;
        private void cmbDevice_SelectedChangedEvent(object sender, EventArgs e)
        {
            int deviceId =int.Parse(this.cmbDevice.SelectedValue);
            var list = DeviceVarDAL.GetDeviceVarListByDeviceId(deviceId);
            List<KeyValuePair<string, string>> cmbSource = new List<KeyValuePair<string, string>>();

            foreach (var item in list)
            {
                cmbSource.Add(new KeyValuePair<string, string>(item.id.ToString(), item.var_name));
            }

            this.cmbVarName.Source = cmbSource;
        }
    }
}
