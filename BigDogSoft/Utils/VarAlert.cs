﻿using Infrastructure;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BigDogSoft.Utils
{
    /// <summary>
    /// 超过阈值报警类
    /// </summary>
    public class VarAlert
    {
        /// <summary>
        /// 初始化
        /// </summary>
        public void Init()
        {
            EventBus.SubScribe("alert", o => {
                 //报警
                 var dv = o as DeviceVar;
                if (dv != null)
                {
                    int yuzhi= dv.alert_var;//报警的上限值
                    int val = Convert.ToInt32(dv.var_val);
                    if (val > yuzhi)
                    {
                        MessageBox.Show(string.Format("{0}设备{1}参数的值超过了阈值{2}",
                            dv.device_name,dv.var_name,dv.alert_var));
                    }
                }
            });
        }
    }
}
