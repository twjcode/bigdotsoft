﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BigDogSoft
{
    /// <summary>
    /// 会话上下文
    /// </summary>
    public sealed class SessionContext
    {
        /// <summary>
        /// 当前用户
        /// </summary>
        public static Users CurrentUser { get; set; }
    }
}
