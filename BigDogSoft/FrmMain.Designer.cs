﻿namespace BigDogSoft
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.基础管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.用户管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.监视预警ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.设备变量设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.监视变量管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.历史数据查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.配料ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.配方管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.配方模板设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.帮助ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.关于我们ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.退出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripFooter = new System.Windows.Forms.ToolStripLabel();
            this.toolStriptDatetime = new System.Windows.Forms.ToolStripLabel();
            this.panelContainer = new System.Windows.Forms.Panel();
            this.timerDateShow = new System.Windows.Forms.Timer(this.components);
            this.menuStrip2.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip2
            // 
            this.menuStrip2.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.menuStrip2.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.基础管理ToolStripMenuItem,
            this.监视预警ToolStripMenuItem,
            this.配料ToolStripMenuItem,
            this.帮助ToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(1049, 32);
            this.menuStrip2.TabIndex = 1;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // 基础管理ToolStripMenuItem
            // 
            this.基础管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.用户管理ToolStripMenuItem});
            this.基础管理ToolStripMenuItem.Image = global::BigDogSoft.Properties.Resources.building_fill;
            this.基础管理ToolStripMenuItem.Name = "基础管理ToolStripMenuItem";
            this.基础管理ToolStripMenuItem.Size = new System.Drawing.Size(122, 28);
            this.基础管理ToolStripMenuItem.Text = "基础管理";
            // 
            // 用户管理ToolStripMenuItem
            // 
            this.用户管理ToolStripMenuItem.Image = global::BigDogSoft.Properties.Resources.user_fill;
            this.用户管理ToolStripMenuItem.Name = "用户管理ToolStripMenuItem";
            this.用户管理ToolStripMenuItem.Size = new System.Drawing.Size(182, 34);
            this.用户管理ToolStripMenuItem.Text = "用户管理";
            this.用户管理ToolStripMenuItem.Click += new System.EventHandler(this.用户管理ToolStripMenuItem_Click);
            // 
            // 监视预警ToolStripMenuItem
            // 
            this.监视预警ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.设备变量设置ToolStripMenuItem,
            this.监视变量管理ToolStripMenuItem,
            this.历史数据查询ToolStripMenuItem});
            this.监视预警ToolStripMenuItem.Image = global::BigDogSoft.Properties.Resources.tv_2_line;
            this.监视预警ToolStripMenuItem.Name = "监视预警ToolStripMenuItem";
            this.监视预警ToolStripMenuItem.Size = new System.Drawing.Size(130, 28);
            this.监视预警ToolStripMenuItem.Text = "监视/预警";
            // 
            // 设备变量设置ToolStripMenuItem
            // 
            this.设备变量设置ToolStripMenuItem.Image = global::BigDogSoft.Properties.Resources.device_line;
            this.设备变量设置ToolStripMenuItem.Name = "设备变量设置ToolStripMenuItem";
            this.设备变量设置ToolStripMenuItem.Size = new System.Drawing.Size(218, 34);
            this.设备变量设置ToolStripMenuItem.Text = "设备变量设置";
            this.设备变量设置ToolStripMenuItem.Click += new System.EventHandler(this.设备变量设置ToolStripMenuItem_Click);
            // 
            // 监视变量管理ToolStripMenuItem
            // 
            this.监视变量管理ToolStripMenuItem.Image = global::BigDogSoft.Properties.Resources.tv_2_line;
            this.监视变量管理ToolStripMenuItem.Name = "监视变量管理ToolStripMenuItem";
            this.监视变量管理ToolStripMenuItem.Size = new System.Drawing.Size(218, 34);
            this.监视变量管理ToolStripMenuItem.Text = "监控变量";
            this.监视变量管理ToolStripMenuItem.Click += new System.EventHandler(this.监视变量管理ToolStripMenuItem_Click);
            // 
            // 历史数据查询ToolStripMenuItem
            // 
            this.历史数据查询ToolStripMenuItem.Image = global::BigDogSoft.Properties.Resources.line_chart_line;
            this.历史数据查询ToolStripMenuItem.Name = "历史数据查询ToolStripMenuItem";
            this.历史数据查询ToolStripMenuItem.Size = new System.Drawing.Size(218, 34);
            this.历史数据查询ToolStripMenuItem.Text = "历史数据查询";
            this.历史数据查询ToolStripMenuItem.Click += new System.EventHandler(this.历史数据查询ToolStripMenuItem_Click);
            // 
            // 配料ToolStripMenuItem
            // 
            this.配料ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.配方管理ToolStripMenuItem,
            this.配方模板设置ToolStripMenuItem});
            this.配料ToolStripMenuItem.Image = global::BigDogSoft.Properties.Resources.bill_line;
            this.配料ToolStripMenuItem.Name = "配料ToolStripMenuItem";
            this.配料ToolStripMenuItem.Size = new System.Drawing.Size(86, 28);
            this.配料ToolStripMenuItem.Text = "配方";
            // 
            // 配方管理ToolStripMenuItem
            // 
            this.配方管理ToolStripMenuItem.Image = global::BigDogSoft.Properties.Resources.bill_line;
            this.配方管理ToolStripMenuItem.Name = "配方管理ToolStripMenuItem";
            this.配方管理ToolStripMenuItem.Size = new System.Drawing.Size(218, 34);
            this.配方管理ToolStripMenuItem.Text = "配方管理";
            this.配方管理ToolStripMenuItem.Click += new System.EventHandler(this.配方管理ToolStripMenuItem_Click);
            // 
            // 配方模板设置ToolStripMenuItem
            // 
            this.配方模板设置ToolStripMenuItem.Image = global::BigDogSoft.Properties.Resources.file_copy_fill;
            this.配方模板设置ToolStripMenuItem.Name = "配方模板设置ToolStripMenuItem";
            this.配方模板设置ToolStripMenuItem.Size = new System.Drawing.Size(218, 34);
            this.配方模板设置ToolStripMenuItem.Text = "配方模板设置";
            this.配方模板设置ToolStripMenuItem.Click += new System.EventHandler(this.配方模板设置ToolStripMenuItem_Click);
            // 
            // 帮助ToolStripMenuItem
            // 
            this.帮助ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.关于我们ToolStripMenuItem,
            this.toolStripMenuItem1,
            this.退出ToolStripMenuItem});
            this.帮助ToolStripMenuItem.Image = global::BigDogSoft.Properties.Resources.questionnaire_line;
            this.帮助ToolStripMenuItem.Name = "帮助ToolStripMenuItem";
            this.帮助ToolStripMenuItem.Size = new System.Drawing.Size(86, 28);
            this.帮助ToolStripMenuItem.Text = "帮助";
            // 
            // 关于我们ToolStripMenuItem
            // 
            this.关于我们ToolStripMenuItem.Name = "关于我们ToolStripMenuItem";
            this.关于我们ToolStripMenuItem.Size = new System.Drawing.Size(182, 34);
            this.关于我们ToolStripMenuItem.Text = "关于我们";
            this.关于我们ToolStripMenuItem.Click += new System.EventHandler(this.关于我们ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Image = global::BigDogSoft.Properties.Resources.key_line;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(182, 34);
            this.toolStripMenuItem1.Text = "修改密码";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // 退出ToolStripMenuItem
            // 
            this.退出ToolStripMenuItem.Image = global::BigDogSoft.Properties.Resources.picture_in_picture_exit_line;
            this.退出ToolStripMenuItem.Name = "退出ToolStripMenuItem";
            this.退出ToolStripMenuItem.Size = new System.Drawing.Size(182, 34);
            this.退出ToolStripMenuItem.Text = "退出";
            this.退出ToolStripMenuItem.Click += new System.EventHandler(this.退出ToolStripMenuItem_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripFooter,
            this.toolStriptDatetime});
            this.toolStrip1.Location = new System.Drawing.Point(0, 631);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1049, 29);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripFooter
            // 
            this.toolStripFooter.Name = "toolStripFooter";
            this.toolStripFooter.Size = new System.Drawing.Size(140, 24);
            this.toolStripFooter.Text = "当前用户Admin";
            // 
            // toolStriptDatetime
            // 
            this.toolStriptDatetime.ForeColor = System.Drawing.Color.Red;
            this.toolStriptDatetime.Name = "toolStriptDatetime";
            this.toolStriptDatetime.Size = new System.Drawing.Size(193, 24);
            this.toolStriptDatetime.Text = "2020-01-01 13:01:01";
            // 
            // panelContainer
            // 
            this.panelContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelContainer.Location = new System.Drawing.Point(0, 32);
            this.panelContainer.Name = "panelContainer";
            this.panelContainer.Size = new System.Drawing.Size(1049, 599);
            this.panelContainer.TabIndex = 3;
            // 
            // timerDateShow
            // 
            this.timerDateShow.Interval = 1000;
            this.timerDateShow.Tick += new System.EventHandler(this.fetchDataTimer_Tick);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(144F, 144F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1049, 660);
            this.Controls.Add(this.panelContainer);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmMain";
            this.Text = "其然C#上位机软件V1.0";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem 基础管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 用户管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 监视预警ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 监视变量管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 历史数据查询ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 配料ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 配方管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 帮助ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 关于我们ToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripFooter;
        private System.Windows.Forms.Panel panelContainer;
        private System.Windows.Forms.ToolStripMenuItem 退出ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 设备变量设置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 配方模板设置ToolStripMenuItem;
        private System.Windows.Forms.Timer timerDateShow;
        private System.Windows.Forms.ToolStripLabel toolStriptDatetime;
    }
}