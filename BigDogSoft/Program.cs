﻿using BigDogSoft.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace BigDogSoft
{
    static class Program
    {
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
           // HslControls.Authorization.SetAuthorizationCode("123456");
            HslCommunication.Authorization.SetAuthorizationCode("123456");
            if (Environment.OSVersion.Version.Major >= 6)
                SetProcessDPIAware();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.ThreadException += Application_ThreadException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            Application.ApplicationExit += Application_ApplicationExit;
#if DEBUG
            Application.Run(new FrmLogin());
#else
            Application.Run(new FrmLogin());
#endif
        }
        static void Application_ApplicationExit(object sender, EventArgs e)
        {
            DeviceComminucationFactory.CloseAll();
            Console.WriteLine("程序退出成功");
        }
        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            _logger.Error(e.Exception);
            MessageBox.Show(e.Exception.Message);
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            _logger.Error((Exception)e.ExceptionObject);
            MessageBox.Show(((Exception)e.ExceptionObject).Message);
        }
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern bool SetProcessDPIAware();
    }


    
}
