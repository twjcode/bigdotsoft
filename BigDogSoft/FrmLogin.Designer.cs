﻿namespace BigDogSoft
{
    partial class FrmLogin
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAccount = new HZH_Controls.Controls.TextBoxEx();
            this.txtPassword = new HZH_Controls.Controls.TextBoxEx();
            this.btnCancel = new HZH_Controls.Controls.UCBtnExt();
            this.btnLogin = new HZH_Controls.Controls.UCBtnExt();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(83, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 24);
            this.label1.TabIndex = 7;
            this.label1.Text = "账号：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(83, 145);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 24);
            this.label2.TabIndex = 7;
            this.label2.Text = "密码：";
            // 
            // txtAccount
            // 
            this.txtAccount.DecLength = 2;
            this.txtAccount.InputType = HZH_Controls.TextInputType.NotControl;
            this.txtAccount.Location = new System.Drawing.Point(189, 87);
            this.txtAccount.MaxValue = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.txtAccount.MinValue = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.txtAccount.MyRectangle = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.txtAccount.Name = "txtAccount";
            this.txtAccount.OldText = null;
            this.txtAccount.PromptColor = System.Drawing.Color.Gray;
            this.txtAccount.PromptFont = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtAccount.PromptText = "请输入账号";
            this.txtAccount.RegexPattern = "";
            this.txtAccount.Size = new System.Drawing.Size(201, 31);
            this.txtAccount.TabIndex = 8;
            this.txtAccount.Text = "admin";
            // 
            // txtPassword
            // 
            this.txtPassword.DecLength = 2;
            this.txtPassword.InputType = HZH_Controls.TextInputType.NotControl;
            this.txtPassword.Location = new System.Drawing.Point(189, 145);
            this.txtPassword.MaxValue = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.txtPassword.MinValue = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.txtPassword.MyRectangle = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.OldText = null;
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.PromptColor = System.Drawing.Color.Gray;
            this.txtPassword.PromptFont = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtPassword.PromptText = "请输入密码";
            this.txtPassword.RegexPattern = "";
            this.txtPassword.Size = new System.Drawing.Size(201, 31);
            this.txtPassword.TabIndex = 8;
            this.txtPassword.Text = "123456";
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.White;
            this.btnCancel.BtnBackColor = System.Drawing.Color.White;
            this.btnCancel.BtnFont = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnCancel.BtnForeColor = System.Drawing.Color.White;
            this.btnCancel.BtnText = "取消";
            this.btnCancel.ConerRadius = 5;
            this.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancel.EnabledMouseEffect = false;
            this.btnCancel.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnCancel.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnCancel.IsRadius = true;
            this.btnCancel.IsShowRect = true;
            this.btnCancel.IsShowTips = false;
            this.btnCancel.Location = new System.Drawing.Point(104, 207);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(0);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnCancel.RectWidth = 10;
            this.btnCancel.Size = new System.Drawing.Size(113, 39);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.TabStop = false;
            this.btnCancel.TipsColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(30)))), ((int)(((byte)(99)))));
            this.btnCancel.TipsText = "取消";
            this.btnCancel.BtnClick += new System.EventHandler(this.btnCancel_BtnClick);
            // 
            // btnLogin
            // 
            this.btnLogin.BackColor = System.Drawing.Color.White;
            this.btnLogin.BtnBackColor = System.Drawing.Color.White;
            this.btnLogin.BtnFont = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnLogin.BtnForeColor = System.Drawing.Color.White;
            this.btnLogin.BtnText = "登录";
            this.btnLogin.ConerRadius = 5;
            this.btnLogin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLogin.EnabledMouseEffect = false;
            this.btnLogin.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(77)))), ((int)(((byte)(59)))));
            this.btnLogin.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnLogin.IsRadius = true;
            this.btnLogin.IsShowRect = true;
            this.btnLogin.IsShowTips = false;
            this.btnLogin.Location = new System.Drawing.Point(258, 207);
            this.btnLogin.Margin = new System.Windows.Forms.Padding(0);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(77)))), ((int)(((byte)(58)))));
            this.btnLogin.RectWidth = 1;
            this.btnLogin.Size = new System.Drawing.Size(113, 39);
            this.btnLogin.TabIndex = 10;
            this.btnLogin.TabStop = false;
            this.btnLogin.TipsColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(30)))), ((int)(((byte)(99)))));
            this.btnLogin.TipsText = "";
            this.btnLogin.BtnClick += new System.EventHandler(this.btnLogin_BtnClick);
            // 
            // FrmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(144F, 144F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(481, 273);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtAccount);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FrmLogin";
            this.Text = "登录窗体";
            this.Load += new System.EventHandler(this.FrmLogin_Load);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.txtAccount, 0);
            this.Controls.SetChildIndex(this.txtPassword, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnLogin, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private HZH_Controls.Controls.TextBoxEx txtAccount;
        private HZH_Controls.Controls.TextBoxEx txtPassword;
        private HZH_Controls.Controls.UCBtnExt btnCancel;
        private HZH_Controls.Controls.UCBtnExt btnLogin;
    }
}

