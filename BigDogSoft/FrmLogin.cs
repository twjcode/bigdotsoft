﻿using HZH_Controls.Forms;
using Infrastructure.DAL;
using Infrastructure.Models;
using Infrastructure.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BigDogSoft
{
    /// <summary>
    /// 登录窗体
    /// </summary>
    public partial class FrmLogin : FrmWithTitle
    {
        private UserDAL userDal = new UserDAL();
        public FrmLogin()
        {
            InitializeComponent();
            this.Title = "登录";
            this.IsShowCloseBtn = true;
        }

        private void btnCancel_BtnClick(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// 登录事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLogin_BtnClick(object sender, EventArgs e)
        {
            string userName = this.txtAccount.Text.Trim();
            string pwd = this.txtPassword.Text.Trim();
            Users user= userDal.GetByUserNameAndPwd(userName,MD5Helper.GetMD5(pwd));
            if (user!=null)
            {
                SessionContext.CurrentUser = user;//记录当前用户
                this.Hide();
                FrmMain main=new FrmMain();
                main.Show();
                
            }
            else
            {
                MessageBox.Show("用户名或密码错误！");
            }
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
           
        }
    }
}
