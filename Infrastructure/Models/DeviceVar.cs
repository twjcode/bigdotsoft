﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Models
{
    /// <summary>
    /// 设备变量
    /// </summary>
    public class DeviceVar
    {
       public int id { get; set; }
        /// <summary>
        /// 设备编号
        /// </summary>
       public int device_id { get; set; }
        /// <summary>
        /// 变量名称
        /// </summary>
       public string var_name { get; set; }
        /// <summary>
        /// 变量类型
        /// </summary>
       public string var_type { get; set; }
        /// <summary>
        /// 变量长度
        /// </summary>
       public int var_length { get; set; }
        /// <summary>
        /// 报警阈值
        /// </summary>
       public int alert_var { get; set; }
        /// <summary>
        /// 变量地址
        /// </summary>
       public string var_address { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime create_time { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public string create_user { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime update_time { get; set; }
        /// <summary>
        /// 更新人
        /// </summary>
        public string update_user { get; set; }
        /// <summary>
        /// 设备名称
        /// </summary>
        public string device_name { get; set; }
        /// <summary>
        /// 变量实时数据
        /// </summary>
        public string var_val { get; set; }
    }
}
