﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Models
{
    /// <summary>
    /// 配方表
    /// </summary>
    public class Formula
    {
        public int id { get; set; }
        /// <summary>
        /// 配方模板编号
        /// </summary>
        public int formula_tpl_id { get; set; }
        /// <summary>
        /// 配方名称
        /// </summary>
        public string formula_name { get; set; }
        /// <summary>
        /// 动态字段P1
        /// </summary>
        public string p1 { get; set; }
        public string p2 { get; set; }
        public string p3 { get; set; }
        public string p4 { get; set; }
        public string p5 { get; set; }
        public string p6 { get; set; }
        public string p7 { get; set; }
        public string p8 { get; set; }
        public string p9 { get; set; }
        public string p10 { get; set; }
        public string p11 { get; set; }
        public string p12 { get; set; }
        public string p13 { get; set; }
        public string p14 { get; set; }
        public string p15 { get; set; }

        public string p16 { get; set; }

        public string p17 { get; set; }

        public string p18 { get; set; }

        public string p19 { get; set; }
        public string p20 { get; set; }
        public string p21 { get; set; }
        public string p22 { get; set; }
        public string p23 { get; set; }
        public string p24 { get; set; }
        public string p25 { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime create_time { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public string create_user { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime update_time { get; set; }
        /// <summary>
        /// 更新人
        /// </summary>
        public string update_user { get; set; }

        public string template_name { get; set; }
    }
}
