﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Models
{
    /// <summary>
    /// 模板参数表
    /// </summary>
    public class FormulaTplParam
    {
         public int id { get; set; }
        public int tpl_id { get; set; }
        /// <summary>
        /// 参数名称
        /// </summary>
        public string param_name { get; set; }
        /// <summary>
        /// 参数类型
        /// </summary>
         public string param_type { get; set; }
        /// <summary>
        /// 参数地址
        /// </summary>
         public string param_address { get; set; }
        /// <summary>
        /// 校验地址
        /// </summary>
         public string verify_address { get; set; }
        /// <summary>
        /// 配方表中字段名称p1-p25
        /// </summary>
         public string param_field { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime create_time { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public string create_user { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime update_time { get; set; }
        /// <summary>
        /// 更新人
        /// </summary>
        public string update_user { get; set; }
    }
}
