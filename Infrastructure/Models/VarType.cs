﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Models
{
    /// <summary>
    /// 变量 类型枚举
    /// </summary>
    public enum VarType
    {
        UInt16,
        UInt32,
        Int16,
        Int32,
        Float,
        Boolean
    }
}
