namespace HslCommunication.MQTT
{
	/// <summary>
	/// 定义了Mqtt的相关的控制报文的信息
	/// </summary>
	public class MqttControlMessage
	{
		/// <summary>
		/// 连接标识
		/// </summary>
		public const byte CONNECT = 1;

		/// <summary>
		/// 连接返回的标识
		/// </summary>
		public const byte CONNACK = 2;

		/// <summary>
		/// 发布消息
		/// </summary>
		public const byte PUBLISH = 3;

		/// <summary>
		/// QoS 1消息发布收到确认
		/// </summary>
		public const byte PUBACK = 4;

		/// <summary>
		/// 发布收到（保证交付第一步）
		/// </summary>
		public const byte PUBREC = 5;

		/// <summary>
		/// 发布释放（保证交付第二步）
		/// </summary>
		public const byte PUBREL = 6;

		/// <summary>
		/// QoS 2消息发布完成（保证交互第三步）
		/// </summary>
		public const byte PUBCOMP = 7;

		/// <summary>
		/// 客户端订阅请求
		/// </summary>
		public const byte SUBSCRIBE = 8;

		/// <summary>
		/// 订阅请求报文确认
		/// </summary>
		public const byte SUBACK = 9;

		/// <summary>
		/// 客户端取消订阅请求
		/// </summary>
		public const byte UNSUBSCRIBE = 10;

		/// <summary>
		/// 取消订阅报文确认
		/// </summary>
		public const byte UNSUBACK = 11;

		/// <summary>
		/// 心跳请求
		/// </summary>
		public const byte PINGREQ = 12;

		/// <summary>
		/// 心跳响应
		/// </summary>
		public const byte PINGRESP = 13;

		/// <summary>
		/// 客户端断开连接
		/// </summary>
		public const byte DISCONNECT = 14;

		/// <summary>
		/// 报告进度
		/// </summary>
		public const byte REPORTPROGRESS = 15;
	}
}
