namespace HslCommunication.CNC.Fanuc
{
	/// <summary>
	/// 设备的工作模式
	/// </summary>
	public enum CNCWorkMode
	{
		/// <summary>
		/// 手动输入
		/// </summary>
		MDI = 0,
		/// <summary>
		/// 自动循环
		/// </summary>
		AUTO = 1,
		/// <summary>
		/// 程序编辑
		/// </summary>
		EDIT = 3,
		/// <summary>
		/// ×100
		/// </summary>
		HANDLE = 4,
		/// <summary>
		/// 连续进给
		/// </summary>
		JOG = 5,
		TeachInJOG = 6,
		TeachInHandle = 7,
		INCfeed = 8,
		/// <summary>
		/// 机床回零
		/// </summary>
		REFerence = 9,
		ReMoTe = 10
	}
}
