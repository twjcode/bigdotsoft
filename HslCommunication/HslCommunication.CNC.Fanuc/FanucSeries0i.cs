using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using HslCommunication.BasicFramework;
using HslCommunication.Core;
using HslCommunication.Core.IMessage;
using HslCommunication.Core.Net;

namespace HslCommunication.CNC.Fanuc
{
	/// <summary>
	/// 一个FANUC的机床通信类对象
	/// </summary>
	public class FanucSeries0i : NetworkDoubleBase
	{
		/// <summary>
		/// 根据IP及端口来实例化一个对象内容
		/// </summary>
		/// <param name="ipAddress">Ip地址信息</param>
		/// <param name="port">端口号</param>
		public FanucSeries0i(string ipAddress, int port = 8193)
		{
			IpAddress = ipAddress;
			Port = port;
			base.ByteTransform = new ReverseBytesTransform();
		}

		/// <inheritdoc />
		protected override INetMessage GetNewNetMessage()
		{
			return new CNCFanucSeriesMessage();
		}

		/// <inheritdoc />
		protected override OperateResult InitializationOnConnect(Socket socket)
		{
			OperateResult<byte[]> operateResult = ReadFromCoreServer(socket, "a0 a0 a0 a0 00 01 01 01 00 02 00 02".ToHexBytes());
			if (!operateResult.IsSuccess)
			{
				return operateResult;
			}
			OperateResult<byte[]> operateResult2 = ReadFromCoreServer(socket, "a0 a0 a0 a0 00 01 21 01 00 1e 00 01 00 1c 00 01 00 01 00 18 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00".ToHexBytes());
			if (!operateResult2.IsSuccess)
			{
				return operateResult2;
			}
			return OperateResult.CreateSuccessResult();
		}

		/// <inheritdoc />
		protected override OperateResult ExtraOnDisconnect(Socket socket)
		{
			return ReadFromCoreServer(socket, "a0 a0 a0 a0 00 01 02 01 00 00".ToHexBytes());
		}

		/// <inheritdoc />
		protected override async Task<OperateResult> InitializationOnConnectAsync(Socket socket)
		{
			OperateResult<byte[]> read1 = await ReadFromCoreServerAsync(socket, "a0 a0 a0 a0 00 01 01 01 00 02 00 02".ToHexBytes());
			if (!read1.IsSuccess)
			{
				return read1;
			}
			OperateResult<byte[]> read2 = await ReadFromCoreServerAsync(socket, "a0 a0 a0 a0 00 01 21 01 00 1e 00 01 00 1c 00 01 00 01 00 18 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00".ToHexBytes());
			if (!read2.IsSuccess)
			{
				return read2;
			}
			return OperateResult.CreateSuccessResult();
		}

		/// <inheritdoc />
		protected override async Task<OperateResult> ExtraOnDisconnectAsync(Socket socket)
		{
			return await ReadFromCoreServerAsync(socket, "a0 a0 a0 a0 00 01 02 01 00 00".ToHexBytes());
		}

		private double GetFanucDouble(byte[] content, int index)
		{
			return GetFanucDouble(content, index, 1)[0];
		}

		private double[] GetFanucDouble(byte[] content, int index, int length)
		{
			double[] array = new double[length];
			for (int i = 0; i < length; i++)
			{
				int num = base.ByteTransform.TransInt32(content, index + 8 * i);
				int num2 = base.ByteTransform.TransInt16(content, index + 8 * i + 6);
				if (num == 0)
				{
					array[i] = 0.0;
				}
				else
				{
					array[i] = Math.Round((double)num * Math.Pow(0.1, num2), num2);
				}
			}
			return array;
		}

		private byte[] CreateFromFanucDouble(double value)
		{
			byte[] array = new byte[8];
			int value2 = (int)(value * 1000.0);
			base.ByteTransform.TransByte(value2).CopyTo(array, 0);
			array[5] = 10;
			array[7] = 3;
			return array;
		}

		/// <summary>
		/// 主轴转速及进给倍率
		/// </summary>
		/// <returns>主轴转速及进给倍率</returns>
		public OperateResult<double, double> ReadSpindleSpeedAndFeedRate()
		{
			OperateResult<byte[]> operateResult = ReadFromCoreServer(BuildReadArray(BuildReadSingle(164, 3, 0, 0, 0, 0), BuildReadSingle(138, 1, 0, 0, 0, 0), BuildReadSingle(136, 3, 0, 0, 0, 0), BuildReadSingle(136, 4, 0, 0, 0, 0), BuildReadSingle(36, 0, 0, 0, 0, 0), BuildReadSingle(37, 0, 0, 0, 0, 0), BuildReadSingle(164, 3, 0, 0, 0, 0)));
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<double, double>(operateResult);
			}
			List<byte[]> list = ExtraContentArray(operateResult.Content.RemoveBegin(10));
			return OperateResult.CreateSuccessResult(GetFanucDouble(list[5], 14), GetFanucDouble(list[4], 14));
		}

		/// <summary>
		/// 读取程序名及程序号
		/// </summary>
		/// <returns>程序名及程序号</returns>
		public OperateResult<string, int> ReadSystemProgramCurrent()
		{
			OperateResult<byte[]> operateResult = ReadFromCoreServer(BuildReadArray(BuildReadSingle(207, 0, 0, 0, 0, 0)));
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<string, int>(operateResult);
			}
			List<byte[]> list = ExtraContentArray(operateResult.Content.RemoveBegin(10));
			int value = base.ByteTransform.TransInt32(list[0], 14);
			string value2 = Encoding.Default.GetString(list[0].SelectMiddle(18, 36)).TrimEnd(default(char));
			return OperateResult.CreateSuccessResult(value2, value);
		}

		/// <summary>
		/// 读取宏变量，可以用来读取刀具号
		/// </summary>
		/// <param name="number"></param>
		/// <returns>读宏变量信息</returns>
		public OperateResult<double> ReadSystemMacroValue(int number)
		{
			return ByteTransformHelper.GetResultFromArray(ReadSystemMacroValue(number, 1));
		}

		/// <summary>
		/// 读取宏变量，可以用来读取刀具号
		/// </summary>
		/// <param name="number"></param>
		/// <param name="length">读取的长度信息</param>
		/// <returns>是否成功</returns>
		public OperateResult<double[]> ReadSystemMacroValue(int number, int length)
		{
			int[] array = SoftBasic.SplitIntegerToArray(length, 5);
			int num = number;
			List<byte> list = new List<byte>();
			for (int i = 0; i < array.Length; i++)
			{
				OperateResult<byte[]> operateResult = ReadFromCoreServer(BuildReadArray(BuildReadSingle(21, num, num + array[i] - 1, 0, 0, 0)));
				if (!operateResult.IsSuccess)
				{
					return OperateResult.CreateFailedResult<double[]>(operateResult);
				}
				list.AddRange(ExtraContentArray(operateResult.Content.RemoveBegin(10))[0].RemoveBegin(14));
				num += array[i];
			}
			try
			{
				return OperateResult.CreateSuccessResult(GetFanucDouble(list.ToArray(), 0, length));
			}
			catch (Exception ex)
			{
				return new OperateResult<double[]>(ex.Message + " Source:" + list.ToArray().ToHexString(' '));
			}
		}

		/// <summary>
		/// 写宏变量
		/// </summary>
		/// <param name="number">地址</param>
		/// <param name="values">数据值</param>
		/// <returns>是否成功</returns>
		public OperateResult WriteSystemMacroValue(int number, double[] values)
		{
			OperateResult<byte[]> operateResult = ReadFromCoreServer(BuildReadArray(BuildWriteSingle(22, number, number + values.Length - 1, 0, 0, values)));
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<string, int>(operateResult);
			}
			List<byte[]> list = ExtraContentArray(operateResult.Content.RemoveBegin(10));
			if (base.ByteTransform.TransUInt16(list[0], 6) == 0)
			{
				return OperateResult.CreateSuccessResult();
			}
			return new OperateResult(base.ByteTransform.TransUInt16(list[0], 6), "Unknown Error");
		}

		/// <summary>
		/// 根据刀具号写入长度形状补偿，刀具号为1-24
		/// </summary>
		/// <param name="cutter">刀具号，范围为1-24</param>
		/// <param name="offset">补偿值</param>
		/// <returns>是否写入成功</returns>
		public OperateResult WriteCutterLengthSharpOffset(int cutter, double offset)
		{
			return WriteSystemMacroValue(11000 + cutter, new double[1]
			{
				offset
			});
		}

		/// <summary>
		/// 根据刀具号写入长度磨损补偿，刀具号为1-24
		/// </summary>
		/// <param name="cutter">刀具号，范围为1-24</param>
		/// <param name="offset">补偿值</param>
		/// <returns>是否写入成功</returns>
		public OperateResult WriteCutterLengthWearOffset(int cutter, double offset)
		{
			return WriteSystemMacroValue(10000 + cutter, new double[1]
			{
				offset
			});
		}

		/// <summary>
		/// 根据刀具号写入半径形状补偿，刀具号为1-24
		/// </summary>
		/// <param name="cutter">刀具号，范围为1-24</param>
		/// <param name="offset">补偿值</param>
		/// <returns>是否写入成功</returns>
		public OperateResult WriteCutterRadiusSharpOffset(int cutter, double offset)
		{
			return WriteSystemMacroValue(13000 + cutter, new double[1]
			{
				offset
			});
		}

		/// <summary>
		/// 根据刀具号写入半径磨损补偿，刀具号为1-24
		/// </summary>
		/// <param name="cutter">刀具号，范围为1-24</param>
		/// <param name="offset">补偿值</param>
		/// <returns>是否写入成功</returns>
		public OperateResult WriteCutterRadiusWearOffset(int cutter, double offset)
		{
			return WriteSystemMacroValue(12000 + cutter, new double[1]
			{
				offset
			});
		}

		/// <summary>
		/// 读取伺服负载
		/// </summary>
		/// <returns>轴负载</returns>
		public OperateResult<double[]> ReadFanucAxisLoad()
		{
			OperateResult<byte[]> operateResult = ReadFromCoreServer(BuildReadArray(BuildReadSingle(164, 2, 0, 0, 0, 0), BuildReadSingle(137, 0, 0, 0, 0, 0), BuildReadSingle(86, 1, 0, 0, 0, 0), BuildReadSingle(164, 2, 0, 0, 0, 0)));
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<double[]>(operateResult);
			}
			List<byte[]> list = ExtraContentArray(operateResult.Content.RemoveBegin(10));
			int length = base.ByteTransform.TransUInt16(list[0], 14);
			return OperateResult.CreateSuccessResult(GetFanucDouble(list[2], 14, length));
		}

		/// <summary>
		/// 读取机床的坐标，包括机械坐标，绝对坐标，相对坐标
		/// </summary>
		/// <returns>数控机床的坐标信息，包括机械坐标，绝对坐标，相对坐标</returns>
		public OperateResult<SysAllCoors> ReadSysAllCoors()
		{
			OperateResult<byte[]> operateResult = ReadFromCoreServer(BuildReadArray(BuildReadSingle(164, 0, 0, 0, 0, 0), BuildReadSingle(137, -1, 0, 0, 0, 0), BuildReadSingle(136, 1, 0, 0, 0, 0), BuildReadSingle(136, 2, 0, 0, 0, 0), BuildReadSingle(163, 0, -1, 0, 0, 0), BuildReadSingle(38, 0, -1, 0, 0, 0), BuildReadSingle(38, 1, -1, 0, 0, 0), BuildReadSingle(38, 2, -1, 0, 0, 0), BuildReadSingle(38, 3, -1, 0, 0, 0), BuildReadSingle(164, 0, 0, 0, 0, 0)));
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<SysAllCoors>(operateResult);
			}
			List<byte[]> list = ExtraContentArray(operateResult.Content.RemoveBegin(10));
			int length = base.ByteTransform.TransUInt16(list[0], 14);
			SysAllCoors sysAllCoors = new SysAllCoors();
			sysAllCoors.Absolute = GetFanucDouble(list[5], 14, length);
			sysAllCoors.Machine = GetFanucDouble(list[6], 14, length);
			sysAllCoors.Relative = GetFanucDouble(list[7], 14, length);
			return OperateResult.CreateSuccessResult(sysAllCoors);
		}

		/// <summary>
		/// 读取报警信息
		/// </summary>
		/// <returns>机床的当前的所有的报警信息</returns>
		public OperateResult<SysAlarm[]> ReadSystemAlarm()
		{
			OperateResult<byte[]> operateResult = ReadFromCoreServer(BuildReadArray(BuildReadSingle(35, -1, 10, 2, 64, 0)));
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<SysAlarm[]>(operateResult);
			}
			List<byte[]> list = ExtraContentArray(operateResult.Content.RemoveBegin(10));
			if (base.ByteTransform.TransUInt16(list[0], 12) > 0)
			{
				int num = (int)base.ByteTransform.TransUInt16(list[0], 12) / 80;
				SysAlarm[] array = new SysAlarm[num];
				for (int i = 0; i < array.Length; i++)
				{
					array[i] = new SysAlarm();
					array[i].AlarmId = base.ByteTransform.TransInt32(list[0], 14 + 80 * i);
					array[i].Type = base.ByteTransform.TransInt16(list[0], 20 + 80 * i);
					array[i].Axis = base.ByteTransform.TransInt16(list[0], 24 + 80 * i);
					ushort count = base.ByteTransform.TransUInt16(list[0], 28 + 80 * i);
					array[i].Message = Encoding.Default.GetString(list[0], 30 + 80 * i, count);
				}
				return OperateResult.CreateSuccessResult(array);
			}
			return OperateResult.CreateSuccessResult(new SysAlarm[0]);
		}

		/// <summary>
		/// 读取fanuc机床的时间，0是开机时间，1是运行时间，2是切割时间，3是循环时间，4是空闲时间，返回秒为单位的信息
		/// </summary>
		/// <param name="timeType">读取的时间类型</param>
		/// <returns>秒为单位的结果</returns>
		public OperateResult<long> ReadTimeData(int timeType)
		{
			OperateResult<byte[]> operateResult = ReadFromCoreServer(BuildReadArray(BuildReadSingle(288, timeType, 0, 0, 0, 0)));
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<long>(operateResult);
			}
			List<byte[]> list = ExtraContentArray(operateResult.Content.RemoveBegin(10));
			int num = base.ByteTransform.TransInt32(list[0], 18);
			long num2 = base.ByteTransform.TransInt32(list[0], 14);
			if (num < 0 || num > 60000)
			{
				num = BitConverter.ToInt32(list[0], 18);
				num2 = BitConverter.ToInt32(list[0], 14);
			}
			long num3 = num / 1000;
			return OperateResult.CreateSuccessResult(num2 * 60 + num3);
		}

		/// <summary>
		/// 读取报警状态信息
		/// </summary>
		/// <returns>报警状态数据</returns>
		public OperateResult<int> ReadAlarmStatus()
		{
			OperateResult<byte[]> operateResult = ReadFromCoreServer(BuildReadArray(BuildReadSingle(26, 0, 0, 0, 0, 0)));
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<int>(operateResult);
			}
			List<byte[]> list = ExtraContentArray(operateResult.Content.RemoveBegin(10));
			return OperateResult.CreateSuccessResult((int)base.ByteTransform.TransUInt16(list[0], 16));
		}

		/// <summary>
		/// 读取系统的基本信息状态，工作模式，运行状态，是否急停等等操作
		/// </summary>
		/// <returns>结果信息数据</returns>
		public OperateResult<SysStatusInfo> ReadSysStatusInfo()
		{
			OperateResult<byte[]> operateResult = ReadFromCoreServer(BuildReadArray(BuildReadSingle(25, 0, 0, 0, 0, 0), BuildReadSingle(225, 0, 0, 0, 0, 0), BuildReadSingle(152, 0, 0, 0, 0, 0)));
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<SysStatusInfo>(operateResult);
			}
			List<byte[]> list = ExtraContentArray(operateResult.Content.RemoveBegin(10));
			SysStatusInfo sysStatusInfo = new SysStatusInfo();
			sysStatusInfo.Dummy = base.ByteTransform.TransInt16(list[1], 14);
			sysStatusInfo.TMMode = base.ByteTransform.TransInt16(list[2], 14);
			sysStatusInfo.WorkMode = (CNCWorkMode)base.ByteTransform.TransInt16(list[0], 14);
			sysStatusInfo.RunStatus = (CNCRunStatus)base.ByteTransform.TransInt16(list[0], 16);
			sysStatusInfo.Motion = base.ByteTransform.TransInt16(list[0], 18);
			sysStatusInfo.MSTB = base.ByteTransform.TransInt16(list[0], 20);
			sysStatusInfo.Emergency = base.ByteTransform.TransInt16(list[0], 22);
			sysStatusInfo.Alarm = base.ByteTransform.TransInt16(list[0], 24);
			sysStatusInfo.Edit = base.ByteTransform.TransInt16(list[0], 26);
			return OperateResult.CreateSuccessResult(sysStatusInfo);
		}

		/// <summary>
		/// 读取设备的程序列表
		/// </summary>
		/// <returns>读取结果信息</returns>
		public OperateResult<int[]> ReadProgramList()
		{
			OperateResult<byte[]> operateResult = ReadFromCoreServer(BuildReadArray(BuildReadSingle(6, 1, 19, 0, 0, 0)));
			OperateResult<byte[]> operateResult2 = ReadFromCoreServer(BuildReadArray(BuildReadSingle(6, 6667, 19, 0, 0, 0)));
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<int[]>(operateResult);
			}
			if (!operateResult2.IsSuccess)
			{
				return OperateResult.CreateFailedResult<int[]>(operateResult);
			}
			List<byte[]> list = ExtraContentArray(operateResult.Content.RemoveBegin(10));
			int num = (list[0].Length - 14) / 72;
			int[] array = new int[num];
			for (int i = 0; i < num; i++)
			{
				array[i] = base.ByteTransform.TransInt32(list[0], 14 + 72 * i);
			}
			return OperateResult.CreateSuccessResult(array);
		}

		/// <summary>
		/// 读取当前的刀具补偿信息
		/// </summary>
		/// <param name="cutterNumber">刀具数量</param>
		/// <returns>结果内容</returns>
		public OperateResult<CutterInfo[]> ReadCutterInfos(int cutterNumber = 24)
		{
			OperateResult<byte[]> operateResult = ReadFromCoreServer(BuildReadArray(BuildReadSingle(8, 1, cutterNumber, 0, 0, 0)));
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<CutterInfo[]>(operateResult);
			}
			OperateResult<byte[]> operateResult2 = ReadFromCoreServer(BuildReadArray(BuildReadSingle(8, 1, cutterNumber, 1, 0, 0)));
			if (!operateResult2.IsSuccess)
			{
				return OperateResult.CreateFailedResult<CutterInfo[]>(operateResult2);
			}
			OperateResult<byte[]> operateResult3 = ReadFromCoreServer(BuildReadArray(BuildReadSingle(8, 1, cutterNumber, 2, 0, 0)));
			if (!operateResult3.IsSuccess)
			{
				return OperateResult.CreateFailedResult<CutterInfo[]>(operateResult3);
			}
			OperateResult<byte[]> operateResult4 = ReadFromCoreServer(BuildReadArray(BuildReadSingle(8, 1, cutterNumber, 3, 0, 0)));
			if (!operateResult4.IsSuccess)
			{
				return OperateResult.CreateFailedResult<CutterInfo[]>(operateResult4);
			}
			List<byte[]> list = ExtraContentArray(operateResult.Content.RemoveBegin(10));
			List<byte[]> list2 = ExtraContentArray(operateResult2.Content.RemoveBegin(10));
			List<byte[]> list3 = ExtraContentArray(operateResult3.Content.RemoveBegin(10));
			List<byte[]> list4 = ExtraContentArray(operateResult4.Content.RemoveBegin(10));
			CutterInfo[] array = new CutterInfo[cutterNumber];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = new CutterInfo();
				array[i].LengthSharpOffset = GetFanucDouble(list[0], 14 + 8 * i);
				array[i].LengthWearOffset = GetFanucDouble(list2[0], 14 + 8 * i);
				array[i].RadiusSharpOffset = GetFanucDouble(list3[0], 14 + 8 * i);
				array[i].RadiusWearOffset = GetFanucDouble(list4[0], 14 + 8 * i);
			}
			return OperateResult.CreateSuccessResult(array);
		}

		/// <summary>
		/// 读取工件尺寸
		/// </summary>
		/// <returns>结果数据信息</returns>
		public OperateResult<double[]> ReadDeviceWorkPiecesSize()
		{
			return ReadSystemMacroValue(601, 20);
		}

		/// <summary>
		/// 读取指定的程序内容，目前还没有测试通
		/// </summary>
		/// <param name="program">程序号</param>
		/// <returns>程序内容</returns>
		[Obsolete]
		private OperateResult<string> ReadProgram(int program)
		{
			OperateResult<byte[]> operateResult = ReadFromCoreServer(BuildReadProgram(program));
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<string>(operateResult);
			}
			Console.WriteLine("等待第二次数据接收。");
			Thread.Sleep(100);
			OperateResult<byte[]> operateResult2 = ReadFromCoreServer("a0 a0 a0 a0 00 01 18 04 00 08 00 00 00 00 00 00 00 00".ToHexBytes());
			if (!operateResult2.IsSuccess)
			{
				return OperateResult.CreateFailedResult<string>(operateResult2);
			}
			string @string = Encoding.ASCII.GetString(operateResult2.Content, 10, operateResult2.Content.Length - 10);
			return OperateResult.CreateSuccessResult(@string);
		}

		/// <summary>
		/// 读取当前程序的前台路径
		/// </summary>
		/// <returns>程序的路径信息</returns>
		public OperateResult<string> ReadCurrentForegroundDir()
		{
			OperateResult<byte[]> operateResult = ReadFromCoreServer(BuildReadArray(BuildReadSingle(176, 1, 0, 0, 0, 0)));
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<string>(operateResult);
			}
			List<byte[]> list = ExtraContentArray(operateResult.Content.RemoveBegin(10));
			int num = 0;
			for (int i = 14; i < list[0].Length; i++)
			{
				if (list[0][i] == 0)
				{
					num = i;
					break;
				}
			}
			if (num == 0)
			{
				num = list[0].Length;
			}
			return OperateResult.CreateSuccessResult(Encoding.ASCII.GetString(list[0], 14, num - 14));
		}

		/// <summary>
		/// 设置指定路径为当前路径
		/// </summary>
		/// <param name="programName">程序名</param>
		/// <returns>结果信息</returns>
		public OperateResult SetDeviceProgsCurr(string programName)
		{
			OperateResult<string> operateResult = ReadCurrentForegroundDir();
			if (!operateResult.IsSuccess)
			{
				return operateResult;
			}
			byte[] array = new byte[256];
			Encoding.ASCII.GetBytes(operateResult.Content + programName).CopyTo(array, 0);
			OperateResult<byte[]> operateResult2 = ReadFromCoreServer(BuildReadArray(BuildWriteSingle(186, 0, 0, 0, 0, array)));
			if (!operateResult2.IsSuccess)
			{
				return OperateResult.CreateFailedResult<string>(operateResult2);
			}
			List<byte[]> list = ExtraContentArray(operateResult2.Content.RemoveBegin(10));
			int num = list[0][10] * 256 + list[0][11];
			if (num == 0)
			{
				return OperateResult.CreateSuccessResult();
			}
			return new OperateResult(num, StringResources.Language.UnknownError);
		}

		/// <summary>
		/// 读取机床的当前时间信息
		/// </summary>
		/// <returns>时间信息</returns>
		public OperateResult<DateTime> ReadCurrentDateTime()
		{
			OperateResult<double> operateResult = ReadSystemMacroValue(3011);
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<DateTime>(operateResult);
			}
			OperateResult<double> operateResult2 = ReadSystemMacroValue(3012);
			if (!operateResult2.IsSuccess)
			{
				return OperateResult.CreateFailedResult<DateTime>(operateResult2);
			}
			string text = Convert.ToInt32(operateResult.Content).ToString();
			string text2 = Convert.ToInt32(operateResult2.Content).ToString().PadLeft(6, '0');
			return OperateResult.CreateSuccessResult(new DateTime(int.Parse(text.Substring(0, 4)), int.Parse(text.Substring(4, 2)), int.Parse(text.Substring(6)), int.Parse(text2.Substring(0, 2)), int.Parse(text2.Substring(2, 2)), int.Parse(text2.Substring(4))));
		}

		/// <summary>
		/// 读取当前的已加工的零件数量
		/// </summary>
		/// <returns>已经加工的零件数量</returns>
		public OperateResult<int> ReadCurrentProduceCount()
		{
			OperateResult<double> operateResult = ReadSystemMacroValue(3901);
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<int>(operateResult);
			}
			return OperateResult.CreateSuccessResult(Convert.ToInt32(operateResult.Content));
		}

		/// <summary>
		/// 读取期望的加工的零件数量
		/// </summary>
		/// <returns>期望的加工的零件数量</returns>
		public OperateResult<int> ReadExpectProduceCount()
		{
			OperateResult<double> operateResult = ReadSystemMacroValue(3902);
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<int>(operateResult);
			}
			return OperateResult.CreateSuccessResult(Convert.ToInt32(operateResult.Content));
		}

		/// <summary>
		/// 构建读取一个命令的数据内容
		/// </summary>
		/// <param name="code">命令码</param>
		/// <param name="a">第一个参数内容</param>
		/// <param name="b">第二个参数内容</param>
		/// <param name="c">第三个参数内容</param>
		/// <param name="d">第四个参数内容</param>
		/// <param name="e">第五个参数内容</param>
		/// <returns>总报文信息</returns>
		public byte[] BuildReadSingle(ushort code, int a, int b, int c, int d, int e)
		{
			byte[] array = new byte[28];
			array[1] = 28;
			array[3] = 1;
			array[5] = 1;
			base.ByteTransform.TransByte(code).CopyTo(array, 6);
			base.ByteTransform.TransByte(a).CopyTo(array, 8);
			base.ByteTransform.TransByte(b).CopyTo(array, 12);
			base.ByteTransform.TransByte(c).CopyTo(array, 16);
			base.ByteTransform.TransByte(d).CopyTo(array, 20);
			base.ByteTransform.TransByte(e).CopyTo(array, 24);
			return array;
		}

		/// <summary>
		/// 创建写入byte[]数组的报文信息
		/// </summary>
		/// <param name="code">命令码</param>
		/// <param name="a">第一个参数内容</param>
		/// <param name="b">第二个参数内容</param>
		/// <param name="c">第三个参数内容</param>
		/// <param name="d">第四个参数内容</param>
		/// <param name="data">等待写入的byte数组信息</param>
		/// <returns>总报文信息</returns>
		public byte[] BuildWriteSingle(ushort code, int a, int b, int c, int d, byte[] data)
		{
			byte[] array = new byte[28 + data.Length];
			base.ByteTransform.TransByte((ushort)array.Length).CopyTo(array, 0);
			array[3] = 1;
			array[5] = 1;
			base.ByteTransform.TransByte(code).CopyTo(array, 6);
			base.ByteTransform.TransByte(a).CopyTo(array, 8);
			base.ByteTransform.TransByte(b).CopyTo(array, 12);
			base.ByteTransform.TransByte(c).CopyTo(array, 16);
			base.ByteTransform.TransByte(d).CopyTo(array, 20);
			base.ByteTransform.TransByte(data.Length).CopyTo(array, 24);
			if (data.Length != 0)
			{
				data.CopyTo(array, 28);
			}
			return array;
		}

		/// <summary>
		/// 创建写入单个double数组的报文信息
		/// </summary>
		/// <param name="code">功能码</param>
		/// <param name="a">第一个参数内容</param>
		/// <param name="b">第二个参数内容</param>
		/// <param name="c">第三个参数内容</param>
		/// <param name="d">第四个参数内容</param>
		/// <param name="data">等待写入的double数组信息</param>
		/// <returns>总报文信息</returns>
		public byte[] BuildWriteSingle(ushort code, int a, int b, int c, int d, double[] data)
		{
			byte[] array = new byte[data.Length * 8];
			for (int i = 0; i < data.Length; i++)
			{
				CreateFromFanucDouble(data[i]).CopyTo(array, 0);
			}
			return BuildWriteSingle(code, a, b, c, d, array);
		}

		/// <summary>
		/// 创建读取运行程序的报文信息
		/// </summary>
		/// <param name="program">程序号</param>
		/// <returns>总报文</returns>
		public byte[] BuildReadProgram(int program)
		{
			return "\r\na0 a0 a0 a0 00 01 15 01 02 04 00 00 00 01 4f 36\r\n30 30 32 2d 4f 36 30 30 32 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n00 00 00 00 00 00 00 00 00 00 00 00 00 00\r\n".ToHexBytes();
		}

		/// <summary>
		/// 创建多个命令报文的总报文信息
		/// </summary>
		/// <param name="commands">报文命令的数组</param>
		/// <returns>总报文信息</returns>
		public byte[] BuildReadArray(params byte[][] commands)
		{
			MemoryStream memoryStream = new MemoryStream();
			memoryStream.Write(new byte[10]
			{
				160,
				160,
				160,
				160,
				0,
				1,
				33,
				1,
				0,
				30
			}, 0, 10);
			memoryStream.Write(base.ByteTransform.TransByte((ushort)commands.Length), 0, 2);
			for (int i = 0; i < commands.Length; i++)
			{
				memoryStream.Write(commands[i], 0, commands[i].Length);
			}
			byte[] array = memoryStream.ToArray();
			base.ByteTransform.TransByte((ushort)(array.Length - 10)).CopyTo(array, 8);
			return array;
		}

		/// <summary>
		/// 从机床返回的数据里解析出实际的数据内容，去除了一些多余的信息报文。
		/// </summary>
		/// <param name="content">返回的报文信息</param>
		/// <returns>解析之后的报文信息</returns>
		public List<byte[]> ExtraContentArray(byte[] content)
		{
			List<byte[]> list = new List<byte[]>();
			int num = base.ByteTransform.TransUInt16(content, 0);
			int num2 = 2;
			for (int i = 0; i < num; i++)
			{
				ushort num3 = base.ByteTransform.TransUInt16(content, num2);
				list.Add(content.SelectMiddle(num2 + 2, num3 - 2));
				num2 += num3;
			}
			return list;
		}
	}
}
