namespace HslCommunication.CNC.Fanuc
{
	/// <summary>
	/// CNC的运行状态
	/// </summary>
	public enum CNCRunStatus
	{
		RESET,
		STOP,
		HOLD,
		START,
		MSTR
	}
}
