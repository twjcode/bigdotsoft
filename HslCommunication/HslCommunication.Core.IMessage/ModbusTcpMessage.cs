namespace HslCommunication.Core.IMessage
{
	/// <summary>
	/// Modbus-Tcp协议支持的消息解析类
	/// </summary>
	public class ModbusTcpMessage : INetMessage
	{
		/// <inheritdoc cref="P:HslCommunication.Core.IMessage.INetMessage.ProtocolHeadBytesLength" />
		public int ProtocolHeadBytesLength => 8;

		/// <inheritdoc cref="P:HslCommunication.Core.IMessage.INetMessage.HeadBytes" />
		public byte[] HeadBytes
		{
			get;
			set;
		}

		/// <inheritdoc cref="P:HslCommunication.Core.IMessage.INetMessage.ContentBytes" />
		public byte[] ContentBytes
		{
			get;
			set;
		}

		/// <inheritdoc cref="P:HslCommunication.Core.IMessage.INetMessage.SendBytes" />
		public byte[] SendBytes
		{
			get;
			set;
		}

		/// <inheritdoc cref="M:HslCommunication.Core.IMessage.INetMessage.GetContentLengthByHeadBytes" />
		public int GetContentLengthByHeadBytes()
		{
			if (HeadBytes?.Length >= ProtocolHeadBytesLength)
			{
				int num = HeadBytes[4] * 256 + HeadBytes[5];
				if (num == 0)
				{
					byte[] array = new byte[ProtocolHeadBytesLength - 1];
					for (int i = 0; i < array.Length; i++)
					{
						array[i] = HeadBytes[i + 1];
					}
					HeadBytes = array;
					return HeadBytes[5] * 256 + HeadBytes[6] - 1;
				}
				return num - 2;
			}
			return 0;
		}

		/// <inheritdoc cref="M:HslCommunication.Core.IMessage.INetMessage.CheckHeadBytesLegal(System.Byte[])" />
		public bool CheckHeadBytesLegal(byte[] token)
		{
			if (HeadBytes == null)
			{
				return false;
			}
			if (SendBytes[0] != HeadBytes[0] || SendBytes[1] != HeadBytes[1])
			{
				return false;
			}
			return HeadBytes[2] == 0 && HeadBytes[3] == 0;
		}

		/// <inheritdoc cref="M:HslCommunication.Core.IMessage.INetMessage.GetHeadBytesIdentity" />
		public int GetHeadBytesIdentity()
		{
			return HeadBytes[0] * 256 + HeadBytes[1];
		}
	}
}
