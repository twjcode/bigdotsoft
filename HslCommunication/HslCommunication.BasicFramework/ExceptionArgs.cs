using System;

namespace HslCommunication.BasicFramework
{
	/// <summary>
	/// 异常消息基类
	/// </summary>
	[Serializable]
	public abstract class ExceptionArgs
	{
		/// <inheritdoc />
		public virtual string Message => string.Empty;
	}
}
