using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using HslCommunication.Controls;

namespace HslCommunication.BasicFramework
{
	/// <summary>
	/// 用来测试版软件授权的窗口
	/// </summary>
	public class FormAuthorize : Form
	{
		private SoftAuthorize softAuthorize = null;

		private Func<string, string> Encrypt = null;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private IContainer components = null;

		private TextBox textBox2;

		private TextBox textBox1;

		private Label label2;

		private Label label1;

		private UserButton userButton1;

		private LinkLabel linkLabel1;

		private string AboutCode
		{
			get;
			set;
		} = "";


		/// <summary>
		/// 实例化授权注册窗口
		/// </summary>
		/// <param name="authorize"></param>
		/// <param name="aboutCode">提示关于怎么获取注册码的信息</param>
		/// <param name="encrypt">加密的方法</param>
		public FormAuthorize(SoftAuthorize authorize, string aboutCode, Func<string, string> encrypt)
		{
			InitializeComponent();
			softAuthorize = authorize;
			AboutCode = aboutCode;
			Encrypt = encrypt;
		}

		private void FormAuthorize_Load(object sender, EventArgs e)
		{
			textBox1.Text = softAuthorize.GetMachineCodeString();
		}

		private void userButton1_Click(object sender, EventArgs e)
		{
			if (softAuthorize.CheckAuthorize(textBox2.Text, Encrypt))
			{
				base.DialogResult = DialogResult.OK;
			}
			else
			{
				MessageBox.Show("注册码不正确");
			}
		}

		private void linkLabel1_Click(object sender, EventArgs e)
		{
			MessageBox.Show(AboutCode);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			textBox2 = new System.Windows.Forms.TextBox();
			textBox1 = new System.Windows.Forms.TextBox();
			label2 = new System.Windows.Forms.Label();
			label1 = new System.Windows.Forms.Label();
			linkLabel1 = new System.Windows.Forms.LinkLabel();
			userButton1 = new HslCommunication.Controls.UserButton();
			SuspendLayout();
			textBox2.Font = new System.Drawing.Font("宋体", 15f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 134);
			textBox2.Location = new System.Drawing.Point(124, 78);
			textBox2.Name = "textBox2";
			textBox2.Size = new System.Drawing.Size(292, 30);
			textBox2.TabIndex = 7;
			textBox1.Font = new System.Drawing.Font("宋体", 15f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 134);
			textBox1.Location = new System.Drawing.Point(124, 35);
			textBox1.Name = "textBox1";
			textBox1.ReadOnly = true;
			textBox1.Size = new System.Drawing.Size(292, 30);
			textBox1.TabIndex = 6;
			label2.AutoSize = true;
			label2.Font = new System.Drawing.Font("微软雅黑", 18f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 134);
			label2.Location = new System.Drawing.Point(12, 77);
			label2.Name = "label2";
			label2.Size = new System.Drawing.Size(110, 31);
			label2.TabIndex = 5;
			label2.Text = "注册码：";
			label1.AutoSize = true;
			label1.Font = new System.Drawing.Font("微软雅黑", 18f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 134);
			label1.Location = new System.Drawing.Point(12, 34);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(110, 31);
			label1.TabIndex = 4;
			label1.Text = "机器码：";
			linkLabel1.Font = new System.Drawing.Font("微软雅黑", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 134);
			linkLabel1.Location = new System.Drawing.Point(295, 174);
			linkLabel1.Name = "linkLabel1";
			linkLabel1.Size = new System.Drawing.Size(137, 23);
			linkLabel1.TabIndex = 9;
			linkLabel1.TabStop = true;
			linkLabel1.Text = "关于注册码";
			linkLabel1.TextAlign = System.Drawing.ContentAlignment.TopRight;
			linkLabel1.Click += new System.EventHandler(linkLabel1_Click);
			userButton1.BackColor = System.Drawing.Color.Transparent;
			userButton1.CustomerInformation = "";
			userButton1.EnableColor = System.Drawing.Color.FromArgb(190, 190, 190);
			userButton1.Font = new System.Drawing.Font("微软雅黑", 9f);
			userButton1.Location = new System.Drawing.Point(135, 130);
			userButton1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			userButton1.Name = "userButton1";
			userButton1.Size = new System.Drawing.Size(166, 33);
			userButton1.TabIndex = 8;
			userButton1.UIText = "注册";
			userButton1.Click += new System.EventHandler(userButton1_Click);
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 12f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(435, 193);
			base.Controls.Add(linkLabel1);
			base.Controls.Add(userButton1);
			base.Controls.Add(textBox2);
			base.Controls.Add(textBox1);
			base.Controls.Add(label2);
			base.Controls.Add(label1);
			base.MaximizeBox = false;
			MaximumSize = new System.Drawing.Size(451, 232);
			base.MinimizeBox = false;
			MinimumSize = new System.Drawing.Size(451, 232);
			base.Name = "FormAuthorize";
			base.ShowIcon = false;
			base.ShowInTaskbar = false;
			base.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			Text = "注册软件";
			base.TopMost = true;
			base.Load += new System.EventHandler(FormAuthorize_Load);
			ResumeLayout(false);
			PerformLayout();
		}
	}
}
