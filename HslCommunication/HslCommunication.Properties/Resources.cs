using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace HslCommunication.Properties
{
	/// <summary>
	///   一个强类型的资源类，用于查找本地化的字符串等。
	/// </summary>
	[GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
	[DebuggerNonUserCode]
	[CompilerGenerated]
	internal class Resources
	{
		private static ResourceManager resourceMan;

		private static CultureInfo resourceCulture;

		/// <summary>
		///   返回此类使用的缓存的 ResourceManager 实例。
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal static ResourceManager ResourceManager
		{
			get
			{
				if (resourceMan == null)
				{
					ResourceManager resourceManager = resourceMan = new ResourceManager("HslCommunication.Properties.Resources", typeof(Resources).Assembly);
				}
				return resourceMan;
			}
		}

		/// <summary>
		///   使用此强类型资源类，为所有资源查找
		///   重写当前线程的 CurrentUICulture 属性。
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal static CultureInfo Culture
		{
			get
			{
				return resourceCulture;
			}
			set
			{
				resourceCulture = value;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap abstr1
		{
			get
			{
				object @object = ResourceManager.GetObject("abstr1", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap abstr11
		{
			get
			{
				object @object = ResourceManager.GetObject("abstr11", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap Activity_16xLG
		{
			get
			{
				object @object = ResourceManager.GetObject("Activity_16xLG", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap AddressViewer
		{
			get
			{
				object @object = ResourceManager.GetObject("AddressViewer", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap alipay
		{
			get
			{
				object @object = ResourceManager.GetObject("alipay", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap asset_progressBar_24x24_on
		{
			get
			{
				object @object = ResourceManager.GetObject("asset_progressBar_24x24_on", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap AudioRecording
		{
			get
			{
				object @object = ResourceManager.GetObject("AudioRecording", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap brackets_Square_16xMD
		{
			get
			{
				object @object = ResourceManager.GetObject("brackets_Square_16xMD", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap Class_489
		{
			get
			{
				object @object = ResourceManager.GetObject("Class_489", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap ClassIcon
		{
			get
			{
				object @object = ResourceManager.GetObject("ClassIcon", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap Cloud_16xLG
		{
			get
			{
				object @object = ResourceManager.GetObject("Cloud_16xLG", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap Copy_6524
		{
			get
			{
				object @object = ResourceManager.GetObject("Copy_6524", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap Delegate_8339
		{
			get
			{
				object @object = ResourceManager.GetObject("Delegate_8339", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap docview_xaml_on_16x16
		{
			get
			{
				object @object = ResourceManager.GetObject("docview_xaml_on_16x16", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap Enum_582
		{
			get
			{
				object @object = ResourceManager.GetObject("Enum_582", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap Event_594
		{
			get
			{
				object @object = ResourceManager.GetObject("Event_594", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap Event_594_exp
		{
			get
			{
				object @object = ResourceManager.GetObject("Event_594_exp", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap glasses_16xLG
		{
			get
			{
				object @object = ResourceManager.GetObject("glasses_16xLG", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap grid_Data_16xLG
		{
			get
			{
				object @object = ResourceManager.GetObject("grid_Data_16xLG", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap lightningBolt_16xLG
		{
			get
			{
				object @object = ResourceManager.GetObject("lightningBolt_16xLG", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap ListView_687
		{
			get
			{
				object @object = ResourceManager.GetObject("ListView_687", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap Method_636
		{
			get
			{
				object @object = ResourceManager.GetObject("Method_636", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap mm_facetoface_collect_qrcode_1525331158525
		{
			get
			{
				object @object = ResourceManager.GetObject("mm_facetoface_collect_qrcode_1525331158525", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap Module_648
		{
			get
			{
				object @object = ResourceManager.GetObject("Module_648", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap PropertyIcon
		{
			get
			{
				object @object = ResourceManager.GetObject("PropertyIcon", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap qrcode_for_gh_319218678954_258
		{
			get
			{
				object @object = ResourceManager.GetObject("qrcode_for_gh_319218678954_258", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap Soundfile_461
		{
			get
			{
				object @object = ResourceManager.GetObject("Soundfile_461", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap Structure_507
		{
			get
			{
				object @object = ResourceManager.GetObject("Structure_507", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap TabControl_707
		{
			get
			{
				object @object = ResourceManager.GetObject("TabControl_707", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap Table_748
		{
			get
			{
				object @object = ResourceManager.GetObject("Table_748", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap Tag_7213
		{
			get
			{
				object @object = ResourceManager.GetObject("Tag_7213", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap Textfile_818_16x
		{
			get
			{
				object @object = ResourceManager.GetObject("Textfile_818_16x", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap TreeView_713
		{
			get
			{
				object @object = ResourceManager.GetObject("TreeView_713", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap usbcontroller
		{
			get
			{
				object @object = ResourceManager.GetObject("usbcontroller", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap UseCaseDiagramFile_usecasediagram_13447_16x
		{
			get
			{
				object @object = ResourceManager.GetObject("UseCaseDiagramFile_usecasediagram_13447_16x", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap VirtualMachine
		{
			get
			{
				object @object = ResourceManager.GetObject("VirtualMachine", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap WebForm_ASPX__815_16x
		{
			get
			{
				object @object = ResourceManager.GetObject("WebForm(ASPX)_815_16x", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap WebFormTemplate_11274_16x_color
		{
			get
			{
				object @object = ResourceManager.GetObject("WebFormTemplate_11274_16x_color", resourceCulture);
				return (Bitmap)@object;
			}
		}

		/// <summary>
		///   查找 System.Drawing.Bitmap 类型的本地化资源。
		/// </summary>
		internal static Bitmap WindowsForm_817_16x
		{
			get
			{
				object @object = ResourceManager.GetObject("WindowsForm_817_16x", resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal Resources()
		{
		}
	}
}
