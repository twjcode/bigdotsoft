using System;
using System.IO;
using System.IO.Ports;
using System.Text;
using System.Threading;
using HslCommunication.Core;
using HslCommunication.LogNet;

namespace HslCommunication.Serial
{
	/// <summary>
	/// 所有串行通信类的基类，提供了一些基础的服务，核心的通信实现<br />
	/// The base class of all serial communication classes provides some basic services for the core communication implementation
	/// </summary>
	public class SerialBase : IDisposable
	{
		/// <inheritdoc cref="F:HslCommunication.Core.Net.NetworkDoubleBase.LogMsgFormatBinary" />
		protected bool LogMsgFormatBinary = true;

		private bool disposedValue = false;

		/// <summary>
		/// 串口交互的核心
		/// </summary>
		protected SerialPort sP_ReadData = null;

		private SimpleHybirdLock hybirdLock;

		private ILogNet logNet;

		private int receiveTimeout = 5000;

		private int sleepTime = 20;

		private bool isClearCacheBeforeRead = false;

		private int connectErrorCount = 0;

		/// <inheritdoc cref="P:HslCommunication.Core.Net.NetworkBase.LogNet" />
		public ILogNet LogNet
		{
			get
			{
				return logNet;
			}
			set
			{
				logNet = value;
			}
		}

		/// <summary>
		/// 获取或设置一个值，该值指示在串行通信中是否启用请求发送 (RTS) 信号。<br />
		/// Gets or sets a value indicating whether the request sending (RTS) signal is enabled in serial communication.
		/// </summary>
		public bool RtsEnable
		{
			get
			{
				return sP_ReadData.RtsEnable;
			}
			set
			{
				sP_ReadData.RtsEnable = value;
			}
		}

		/// <summary>
		/// 接收数据的超时时间，默认5000ms<br />
		/// Timeout for receiving data, default is 5000ms
		/// </summary>
		public int ReceiveTimeout
		{
			get
			{
				return receiveTimeout;
			}
			set
			{
				receiveTimeout = value;
			}
		}

		/// <summary>
		/// 连续串口缓冲数据检测的间隔时间，默认20ms，该值越小，通信速度越快，但是越不稳定。<br />
		/// Continuous serial port buffer data detection interval, the default 20ms, the smaller the value, the faster the communication, but the more unstable.
		/// </summary>
		public int SleepTime
		{
			get
			{
				return sleepTime;
			}
			set
			{
				if (value > 0)
				{
					sleepTime = value;
				}
			}
		}

		/// <summary>
		/// 是否在发送数据前清空缓冲数据，默认是false<br />
		/// Whether to empty the buffer before sending data, the default is false
		/// </summary>
		public bool IsClearCacheBeforeRead
		{
			get
			{
				return isClearCacheBeforeRead;
			}
			set
			{
				isClearCacheBeforeRead = value;
			}
		}

		/// <summary>
		/// 当前连接串口信息的端口号名称<br />
		/// The port name of the current connection serial port information
		/// </summary>
		public string PortName
		{
			get;
			private set;
		}

		/// <summary>
		/// 当前连接串口信息的波特率<br />
		/// Baud rate of current connection serial port information
		/// </summary>
		public int BaudRate
		{
			get;
			private set;
		}

		/// <summary>
		/// 实例化一个无参的构造方法<br />
		/// Instantiate a parameterless constructor
		/// </summary>
		public SerialBase()
		{
			sP_ReadData = new SerialPort();
			hybirdLock = new SimpleHybirdLock();
		}

		/// <summary>
		/// 初始化串口信息，9600波特率，8位数据位，1位停止位，无奇偶校验<br />
		/// Initial serial port information, 9600 baud rate, 8 data bits, 1 stop bit, no parity
		/// </summary>
		/// <param name="portName">端口号信息，例如"COM3"</param>
		public void SerialPortInni(string portName)
		{
			SerialPortInni(portName, 9600);
		}

		/// <summary>
		/// 初始化串口信息，波特率，8位数据位，1位停止位，无奇偶校验<br />
		/// Initializes serial port information, baud rate, 8-bit data bit, 1-bit stop bit, no parity
		/// </summary>
		/// <param name="portName">端口号信息，例如"COM3"</param>
		/// <param name="baudRate">波特率</param>
		public void SerialPortInni(string portName, int baudRate)
		{
			SerialPortInni(portName, baudRate, 8, StopBits.One, Parity.None);
		}

		/// <summary>
		/// 初始化串口信息，波特率，数据位，停止位，奇偶校验需要全部自己来指定<br />
		/// Start serial port information, baud rate, data bit, stop bit, parity all need to be specified
		/// </summary>
		/// <param name="portName">端口号信息，例如"COM3"</param>
		/// <param name="baudRate">波特率</param>
		/// <param name="dataBits">数据位</param>
		/// <param name="stopBits">停止位</param>
		/// <param name="parity">奇偶校验</param>
		public void SerialPortInni(string portName, int baudRate, int dataBits, StopBits stopBits, Parity parity)
		{
			if (!sP_ReadData.IsOpen)
			{
				sP_ReadData.PortName = portName;
				sP_ReadData.BaudRate = baudRate;
				sP_ReadData.DataBits = dataBits;
				sP_ReadData.StopBits = stopBits;
				sP_ReadData.Parity = parity;
				PortName = sP_ReadData.PortName;
				BaudRate = sP_ReadData.BaudRate;
			}
		}

		/// <summary>
		/// 根据自定义初始化方法进行初始化串口信息<br />
		/// Initialize the serial port information according to the custom initialization method
		/// </summary>
		/// <param name="initi">初始化的委托方法</param>
		public void SerialPortInni(Action<SerialPort> initi)
		{
			if (!sP_ReadData.IsOpen)
			{
				sP_ReadData.PortName = "COM5";
				sP_ReadData.BaudRate = 9600;
				sP_ReadData.DataBits = 8;
				sP_ReadData.StopBits = StopBits.One;
				sP_ReadData.Parity = Parity.None;
				initi(sP_ReadData);
				PortName = sP_ReadData.PortName;
				BaudRate = sP_ReadData.BaudRate;
			}
		}

		/// <summary>
		/// 打开一个新的串行端口连接<br />
		/// Open a new serial port connection
		/// </summary>
		public OperateResult Open()
		{
			try
			{
				if (!sP_ReadData.IsOpen)
				{
					sP_ReadData.Open();
					return InitializationOnOpen();
				}
				return OperateResult.CreateSuccessResult();
			}
			catch (Exception ex)
			{
				if (connectErrorCount < 100000000)
				{
					connectErrorCount++;
				}
				return new OperateResult(-connectErrorCount, ex.Message);
			}
		}

		/// <summary>
		/// 获取一个值，指示串口是否处于打开状态<br />
		/// Gets a value indicating whether the serial port is open
		/// </summary>
		/// <returns>是或否</returns>
		public bool IsOpen()
		{
			return sP_ReadData.IsOpen;
		}

		/// <summary>
		/// 关闭当前的串口连接<br />
		/// Close the current serial connection
		/// </summary>
		public void Close()
		{
			if (sP_ReadData.IsOpen)
			{
				ExtraOnClose();
				sP_ReadData.Close();
			}
		}

		/// <summary>
		/// 将原始的字节数据发送到串口，然后从串口接收一条数据。<br />
		/// The raw byte data is sent to the serial port, and then a piece of data is received from the serial port.
		/// </summary>
		/// <param name="send">发送的原始字节数据</param>
		/// <returns>带接收字节的结果对象</returns>
		public OperateResult<byte[]> ReadBase(byte[] send)
		{
			return ReadBase(send, sendOnly: false);
		}

		/// <summary>
		/// 将原始的字节数据发送到串口，然后从串口接收一条数据。<br />
		/// The raw byte data is sent to the serial port, and then a piece of data is received from the serial port.
		/// </summary>
		/// <param name="send">发送的原始字节数据</param>
		/// <param name="sendOnly">是否只是发送，如果为true, 不需要等待数据返回，如果为false, 需要等待数据返回</param>
		/// <returns>带接收字节的结果对象</returns>
		public OperateResult<byte[]> ReadBase(byte[] send, bool sendOnly)
		{
			LogNet?.WriteDebug(ToString(), StringResources.Language.Send + " : " + (LogMsgFormatBinary ? send.ToHexString(' ') : Encoding.ASCII.GetString(send)));
			hybirdLock.Enter();
			OperateResult operateResult = Open();
			if (!operateResult.IsSuccess)
			{
				hybirdLock.Leave();
				return OperateResult.CreateFailedResult<byte[]>(operateResult);
			}
			if (IsClearCacheBeforeRead)
			{
				ClearSerialCache();
			}
			OperateResult operateResult2 = SPSend(sP_ReadData, send);
			if (!operateResult2.IsSuccess)
			{
				hybirdLock.Leave();
				return OperateResult.CreateFailedResult<byte[]>(operateResult2);
			}
			if (sendOnly)
			{
				hybirdLock.Leave();
				return OperateResult.CreateSuccessResult(new byte[0]);
			}
			OperateResult<byte[]> operateResult3 = SPReceived(sP_ReadData, awaitData: true);
			hybirdLock.Leave();
			LogNet?.WriteDebug(ToString(), StringResources.Language.Receive + " : " + (LogMsgFormatBinary ? operateResult3.Content.ToHexString(' ') : Encoding.ASCII.GetString(operateResult3.Content)));
			return operateResult3;
		}

		/// <summary>
		/// 清除串口缓冲区的数据，并返回该数据，如果缓冲区没有数据，返回的字节数组长度为0<br />
		/// The number sent clears the data in the serial port buffer and returns that data, or if there is no data in the buffer, the length of the byte array returned is 0
		/// </summary>
		/// <returns>是否操作成功的方法</returns>
		public OperateResult<byte[]> ClearSerialCache()
		{
			return SPReceived(sP_ReadData, awaitData: false);
		}

		/// <inheritdoc cref="M:HslCommunication.Core.Net.NetworkDoubleBase.InitializationOnConnect(System.Net.Sockets.Socket)" />
		protected virtual OperateResult InitializationOnOpen()
		{
			return OperateResult.CreateSuccessResult();
		}

		/// <inheritdoc cref="M:HslCommunication.Core.Net.NetworkDoubleBase.ExtraOnDisconnect(System.Net.Sockets.Socket)" />
		protected virtual OperateResult ExtraOnClose()
		{
			return OperateResult.CreateSuccessResult();
		}

		/// <summary>
		/// 发送数据到串口去。<br />
		/// Send data to serial port.
		/// </summary>
		/// <param name="serialPort">串口对象</param>
		/// <param name="data">字节数据</param>
		/// <returns>是否发送成功</returns>
		protected virtual OperateResult SPSend(SerialPort serialPort, byte[] data)
		{
			if (data != null && data.Length != 0)
			{
				if (!Authorization.nzugaydgwadawdibbas())
				{
					return new OperateResult<byte[]>(StringResources.Language.AuthorizationFailed);
				}
				try
				{
					serialPort.Write(data, 0, data.Length);
					return OperateResult.CreateSuccessResult();
				}
				catch (Exception ex)
				{
					if (connectErrorCount < 100000000)
					{
						connectErrorCount++;
					}
					return new OperateResult(-connectErrorCount, ex.Message);
				}
			}
			return OperateResult.CreateSuccessResult();
		}

		/// <summary>
		/// 从串口接收一串字节数据信息，直到没有数据为止，如果参数awaitData为false, 第一轮接收没有数据则返回<br />
		/// Receives a string of bytes of data information from the serial port until there is no data, and returns if the parameter awaitData is false
		/// </summary>
		/// <param name="serialPort">串口对象</param>
		/// <param name="awaitData">是否必须要等待数据返回</param>
		/// <returns>结果数据对象</returns>
		protected virtual OperateResult<byte[]> SPReceived(SerialPort serialPort, bool awaitData)
		{
			if (!Authorization.nzugaydgwadawdibbas())
			{
				return new OperateResult<byte[]>(StringResources.Language.AuthorizationFailed);
			}
			byte[] array = new byte[1024];
			MemoryStream memoryStream = new MemoryStream();
			DateTime now = DateTime.Now;
			while (true)
			{
				Thread.Sleep(sleepTime);
				try
				{
					if (serialPort.BytesToRead < 1)
					{
						if ((DateTime.Now - now).TotalMilliseconds > (double)ReceiveTimeout)
						{
							memoryStream.Dispose();
							if (connectErrorCount < 100000000)
							{
								connectErrorCount++;
							}
							return new OperateResult<byte[]>(-connectErrorCount, $"Time out: {ReceiveTimeout}");
						}
						if (memoryStream.Length > 0 || !awaitData)
						{
							break;
						}
						continue;
					}
					int count = serialPort.Read(array, 0, array.Length);
					memoryStream.Write(array, 0, count);
					continue;
				}
				catch (Exception ex)
				{
					memoryStream.Dispose();
					if (connectErrorCount < 100000000)
					{
						connectErrorCount++;
					}
					return new OperateResult<byte[]>(-connectErrorCount, ex.Message);
				}
			}
			byte[] value = memoryStream.ToArray();
			memoryStream.Dispose();
			connectErrorCount = 0;
			return OperateResult.CreateSuccessResult(value);
		}

		/// <summary>
		/// 释放当前的对象
		/// </summary>
		/// <param name="disposing">是否在</param>
		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					hybirdLock?.Dispose();
					sP_ReadData?.Dispose();
				}
				disposedValue = true;
			}
		}

		/// <summary>
		/// 释放当前的对象
		/// </summary>
		public void Dispose()
		{
			Dispose(disposing: true);
		}

		/// <inheritdoc />
		public override string ToString()
		{
			return $"SerialBase[{sP_ReadData.PortName},{sP_ReadData.BaudRate},{sP_ReadData.DataBits},{sP_ReadData.StopBits},{sP_ReadData.Parity}]";
		}
	}
}
