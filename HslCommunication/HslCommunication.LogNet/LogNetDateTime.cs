using System;
using System.Globalization;
using System.IO;

namespace HslCommunication.LogNet
{
	/// <summary>
	/// 一个日志组件，可以根据时间来区分不同的文件存储<br />
	/// A log component that can distinguish different file storages based on time
	/// </summary>
	/// <remarks>
	/// 此日志实例将根据日期时间来进行分类，支持的时间分类如下：
	/// <list type="number">
	/// <item>小时</item>
	/// <item>天</item>
	/// <item>周</item>
	/// <item>月份</item>
	/// <item>季度</item>
	/// <item>年份</item>
	/// </list>
	/// </remarks>
	/// <example>
	/// <code lang="cs" source="HslCommunication_Net45.Test\Documentation\Samples\LogNet\LogNetSingle.cs" region="Example3" title="日期存储实例化" />
	/// <code lang="cs" source="HslCommunication_Net45.Test\Documentation\Samples\LogNet\LogNetSingle.cs" region="Example4" title="基本的使用" />
	/// <code lang="cs" source="HslCommunication_Net45.Test\Documentation\Samples\LogNet\LogNetSingle.cs" region="Example5" title="所有日志不存储" />
	/// <code lang="cs" source="HslCommunication_Net45.Test\Documentation\Samples\LogNet\LogNetSingle.cs" region="Example6" title="仅存储ERROR等级" />
	/// <code lang="cs" source="HslCommunication_Net45.Test\Documentation\Samples\LogNet\LogNetSingle.cs" region="Example7" title="不指定路径" />
	/// </example>
	public class LogNetDateTime : LogNetBase, ILogNet, IDisposable
	{
		private string fileName = string.Empty;

		private string filePath = string.Empty;

		private GenerateMode generateMode = GenerateMode.ByEveryYear;

		/// <summary>
		/// 实例化一个根据时间存储的日志组件，需要指定每个文件的存储时间范围<br />
		/// Instantiate a log component based on time, you need to specify the storage time range for each file
		/// </summary>
		/// <param name="filePath">文件存储的路径</param>
		/// <param name="generateMode">存储文件的间隔</param>
		public LogNetDateTime(string filePath, GenerateMode generateMode = GenerateMode.ByEveryYear)
		{
			this.filePath = filePath;
			this.generateMode = generateMode;
			base.LogSaveMode = LogSaveMode.Time;
			if (!string.IsNullOrEmpty(filePath) && !Directory.Exists(filePath))
			{
				Directory.CreateDirectory(filePath);
			}
		}

		/// <inheritdoc />
		protected override string GetFileSaveName()
		{
			if (string.IsNullOrEmpty(filePath))
			{
				return string.Empty;
			}
			switch (generateMode)
			{
			case GenerateMode.ByEveryHour:
				return Path.Combine(filePath, "Logs_" + DateTime.Now.ToString("yyyyMMdd_HH") + ".txt");
			case GenerateMode.ByEveryDay:
				return Path.Combine(filePath, "Logs_" + DateTime.Now.ToString("yyyyMMdd") + ".txt");
			case GenerateMode.ByEveryWeek:
			{
				GregorianCalendar gregorianCalendar = new GregorianCalendar();
				int weekOfYear = gregorianCalendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
				return Path.Combine(filePath, "Logs_" + DateTime.Now.Year + "_W" + weekOfYear + ".txt");
			}
			case GenerateMode.ByEveryMonth:
				return Path.Combine(filePath, "Logs_" + DateTime.Now.ToString("yyyy_MM") + ".txt");
			case GenerateMode.ByEverySeason:
				return Path.Combine(filePath, "Logs_" + DateTime.Now.Year + "_Q" + (DateTime.Now.Month / 3 + 1) + ".txt");
			case GenerateMode.ByEveryYear:
				return Path.Combine(filePath, "Logs_" + DateTime.Now.Year + ".txt");
			default:
				return string.Empty;
			}
		}

		/// <inheritdoc cref="M:HslCommunication.LogNet.LogNetFileSize.GetExistLogFileNames" />
		public string[] GetExistLogFileNames()
		{
			if (!string.IsNullOrEmpty(filePath))
			{
				return Directory.GetFiles(filePath, "Logs_*.txt");
			}
			return new string[0];
		}

		/// <inheritdoc />
		public override string ToString()
		{
			return $"LogNetDateTime[{generateMode}]";
		}
	}
}
