using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HslCommunication.BasicFramework;
using HslCommunication.Core;
using HslCommunication.Core.Address;
using HslCommunication.Core.Net;

namespace HslCommunication.ModBus
{
	/// <summary>
	/// Modbus-Udp协议的客户端通讯类，方便的和服务器进行数据交互，支持标准的功能码，也支持扩展的功能码实现，地址采用富文本的形式，详细见备注说明<br />
	/// The client communication class of Modbus-Udp protocol is convenient for data interaction with the server. It supports standard function codes and also supports extended function codes. 
	/// The address is in rich text. For details, see the remarks.
	/// </summary>
	/// <remarks>
	/// <inheritdoc cref="T:HslCommunication.ModBus.ModbusTcpNet" path="remarks" />
	/// </remarks>
	/// <example>
	/// <inheritdoc cref="T:HslCommunication.ModBus.ModbusTcpNet" path="example" />
	/// </example>
	public class ModbusUdpNet : NetworkUdpDeviceBase
	{
		private byte station = 1;

		private SoftIncrementCount softIncrementCount;

		private bool isAddressStartWithZero = true;

		/// <inheritdoc cref="P:HslCommunication.ModBus.ModbusTcpNet.AddressStartWithZero" />
		public bool AddressStartWithZero
		{
			get
			{
				return isAddressStartWithZero;
			}
			set
			{
				isAddressStartWithZero = value;
			}
		}

		/// <inheritdoc cref="P:HslCommunication.ModBus.ModbusTcpNet.Station" />
		public byte Station
		{
			get
			{
				return station;
			}
			set
			{
				station = value;
			}
		}

		/// <inheritdoc cref="P:HslCommunication.ModBus.ModbusTcpNet.DataFormat" />
		public DataFormat DataFormat
		{
			get
			{
				return base.ByteTransform.DataFormat;
			}
			set
			{
				base.ByteTransform.DataFormat = value;
			}
		}

		/// <inheritdoc cref="P:HslCommunication.ModBus.ModbusTcpNet.IsStringReverse" />
		public bool IsStringReverse
		{
			get
			{
				return base.ByteTransform.IsStringReverseByteWord;
			}
			set
			{
				base.ByteTransform.IsStringReverseByteWord = value;
			}
		}

		/// <inheritdoc cref="P:HslCommunication.ModBus.ModbusTcpNet.MessageId" />
		public SoftIncrementCount MessageId => softIncrementCount;

		/// <summary>
		/// 实例化一个MOdbus-Udp协议的客户端对象<br />
		/// Instantiate a client object of the MOdbus-Udp protocol
		/// </summary>
		public ModbusUdpNet()
		{
			base.ByteTransform = new ReverseWordTransform();
			softIncrementCount = new SoftIncrementCount(65535L, 0L);
			base.WordLength = 1;
			station = 1;
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusTcpNet.#ctor(System.String,System.Int32,System.Byte)" />
		public ModbusUdpNet(string ipAddress, int port = 502, byte station = 1)
		{
			base.ByteTransform = new ReverseWordTransform();
			softIncrementCount = new SoftIncrementCount(65535L, 0L);
			IpAddress = ipAddress;
			Port = port;
			base.WordLength = 1;
			this.station = station;
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusTcpNet.ReadCoil(System.String)" />
		public OperateResult<bool> ReadCoil(string address)
		{
			return ReadBool(address);
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusTcpNet.ReadCoil(System.String,System.UInt16)" />
		public OperateResult<bool[]> ReadCoil(string address, ushort length)
		{
			return ReadBool(address, length);
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusTcpNet.ReadDiscrete(System.String)" />
		public OperateResult<bool> ReadDiscrete(string address)
		{
			return ByteTransformHelper.GetResultFromArray(ReadDiscrete(address, 1));
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusUdpNet.ReadDiscrete(System.String,System.UInt16)" />
		public OperateResult<bool[]> ReadDiscrete(string address, ushort length)
		{
			OperateResult<byte[]> operateResult = ModbusInfo.BuildReadModbusCommand(address, length, Station, AddressStartWithZero, 2);
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<bool[]>(operateResult);
			}
			OperateResult<byte[]> operateResult2 = ReadFromCoreServer(ModbusInfo.PackCommandToTcp(operateResult.Content, (ushort)softIncrementCount.GetCurrentValue()));
			if (!operateResult2.IsSuccess)
			{
				return OperateResult.CreateFailedResult<bool[]>(operateResult2);
			}
			OperateResult<byte[]> operateResult3 = ModbusInfo.ExtractActualData(ModbusInfo.ExplodeTcpCommandToCore(operateResult2.Content));
			if (!operateResult3.IsSuccess)
			{
				return OperateResult.CreateFailedResult<bool[]>(operateResult3);
			}
			return OperateResult.CreateSuccessResult(SoftBasic.ByteToBoolArray(operateResult3.Content, length));
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusTcpNet.Read(System.String,System.UInt16)" />
		public override OperateResult<byte[]> Read(string address, ushort length)
		{
			OperateResult<ModbusAddress> operateResult = ModbusInfo.AnalysisAddress(address, Station, isAddressStartWithZero, 3);
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<byte[]>(operateResult);
			}
			List<byte> list = new List<byte>();
			ushort num = 0;
			while (num < length)
			{
				ushort num2 = (ushort)Math.Min(length - num, 120);
				OperateResult<byte[]> operateResult2 = ReadModBus(operateResult.Content.AddressAdd(num), num2);
				if (!operateResult2.IsSuccess)
				{
					return OperateResult.CreateFailedResult<byte[]>(operateResult2);
				}
				list.AddRange(operateResult2.Content);
				num = (ushort)(num + num2);
			}
			return OperateResult.CreateSuccessResult(list.ToArray());
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusTcpNet.ReadModBus(HslCommunication.Core.Address.ModbusAddress,System.UInt16)" />
		private OperateResult<byte[]> ReadModBus(ModbusAddress address, ushort length)
		{
			OperateResult<byte[]> operateResult = ModbusInfo.BuildReadModbusCommand(address, length);
			if (!operateResult.IsSuccess)
			{
				return operateResult;
			}
			OperateResult<byte[]> operateResult2 = ReadFromCoreServer(ModbusInfo.PackCommandToTcp(operateResult.Content, (ushort)softIncrementCount.GetCurrentValue()));
			if (!operateResult2.IsSuccess)
			{
				return operateResult2;
			}
			return ModbusInfo.ExtractActualData(ModbusInfo.ExplodeTcpCommandToCore(operateResult2.Content));
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusTcpNet.Write(System.String,System.Byte[])" />
		public override OperateResult Write(string address, byte[] value)
		{
			OperateResult<byte[]> operateResult = ModbusInfo.BuildWriteWordModbusCommand(address, value, Station, AddressStartWithZero, 16);
			if (!operateResult.IsSuccess)
			{
				return operateResult;
			}
			OperateResult<byte[]> operateResult2 = ReadFromCoreServer(ModbusInfo.PackCommandToTcp(operateResult.Content, (ushort)softIncrementCount.GetCurrentValue()));
			if (!operateResult2.IsSuccess)
			{
				return operateResult2;
			}
			return ModbusInfo.ExtractActualData(ModbusInfo.ExplodeTcpCommandToCore(operateResult2.Content));
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusTcpNet.Write(System.String,System.Int16)" />
		public override OperateResult Write(string address, short value)
		{
			OperateResult<byte[]> operateResult = ModbusInfo.BuildWriteWordModbusCommand(address, value, Station, AddressStartWithZero, 6);
			if (!operateResult.IsSuccess)
			{
				return operateResult;
			}
			OperateResult<byte[]> operateResult2 = ReadFromCoreServer(ModbusInfo.PackCommandToTcp(operateResult.Content, (ushort)softIncrementCount.GetCurrentValue()));
			if (!operateResult2.IsSuccess)
			{
				return operateResult2;
			}
			return ModbusInfo.ExtractActualData(ModbusInfo.ExplodeTcpCommandToCore(operateResult2.Content));
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusTcpNet.Write(System.String,System.UInt16)" />
		public override OperateResult Write(string address, ushort value)
		{
			OperateResult<byte[]> operateResult = ModbusInfo.BuildWriteWordModbusCommand(address, value, Station, AddressStartWithZero, 6);
			if (!operateResult.IsSuccess)
			{
				return operateResult;
			}
			OperateResult<byte[]> operateResult2 = ReadFromCoreServer(ModbusInfo.PackCommandToTcp(operateResult.Content, (ushort)softIncrementCount.GetCurrentValue()));
			if (!operateResult2.IsSuccess)
			{
				return operateResult2;
			}
			return ModbusInfo.ExtractActualData(ModbusInfo.ExplodeTcpCommandToCore(operateResult2.Content));
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusTcpNet.WriteMask(System.String,System.UInt16,System.UInt16)" />
		public OperateResult WriteMask(string address, ushort andMask, ushort orMask)
		{
			OperateResult<byte[]> operateResult = ModbusInfo.BuildWriteMaskModbusCommand(address, andMask, orMask, Station, AddressStartWithZero, 22);
			if (!operateResult.IsSuccess)
			{
				return operateResult;
			}
			OperateResult<byte[]> operateResult2 = ReadFromCoreServer(ModbusInfo.PackCommandToTcp(operateResult.Content, (ushort)softIncrementCount.GetCurrentValue()));
			if (!operateResult2.IsSuccess)
			{
				return operateResult2;
			}
			return ModbusInfo.ExtractActualData(ModbusInfo.ExplodeTcpCommandToCore(operateResult2.Content));
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusTcpNet.WriteOneRegister(System.String,System.Int16)" />
		public OperateResult WriteOneRegister(string address, short value)
		{
			return Write(address, value);
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusTcpNet.WriteOneRegister(System.String,System.UInt16)" />
		public OperateResult WriteOneRegister(string address, ushort value)
		{
			return Write(address, value);
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusTcpNet.ReadCoilAsync(System.String)" />
		public async Task<OperateResult<bool>> ReadCoilAsync(string address)
		{
			return await Task.Run(() => ReadCoil(address));
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusTcpNet.ReadCoilAsync(System.String,System.UInt16)" />
		public async Task<OperateResult<bool[]>> ReadCoilAsync(string address, ushort length)
		{
			return await Task.Run(() => ReadCoil(address, length));
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusTcpNet.ReadDiscreteAsync(System.String)" />
		public async Task<OperateResult<bool>> ReadDiscreteAsync(string address)
		{
			return await Task.Run(() => ReadDiscrete(address));
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusTcpNet.ReadDiscreteAsync(System.String,System.UInt16)" />
		public async Task<OperateResult<bool[]>> ReadDiscreteAsync(string address, ushort length)
		{
			return await Task.Run(() => ReadDiscrete(address, length));
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusUdpNet.Write(System.String,System.Int16)" />/param&gt;
		public override async Task<OperateResult> WriteAsync(string address, short value)
		{
			return await Task.Run(() => Write(address, value));
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusUdpNet.Write(System.String,System.UInt16)" />/param&gt;
		public override async Task<OperateResult> WriteAsync(string address, ushort value)
		{
			return await Task.Run(() => Write(address, value));
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusTcpNet.WriteOneRegister(System.String,System.Int16)" />
		public async Task<OperateResult> WriteOneRegisterAsync(string address, short value)
		{
			return await Task.Run(() => WriteOneRegister(address, value));
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusTcpNet.WriteOneRegister(System.String,System.UInt16)" />
		public async Task<OperateResult> WriteOneRegisterAsync(string address, ushort value)
		{
			return await Task.Run(() => WriteOneRegister(address, value));
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusTcpNet.WriteMask(System.String,System.UInt16,System.UInt16)" />
		public async Task<OperateResult> WriteMaskAsync(string address, ushort andMask, ushort orMask)
		{
			return await Task.Run(() => WriteMask(address, andMask, orMask));
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusTcpNet.ReadBool(System.String,System.UInt16)" />
		public override OperateResult<bool[]> ReadBool(string address, ushort length)
		{
			OperateResult<byte[]> operateResult = ModbusInfo.BuildReadModbusCommand(address, length, Station, AddressStartWithZero, 1);
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<bool[]>(operateResult);
			}
			OperateResult<byte[]> operateResult2 = ReadFromCoreServer(ModbusInfo.PackCommandToTcp(operateResult.Content, (ushort)softIncrementCount.GetCurrentValue()));
			if (!operateResult2.IsSuccess)
			{
				return OperateResult.CreateFailedResult<bool[]>(operateResult2);
			}
			OperateResult<byte[]> operateResult3 = ModbusInfo.ExtractActualData(ModbusInfo.ExplodeTcpCommandToCore(operateResult2.Content));
			if (!operateResult3.IsSuccess)
			{
				return OperateResult.CreateFailedResult<bool[]>(operateResult3);
			}
			return OperateResult.CreateSuccessResult(SoftBasic.ByteToBoolArray(operateResult3.Content, length));
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusTcpNet.Write(System.String,System.Boolean[])" />
		public override OperateResult Write(string address, bool[] values)
		{
			OperateResult<byte[]> operateResult = ModbusInfo.BuildWriteBoolModbusCommand(address, values, Station, AddressStartWithZero, 15);
			if (!operateResult.IsSuccess)
			{
				return operateResult;
			}
			OperateResult<byte[]> operateResult2 = ReadFromCoreServer(ModbusInfo.PackCommandToTcp(operateResult.Content, (ushort)softIncrementCount.GetCurrentValue()));
			if (!operateResult2.IsSuccess)
			{
				return operateResult2;
			}
			return ModbusInfo.ExtractActualData(ModbusInfo.ExplodeTcpCommandToCore(operateResult2.Content));
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusTcpNet.Write(System.String,System.Boolean)" />
		public override OperateResult Write(string address, bool value)
		{
			OperateResult<byte[]> operateResult = ModbusInfo.BuildWriteBoolModbusCommand(address, value, Station, AddressStartWithZero, 5);
			if (!operateResult.IsSuccess)
			{
				return operateResult;
			}
			OperateResult<byte[]> operateResult2 = ReadFromCoreServer(ModbusInfo.PackCommandToTcp(operateResult.Content, (ushort)softIncrementCount.GetCurrentValue()));
			if (!operateResult2.IsSuccess)
			{
				return operateResult2;
			}
			return ModbusInfo.ExtractActualData(ModbusInfo.ExplodeTcpCommandToCore(operateResult2.Content));
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusTcpNet.WriteAsync(System.String,System.Boolean)" />
		public override async Task<OperateResult> WriteAsync(string address, bool value)
		{
			return await Task.Run(() => Write(address, value));
		}

		/// <inheritdoc />
		public override string ToString()
		{
			return $"ModbusUdpNet[{IpAddress}:{Port}]";
		}
	}
}
