using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using HslCommunication.BasicFramework;
using HslCommunication.Core;
using HslCommunication.Core.Address;
using HslCommunication.Core.IMessage;
using HslCommunication.Core.Net;
using HslCommunication.Serial;

namespace HslCommunication.ModBus
{
	/// <summary>
	/// Modbus的虚拟服务器，同时支持Tcp和Rtu的机制，支持线圈，离散输入，寄存器和输入寄存器的读写操作，可以用来当做系统的数据交换池
	/// </summary>
	/// <remarks>
	/// 可以基于本类实现一个功能复杂的modbus服务器，在传统的.NET版本里，还支持modbus-rtu指令的收发，.NET Standard版本服务器不支持rtu操作。服务器支持的数据池如下：
	/// <list type="number">
	/// <item>线圈，功能码对应01，05，15</item>
	/// <item>离散输入，功能码对应02</item>
	/// <item>寄存器，功能码对应03，06，16</item>
	/// <item>输入寄存器，功能码对应04，输入寄存器在服务器端可以实现读写的操作</item>
	/// </list>
	/// </remarks>
	/// <example>
	/// 读写的地址格式为富文本地址，具体请参照下面的示例代码。
	/// <code lang="cs" source="HslCommunication_Net45.Test\Documentation\Samples\Modbus\ModbusTcpServer.cs" region="ModbusTcpServerExample" title="ModbusTcpServer示例" />
	/// </example>
	public class ModbusTcpServer : NetworkDataServerBase
	{
		private List<ModBusMonitorAddress> subscriptions;

		private SimpleHybirdLock subcriptionHybirdLock;

		private SerialPort serialPort;

		private SoftBuffer coilBuffer;

		private SoftBuffer inputBuffer;

		private SoftBuffer registerBuffer;

		private SoftBuffer inputRegisterBuffer;

		private const int DataPoolLength = 65536;

		private int station = 1;

		/// <inheritdoc cref="P:HslCommunication.ModBus.ModbusTcpNet.DataFormat" />
		public DataFormat DataFormat
		{
			get
			{
				return base.ByteTransform.DataFormat;
			}
			set
			{
				base.ByteTransform.DataFormat = value;
			}
		}

		/// <inheritdoc cref="P:HslCommunication.ModBus.ModbusTcpNet.IsStringReverse" />
		public bool IsStringReverse
		{
			get
			{
				return ((ReverseWordTransform)base.ByteTransform).IsStringReverseByteWord;
			}
			set
			{
				((ReverseWordTransform)base.ByteTransform).IsStringReverseByteWord = value;
			}
		}

		/// <inheritdoc cref="P:HslCommunication.ModBus.ModbusTcpNet.Station" />
		public int Station
		{
			get
			{
				return station;
			}
			set
			{
				station = value;
			}
		}

		/// <summary>
		/// 实例化一个Modbus Tcp及Rtu的服务器，支持数据读写操作
		/// </summary>
		public ModbusTcpServer()
		{
			coilBuffer = new SoftBuffer(65536);
			inputBuffer = new SoftBuffer(65536);
			registerBuffer = new SoftBuffer(131072);
			inputRegisterBuffer = new SoftBuffer(131072);
			registerBuffer.IsBoolReverseByWord = true;
			inputRegisterBuffer.IsBoolReverseByWord = true;
			subscriptions = new List<ModBusMonitorAddress>();
			subcriptionHybirdLock = new SimpleHybirdLock();
			base.ByteTransform = new ReverseWordTransform();
			base.WordLength = 1;
			serialPort = new SerialPort();
		}

		/// <inheritdoc />
		protected override byte[] SaveToBytes()
		{
			byte[] array = new byte[393216];
			Array.Copy(coilBuffer.GetBytes(), 0, array, 0, 65536);
			Array.Copy(inputBuffer.GetBytes(), 0, array, 65536, 65536);
			Array.Copy(registerBuffer.GetBytes(), 0, array, 131072, 131072);
			Array.Copy(inputRegisterBuffer.GetBytes(), 0, array, 262144, 131072);
			return array;
		}

		/// <inheritdoc />
		protected override void LoadFromBytes(byte[] content)
		{
			if (content.Length < 393216)
			{
				throw new Exception("File is not correct");
			}
			coilBuffer.SetBytes(content, 0, 0, 65536);
			inputBuffer.SetBytes(content, 65536, 0, 65536);
			registerBuffer.SetBytes(content, 131072, 0, 131072);
			inputRegisterBuffer.SetBytes(content, 262144, 0, 131072);
		}

		/// <summary>
		/// 读取地址的线圈的通断情况
		/// </summary>
		/// <param name="address">起始地址，示例："100"</param>
		/// <returns><c>True</c>或是<c>False</c></returns>
		/// <exception cref="T:System.IndexOutOfRangeException"></exception>
		public bool ReadCoil(string address)
		{
			ushort index = ushort.Parse(address);
			return coilBuffer.GetByte(index) != 0;
		}

		/// <summary>
		/// 批量读取地址的线圈的通断情况
		/// </summary>
		/// <param name="address">起始地址，示例："100"</param>
		/// <param name="length">读取长度</param>
		/// <returns><c>True</c>或是<c>False</c></returns>
		/// <exception cref="T:System.IndexOutOfRangeException"></exception>
		public bool[] ReadCoil(string address, ushort length)
		{
			ushort index = ushort.Parse(address);
			return (from m in coilBuffer.GetBytes(index, length)
				select m != 0).ToArray();
		}

		/// <summary>
		/// 写入线圈的通断值
		/// </summary>
		/// <param name="address">起始地址，示例："100"</param>
		/// <param name="data">是否通断</param>
		/// <returns><c>True</c>或是<c>False</c></returns>
		/// <exception cref="T:System.IndexOutOfRangeException"></exception>
		public void WriteCoil(string address, bool data)
		{
			ushort index = ushort.Parse(address);
			coilBuffer.SetValue((byte)(data ? 1u : 0u), index);
		}

		/// <summary>
		/// 写入线圈数组的通断值
		/// </summary>
		/// <param name="address">起始地址，示例："100"</param>
		/// <param name="data">是否通断</param>
		/// <returns><c>True</c>或是<c>False</c></returns>
		/// <exception cref="T:System.IndexOutOfRangeException"></exception>
		public void WriteCoil(string address, bool[] data)
		{
			if (data != null)
			{
				ushort destIndex = ushort.Parse(address);
				coilBuffer.SetBytes(data.Select((bool m) => (byte)(m ? 1u : 0u)).ToArray(), destIndex);
			}
		}

		/// <summary>
		/// 读取地址的离散线圈的通断情况
		/// </summary>
		/// <param name="address">起始地址，示例："100"</param>
		/// <returns><c>True</c>或是<c>False</c></returns>
		/// <exception cref="T:System.IndexOutOfRangeException"></exception>
		public bool ReadDiscrete(string address)
		{
			ushort index = ushort.Parse(address);
			return inputBuffer.GetByte(index) != 0;
		}

		/// <summary>
		/// 批量读取地址的离散线圈的通断情况
		/// </summary>
		/// <param name="address">起始地址，示例："100"</param>
		/// <param name="length">读取长度</param>
		/// <returns><c>True</c>或是<c>False</c></returns>
		/// <exception cref="T:System.IndexOutOfRangeException"></exception>
		public bool[] ReadDiscrete(string address, ushort length)
		{
			ushort index = ushort.Parse(address);
			return (from m in inputBuffer.GetBytes(index, length)
				select m != 0).ToArray();
		}

		/// <summary>
		/// 写入离散线圈的通断值
		/// </summary>
		/// <param name="address">起始地址，示例："100"</param>
		/// <param name="data">是否通断</param>
		/// <exception cref="T:System.IndexOutOfRangeException"></exception>
		public void WriteDiscrete(string address, bool data)
		{
			ushort index = ushort.Parse(address);
			inputBuffer.SetValue((byte)(data ? 1u : 0u), index);
		}

		/// <summary>
		/// 写入离散线圈数组的通断值
		/// </summary>
		/// <param name="address">起始地址，示例："100"</param>
		/// <param name="data">是否通断</param>
		/// <exception cref="T:System.IndexOutOfRangeException"></exception>
		public void WriteDiscrete(string address, bool[] data)
		{
			if (data != null)
			{
				ushort destIndex = ushort.Parse(address);
				inputBuffer.SetBytes(data.Select((bool m) => (byte)(m ? 1u : 0u)).ToArray(), destIndex);
			}
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusTcpNet.Read(System.String,System.UInt16)" />
		public override OperateResult<byte[]> Read(string address, ushort length)
		{
			OperateResult<ModbusAddress> operateResult = ModbusInfo.AnalysisAddress(address, (byte)Station, isStartWithZero: true, 3);
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<byte[]>(operateResult);
			}
			if (operateResult.Content.Function == 3)
			{
				return OperateResult.CreateSuccessResult(registerBuffer.GetBytes(operateResult.Content.Address * 2, length * 2));
			}
			if (operateResult.Content.Function == 4)
			{
				return OperateResult.CreateSuccessResult(inputRegisterBuffer.GetBytes(operateResult.Content.Address * 2, length * 2));
			}
			return new OperateResult<byte[]>(StringResources.Language.NotSupportedDataType);
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusTcpNet.Write(System.String,System.Byte[])" />
		public override OperateResult Write(string address, byte[] value)
		{
			OperateResult<ModbusAddress> operateResult = ModbusInfo.AnalysisAddress(address, (byte)Station, isStartWithZero: true, 3);
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<byte[]>(operateResult);
			}
			if (operateResult.Content.Function == 3)
			{
				registerBuffer.SetBytes(value, operateResult.Content.Address * 2);
				return OperateResult.CreateSuccessResult();
			}
			if (operateResult.Content.Function == 4)
			{
				inputRegisterBuffer.SetBytes(value, operateResult.Content.Address * 2);
				return OperateResult.CreateSuccessResult();
			}
			return new OperateResult<byte[]>(StringResources.Language.NotSupportedDataType);
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusTcpNet.ReadBool(System.String,System.UInt16)" />
		public override OperateResult<bool[]> ReadBool(string address, ushort length)
		{
			OperateResult<ModbusAddress> operateResult = ModbusInfo.AnalysisAddress(address, (byte)Station, isStartWithZero: true, 1);
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<bool[]>(operateResult);
			}
			if (operateResult.Content.Function == 1)
			{
				return OperateResult.CreateSuccessResult((from m in coilBuffer.GetBytes(operateResult.Content.Address, length)
					select m != 0).ToArray());
			}
			if (operateResult.Content.Function == 2)
			{
				return OperateResult.CreateSuccessResult((from m in inputBuffer.GetBytes(operateResult.Content.Address, length)
					select m != 0).ToArray());
			}
			return new OperateResult<bool[]>(StringResources.Language.NotSupportedDataType);
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusTcpNet.Write(System.String,System.Boolean[])" />
		public override OperateResult Write(string address, bool[] value)
		{
			OperateResult<ModbusAddress> operateResult = ModbusInfo.AnalysisAddress(address, (byte)Station, isStartWithZero: true, 1);
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<byte[]>(operateResult);
			}
			if (operateResult.Content.Function == 1)
			{
				coilBuffer.SetBytes(value.Select((bool m) => (byte)(m ? 1u : 0u)).ToArray(), operateResult.Content.Address);
				return OperateResult.CreateSuccessResult();
			}
			if (operateResult.Content.Function == 2)
			{
				inputBuffer.SetBytes(value.Select((bool m) => (byte)(m ? 1u : 0u)).ToArray(), operateResult.Content.Address);
				return OperateResult.CreateSuccessResult();
			}
			return new OperateResult<byte[]>(StringResources.Language.NotSupportedDataType);
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusTcpNet.Write(System.String,System.Boolean)" />
		public override OperateResult Write(string address, bool value)
		{
			if (address.IndexOf('.') < 0)
			{
				return base.Write(address, value);
			}
			try
			{
				int num = Convert.ToInt32(address.Substring(address.IndexOf('.') + 1));
				address = address.Substring(0, address.IndexOf('.'));
				OperateResult<ModbusAddress> operateResult = ModbusInfo.AnalysisAddress(address, (byte)Station, isStartWithZero: true, 3);
				if (!operateResult.IsSuccess)
				{
					return operateResult;
				}
				num = operateResult.Content.Address * 16 + num;
				if (operateResult.Content.Function == 3)
				{
					registerBuffer.SetBool(value, num);
					return OperateResult.CreateSuccessResult();
				}
				if (operateResult.Content.Function == 4)
				{
					inputRegisterBuffer.SetBool(value, num);
					return OperateResult.CreateSuccessResult();
				}
				return new OperateResult(StringResources.Language.NotSupportedDataType);
			}
			catch (Exception ex)
			{
				return new OperateResult(ex.Message);
			}
		}

		/// <summary>
		/// 写入寄存器数据，指定字节数据
		/// </summary>
		/// <param name="address">起始地址，示例："100"，如果是输入寄存器："x=4;100"</param>
		/// <param name="high">高位数据</param>
		/// <param name="low">地位数据</param>
		public void Write(string address, byte high, byte low)
		{
			Write(address, new byte[2]
			{
				high,
				low
			});
		}

		/// <inheritdoc />
		protected override void ThreadPoolLoginAfterClientCheck(Socket socket, IPEndPoint endPoint)
		{
			AppSession appSession = new AppSession();
			appSession.IpEndPoint = endPoint;
			appSession.WorkSocket = socket;
			if (socket.BeginReceiveResult(SocketAsyncCallBack, appSession).IsSuccess)
			{
				AddClient(appSession);
			}
			else
			{
				base.LogNet?.WriteDebug(ToString(), string.Format(StringResources.Language.ClientOfflineInfo, endPoint));
			}
		}

		private void SocketAsyncCallBack(IAsyncResult ar)
		{
			AppSession appSession = ar.AsyncState as AppSession;
			if (appSession == null)
			{
				return;
			}
			if (!appSession.WorkSocket.EndReceiveResult(ar).IsSuccess)
			{
				RemoveClient(appSession);
				return;
			}
			OperateResult<byte[]> operateResult = ReceiveByMessage(appSession.WorkSocket, 2000, new ModbusTcpMessage());
			if (!operateResult.IsSuccess)
			{
				RemoveClient(appSession);
				return;
			}
			if (!CheckModbusMessageLegal(operateResult.Content.RemoveBegin(6)))
			{
				RemoveClient(appSession);
				return;
			}
			base.LogNet?.WriteDebug(ToString(), "Tcp " + StringResources.Language.Receive + "：" + operateResult.Content.ToHexString(' '));
			ushort id = (ushort)(operateResult.Content[0] * 256 + operateResult.Content[1]);
			byte[] array = ModbusInfo.PackCommandToTcp(ReadFromModbusCore(operateResult.Content.RemoveBegin(6)), id);
			if (array == null)
			{
				RemoveClient(appSession);
				return;
			}
			if (!Send(appSession.WorkSocket, array).IsSuccess)
			{
				RemoveClient(appSession);
				return;
			}
			base.LogNet?.WriteDebug(ToString(), "Tcp " + StringResources.Language.Send + "：" + array.ToHexString(' '));
			RaiseDataReceived(operateResult.Content);
			if (!appSession.WorkSocket.BeginReceiveResult(SocketAsyncCallBack, appSession).IsSuccess)
			{
				RemoveClient(appSession);
			}
		}

		/// <summary>
		/// 创建特殊的功能标识，然后返回该信息<br />
		/// Create a special feature ID and return this information
		/// </summary>
		/// <param name="modbusCore">modbus核心报文</param>
		/// <param name="error">错误码</param>
		/// <returns>携带错误码的modbus报文</returns>
		private byte[] CreateExceptionBack(byte[] modbusCore, byte error)
		{
			return new byte[3]
			{
				modbusCore[0],
				(byte)(modbusCore[1] + 128),
				error
			};
		}

		/// <summary>
		/// 创建返回消息<br />
		/// Create return message
		/// </summary>
		/// <param name="modbusCore">modbus核心报文</param>
		/// <param name="content">返回的实际数据内容</param>
		/// <returns>携带内容的modbus报文</returns>
		private byte[] CreateReadBack(byte[] modbusCore, byte[] content)
		{
			return SoftBasic.SpliceByteArray(new byte[3]
			{
				modbusCore[0],
				modbusCore[1],
				(byte)content.Length
			}, content);
		}

		/// <summary>
		/// 创建写入成功的反馈信号<br />
		/// Create feedback signal for successful write
		/// </summary>
		/// <param name="modbus">modbus核心报文</param>
		/// <returns>携带成功写入的信息</returns>
		private byte[] CreateWriteBack(byte[] modbus)
		{
			return modbus.SelectBegin(6);
		}

		private byte[] ReadCoilBack(byte[] modbus, string addressHead)
		{
			try
			{
				ushort num = base.ByteTransform.TransUInt16(modbus, 2);
				ushort num2 = base.ByteTransform.TransUInt16(modbus, 4);
				if (num + num2 > 65536)
				{
					return CreateExceptionBack(modbus, 2);
				}
				if (num2 > 2040)
				{
					return CreateExceptionBack(modbus, 3);
				}
				bool[] content = ReadBool(addressHead + num, num2).Content;
				return CreateReadBack(modbus, SoftBasic.BoolArrayToByte(content));
			}
			catch (Exception ex)
			{
				base.LogNet?.WriteException(ToString(), StringResources.Language.ModbusTcpReadCoilException, ex);
				return CreateExceptionBack(modbus, 4);
			}
		}

		private byte[] ReadRegisterBack(byte[] modbus, string addressHead)
		{
			try
			{
				ushort num = base.ByteTransform.TransUInt16(modbus, 2);
				ushort num2 = base.ByteTransform.TransUInt16(modbus, 4);
				if (num + num2 > 65536)
				{
					return CreateExceptionBack(modbus, 2);
				}
				if (num2 > 127)
				{
					return CreateExceptionBack(modbus, 3);
				}
				byte[] content = Read(addressHead + num, num2).Content;
				return CreateReadBack(modbus, content);
			}
			catch (Exception ex)
			{
				base.LogNet?.WriteException(ToString(), StringResources.Language.ModbusTcpReadRegisterException, ex);
				return CreateExceptionBack(modbus, 4);
			}
		}

		private byte[] WriteOneCoilBack(byte[] modbus)
		{
			try
			{
				ushort num = base.ByteTransform.TransUInt16(modbus, 2);
				if (modbus[4] == byte.MaxValue && modbus[5] == 0)
				{
					Write(num.ToString(), value: true);
				}
				else if (modbus[4] == 0 && modbus[5] == 0)
				{
					Write(num.ToString(), value: false);
				}
				return CreateWriteBack(modbus);
			}
			catch (Exception ex)
			{
				base.LogNet?.WriteException(ToString(), StringResources.Language.ModbusTcpWriteCoilException, ex);
				return CreateExceptionBack(modbus, 4);
			}
		}

		private byte[] WriteOneRegisterBack(byte[] modbus)
		{
			try
			{
				ushort address = base.ByteTransform.TransUInt16(modbus, 2);
				short content = ReadInt16(address.ToString()).Content;
				Write(address.ToString(), modbus[4], modbus[5]);
				short content2 = ReadInt16(address.ToString()).Content;
				OnRegisterBeforWrite(address, content, content2);
				return CreateWriteBack(modbus);
			}
			catch (Exception ex)
			{
				base.LogNet?.WriteException(ToString(), StringResources.Language.ModbusTcpWriteRegisterException, ex);
				return CreateExceptionBack(modbus, 4);
			}
		}

		private byte[] WriteCoilsBack(byte[] modbus)
		{
			try
			{
				ushort num = base.ByteTransform.TransUInt16(modbus, 2);
				ushort num2 = base.ByteTransform.TransUInt16(modbus, 4);
				if (num + num2 > 65536)
				{
					return CreateExceptionBack(modbus, 2);
				}
				if (num2 > 2040)
				{
					return CreateExceptionBack(modbus, 3);
				}
				Write(num.ToString(), modbus.RemoveBegin(7).ToBoolArray(num2));
				return CreateWriteBack(modbus);
			}
			catch (Exception ex)
			{
				base.LogNet?.WriteException(ToString(), StringResources.Language.ModbusTcpWriteCoilException, ex);
				return CreateExceptionBack(modbus, 4);
			}
		}

		private byte[] WriteRegisterBack(byte[] modbus)
		{
			try
			{
				ushort num = base.ByteTransform.TransUInt16(modbus, 2);
				ushort num2 = base.ByteTransform.TransUInt16(modbus, 4);
				if (num + num2 > 65536)
				{
					return CreateExceptionBack(modbus, 2);
				}
				if (num2 > 127)
				{
					return CreateExceptionBack(modbus, 3);
				}
				MonitorAddress[] array = new MonitorAddress[num2];
				for (ushort num3 = 0; num3 < num2; num3 = (ushort)(num3 + 1))
				{
					short content = ReadInt16((num + num3).ToString()).Content;
					Write((num + num3).ToString(), modbus[2 * num3 + 7], modbus[2 * num3 + 8]);
					short content2 = ReadInt16((num + num3).ToString()).Content;
					array[num3] = new MonitorAddress
					{
						Address = (ushort)(num + num3),
						ValueOrigin = content,
						ValueNew = content2
					};
				}
				for (int i = 0; i < array.Length; i++)
				{
					OnRegisterBeforWrite(array[i].Address, array[i].ValueOrigin, array[i].ValueNew);
				}
				return CreateWriteBack(modbus);
			}
			catch (Exception ex)
			{
				base.LogNet?.WriteException(ToString(), StringResources.Language.ModbusTcpWriteRegisterException, ex);
				return CreateExceptionBack(modbus, 4);
			}
		}

		private byte[] WriteMaskRegisterBack(byte[] modbus)
		{
			try
			{
				ushort address = base.ByteTransform.TransUInt16(modbus, 2);
				int num = base.ByteTransform.TransUInt16(modbus, 4);
				int num2 = base.ByteTransform.TransUInt16(modbus, 6);
				int content = ReadInt16(address.ToString()).Content;
				short num3 = (short)((content & num) | num2);
				Write(address.ToString(), num3);
				MonitorAddress monitorAddress = default(MonitorAddress);
				monitorAddress.Address = address;
				monitorAddress.ValueOrigin = (short)content;
				monitorAddress.ValueNew = num3;
				MonitorAddress monitorAddress2 = monitorAddress;
				OnRegisterBeforWrite(monitorAddress2.Address, monitorAddress2.ValueOrigin, monitorAddress2.ValueNew);
				return modbus;
			}
			catch (Exception ex)
			{
				base.LogNet?.WriteException(ToString(), StringResources.Language.ModbusTcpWriteRegisterException, ex);
				return CreateExceptionBack(modbus, 4);
			}
		}

		/// <summary>
		/// 新增一个数据监视的任务，针对的是寄存器地址的数据<br />
		/// Added a data monitoring task for data at register addresses
		/// </summary>
		/// <param name="monitor">监视地址对象</param>
		public void AddSubcription(ModBusMonitorAddress monitor)
		{
			subcriptionHybirdLock.Enter();
			subscriptions.Add(monitor);
			subcriptionHybirdLock.Leave();
		}

		/// <summary>
		/// 移除一个数据监视的任务<br />
		/// Remove a data monitoring task
		/// </summary>
		/// <param name="monitor">监视地址对象</param>
		public void RemoveSubcrption(ModBusMonitorAddress monitor)
		{
			subcriptionHybirdLock.Enter();
			subscriptions.Remove(monitor);
			subcriptionHybirdLock.Leave();
		}

		/// <summary>
		/// 在数据变更后，进行触发是否产生订阅<br />
		/// Whether to generate a subscription after triggering data changes
		/// </summary>
		/// <param name="address">数据地址</param>
		/// <param name="before">修改之前的数</param>
		/// <param name="after">修改之后的数</param>
		private void OnRegisterBeforWrite(ushort address, short before, short after)
		{
			subcriptionHybirdLock.Enter();
			for (int i = 0; i < subscriptions.Count; i++)
			{
				if (subscriptions[i].Address == address)
				{
					subscriptions[i].SetValue(after);
					if (before != after)
					{
						subscriptions[i].SetChangeValue(before, after);
					}
				}
			}
			subcriptionHybirdLock.Leave();
		}

		/// <summary>
		/// 检测当前的Modbus接收的指定是否是合法的<br />
		/// Check if the current Modbus received designation is valid
		/// </summary>
		/// <param name="buffer">缓存数据</param>
		/// <returns>是否合格</returns>
		private bool CheckModbusMessageLegal(byte[] buffer)
		{
			bool flag = false;
			switch (buffer[1])
			{
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
				flag = (buffer.Length == 6);
				break;
			case 15:
			case 16:
				flag = (buffer.Length > 6 && buffer[6] == buffer.Length - 7);
				break;
			case 22:
				flag = (buffer.Length == 8);
				break;
			default:
				flag = true;
				break;
			}
			if (!flag)
			{
				base.LogNet?.WriteError(ToString(), "Receive Nosense Modbus-rtu : " + buffer.ToHexString(' '));
			}
			return flag;
		}

		/// <summary>
		/// Modbus核心数据交互方法，允许重写自己来实现，报文只剩下核心的Modbus信息，去除了MPAB报头信息<br />
		/// The Modbus core data interaction method allows you to rewrite it to achieve the message. 
		/// Only the core Modbus information is left in the message, and the MPAB header information is removed.
		/// </summary>
		/// <param name="modbusCore">核心的Modbus报文</param>
		/// <returns>进行数据交互之后的结果</returns>
		protected virtual byte[] ReadFromModbusCore(byte[] modbusCore)
		{
			return modbusCore[1] switch
			{
				1 => ReadCoilBack(modbusCore, string.Empty), 
				2 => ReadCoilBack(modbusCore, "x=2;"), 
				3 => ReadRegisterBack(modbusCore, string.Empty), 
				4 => ReadRegisterBack(modbusCore, "x=4;"), 
				5 => WriteOneCoilBack(modbusCore), 
				6 => WriteOneRegisterBack(modbusCore), 
				15 => WriteCoilsBack(modbusCore), 
				16 => WriteRegisterBack(modbusCore), 
				22 => WriteMaskRegisterBack(modbusCore), 
				_ => CreateExceptionBack(modbusCore, 1), 
			};
		}

		/// <summary>
		/// 启动modbus-rtu的从机服务，使用默认的参数进行初始化串口，9600波特率，8位数据位，无奇偶校验，1位停止位<br />
		/// Start the slave service of modbus-rtu, initialize the serial port with default parameters, 9600 baud rate, 8 data bits, no parity, 1 stop bit
		/// </summary>
		/// <param name="com">串口信息</param>
		public void StartModbusRtu(string com)
		{
			StartModbusRtu(com, 9600);
		}

		/// <summary>
		/// 启动modbus-rtu的从机服务，使用默认的参数进行初始化串口，8位数据位，无奇偶校验，1位停止位<br />
		/// Start the slave service of modbus-rtu, initialize the serial port with default parameters, 8 data bits, no parity, 1 stop bit
		/// </summary>
		/// <param name="com">串口信息</param>
		/// <param name="baudRate">波特率</param>
		public void StartModbusRtu(string com, int baudRate)
		{
			StartModbusRtu(delegate(SerialPort sp)
			{
				sp.PortName = com;
				sp.BaudRate = baudRate;
				sp.DataBits = 8;
				sp.Parity = Parity.None;
				sp.StopBits = StopBits.One;
			});
		}

		/// <summary>
		/// 启动modbus-rtu的从机服务，使用自定义的初始化方法初始化串口的参数<br />
		/// Start the slave service of modbus-rtu and initialize the parameters of the serial port using a custom initialization method
		/// </summary>
		/// <param name="inni">初始化信息的委托</param>
		public void StartModbusRtu(Action<SerialPort> inni)
		{
			if (!serialPort.IsOpen)
			{
				inni?.Invoke(serialPort);
				serialPort.ReadBufferSize = 1024;
				serialPort.ReceivedBytesThreshold = 1;
				serialPort.Open();
				serialPort.DataReceived += SerialPort_DataReceived;
			}
		}

		/// <summary>
		/// 关闭modbus-rtu的串口对象<br />
		/// Close the serial port object of modbus-rtu
		/// </summary>
		public void CloseModbusRtu()
		{
			if (serialPort.IsOpen)
			{
				serialPort.Close();
			}
		}

		/// <summary>
		/// 接收到串口数据的时候触发
		/// </summary>
		/// <param name="sender">串口对象</param>
		/// <param name="e">消息</param>
		private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
		{
			int num = 0;
			byte[] array = new byte[1024];
			int num2;
			do
			{
				Thread.Sleep(20);
				num2 = serialPort.Read(array, num, serialPort.BytesToRead);
				num += num2;
			}
			while (num2 != 0);
			if (num == 0)
			{
				return;
			}
			byte[] array2 = array.SelectBegin(num);
			if (array2.Length < 3)
			{
				base.LogNet?.WriteError(ToString(), "Uknown Data：" + array2.ToHexString(' '));
				return;
			}
			if (array2[0] != 58)
			{
				base.LogNet?.WriteDebug(ToString(), "Rtu " + StringResources.Language.Receive + "：" + array2.ToHexString(' '));
				if (SoftCRC16.CheckCRC16(array2))
				{
					byte[] array3 = array2.RemoveLast(2);
					if (!CheckModbusMessageLegal(array3))
					{
						return;
					}
					if (station >= 0 && station != array3[0])
					{
						base.LogNet?.WriteError(ToString(), "Station not match Modbus-rtu : " + array2.ToHexString(' '));
						return;
					}
					byte[] array4 = ModbusInfo.PackCommandToRtu(ReadFromModbusCore(array3));
					serialPort.Write(array4, 0, array4.Length);
					base.LogNet?.WriteDebug(ToString(), "Rtu " + StringResources.Language.Send + "：" + array4.ToHexString(' '));
					if (base.IsStarted)
					{
						RaiseDataReceived(array2);
					}
				}
				else
				{
					base.LogNet?.WriteWarn("CRC Check Failed : " + array2.ToHexString(' '));
				}
				return;
			}
			base.LogNet?.WriteDebug(ToString(), "Ascii " + StringResources.Language.Receive + "：" + Encoding.ASCII.GetString(array2.RemoveLast(2)));
			OperateResult<byte[]> operateResult = ModbusInfo.TransAsciiPackCommandToCore(array2);
			if (!operateResult.IsSuccess)
			{
				base.LogNet?.WriteError(ToString(), operateResult.Message);
				return;
			}
			byte[] content = operateResult.Content;
			if (!CheckModbusMessageLegal(content))
			{
				return;
			}
			if (station >= 0 && station != content[0])
			{
				base.LogNet?.WriteError(ToString(), "Station not match Modbus-Ascii : " + Encoding.ASCII.GetString(array2.RemoveLast(2)));
				return;
			}
			byte[] array5 = ModbusInfo.TransModbusCoreToAsciiPackCommand(ReadFromModbusCore(content));
			serialPort.Write(array5, 0, array5.Length);
			base.LogNet?.WriteDebug(ToString(), "Ascii " + StringResources.Language.Send + "：" + Encoding.ASCII.GetString(array5.RemoveLast(2)));
			if (base.IsStarted)
			{
				RaiseDataReceived(array2);
			}
		}

		/// <inheritdoc />
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				subcriptionHybirdLock?.Dispose();
				subscriptions?.Clear();
				coilBuffer?.Dispose();
				inputBuffer?.Dispose();
				registerBuffer?.Dispose();
				inputRegisterBuffer?.Dispose();
				serialPort?.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <inheritdoc />
		public override string ToString()
		{
			return $"ModbusTcpServer[{base.Port}]";
		}
	}
}
