namespace HslCommunication.ModBus
{
	/// <summary>
	/// Modbus-Ascii通讯协议的类库，基于rtu类库完善过来，支持标准的功能码，也支持扩展的功能码实现，地址采用富文本的形式，详细见备注说明<br />
	/// The client communication class of Modbus-Ascii protocol is convenient for data interaction with the server. It supports standard function codes and also supports extended function codes. 
	/// The address is in rich text. For details, see the remarks.
	/// </summary>
	/// <remarks>
	/// <inheritdoc cref="T:HslCommunication.ModBus.ModbusTcpNet" path="remarks" />
	/// </remarks>
	/// <example>
	/// 基本的用法请参照下面的代码示例，初始化部分的代码省略
	/// <code lang="cs" source="HslCommunication_Net45.Test\Documentation\Samples\Modbus\ModbusAsciiExample.cs" region="Example" title="Modbus示例" />
	/// 复杂的读取数据的代码示例如下：
	/// <code lang="cs" source="HslCommunication_Net45.Test\Documentation\Samples\Modbus\ModbusAsciiExample.cs" region="ReadExample" title="read示例" />
	/// 写入数据的代码如下：
	/// <code lang="cs" source="HslCommunication_Net45.Test\Documentation\Samples\Modbus\ModbusAsciiExample.cs" region="WriteExample" title="write示例" />
	/// </example>
	public class ModbusAscii : ModbusRtu
	{
		/// <summary>
		/// 实例化一个Modbus-ascii协议的客户端对象<br />
		/// Instantiate a client object of the Modbus-ascii protocol
		/// </summary>
		public ModbusAscii()
		{
			LogMsgFormatBinary = false;
		}

		/// <inheritdoc cref="M:HslCommunication.ModBus.ModbusRtu.#ctor(System.Byte)" />
		public ModbusAscii(byte station = 1)
			: base(station)
		{
			LogMsgFormatBinary = false;
		}

		/// <inheritdoc />
		protected override OperateResult<byte[]> CheckModbusTcpResponse(byte[] send)
		{
			byte[] send2 = ModbusInfo.TransModbusCoreToAsciiPackCommand(send);
			OperateResult<byte[]> operateResult = ReadBase(send2);
			if (!operateResult.IsSuccess)
			{
				return operateResult;
			}
			OperateResult<byte[]> operateResult2 = ModbusInfo.TransAsciiPackCommandToCore(operateResult.Content);
			if (!operateResult2.IsSuccess)
			{
				return operateResult2;
			}
			if (send[1] + 128 == operateResult2.Content[1])
			{
				return new OperateResult<byte[]>(operateResult2.Content[2], ModbusInfo.GetDescriptionByErrorCode(operateResult2.Content[2]));
			}
			return ModbusInfo.ExtractActualData(operateResult2.Content);
		}

		/// <inheritdoc />
		public override string ToString()
		{
			return $"ModbusAscii[{base.PortName}:{base.BaudRate}]";
		}
	}
}
