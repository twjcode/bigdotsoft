using HslCommunication.Core;
using HslCommunication.Serial;

namespace HslCommunication.Profinet.Freedom
{
	/// <summary>
	/// 基于串口的自由协议，需要在地址里传入报文信息，也可以传入数据偏移信息，<see cref="P:HslCommunication.Serial.SerialDeviceBase.ByteTransform" />默认为<see cref="T:HslCommunication.Core.RegularByteTransform" />
	/// </summary>
	/// <example>
	/// <code lang="cs" source="HslCommunication_Net45.Test\Documentation\Samples\Profinet\FreedomExample.cs" region="Sample5" title="实例化" />
	/// <code lang="cs" source="HslCommunication_Net45.Test\Documentation\Samples\Profinet\FreedomExample.cs" region="Sample6" title="读取" />
	/// </example>
	public class FreedomSerial : SerialDeviceBase
	{
		/// <summary>
		/// 实例化一个默认的对象
		/// </summary>
		public FreedomSerial()
		{
			base.ByteTransform = new RegularByteTransform();
		}

		/// <inheritdoc />
		public override OperateResult<byte[]> Read(string address, ushort length)
		{
			OperateResult<byte[], int> operateResult = FreedomTcpNet.AnalysisAddress(address);
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<byte[]>(operateResult);
			}
			OperateResult<byte[]> operateResult2 = ReadBase(operateResult.Content1);
			if (!operateResult2.IsSuccess)
			{
				return operateResult2;
			}
			if (operateResult.Content2 >= operateResult2.Content.Length)
			{
				return new OperateResult<byte[]>(StringResources.Language.ReceiveDataLengthTooShort);
			}
			return OperateResult.CreateSuccessResult(operateResult2.Content.RemoveBegin(operateResult.Content2));
		}

		/// <inheritdoc />
		public override OperateResult Write(string address, byte[] value)
		{
			return Read(address, 0);
		}

		/// <inheritdoc />
		public override string ToString()
		{
			return $"FreedomSerial<{base.ByteTransform.GetType()}>[{base.PortName}:{base.BaudRate}]";
		}
	}
}
