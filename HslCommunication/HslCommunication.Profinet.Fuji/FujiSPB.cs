using System;
using System.Text;
using HslCommunication.BasicFramework;
using HslCommunication.Core;
using HslCommunication.Serial;

namespace HslCommunication.Profinet.Fuji
{
	/// <summary>
	/// 富士PLC的SPB协议，详细的地址信息见api文档说明<br />
	/// Fuji PLC's SPB protocol. For detailed address information, see the api documentation.
	/// </summary>
	/// <remarks>
	/// <inheritdoc cref="T:HslCommunication.Profinet.Fuji.FujiSPBOverTcp" path="remarks" />
	/// </remarks>
	public class FujiSPB : SerialDeviceBase
	{
		private byte station = 1;

		/// <inheritdoc cref="P:HslCommunication.Profinet.Fuji.FujiSPBOverTcp.Station" />
		public byte Station
		{
			get
			{
				return station;
			}
			set
			{
				station = value;
			}
		}

		/// <inheritdoc cref="M:HslCommunication.Profinet.Fuji.FujiSPBOverTcp.#ctor" />
		public FujiSPB()
		{
			base.ByteTransform = new RegularByteTransform();
			base.WordLength = 1;
			LogMsgFormatBinary = false;
		}

		/// <inheritdoc cref="M:HslCommunication.Profinet.Fuji.FujiSPBOverTcp.Read(System.String,System.UInt16)" />
		public override OperateResult<byte[]> Read(string address, ushort length)
		{
			OperateResult<byte[]> operateResult = FujiSPBOverTcp.BuildReadCommand(station, address, length, isBool: false);
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<byte[]>(operateResult);
			}
			OperateResult<byte[]> operateResult2 = ReadBase(operateResult.Content);
			if (!operateResult2.IsSuccess)
			{
				return OperateResult.CreateFailedResult<byte[]>(operateResult2);
			}
			if (operateResult2.Content[0] != 58)
			{
				return new OperateResult<byte[]>(operateResult2.Content[0], "Read Faild:" + SoftBasic.ByteToHexString(operateResult2.Content, ' '));
			}
			if (Encoding.ASCII.GetString(operateResult2.Content, 9, 2) != "00")
			{
				return new OperateResult<byte[]>(operateResult2.Content[5], FujiSPBOverTcp.GetErrorDescriptionFromCode(Encoding.ASCII.GetString(operateResult2.Content, 9, 2)));
			}
			byte[] array = new byte[length * 2];
			for (int i = 0; i < array.Length / 2; i++)
			{
				ushort value = Convert.ToUInt16(Encoding.ASCII.GetString(operateResult2.Content, i * 4 + 6, 4), 16);
				BitConverter.GetBytes(value).CopyTo(array, i * 2);
			}
			return OperateResult.CreateSuccessResult(array);
		}

		/// <inheritdoc cref="M:HslCommunication.Profinet.Fuji.FujiSPBOverTcp.Write(System.String,System.Byte[])" />
		public override OperateResult Write(string address, byte[] value)
		{
			OperateResult<byte[]> operateResult = FujiSPBOverTcp.BuildWriteByteCommand(station, address, value);
			if (!operateResult.IsSuccess)
			{
				return operateResult;
			}
			OperateResult<byte[]> operateResult2 = ReadBase(operateResult.Content);
			if (!operateResult2.IsSuccess)
			{
				return operateResult2;
			}
			if (operateResult2.Content[0] != 58)
			{
				return new OperateResult<byte[]>(operateResult2.Content[0], "Read Faild:" + SoftBasic.ByteToHexString(operateResult2.Content, ' '));
			}
			if (Encoding.ASCII.GetString(operateResult2.Content, 9, 2) != "00")
			{
				return new OperateResult<byte[]>(operateResult2.Content[5], FujiSPBOverTcp.GetErrorDescriptionFromCode(Encoding.ASCII.GetString(operateResult2.Content, 9, 2)));
			}
			return OperateResult.CreateSuccessResult();
		}

		/// <inheritdoc />
		public override string ToString()
		{
			return $"FujiSPB[{base.PortName}:{base.BaudRate}]";
		}
	}
}
