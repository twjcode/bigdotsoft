using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslCommunication.Controls
{
	/// <summary>
	/// 一个时钟控件
	/// </summary>
	public class UserClock : UserControl
	{
		private Timer _Time1s = new Timer();

		private DateTime _NowTime = DateTime.Now;

		private Color _HourColor = Color.Chocolate;

		private Color _MiniteColor = Color.Coral;

		private Color _SecondColor = Color.Green;

		private string _ShowText = "Sweet";

		private string _ShowTextFont = "Courier New";

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取时钟的当前时间
		/// </summary>
		[Category("我的属性")]
		[Description("设置边框的宽度")]
		public DateTime 当前时间 => _NowTime;

		/// <summary>
		/// 获取或设置时钟指针的颜色
		/// </summary>
		[Category("我的属性")]
		[Description("设置时钟的指针颜色")]
		[DefaultValue(typeof(Color), "Chocolate")]
		public Color 时钟指针颜色
		{
			get
			{
				return _HourColor;
			}
			set
			{
				_HourColor = value;
			}
		}

		/// <summary>
		/// 获取或设置时钟分钟指针颜色
		/// </summary>
		[Category("我的属性")]
		[Description("设置分钟的指针颜色")]
		[DefaultValue(typeof(Color), "Coral")]
		public Color 分钟指针颜色
		{
			get
			{
				return _MiniteColor;
			}
			set
			{
				_MiniteColor = value;
			}
		}

		/// <summary>
		/// 获取或设置秒钟指针颜色
		/// </summary>
		[Category("我的属性")]
		[Description("设置秒钟的指针颜色")]
		[DefaultValue(typeof(Color), "Green")]
		public Color 秒钟指针颜色
		{
			get
			{
				return _SecondColor;
			}
			set
			{
				_SecondColor = value;
			}
		}

		/// <summary>
		/// 获取或设置时钟的个性化文本
		/// </summary>
		[Category("我的属性")]
		[Description("设置时钟显示的字符串")]
		[DefaultValue(typeof(string), "Sweet")]
		public string 显示文本
		{
			get
			{
				return _ShowText;
			}
			set
			{
				_ShowText = value;
			}
		}

		/// <summary>
		/// 字体
		/// </summary>
		[Category("我的属性")]
		[Description("设置时钟显示的字符串")]
		[DefaultValue(typeof(string), "Courier New")]
		public string 显示文本字体
		{
			get
			{
				return _ShowTextFont;
			}
			set
			{
				_ShowTextFont = value;
			}
		}

		/// <summary>
		/// 实例化一个时钟控件
		/// </summary>
		public UserClock()
		{
			InitializeComponent();
			_Time1s.Interval = 50;
			_Time1s.Tick += _Time1s_Tick;
			DoubleBuffered = true;
		}

		private void ClockMy_Load(object sender, EventArgs e)
		{
			BackgroundImage = _BackGround();
			_Time1s.Start();
		}

		private void _Time1s_Tick(object sender, EventArgs e)
		{
			_NowTime = DateTime.Now;
			Invalidate();
		}

		private Bitmap _BackGround()
		{
			int width = base.Width;
			Bitmap bitmap = new Bitmap(width - 20, width - 20);
			Graphics graphics = Graphics.FromImage(bitmap);
			graphics.SmoothingMode = SmoothingMode.AntiAlias;
			Point point = new Point(bitmap.Width / 2, bitmap.Height / 2);
			int num = (bitmap.Width - 1) / 2;
			Rectangle rect = new Rectangle(0, 0, bitmap.Width - 1, bitmap.Width - 1);
			Rectangle rect2 = new Rectangle(2, 2, bitmap.Width - 5, bitmap.Width - 5);
			Rectangle rect3 = new Rectangle(point.X - 4, point.Y - 4, 8, 8);
			Rectangle rectangle = new Rectangle(5, 5, bitmap.Width - 11, bitmap.Width - 11);
			Rectangle rectangle2 = new Rectangle(8, 8, bitmap.Width - 17, bitmap.Width - 17);
			graphics.FillEllipse(Brushes.DarkGray, rect);
			graphics.FillEllipse(Brushes.White, rect2);
			graphics.DrawEllipse(new Pen(Brushes.Black, 1.5f), rect3);
			graphics.TranslateTransform(point.X, point.Y);
			for (int i = 0; i < 60; i++)
			{
				graphics.RotateTransform(6f);
				graphics.DrawLine(Pens.DarkGray, new Point(num - 3, 0), new Point(num - 1, 0));
			}
			for (int j = 0; j < 12; j++)
			{
				graphics.RotateTransform(30f);
				graphics.DrawLine(new Pen(Color.Chocolate, 2f), new Point(num - 6, 0), new Point(num - 1, 0));
			}
			graphics.ResetTransform();
			Font font = new Font("Microsoft YaHei UI", 12f);
			int num2 = num / 2;
			int num3 = (int)((double)num * Math.Sqrt(3.0) / 2.0);
			graphics.DrawString("1", font, Brushes.Green, new PointF(num + num2 - 13, num - num3 + 4));
			graphics.DrawString("2", font, Brushes.Green, new PointF(num + num3 - 17, num - num2 - 2));
			graphics.DrawString("3", font, Brushes.Green, new PointF(2 * num - 18, num - 8));
			graphics.DrawString("4", font, Brushes.Green, new PointF(num + num3 - 18, num + num2 - 14));
			graphics.DrawString("5", font, Brushes.Green, new PointF(num + num2 - 14, num + num3 - 19));
			graphics.DrawString("6", font, Brushes.Green, new PointF(num - 6, 2 * num - 22));
			graphics.DrawString("7", font, Brushes.Green, new PointF(num - num2 + 2, num + num3 - 19));
			graphics.DrawString("8", font, Brushes.Green, new PointF(num - num3 + 5, num + num2 - 14));
			graphics.DrawString("9", font, Brushes.Green, new PointF(8f, num - 9));
			graphics.DrawString("10", font, Brushes.Green, new PointF(num - num3 + 4, num - num2 - 2));
			graphics.DrawString("11", font, Brushes.Green, new PointF(num - num2, num - num3 + 3));
			graphics.DrawString("12", font, Brushes.Green, new PointF(num - 10, 7f));
			Bitmap bitmap2 = new Bitmap(base.Width, base.Height);
			Graphics graphics2 = Graphics.FromImage(bitmap2);
			graphics2.DrawImage(bitmap, new Point(10, 10));
			bitmap.Dispose();
			return bitmap2;
		}

		/// <summary>
		/// 重绘控件显示
		/// </summary>
		/// <param name="e"></param>
		protected override void OnPaint(PaintEventArgs e)
		{
			if (Authorization.nzugaydgwadawdibbas())
			{
				base.OnPaint(e);
				int num = (base.Width - 21) / 2;
				int hour = _NowTime.Hour;
				int minute = _NowTime.Minute;
				float num2 = (float)_NowTime.Second + (float)_NowTime.Millisecond / 1000f;
				int num3 = hour * 30 + 270 + minute / 2;
				int num4 = minute * 6 + 270;
				float angle = num2 * 6f + 270f;
				Graphics graphics = e.Graphics;
				graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
				Font font = new Font(_ShowTextFont, 14f);
				graphics.DrawString(point: new PointF(num - graphics.MeasureString(_ShowText, font).ToSize().Width / 2 + 10, num / 2 + 12), s: _ShowText, font: font, brush: Brushes.Green);
				graphics.SmoothingMode = SmoothingMode.AntiAlias;
				graphics.DrawString(point: new PointF(num - graphics.MeasureString(_NowTime.DayOfWeek.ToString(), new Font(_ShowTextFont, 10f)).ToSize().Width / 2 + 10, num * 3 / 2 - 2), s: _NowTime.DayOfWeek.ToString(), font: new Font(_ShowTextFont, 10f), brush: Brushes.Chocolate);
				graphics.TranslateTransform(base.Width / 2, base.Width / 2);
				graphics.RotateTransform(num3, MatrixOrder.Prepend);
				graphics.DrawLine(new Pen(_HourColor, 2f), new Point(4, 0), new Point(9, 0));
				graphics.DrawClosedCurve(new Pen(_HourColor, 1f), new Point[6]
				{
					new Point(12, 2),
					new Point(10, 0),
					new Point(12, -2),
					new Point(num / 2, -2),
					new Point(num / 2 + 6, 0),
					new Point(num / 2, 2)
				});
				graphics.RotateTransform(-num3);
				graphics.RotateTransform(num4, MatrixOrder.Prepend);
				graphics.DrawLine(new Pen(_MiniteColor, 2f), new Point(4, 0), new Point(9, 0));
				graphics.DrawClosedCurve(new Pen(_MiniteColor, 1f), new Point[6]
				{
					new Point(14, 2),
					new Point(10, 0),
					new Point(14, -2),
					new Point(num - 17, -2),
					new Point(num - 10, 0),
					new Point(num - 17, 2)
				});
				graphics.RotateTransform(-num4);
				graphics.RotateTransform(angle, MatrixOrder.Prepend);
				graphics.DrawLine(new Pen(_SecondColor, 1f), new Point(-13, 0), new Point(num - 6, 0));
				graphics.ResetTransform();
				string text = _NowTime.Year + "-" + _NowTime.Month + "-" + _NowTime.Day;
				graphics.DrawString(point: new PointF(num - graphics.MeasureString(text, new Font(_ShowTextFont, 12f)).ToSize().Width / 2 + 10, num * 2 + 15), s: text, font: new Font(_ShowTextFont, 12f), brush: Brushes.Green);
			}
		}

		private void ClockMy_SizeChanged(object sender, EventArgs e)
		{
			BackgroundImage = _BackGround();
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 12f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			BackColor = System.Drawing.Color.Transparent;
			Font = new System.Drawing.Font("Courier New", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 134);
			base.Name = "ClockMy";
			base.Size = new System.Drawing.Size(130, 150);
			base.Load += new System.EventHandler(ClockMy_Load);
			base.SizeChanged += new System.EventHandler(ClockMy_SizeChanged);
			ResumeLayout(false);
		}
	}
}
