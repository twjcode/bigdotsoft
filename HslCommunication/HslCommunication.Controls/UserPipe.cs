using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Text;
using System.Windows.Forms;

namespace HslCommunication.Controls
{
	/// <summary>
	///
	/// </summary>
	public class UserPipe : UserControl
	{
		private List<Point> points = new List<Point>();

		private Timer timer = new Timer();

		private float startOffect = 0f;

		private float lineWidth = 5f;

		private float moveSpeed = 1f;

		private Color activeColor = Color.Blue;

		private bool isActive = true;

		private Color lineColor = Color.FromArgb(150, 150, 150);

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置管道线的宽度。
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置管道线的宽度")]
		[Category("外观")]
		[DefaultValue(5f)]
		public float LineWidth
		{
			get
			{
				return lineWidth;
			}
			set
			{
				if (value > 0f)
				{
					lineWidth = value;
					Invalidate();
				}
			}
		}

		/// <summary>
		/// 获取或设置管道线是否处于活动状态。
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置管道线是否处于活动状态")]
		[Category("外观")]
		[DefaultValue(true)]
		public bool IsActive
		{
			get
			{
				return isActive;
			}
			set
			{
				isActive = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置管道活动状态的颜色。
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置管道活动状态的颜色")]
		[Category("外观")]
		[DefaultValue(typeof(Color), "Blue")]
		public Color ActiveColor
		{
			get
			{
				return activeColor;
			}
			set
			{
				activeColor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置管道的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置管道的背景色")]
		[Category("外观")]
		[DefaultValue(typeof(Color), "(150, 150, 150 )")]
		public Color LineColor
		{
			get
			{
				return lineColor;
			}
			set
			{
				lineColor = value;
			}
		}

		/// <summary>
		/// 获取或设置管道线的移动速度。该速度和管道的宽度有关
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置管道线的移动速度。该速度和管道的宽度有关")]
		[Category("外观")]
		[DefaultValue(1f)]
		public float MoveSpeed
		{
			get
			{
				return moveSpeed;
			}
			set
			{
				moveSpeed = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置管道线的坐标。
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置管道线的坐标，格式为0,0;1,1;2,2 分号间隔点")]
		[DefaultValue("")]
		[Category("外观")]
		public string LinePoints
		{
			get
			{
				StringBuilder stringBuilder = new StringBuilder();
				for (int i = 0; i < points.Count; i++)
				{
					stringBuilder.Append(";");
					stringBuilder.Append(points[i].X.ToString());
					stringBuilder.Append(",");
					stringBuilder.Append(points[i].Y.ToString());
				}
				if (stringBuilder.Length > 0)
				{
					return stringBuilder.ToString().Substring(1);
				}
				return string.Empty;
			}
			set
			{
				try
				{
					if (!string.IsNullOrEmpty(value))
					{
						points.Clear();
						string[] array = value.Split(new char[1]
						{
							';'
						}, StringSplitOptions.RemoveEmptyEntries);
						for (int i = 0; i < array.Length; i++)
						{
							string[] array2 = array[i].Split(new char[1]
							{
								','
							}, StringSplitOptions.RemoveEmptyEntries);
							Point item = default(Point);
							item.X = Convert.ToInt32(array2[0]);
							item.Y = Convert.ToInt32(array2[1]);
							points.Add(item);
						}
						Invalidate();
					}
				}
				catch
				{
				}
			}
		}

		/// <summary>
		/// 管道控件信息
		/// </summary>
		public UserPipe()
		{
			InitializeComponent();
			DoubleBuffered = true;
			timer.Interval = 50;
			timer.Tick += Timer_Tick;
			timer.Start();
		}

		private void UserPipe_Paint(object sender, PaintEventArgs e)
		{
			if (!Authorization.nzugaydgwadawdibbas())
			{
				return;
			}
			Graphics graphics = e.Graphics;
			graphics.SmoothingMode = SmoothingMode.HighQuality;
			graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
			Pen pen = new Pen(lineColor, lineWidth);
			if (points.Count > 1)
			{
				graphics.DrawLines(pen, points.ToArray());
			}
			if (isActive)
			{
				pen.DashStyle = DashStyle.Dash;
				pen.DashPattern = new float[2]
				{
					5f,
					5f
				};
				pen.DashOffset = startOffect;
				pen.Color = activeColor;
				if (points.Count > 1)
				{
					graphics.DrawLines(pen, points.ToArray());
				}
			}
			pen.Dispose();
		}

		private void UserPipe_Load(object sender, EventArgs e)
		{
		}

		private void Timer_Tick(object sender, EventArgs e)
		{
			startOffect -= moveSpeed;
			if (startOffect <= -10f || startOffect >= 10f)
			{
				startOffect = 0f;
			}
			Invalidate();
		}

		/// <summary>
		/// 绘制
		/// </summary>
		/// <param name="g"></param>
		public void OnPaintMainWindow(Graphics g)
		{
			g.TranslateTransform(base.Location.X, base.Location.Y);
			UserPipe_Paint(null, new PaintEventArgs(g, default(Rectangle)));
			g.TranslateTransform(-base.Location.X, -base.Location.Y);
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 12f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			BackColor = System.Drawing.Color.Transparent;
			base.Name = "UserPipe";
			base.Size = new System.Drawing.Size(635, 371);
			base.Load += new System.EventHandler(UserPipe_Load);
			base.Paint += new System.Windows.Forms.PaintEventHandler(UserPipe_Paint);
			ResumeLayout(false);
		}
	}
}
