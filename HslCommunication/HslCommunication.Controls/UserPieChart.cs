using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Linq;
using System.Windows.Forms;
using HslCommunication.Core;

namespace HslCommunication.Controls
{
	/// <summary>
	/// 一个饼图的控件
	/// </summary>
	public class UserPieChart : UserControl
	{
		private HslPieItem[] pieItems = new HslPieItem[0];

		private Random random = null;

		private StringFormat formatCenter = null;

		private int margin = 40;

		private bool m_IsRenderPercent = false;

		private bool m_IsRenderSmall = true;

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 是否显示百分比信息
		/// </summary>
		[Browsable(true)]
		[Category("外观")]
		[DefaultValue(false)]
		[Description("获取或设置是否显示百分比占用")]
		public bool IsRenderPercent
		{
			get
			{
				return m_IsRenderPercent;
			}
			set
			{
				m_IsRenderPercent = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 是否在图形上显示占比非常小的文本信息
		/// </summary>
		[Browsable(true)]
		[Category("外观")]
		[Description("获取或设置是否显示占比很小的文本信息")]
		[DefaultValue(true)]
		public bool IsRenderSmall
		{
			get
			{
				return m_IsRenderSmall;
			}
			set
			{
				m_IsRenderSmall = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 实例化一个饼图的控件
		/// </summary>
		public UserPieChart()
		{
			InitializeComponent();
			random = new Random();
			DoubleBuffered = true;
			formatCenter = new StringFormat();
			formatCenter.Alignment = StringAlignment.Center;
			formatCenter.LineAlignment = StringAlignment.Center;
			pieItems = new HslPieItem[0];
		}

		private void UserPieChart_Load(object sender, EventArgs e)
		{
		}

		private void SetMarginPaint(int value)
		{
			if (value > 500)
			{
				margin = 80;
			}
			else if (value > 300)
			{
				margin = 60;
			}
			else
			{
				margin = 40;
			}
		}

		private Point GetCenterPoint(out int width)
		{
			if (base.Width > base.Height)
			{
				SetMarginPaint(base.Height);
				width = base.Height / 2 - margin;
				return new Point(base.Height / 2 - 1, base.Height / 2 - 1);
			}
			SetMarginPaint(base.Width);
			width = base.Width / 2 - margin;
			return new Point(base.Width / 2 - 1, base.Width / 2 - 1);
		}

		/// <summary>
		/// 随机生成颜色，该颜色相对于白色为深色颜色
		/// </summary>
		/// <returns></returns>
		private Color GetRandomColor()
		{
			int num = random.Next(256);
			int num2 = random.Next(256);
			int blue = (num + num2 > 430) ? random.Next(100) : random.Next(200);
			return Color.FromArgb(num, num2, blue);
		}

		private void UserPieChart_Paint(object sender, PaintEventArgs e)
		{
			if (!Authorization.nzugaydgwadawdibbas())
			{
				return;
			}
			e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
			e.Graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
			int width;
			Point centerPoint = GetCenterPoint(out width);
			Rectangle rectangle = new Rectangle(centerPoint.X - width, centerPoint.Y - width, width + width, width + width);
			if (width > 0 && pieItems.Length != 0)
			{
				e.Graphics.FillEllipse(Brushes.AliceBlue, rectangle);
				e.Graphics.DrawEllipse(Pens.DodgerBlue, rectangle);
				Rectangle rect = new Rectangle(rectangle.X - centerPoint.X, rectangle.Y - centerPoint.Y, rectangle.Width, rectangle.Height);
				e.Graphics.TranslateTransform(centerPoint.X, centerPoint.Y);
				e.Graphics.RotateTransform(90f);
				e.Graphics.DrawLine(Pens.DimGray, 0, 0, width, 0);
				int num = pieItems.Sum((HslPieItem item) => item.Value);
				float num2 = 0f;
				float num3 = -90f;
				for (int i = 0; i < pieItems.Length; i++)
				{
					float num4 = 0f;
					num4 = ((num != 0) ? Convert.ToSingle((double)pieItems[i].Value * 1.0 / (double)num * 360.0) : ((float)(360 / pieItems.Length)));
					using (Brush brush = new SolidBrush(pieItems[i].Back))
					{
						e.Graphics.FillPie(brush, rect, 0f, 0f - num4);
					}
					e.Graphics.RotateTransform(0f - num4 / 2f);
					if (num4 < 2f && !IsRenderSmall)
					{
						num2 += num4;
						continue;
					}
					num2 += num4 / 2f;
					int num5 = 8;
					if (num2 < 45f || num2 > 315f)
					{
						num5 = 15;
					}
					if (num2 > 135f && num2 < 225f)
					{
						num5 = 15;
					}
					e.Graphics.DrawLine(Pens.DimGray, width * 2 / 3, 0, width + num5, 0);
					e.Graphics.TranslateTransform(width + num5, 0f);
					if (num2 - num3 < 5f)
					{
					}
					num3 = num2;
					if (num2 < 90f)
					{
						e.Graphics.RotateTransform(num2 - 90f);
						e.Graphics.DrawLine(Pens.DimGray, 0, 0, margin - num5, 0);
						e.Graphics.DrawString(pieItems[i].Name, Font, Brushes.DimGray, new Point(0, -Font.Height));
						if (IsRenderPercent)
						{
							e.Graphics.DrawString(Math.Round(num4 * 100f / 360f, 2) + "%", Font, Brushes.DodgerBlue, new Point(0, 1));
						}
						e.Graphics.RotateTransform(90f - num2);
					}
					else if (num2 < 180f)
					{
						e.Graphics.RotateTransform(num2 - 90f);
						e.Graphics.DrawLine(Pens.DimGray, 0, 0, margin - num5, 0);
						e.Graphics.DrawString(pieItems[i].Name, Font, Brushes.DimGray, new Point(0, -Font.Height));
						if (IsRenderPercent)
						{
							e.Graphics.DrawString(Math.Round(num4 * 100f / 360f, 2) + "%", Font, Brushes.DodgerBlue, new Point(0, 1));
						}
						e.Graphics.RotateTransform(90f - num2);
					}
					else if (num2 < 270f)
					{
						e.Graphics.RotateTransform(num2 - 270f);
						e.Graphics.DrawLine(Pens.DimGray, 0, 0, margin - num5, 0);
						e.Graphics.TranslateTransform(margin - 8, 0f);
						e.Graphics.RotateTransform(180f);
						e.Graphics.DrawString(pieItems[i].Name, Font, Brushes.DimGray, new Point(0, -Font.Height));
						if (IsRenderPercent)
						{
							e.Graphics.DrawString(Math.Round(num4 * 100f / 360f, 2) + "%", Font, Brushes.DodgerBlue, new Point(0, 1));
						}
						e.Graphics.RotateTransform(-180f);
						e.Graphics.TranslateTransform(8 - margin, 0f);
						e.Graphics.RotateTransform(270f - num2);
					}
					else
					{
						e.Graphics.RotateTransform(num2 - 270f);
						e.Graphics.DrawLine(Pens.DimGray, 0, 0, margin - num5, 0);
						e.Graphics.TranslateTransform(margin - 8, 0f);
						e.Graphics.RotateTransform(180f);
						e.Graphics.DrawString(pieItems[i].Name, Font, Brushes.DimGray, new Point(0, -Font.Height));
						if (IsRenderPercent)
						{
							e.Graphics.DrawString(Math.Round(num4 * 100f / 360f, 2) + "%", Font, Brushes.DodgerBlue, new Point(0, 1));
						}
						e.Graphics.RotateTransform(-180f);
						e.Graphics.TranslateTransform(8 - margin, 0f);
						e.Graphics.RotateTransform(270f - num2);
					}
					e.Graphics.TranslateTransform(-width - num5, 0f);
					e.Graphics.RotateTransform(0f - num4 / 2f);
					num2 += num4 / 2f;
				}
				e.Graphics.ResetTransform();
			}
			else
			{
				e.Graphics.FillEllipse(Brushes.AliceBlue, rectangle);
				e.Graphics.DrawEllipse(Pens.DodgerBlue, rectangle);
				e.Graphics.DrawString("空", Font, Brushes.DimGray, rectangle, formatCenter);
			}
		}

		/// <summary>
		/// 设置显示的数据源
		/// </summary>
		/// <param name="source">特殊的显示对象</param>
		/// <exception cref="T:System.ArgumentNullException"></exception>
		public void SetDataSource(HslPieItem[] source)
		{
			if (source != null)
			{
				pieItems = source;
				Invalidate();
			}
		}

		/// <summary>
		/// 根据名称和值进行数据源的显示，两者的长度需要一致
		/// </summary>
		/// <param name="names">名称</param>
		/// <param name="values">值</param>
		/// <exception cref="T:System.ArgumentNullException"></exception>
		public void SetDataSource(string[] names, int[] values)
		{
			if (names == null)
			{
				throw new ArgumentNullException("names");
			}
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (names.Length != values.Length)
			{
				throw new Exception("两个数组的长度不一致！");
			}
			pieItems = new HslPieItem[names.Length];
			for (int i = 0; i < names.Length; i++)
			{
				pieItems[i] = new HslPieItem
				{
					Name = names[i],
					Value = values[i],
					Back = GetRandomColor()
				};
			}
			Invalidate();
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
				formatCenter?.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleDimensions = new System.Drawing.SizeF(7f, 17f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			BackColor = System.Drawing.Color.Transparent;
			Font = new System.Drawing.Font("微软雅黑", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 134);
			base.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			base.Name = "UserPieChart";
			base.Size = new System.Drawing.Size(200, 200);
			base.Load += new System.EventHandler(UserPieChart_Load);
			base.Paint += new System.Windows.Forms.PaintEventHandler(UserPieChart_Paint);
			ResumeLayout(false);
		}
	}
}
