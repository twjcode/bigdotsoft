using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslCommunication.Controls
{
	/// <summary>
	/// 一个自定义的按钮控件
	/// </summary>
	[DefaultEvent("Click")]
	public class UserButton : UserControl
	{
		private int RoundCorner = 3;

		private StringFormat sf = null;

		private string _text = "button";

		private bool m_Selected = false;

		private Color m_backcor = Color.Lavender;

		private Color m_enablecolor = Color.FromArgb(190, 190, 190);

		private Color m_active = Color.AliceBlue;

		private bool m_BorderVisiable = true;

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 设置或获取显示的文本
		/// </summary>
		[Category("外观")]
		[DefaultValue("button")]
		[Description("用来设置显示的文本信息")]
		public string UIText
		{
			get
			{
				return _text;
			}
			set
			{
				_text = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 设置或获取显示文本的颜色
		/// </summary>
		[Category("外观")]
		[DefaultValue(typeof(Color), "Black")]
		[Description("用来设置显示的文本的颜色")]
		public Color TextColor
		{
			get;
			set;
		} = Color.Black;


		/// <summary>
		/// 设置按钮的圆角
		/// </summary>
		[Category("外观")]
		[DefaultValue(3)]
		[Description("按钮框的圆角属性")]
		public int CornerRadius
		{
			get
			{
				return RoundCorner;
			}
			set
			{
				RoundCorner = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 用来设置按钮的选中状态
		/// </summary>
		[Category("外观")]
		[DefaultValue(false)]
		[Description("指示按钮的选中状态")]
		public bool Selected
		{
			get
			{
				return m_Selected;
			}
			set
			{
				m_Selected = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 已经弃用
		/// </summary>
		[Browsable(false)]
		public override Color ForeColor
		{
			get
			{
				return base.ForeColor;
			}
			set
			{
				base.ForeColor = value;
			}
		}

		/// <summary>
		/// 已经弃用
		/// </summary>
		[Browsable(false)]
		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
			}
		}

		/// <summary>
		/// 按钮的背景色
		/// </summary>
		[Category("外观")]
		[DefaultValue(typeof(Color), "Lavender")]
		[Description("按钮的背景色")]
		public Color OriginalColor
		{
			get
			{
				return m_backcor;
			}
			set
			{
				m_backcor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 按钮的背景色
		/// </summary>
		[Category("外观")]
		[Description("按钮的活动色")]
		public Color EnableColor
		{
			get
			{
				return m_enablecolor;
			}
			set
			{
				m_enablecolor = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 鼠标挪动时的活动颜色
		/// </summary>
		[Category("外观")]
		[DefaultValue(typeof(Color), "AliceBlue")]
		[Description("按钮的活动色")]
		public Color ActiveColor
		{
			get
			{
				return m_active;
			}
			set
			{
				m_active = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 设置按钮的边框是否可见
		/// </summary>
		[Category("外观")]
		[Browsable(true)]
		[DefaultValue(true)]
		[Description("指示按钮是否存在边框")]
		public bool BorderVisiable
		{
			get
			{
				return m_BorderVisiable;
			}
			set
			{
				m_BorderVisiable = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 存放用户需要保存的一些额外的信息
		/// </summary>
		[Browsable(false)]
		public string CustomerInformation
		{
			get;
			set;
		} = "";


		private bool is_mouse_on
		{
			get;
			set;
		} = false;


		private bool is_left_mouse_down
		{
			get;
			set;
		} = false;


		/// <summary>
		/// 实例化一个按钮对象
		/// </summary>
		public UserButton()
		{
			InitializeComponent();
		}

		private void UserButton_Load(object sender, EventArgs e)
		{
			sf = new StringFormat();
			sf.Alignment = StringAlignment.Center;
			sf.LineAlignment = StringAlignment.Center;
			base.SizeChanged += UserButton_SizeChanged;
			Font = new Font("微软雅黑", Font.Size, Font.Style);
			base.MouseEnter += UserButton_MouseEnter;
			base.MouseLeave += UserButton_MouseLeave;
			base.MouseDown += UserButton_MouseDown;
			base.MouseUp += UserButton_MouseUp;
			SetStyle(ControlStyles.ResizeRedraw, value: true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, value: true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, value: true);
		}

		private void UserButton_SizeChanged(object sender, EventArgs e)
		{
			if (base.Width > 1)
			{
				Invalidate();
			}
		}

		private void UserButton_MouseLeave(object sender, EventArgs e)
		{
			is_mouse_on = false;
			Invalidate();
		}

		private void UserButton_MouseEnter(object sender, EventArgs e)
		{
			is_mouse_on = true;
			Invalidate();
		}

		private void UserButton_MouseUp(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				is_left_mouse_down = false;
				Invalidate();
			}
		}

		private void UserButton_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				is_left_mouse_down = true;
				Invalidate();
			}
		}

		/// <summary>
		/// 触发一次点击的事件
		/// </summary>
		public void PerformClick()
		{
			OnClick(new EventArgs());
		}

		/// <summary>
		/// 重绘数据区
		/// </summary>
		/// <param name="e"></param>
		protected override void OnPaint(PaintEventArgs e)
		{
			if (!Authorization.nzugaydgwadawdibbas())
			{
				return;
			}
			e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
			e.Graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
			GraphicsPath graphicsPath = new GraphicsPath();
			graphicsPath.AddLine(RoundCorner, 0, base.Width - RoundCorner - 1, 0);
			graphicsPath.AddArc(base.Width - RoundCorner * 2 - 1, 0, RoundCorner * 2, RoundCorner * 2, 270f, 90f);
			graphicsPath.AddLine(base.Width - 1, RoundCorner, base.Width - 1, base.Height - RoundCorner - 1);
			graphicsPath.AddArc(base.Width - RoundCorner * 2 - 1, base.Height - RoundCorner * 2 - 1, RoundCorner * 2, RoundCorner * 2, 0f, 90f);
			graphicsPath.AddLine(base.Width - RoundCorner - 1, base.Height - 1, RoundCorner, base.Height - 1);
			graphicsPath.AddArc(0, base.Height - RoundCorner * 2 - 1, RoundCorner * 2, RoundCorner * 2, 90f, 90f);
			graphicsPath.AddLine(0, base.Height - RoundCorner - 1, 0, RoundCorner);
			graphicsPath.AddArc(0, 0, RoundCorner * 2, RoundCorner * 2, 180f, 90f);
			Brush brush = null;
			Brush brush2 = null;
			Rectangle r = new Rectangle(base.ClientRectangle.X, base.ClientRectangle.Y, base.ClientRectangle.Width, base.ClientRectangle.Height);
			if (base.Enabled)
			{
				brush = new SolidBrush(TextColor);
				brush2 = (Selected ? new SolidBrush(Color.DodgerBlue) : ((!is_mouse_on) ? new SolidBrush(OriginalColor) : new SolidBrush(ActiveColor)));
				if (is_left_mouse_down)
				{
					r.Offset(1, 1);
				}
			}
			else
			{
				brush = new SolidBrush(Color.Gray);
				brush2 = new SolidBrush(EnableColor);
			}
			e.Graphics.FillPath(brush2, graphicsPath);
			Pen pen = new Pen(Color.FromArgb(170, 170, 170));
			if (BorderVisiable)
			{
				e.Graphics.DrawPath(pen, graphicsPath);
			}
			e.Graphics.DrawString(UIText, Font, brush, r, sf);
			brush.Dispose();
			brush2.Dispose();
			pen.Dispose();
			graphicsPath.Dispose();
		}

		private void UserButton_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Return)
			{
				OnClick(new EventArgs());
			}
		}

		/// <summary>
		/// 点击按钮的触发事件
		/// </summary>
		/// <param name="e"></param>
		protected override void OnClick(EventArgs e)
		{
			if (base.Enabled)
			{
				base.OnClick(e);
			}
		}

		/// <summary>
		/// 点击的时候触发事件
		/// </summary>
		/// <param name="e"></param>
		protected override void OnMouseClick(MouseEventArgs e)
		{
			if (base.Enabled)
			{
				base.OnMouseClick(e);
			}
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 12f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			BackColor = System.Drawing.Color.Transparent;
			base.Name = "UserButton";
			base.Size = new System.Drawing.Size(78, 25);
			base.Load += new System.EventHandler(UserButton_Load);
			base.KeyDown += new System.Windows.Forms.KeyEventHandler(UserButton_KeyDown);
			ResumeLayout(false);
		}
	}
}
