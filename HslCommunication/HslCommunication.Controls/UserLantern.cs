using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslCommunication.Controls
{
	/// <summary>
	/// 信号灯的控件类
	/// </summary>
	public class UserLantern : UserControl
	{
		private Color color_lantern_background = Color.LimeGreen;

		private Brush brush_lantern_background = null;

		private Pen pen_lantern_background = null;

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置开关按钮的背景色
		/// </summary>
		[Browsable(true)]
		[Description("获取或设置信号灯的背景色")]
		[Category("外观")]
		[DefaultValue(typeof(Color), "LimeGreen")]
		public Color LanternBackground
		{
			get
			{
				return color_lantern_background;
			}
			set
			{
				color_lantern_background = value;
				brush_lantern_background?.Dispose();
				pen_lantern_background?.Dispose();
				brush_lantern_background = new SolidBrush(color_lantern_background);
				pen_lantern_background = new Pen(color_lantern_background, 2f);
				Invalidate();
			}
		}

		/// <summary>
		/// 实例化一个信号灯控件的对象
		/// </summary>
		public UserLantern()
		{
			InitializeComponent();
			DoubleBuffered = true;
			brush_lantern_background = new SolidBrush(color_lantern_background);
			pen_lantern_background = new Pen(color_lantern_background, 2f);
		}

		private void UserLantern_Load(object sender, EventArgs e)
		{
		}

		private void UserLantern_Paint(object sender, PaintEventArgs e)
		{
			if (Authorization.nzugaydgwadawdibbas())
			{
				e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
				e.Graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
				Point centerPoint = GetCenterPoint();
				e.Graphics.TranslateTransform(centerPoint.X, centerPoint.Y);
				int num = centerPoint.X - 5;
				if (num >= 5)
				{
					Rectangle rect = new Rectangle(-num - 4, -num - 4, 2 * num + 8, 2 * num + 8);
					Rectangle rect2 = new Rectangle(-num, -num, 2 * num, 2 * num);
					e.Graphics.DrawEllipse(pen_lantern_background, rect);
					e.Graphics.FillEllipse(brush_lantern_background, rect2);
				}
			}
		}

		private Point GetCenterPoint()
		{
			if (base.Height > base.Width)
			{
				return new Point((base.Width - 1) / 2, (base.Width - 1) / 2);
			}
			return new Point((base.Height - 1) / 2, (base.Height - 1) / 2);
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 12f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			BackColor = System.Drawing.Color.Transparent;
			base.Name = "UserLantern";
			base.Load += new System.EventHandler(UserLantern_Load);
			base.Paint += new System.Windows.Forms.PaintEventHandler(UserLantern_Paint);
			ResumeLayout(false);
		}
	}
}
