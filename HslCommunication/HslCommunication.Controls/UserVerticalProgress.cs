using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Text;
using System.Threading;
using System.Windows.Forms;
using HslCommunication.Core;

namespace HslCommunication.Controls
{
	/// <summary>
	/// 一个直立的进度条控件，满足不同的情况使用
	/// </summary>
	public class UserVerticalProgress : UserControl
	{
		private int m_version = 0;

		private int m_Max = 100;

		private int m_value = 0;

		private int m_actual = 0;

		private Pen m_borderPen;

		private Color m_borderColor;

		private Brush m_backBrush;

		private Brush m_foreBrush;

		private Color m_progressColor;

		private StringFormat m_formatCenter;

		private bool m_isTextRender = true;

		private Action m_UpdateAction;

		private SimpleHybirdLock hybirdLock;

		private int m_speed = 1;

		private ProgressStyle m_progressStyle = ProgressStyle.Vertical;

		private bool m_UseAnimation = false;

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置光标在控件上显示的信息
		/// </summary>
		public override Cursor Cursor
		{
			get
			{
				return base.Cursor;
			}
			set
			{
				base.Cursor = value;
			}
		}

		/// <summary>
		/// 获取或设置控件的背景颜色值
		/// </summary>
		[Description("获取或设置进度条的背景色")]
		[Category("外观")]
		[Browsable(true)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
				m_backBrush?.Dispose();
				m_backBrush = new SolidBrush(value);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置进度的颜色
		/// </summary>
		[Description("获取或设置进度条的前景色")]
		[Category("外观")]
		[Browsable(true)]
		[DefaultValue(typeof(Color), "Tomato")]
		public Color ProgressColor
		{
			get
			{
				return m_progressColor;
			}
			set
			{
				m_progressColor = value;
				m_foreBrush?.Dispose();
				m_foreBrush = new SolidBrush(value);
				Invalidate();
			}
		}

		/// <summary>
		/// 进度条的最大值，默认为100
		/// </summary>
		[Description("获取或设置进度条的最大值，默认为100")]
		[Category("外观")]
		[Browsable(true)]
		[DefaultValue(100)]
		public int Max
		{
			get
			{
				return m_Max;
			}
			set
			{
				if (value > 1)
				{
					m_Max = value;
				}
				if (m_value > m_Max)
				{
					m_value = m_Max;
				}
				Invalidate();
			}
		}

		/// <summary>
		/// 当前进度条的值，不能大于最大值或小于0
		/// </summary>
		[Description("获取或设置当前进度条的值")]
		[Category("外观")]
		[Browsable(true)]
		[DefaultValue(0)]
		public int Value
		{
			get
			{
				return m_value;
			}
			set
			{
				if (value >= 0 && value <= m_Max && value != m_value)
				{
					m_value = value;
					if (UseAnimation)
					{
						int num = Interlocked.Increment(ref m_version);
						ThreadPool.QueueUserWorkItem(ThreadPoolUpdateProgress, num);
					}
					else
					{
						m_actual = value;
						Invalidate();
					}
				}
			}
		}

		/// <summary>
		/// 是否显示进度
		/// </summary>
		[Description("获取或设置是否显示进度文本")]
		[Category("外观")]
		[Browsable(true)]
		[DefaultValue(true)]
		public bool IsTextRender
		{
			get
			{
				return m_isTextRender;
			}
			set
			{
				m_isTextRender = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 设置进度条的边框颜色
		/// </summary>
		[Description("获取或设置进度条的边框颜色")]
		[Category("外观")]
		[Browsable(true)]
		[DefaultValue(typeof(Color), "DimGray")]
		public Color BorderColor
		{
			get
			{
				return m_borderColor;
			}
			set
			{
				m_borderColor = value;
				m_borderPen?.Dispose();
				m_borderPen = new Pen(value);
				Invalidate();
			}
		}

		/// <summary>
		/// 设置进度变更的速度
		/// </summary>
		[Description("获取或设置进度条的变化进度")]
		[Category("外观")]
		[Browsable(true)]
		[DefaultValue(1)]
		public int ValueChangeSpeed
		{
			get
			{
				return m_speed;
			}
			set
			{
				if (value >= 1)
				{
					m_speed = value;
				}
			}
		}

		/// <summary>
		/// 获取或设置进度条变化的时候是否采用动画效果
		/// </summary>
		[Description("获取或设置进度条变化的时候是否采用动画效果")]
		[Category("外观")]
		[Browsable(true)]
		[DefaultValue(false)]
		public bool UseAnimation
		{
			get
			{
				return m_UseAnimation;
			}
			set
			{
				m_UseAnimation = value;
			}
		}

		/// <summary>
		/// 进度条的样式
		/// </summary>
		[Description("获取或设置进度条的样式")]
		[Category("外观")]
		[Browsable(true)]
		[DefaultValue(typeof(ProgressStyle), "Vertical")]
		public ProgressStyle ProgressStyle
		{
			get
			{
				return m_progressStyle;
			}
			set
			{
				m_progressStyle = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 实例化一个对象
		/// </summary>
		public UserVerticalProgress()
		{
			InitializeComponent();
			m_borderPen = new Pen(Color.DimGray);
			m_backBrush = new SolidBrush(Color.Transparent);
			m_foreBrush = new SolidBrush(Color.Tomato);
			m_progressColor = Color.Tomato;
			m_borderColor = Color.DimGray;
			m_formatCenter = new StringFormat();
			m_formatCenter.Alignment = StringAlignment.Center;
			m_formatCenter.LineAlignment = StringAlignment.Center;
			m_UpdateAction = UpdateRender;
			hybirdLock = new SimpleHybirdLock();
			DoubleBuffered = true;
		}

		private void UserVerticalProgress_Load(object sender, EventArgs e)
		{
		}

		private void UserVerticalProgress_Paint(object sender, PaintEventArgs e)
		{
			if (!Authorization.nzugaydgwadawdibbas())
			{
				return;
			}
			try
			{
				Graphics graphics = e.Graphics;
				graphics.FillRectangle(rect: new Rectangle(0, 0, base.Width - 1, base.Height - 1), brush: m_backBrush);
				graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
				switch (m_progressStyle)
				{
				case ProgressStyle.Vertical:
				{
					int num2 = (int)((long)m_actual * (long)(base.Height - 2) / m_Max);
					graphics.FillRectangle(rect: new Rectangle(0, base.Height - 1 - num2, base.Width - 1, num2), brush: m_foreBrush);
					break;
				}
				case ProgressStyle.Horizontal:
				{
					int num = (int)((long)m_actual * (long)(base.Width - 2) / m_Max);
					graphics.FillRectangle(rect: new Rectangle(0, 0, num + 1, base.Height - 1), brush: m_foreBrush);
					break;
				}
				}
				Rectangle rectangle = new Rectangle(0, 0, base.Width - 1, base.Height - 1);
				if (m_isTextRender)
				{
					string s = (long)m_actual * 100L / m_Max + "%";
					using Brush brush = new SolidBrush(ForeColor);
					graphics.DrawString(s, Font, brush, rectangle, m_formatCenter);
				}
				graphics.DrawRectangle(m_borderPen, rectangle);
			}
			catch (Exception)
			{
			}
		}

		private void UserVerticalProgress_SizeChanged(object sender, EventArgs e)
		{
			Invalidate();
		}

		private void ThreadPoolUpdateProgress(object obj)
		{
			try
			{
				int num = (int)obj;
				if (m_speed < 1)
				{
					m_speed = 1;
				}
				while (m_actual != m_value)
				{
					Thread.Sleep(17);
					if (num != m_version)
					{
						break;
					}
					hybirdLock.Enter();
					int num2 = 0;
					if (m_actual > m_value)
					{
						int num3 = m_actual - m_value;
						if (num3 > m_speed)
						{
							num3 = m_speed;
						}
						num2 = m_actual - num3;
					}
					else
					{
						int num4 = m_value - m_actual;
						if (num4 > m_speed)
						{
							num4 = m_speed;
						}
						num2 = m_actual + num4;
					}
					m_actual = num2;
					hybirdLock.Leave();
					if (num == m_version)
					{
						if (base.IsHandleCreated)
						{
							Invoke(m_UpdateAction);
						}
						continue;
					}
					break;
				}
			}
			catch (Exception)
			{
			}
		}

		private void UpdateRender()
		{
			Invalidate();
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleDimensions = new System.Drawing.SizeF(7f, 17f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			BackColor = System.Drawing.SystemColors.Control;
			Font = new System.Drawing.Font("微软雅黑", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 134);
			base.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			base.Name = "UserVerticalProgress";
			base.Size = new System.Drawing.Size(44, 192);
			base.Load += new System.EventHandler(UserVerticalProgress_Load);
			base.SizeChanged += new System.EventHandler(UserVerticalProgress_SizeChanged);
			base.Paint += new System.Windows.Forms.PaintEventHandler(UserVerticalProgress_Paint);
			ResumeLayout(false);
		}
	}
}
