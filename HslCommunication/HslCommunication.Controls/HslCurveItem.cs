using System.Drawing;

namespace HslCommunication.Controls
{
	/// <summary>
	/// 曲线数据对象
	/// </summary>
	internal class HslCurveItem
	{
		/// <summary>
		/// 数据
		/// </summary>
		public float[] Data = null;

		/// <summary>
		/// 线条的宽度
		/// </summary>
		public float LineThickness
		{
			get;
			set;
		}

		/// <summary>
		/// 曲线颜色
		/// </summary>
		public Color LineColor
		{
			get;
			set;
		}

		/// <summary>
		/// 是否左侧参考系，True为左侧，False为右侧
		/// </summary>
		public bool IsLeftFrame
		{
			get;
			set;
		}

		/// <summary>
		/// 本曲线是否显示出来，默认为显示
		/// </summary>
		public bool Visible
		{
			get;
			set;
		}

		/// <summary>
		/// 实例化一个对象
		/// </summary>
		public HslCurveItem()
		{
			LineThickness = 1f;
			IsLeftFrame = true;
			Visible = true;
		}
	}
}
