using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslCommunication.Controls
{
	/// <summary>
	/// 瓶子控件
	/// </summary>
	public class UserBottle : UserControl
	{
		private double value = 50.0;

		private bool isOpen = false;

		private string bottleTag = "";

		private StringFormat stringFormat = new StringFormat();

		private string headTag = "原料1";

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置瓶子的液位值。
		/// </summary>
		[Browsable(true)]
		[DefaultValue(typeof(double), "60")]
		[Category("外观")]
		public double Value
		{
			get
			{
				return value;
			}
			set
			{
				if (value >= 0.0 && value <= 100.0)
				{
					this.value = value;
					Invalidate();
				}
			}
		}

		/// <summary>
		/// 获取或设置瓶子是否处于打开的状态。
		/// </summary>
		[Browsable(true)]
		[DefaultValue(typeof(bool), "false")]
		[Category("外观")]
		public bool IsOpen
		{
			get
			{
				return isOpen;
			}
			set
			{
				isOpen = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置瓶子的标签信息，用于绘制在瓶子上的信息。
		/// </summary>
		[Browsable(true)]
		[DefaultValue(typeof(string), "")]
		[Category("外观")]
		public string BottleTag
		{
			get
			{
				return bottleTag;
			}
			set
			{
				bottleTag = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置瓶子的备注信息，用于绘制在瓶子顶部的信息。
		/// </summary>
		[Browsable(true)]
		[DefaultValue(typeof(string), "原料1")]
		[Category("外观")]
		public string HeadTag
		{
			get
			{
				return headTag;
			}
			set
			{
				headTag = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 实例化一个新的控件对象
		/// </summary>
		public UserBottle()
		{
			InitializeComponent();
			DoubleBuffered = true;
			stringFormat.Alignment = StringAlignment.Center;
			stringFormat.LineAlignment = StringAlignment.Center;
		}

		/// <summary>
		/// 重写消息处理机制
		/// </summary>
		/// <param name="m">系统消息</param>
		protected override void WndProc(ref Message m)
		{
			if (m.Msg != 20)
			{
				base.WndProc(ref m);
			}
		}

		/// <summary>
		/// 重新绘制界面图形
		/// </summary>
		/// <param name="e">绘制消息</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			if (!Authorization.nzugaydgwadawdibbas())
			{
				return;
			}
			Graphics graphics = e.Graphics;
			graphics.SmoothingMode = SmoothingMode.HighQuality;
			graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
			if (base.Width >= 15 && base.Height >= 15)
			{
				int num = base.Width / 2;
				int num2 = base.Height - base.Width - (base.Height - base.Width - 20) * Convert.ToInt32(value) / 100;
				GraphicsPath graphicsPath = new GraphicsPath();
				graphicsPath.AddPolygon(new Point[5]
				{
					new Point(0, 20),
					new Point(0, base.Height - base.Width),
					new Point(num + 1, base.Height - 8),
					new Point(num + 1, 20),
					new Point(0, 20)
				});
				Brush brush = new LinearGradientBrush(new Point(0, 20), new Point(num + 1, 20), Color.FromArgb(142, 196, 216), Color.FromArgb(240, 240, 240));
				graphics.FillPath(brush, graphicsPath);
				graphicsPath.Reset();
				graphicsPath.AddPolygon(new Point[5]
				{
					new Point(num, 20),
					new Point(num, base.Height - 8),
					new Point(base.Width - 1, base.Height - base.Width),
					new Point(base.Width - 1, 20),
					new Point(num, 20)
				});
				brush.Dispose();
				brush = new LinearGradientBrush(new Point(num - 1, 20), new Point(base.Width - 1, 20), Color.FromArgb(240, 240, 240), Color.FromArgb(142, 196, 216));
				graphics.FillPath(brush, graphicsPath);
				brush.Dispose();
				brush = new SolidBrush(Color.FromArgb(151, 232, 244));
				graphics.FillEllipse(brush, 1, 17, base.Width - 3, 6);
				brush.Dispose();
				graphicsPath.Reset();
				graphicsPath.AddPolygon(new Point[5]
				{
					new Point(0, num2),
					new Point(0, base.Height - base.Width),
					new Point(num + 1, base.Height - 8),
					new Point(num + 1, num2),
					new Point(0, num2)
				});
				brush = new LinearGradientBrush(new Point(0, 20), new Point(num + 1, 20), Color.FromArgb(194, 190, 77), Color.FromArgb(226, 221, 98));
				graphics.FillPath(brush, graphicsPath);
				brush.Dispose();
				graphicsPath.Reset();
				graphicsPath.AddPolygon(new Point[5]
				{
					new Point(num, num2),
					new Point(num, base.Height - 8),
					new Point(base.Width - 1, base.Height - base.Width),
					new Point(base.Width - 1, num2),
					new Point(num, num2)
				});
				brush = new LinearGradientBrush(new Point(num - 1, 20), new Point(base.Width - 1, 20), Color.FromArgb(226, 221, 98), Color.FromArgb(194, 190, 77));
				graphics.FillPath(brush, graphicsPath);
				brush.Dispose();
				graphicsPath.Dispose();
				brush = new SolidBrush(Color.FromArgb(243, 245, 139));
				graphics.FillEllipse(brush, 1, num2 - 3, base.Width - 3, 6);
				brush.Dispose();
				graphics.FillEllipse(Brushes.White, 4, base.Height - base.Width, base.Width - 9, base.Width - 9);
				Pen pen = new Pen(Color.Gray, 3f);
				if (isOpen)
				{
					pen.Color = Color.LimeGreen;
				}
				graphics.DrawEllipse(pen, 4, base.Height - base.Width, base.Width - 9, base.Width - 9);
				graphics.FillEllipse(isOpen ? Brushes.LimeGreen : Brushes.Gray, 8, base.Height - base.Width + 4, base.Width - 17, base.Width - 17);
				pen.Dispose();
				if (!string.IsNullOrEmpty(bottleTag))
				{
					graphics.DrawString(bottleTag, Font, Brushes.Gray, new Rectangle(-10, 26, base.Width + 20, 20), stringFormat);
				}
				if (!string.IsNullOrEmpty(headTag))
				{
					graphics.DrawString(headTag, Font, Brushes.DimGray, new Rectangle(-10, 0, base.Width + 20, 20), stringFormat);
				}
			}
		}

		private void UserBottle_Paint(object sender, PaintEventArgs e)
		{
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 12f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			BackColor = System.Drawing.Color.Transparent;
			base.Name = "UserBottle";
			base.Size = new System.Drawing.Size(66, 172);
			base.Paint += new System.Windows.Forms.PaintEventHandler(UserBottle_Paint);
			ResumeLayout(false);
		}
	}
}
