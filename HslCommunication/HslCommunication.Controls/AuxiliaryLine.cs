using System;
using System.Drawing;

namespace HslCommunication.Controls
{
	/// <summary>
	/// 辅助线对象
	/// </summary>
	internal class AuxiliaryLine : IDisposable
	{
		private bool disposedValue = false;

		/// <summary>
		/// 实际的数据值
		/// </summary>
		public float Value
		{
			get;
			set;
		}

		/// <summary>
		/// 实际的数据绘制
		/// </summary>
		public float PaintValue
		{
			get;
			set;
		}

		/// <summary>
		/// 辅助线的颜色
		/// </summary>
		public Color LineColor
		{
			get;
			set;
		}

		/// <summary>
		/// 辅助线的画笔资源
		/// </summary>
		public Pen PenDash
		{
			get;
			set;
		}

		/// <summary>
		/// 辅助线的宽度
		/// </summary>
		public float LineThickness
		{
			get;
			set;
		}

		/// <summary>
		/// 辅助线文本的画刷
		/// </summary>
		public Brush LineTextBrush
		{
			get;
			set;
		}

		/// <summary>
		/// 是否左侧参考系，True为左侧，False为右侧
		/// </summary>
		public bool IsLeftFrame
		{
			get;
			set;
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					PenDash?.Dispose();
					LineTextBrush.Dispose();
				}
				disposedValue = true;
			}
		}

		/// <summary>
		/// 释放内存信息
		/// </summary>
		public void Dispose()
		{
			Dispose(disposing: true);
		}
	}
}
