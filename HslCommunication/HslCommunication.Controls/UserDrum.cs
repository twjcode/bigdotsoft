using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace HslCommunication.Controls
{
	/// <summary>
	/// 一个罐子形状的控件
	/// </summary>
	[DefaultBindingProperty("Text")]
	[DefaultProperty("Text")]
	public class UserDrum : UserControl
	{
		private Color backColor = Color.Silver;

		private Brush backBrush = new SolidBrush(Color.Silver);

		private Color borderColor = Color.DimGray;

		private Pen borderPen = new Pen(Color.DimGray);

		private Color textColor = Color.White;

		private Brush textBrush = new SolidBrush(Color.White);

		private Color textBackColor = Color.DarkGreen;

		private Brush textBackBrush = new SolidBrush(Color.DarkGreen);

		private string text = string.Empty;

		private StringFormat stringFormat = new StringFormat();

		/// <summary> 
		/// 必需的设计器变量。
		/// </summary>
		private IContainer components = null;

		/// <summary>
		/// 获取或设置容器罐的背景色。
		/// </summary>
		[Browsable(true)]
		[DefaultValue(typeof(Color), "Silver")]
		[Category("外观")]
		[Description("获取或设置容器罐的背景色。")]
		public Color DrumBackColor
		{
			get
			{
				return backColor;
			}
			set
			{
				backColor = value;
				backBrush.Dispose();
				backBrush = new SolidBrush(value);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置容器罐的边框色。
		/// </summary>
		[Browsable(true)]
		[DefaultValue(typeof(Color), "DimGray")]
		[Category("外观")]
		[Description("获取或设置容器罐的边框色。")]
		public Color BorderColor
		{
			get
			{
				return borderColor;
			}
			set
			{
				borderColor = value;
				borderPen.Dispose();
				borderPen = new Pen(value);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置显示的文本信息
		/// </summary>
		[Browsable(true)]
		[Category("外观")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Bindable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[Description("获取或设置在容器上显示的文本")]
		public override string Text
		{
			get
			{
				return text;
			}
			set
			{
				text = value;
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置文本的颜色
		/// </summary>
		[Browsable(true)]
		[Category("外观")]
		[DefaultValue(typeof(Color), "White")]
		[Description("获取或设置文本的颜色")]
		public override Color ForeColor
		{
			get
			{
				return textColor;
			}
			set
			{
				textColor = value;
				textBrush.Dispose();
				textBrush = new SolidBrush(value);
				Invalidate();
			}
		}

		/// <summary>
		/// 获取或设置文本的背景色
		/// </summary>
		[Browsable(true)]
		[Category("外观")]
		[DefaultValue(typeof(Color), "DarkGreen")]
		[Description("获取或设置文本的背景色")]
		public Color TextBackColor
		{
			get
			{
				return textBackColor;
			}
			set
			{
				textBackColor = value;
				textBackBrush.Dispose();
				textBackBrush = new SolidBrush(value);
				Invalidate();
			}
		}

		/// <summary>
		/// 实例化一个罐子形状的控件
		/// </summary>
		public UserDrum()
		{
			DoubleBuffered = true;
			stringFormat.Alignment = StringAlignment.Center;
			stringFormat.LineAlignment = StringAlignment.Center;
			InitializeComponent();
		}

		private void UserDrum_Paint(object sender, PaintEventArgs e)
		{
			if (!Authorization.nzugaydgwadawdibbas() || base.Width < 40 || base.Height < 50)
			{
				return;
			}
			Graphics graphics = e.Graphics;
			graphics.SmoothingMode = SmoothingMode.HighQuality;
			graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
			Point[] points = new Point[7]
			{
				new Point(base.Width / 2, 20),
				new Point(base.Width - 10, base.Height * 3 / 10),
				new Point(base.Width - 10, base.Height * 7 / 10),
				new Point(base.Width / 2, base.Height - 20),
				new Point(10, base.Height * 7 / 10),
				new Point(10, base.Height * 3 / 10),
				new Point(base.Width / 2, 20)
			};
			graphics.FillPolygon(backBrush, points);
			graphics.DrawLines(borderPen, points);
			graphics.DrawCurve(borderPen, new Point[3]
			{
				new Point(10, base.Height * 3 / 10),
				new Point(base.Width / 2, base.Height * 3 / 10 + base.Height / 25),
				new Point(base.Width - 10, base.Height * 3 / 10)
			});
			graphics.DrawCurve(borderPen, new Point[3]
			{
				new Point(10, base.Height * 7 / 10),
				new Point(base.Width / 2, base.Height * 7 / 10 + base.Height / 25),
				new Point(base.Width - 10, base.Height * 7 / 10)
			});
			if (!string.IsNullOrEmpty(text))
			{
				SizeF sizeF = graphics.MeasureString(text, Font, (base.Width - 20) * 3 / 5);
				if (sizeF.Width < (float)((base.Width - 20) * 4 / 5))
				{
					sizeF.Width = (base.Width - 20) * 3 / 5;
				}
				sizeF.Width += 10f;
				sizeF.Height += 5f;
				Rectangle rectangle = new Rectangle(base.Width / 2 - (int)(sizeF.Width / 2f), base.Height / 2 - (int)(sizeF.Height / 2f), (int)sizeF.Width, (int)sizeF.Height);
				graphics.FillRectangle(Brushes.DarkGreen, rectangle);
				graphics.DrawString(text, Font, textBrush, rectangle, stringFormat);
			}
		}

		/// <summary> 
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary> 
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			SuspendLayout();
			base.AutoScaleDimensions = new System.Drawing.SizeF(7f, 17f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			BackColor = System.Drawing.Color.Transparent;
			Font = new System.Drawing.Font("微软雅黑", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 134);
			base.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			base.Name = "UserDrum";
			base.Size = new System.Drawing.Size(166, 196);
			base.Paint += new System.Windows.Forms.PaintEventHandler(UserDrum_Paint);
			ResumeLayout(false);
		}
	}
}
