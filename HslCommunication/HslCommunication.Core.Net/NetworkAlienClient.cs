using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using HslCommunication.Core.IMessage;

namespace HslCommunication.Core.Net
{
	/// <summary>
	/// 异形客户端的基类，提供了基础的异形操作<br />
	/// The base class of the profiled client provides the basic profiled operation
	/// </summary>
	public class NetworkAlienClient : NetworkServerBase
	{
		/// <summary>
		/// 客户上线的委托事件
		/// </summary>
		/// <param name="session">异形客户端的会话信息</param>
		public delegate void OnClientConnectedDelegate(AlienSession session);

		private byte[] password;

		private List<string> trustOnline;

		private SimpleHybirdLock trustLock;

		private bool isResponseAck = true;

		private bool isCheckPwd = true;

		/// <summary>
		/// 状态登录成功
		/// </summary>
		public const byte StatusOk = 0;

		/// <summary>
		/// 重复登录
		/// </summary>
		public const byte StatusLoginRepeat = 1;

		/// <summary>
		/// 禁止登录
		/// </summary>
		public const byte StatusLoginForbidden = 2;

		/// <summary>
		/// 密码错误
		/// </summary>
		public const byte StatusPasswodWrong = 3;

		/// <summary>
		/// 是否返回响应，默认为 <c>True</c><br />
		/// The default is <c>True</c>
		/// </summary>
		public bool IsResponseAck
		{
			get
			{
				return isResponseAck;
			}
			set
			{
				isResponseAck = value;
			}
		}

		/// <summary>
		/// 是否统一检查密码，如果每个会话需要自己检查密码，就需要设置为false<br />
		/// Whether to check the password uniformly, if each session needs to check the password by itself, it needs to be set to false
		/// </summary>
		public bool IsCheckPwd
		{
			get
			{
				return isCheckPwd;
			}
			set
			{
				isCheckPwd = value;
			}
		}

		/// <summary>
		/// 当有服务器连接上来的时候触发<br />
		/// Triggered when a server is connected
		/// </summary>
		public event OnClientConnectedDelegate OnClientConnected = null;

		/// <summary>
		/// 默认的无参构造方法<br />
		/// The default parameterless constructor
		/// </summary>
		public NetworkAlienClient()
		{
			password = new byte[6];
			trustOnline = new List<string>();
			trustLock = new SimpleHybirdLock();
		}

		/// <summary>
		/// 当接收到了新的请求的时候执行的操作<br />
		/// An action performed when a new request is received
		/// </summary>
		/// <param name="socket">异步对象</param>
		/// <param name="endPoint">终结点</param>
		protected override void ThreadPoolLogin(Socket socket, IPEndPoint endPoint)
		{
			OperateResult<byte[]> operateResult = ReceiveByMessage(socket, 5000, new AlienMessage());
			if (!operateResult.IsSuccess)
			{
				return;
			}
			string text = Encoding.ASCII.GetString(operateResult.Content, 5, 11).Trim('\0', ' ');
			bool flag = true;
			if (isCheckPwd)
			{
				for (int i = 0; i < password.Length; i++)
				{
					if (operateResult.Content[16 + i] != password[i])
					{
						flag = false;
						break;
					}
				}
			}
			if (!flag)
			{
				if (isResponseAck)
				{
					OperateResult operateResult2 = Send(socket, GetResponse(3));
					if (operateResult2.IsSuccess)
					{
						socket?.Close();
					}
				}
				else
				{
					socket?.Close();
				}
				base.LogNet?.WriteWarn(ToString(), "Login Password Wrong, Id:" + text);
				return;
			}
			AlienSession alienSession = new AlienSession
			{
				DTU = text,
				Socket = socket,
				IsStatusOk = true,
				Pwd = operateResult.Content.SelectMiddle(16, 6).ToHexString()
			};
			if (!IsClientPermission(alienSession))
			{
				if (isResponseAck)
				{
					OperateResult operateResult3 = Send(socket, GetResponse(2));
					if (operateResult3.IsSuccess)
					{
						socket?.Close();
					}
				}
				else
				{
					socket?.Close();
				}
				base.LogNet?.WriteWarn(ToString(), "Login Forbidden, Id:" + alienSession.DTU);
				return;
			}
			int num = IsClientOnline(alienSession);
			if (num != 0)
			{
				if (isResponseAck)
				{
					OperateResult operateResult4 = Send(socket, GetResponse(1));
					if (operateResult4.IsSuccess)
					{
						socket?.Close();
					}
				}
				else
				{
					socket?.Close();
				}
				base.LogNet?.WriteWarn(ToString(), GetMsgFromCode(alienSession.DTU, num));
				return;
			}
			if (isResponseAck)
			{
				OperateResult operateResult5 = Send(socket, GetResponse(0));
				if (!operateResult5.IsSuccess)
				{
					return;
				}
			}
			base.LogNet?.WriteWarn(ToString(), GetMsgFromCode(alienSession.DTU, num));
			this.OnClientConnected?.Invoke(alienSession);
		}

		/// <summary>
		/// 获取返回的命令信息
		/// </summary>
		/// <param name="status">状态</param>
		/// <returns>回发的指令信息</returns>
		private byte[] GetResponse(byte status)
		{
			byte[] obj = new byte[6]
			{
				72,
				115,
				110,
				0,
				1,
				0
			};
			obj[5] = status;
			return obj;
		}

		/// <summary>
		/// 检测当前的DTU是否在线
		/// </summary>
		/// <param name="session">当前的会话信息</param>
		/// <returns>当前的会话是否在线</returns>
		public virtual int IsClientOnline(AlienSession session)
		{
			return 0;
		}

		/// <summary>
		/// 检测当前的dtu是否允许登录
		/// </summary>
		/// <param name="session">当前的会话信息</param>
		/// <returns>当前的id是否可允许登录</returns>
		private bool IsClientPermission(AlienSession session)
		{
			bool result = false;
			trustLock.Enter();
			if (trustOnline.Count == 0)
			{
				result = true;
			}
			else
			{
				for (int i = 0; i < trustOnline.Count; i++)
				{
					if (trustOnline[i] == session.DTU)
					{
						result = true;
						break;
					}
				}
			}
			trustLock.Leave();
			return result;
		}

		/// <summary>
		/// 设置密码，需要传入长度为6的字节数组<br />
		/// To set the password, you need to pass in an array of bytes of length 6
		/// </summary>
		/// <param name="password">密码信息</param>
		public void SetPassword(byte[] password)
		{
			if (password != null && password.Length == 6)
			{
				password.CopyTo(this.password, 0);
			}
		}

		/// <summary>
		/// 设置可信任的客户端列表，传入一个DTU的列表信息<br />
		/// Set up the list of trusted clients, passing in the list information for a DTU
		/// </summary>
		/// <param name="clients">客户端列表</param>
		public void SetTrustClients(string[] clients)
		{
			trustOnline = new List<string>(clients);
		}

		/// <inheritdoc />
		public override string ToString()
		{
			return "NetworkAlienBase";
		}

		/// <summary>
		/// 获取错误的描述信息
		/// </summary>
		/// <param name="dtu">dtu信息</param>
		/// <param name="code">错误码</param>
		/// <returns>错误信息</returns>
		public static string GetMsgFromCode(string dtu, int code)
		{
			return code switch
			{
				0 => "Login Success, Id:" + dtu, 
				1 => "Login Repeat, Id:" + dtu, 
				2 => "Login Forbidden, Id:" + dtu, 
				3 => "Login Passwod Wrong, Id:" + dtu, 
				_ => "Login Unknow reason, Id:" + dtu, 
			};
		}
	}
}
