using System;

namespace HslCommunication.Profinet.Inovance
{
	/// <summary>
	/// 安川机器人的辅助类，提供一些地址解析的方法<br />
	/// Auxiliary class of Yaskawa robot, providing some methods of address resolution
	/// </summary>
	public class InovanceHelper
	{
		private static int CalculateStartAddress(string address)
		{
			if (address.IndexOf('.') < 0)
			{
				return int.Parse(address);
			}
			string[] array = address.Split(new char[1]
			{
				'.'
			}, StringSplitOptions.RemoveEmptyEntries);
			return int.Parse(array[0]) * 8 + int.Parse(array[1]);
		}

		/// <summary>
		/// 根据安川PLC的地址，解析出转换后的modbus协议信息
		/// </summary>
		/// <param name="address">安川plc的地址信息</param>
		/// <param name="modbusCode">原始的对应的modbus信息</param>
		/// <returns>还原后的modbus地址</returns>
		public static OperateResult<string> PraseInovanceAMAddress(string address, byte modbusCode)
		{
			try
			{
				if (address.StartsWith("QX") || address.StartsWith("qx"))
				{
					return OperateResult.CreateSuccessResult(CalculateStartAddress(address.Substring(2)).ToString());
				}
				if (address.StartsWith("Q") || address.StartsWith("q"))
				{
					return OperateResult.CreateSuccessResult(CalculateStartAddress(address.Substring(1)).ToString());
				}
				if (address.StartsWith("IX") || address.StartsWith("ix"))
				{
					return OperateResult.CreateSuccessResult("x=2;" + CalculateStartAddress(address.Substring(2)));
				}
				if (address.StartsWith("I") || address.StartsWith("i"))
				{
					return OperateResult.CreateSuccessResult("x=2;" + CalculateStartAddress(address.Substring(1)));
				}
				if (address.StartsWith("MW") || address.StartsWith("mw"))
				{
					return OperateResult.CreateSuccessResult(address.Substring(2));
				}
				if (address.StartsWith("M") || address.StartsWith("m"))
				{
					return OperateResult.CreateSuccessResult(address.Substring(1));
				}
				if (modbusCode == 1 || modbusCode == 15 || modbusCode == 5)
				{
					if (address.StartsWith("SMX") || address.StartsWith("smx"))
					{
						return OperateResult.CreateSuccessResult($"x={modbusCode + 48};" + CalculateStartAddress(address.Substring(3)));
					}
					if (address.StartsWith("SM") || address.StartsWith("sm"))
					{
						return OperateResult.CreateSuccessResult($"x={modbusCode + 48};" + CalculateStartAddress(address.Substring(2)));
					}
				}
				else
				{
					if (address.StartsWith("SDW") || address.StartsWith("sdw"))
					{
						return OperateResult.CreateSuccessResult($"x={modbusCode + 48};" + address.Substring(3));
					}
					if (address.StartsWith("SD") || address.StartsWith("sd"))
					{
						return OperateResult.CreateSuccessResult($"x={modbusCode + 48};" + address.Substring(2));
					}
				}
				return new OperateResult<string>(StringResources.Language.NotSupportedDataType);
			}
			catch (Exception ex)
			{
				return new OperateResult<string>(ex.Message);
			}
		}

		private static int CalculateH3UStartAddress(string address)
		{
			if (address.IndexOf('.') < 0)
			{
				return Convert.ToInt32(address, 8);
			}
			string[] array = address.Split(new char[1]
			{
				'.'
			}, StringSplitOptions.RemoveEmptyEntries);
			return Convert.ToInt32(array[0], 8) * 8 + int.Parse(array[1]);
		}

		/// <summary>
		/// 根据安川PLC的地址，解析出转换后的modbus协议信息，适用H3U系列
		/// </summary>
		/// <param name="address">安川plc的地址信息</param>
		/// <param name="modbusCode">原始的对应的modbus信息</param>
		/// <returns>还原后的modbus地址</returns>
		public static OperateResult<string> PraseInovanceH3UAddress(string address, byte modbusCode)
		{
			try
			{
				if (modbusCode == 1 || modbusCode == 15 || modbusCode == 5)
				{
					if (address.StartsWith("X") || address.StartsWith("x"))
					{
						return OperateResult.CreateSuccessResult((CalculateH3UStartAddress(address.Substring(1)) + 63488).ToString());
					}
					if (address.StartsWith("Y") || address.StartsWith("y"))
					{
						return OperateResult.CreateSuccessResult((CalculateH3UStartAddress(address.Substring(1)) + 64512).ToString());
					}
					if (address.StartsWith("SM") || address.StartsWith("sm"))
					{
						return OperateResult.CreateSuccessResult((Convert.ToInt32(address.Substring(2)) + 9216).ToString());
					}
					if (address.StartsWith("S") || address.StartsWith("s"))
					{
						return OperateResult.CreateSuccessResult((Convert.ToInt32(address.Substring(1)) + 57344).ToString());
					}
					if (address.StartsWith("T") || address.StartsWith("t"))
					{
						return OperateResult.CreateSuccessResult((Convert.ToInt32(address.Substring(1)) + 61440).ToString());
					}
					if (address.StartsWith("C") || address.StartsWith("c"))
					{
						return OperateResult.CreateSuccessResult((Convert.ToInt32(address.Substring(1)) + 62464).ToString());
					}
					if (address.StartsWith("M") || address.StartsWith("m"))
					{
						int num = Convert.ToInt32(address.Substring(1));
						if (num >= 8000)
						{
							return OperateResult.CreateSuccessResult((num - 8000 + 8000).ToString());
						}
						return OperateResult.CreateSuccessResult(num.ToString());
					}
				}
				else
				{
					if (address.StartsWith("D") || address.StartsWith("d"))
					{
						return OperateResult.CreateSuccessResult(Convert.ToInt32(address.Substring(1)).ToString());
					}
					if (address.StartsWith("SD") || address.StartsWith("sd"))
					{
						return OperateResult.CreateSuccessResult((Convert.ToInt32(address.Substring(2)) + 9216).ToString());
					}
					if (address.StartsWith("R") || address.StartsWith("r"))
					{
						return OperateResult.CreateSuccessResult((Convert.ToInt32(address.Substring(1)) + 12288).ToString());
					}
					if (address.StartsWith("T") || address.StartsWith("t"))
					{
						return OperateResult.CreateSuccessResult((Convert.ToInt32(address.Substring(1)) + 61440).ToString());
					}
					if (address.StartsWith("C") || address.StartsWith("c"))
					{
						int num2 = Convert.ToInt32(address.Substring(1));
						if (num2 >= 200)
						{
							return OperateResult.CreateSuccessResult(((num2 - 200) * 2 + 63232).ToString());
						}
						return OperateResult.CreateSuccessResult((num2 + 62464).ToString());
					}
				}
				return new OperateResult<string>(StringResources.Language.NotSupportedDataType);
			}
			catch (Exception ex)
			{
				return new OperateResult<string>(ex.Message);
			}
		}

		/// <summary>
		/// 根据安川PLC的地址，解析出转换后的modbus协议信息，适用H5U系列
		/// </summary>
		/// <param name="address">安川plc的地址信息</param>
		/// <param name="modbusCode">原始的对应的modbus信息</param>
		/// <returns>还原后的modbus地址</returns>
		public static OperateResult<string> PraseInovanceH5UAddress(string address, byte modbusCode)
		{
			try
			{
				if (modbusCode == 1 || modbusCode == 15 || modbusCode == 5)
				{
					if (address.StartsWith("X") || address.StartsWith("x"))
					{
						return OperateResult.CreateSuccessResult((CalculateH3UStartAddress(address.Substring(1)) + 63488).ToString());
					}
					if (address.StartsWith("Y") || address.StartsWith("y"))
					{
						return OperateResult.CreateSuccessResult((CalculateH3UStartAddress(address.Substring(1)) + 64512).ToString());
					}
					if (address.StartsWith("S") || address.StartsWith("s"))
					{
						return OperateResult.CreateSuccessResult((Convert.ToInt32(address.Substring(1)) + 57344).ToString());
					}
					if (address.StartsWith("B") || address.StartsWith("b"))
					{
						return OperateResult.CreateSuccessResult((Convert.ToInt32(address.Substring(1)) + 12288).ToString());
					}
					if (address.StartsWith("M") || address.StartsWith("m"))
					{
						return OperateResult.CreateSuccessResult(Convert.ToInt32(address.Substring(1)).ToString());
					}
				}
				else
				{
					if (address.StartsWith("D") || address.StartsWith("d"))
					{
						return OperateResult.CreateSuccessResult(Convert.ToInt32(address.Substring(1)).ToString());
					}
					if (address.StartsWith("R") || address.StartsWith("r"))
					{
						return OperateResult.CreateSuccessResult((Convert.ToInt32(address.Substring(1)) + 12288).ToString());
					}
				}
				return new OperateResult<string>(StringResources.Language.NotSupportedDataType);
			}
			catch (Exception ex)
			{
				return new OperateResult<string>(ex.Message);
			}
		}
	}
}
