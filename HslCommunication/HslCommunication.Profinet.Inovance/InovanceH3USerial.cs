using HslCommunication.ModBus;

namespace HslCommunication.Profinet.Inovance
{
	/// <summary>
	/// 汇川的串口通信协议，适用于H3U, XP 等系列，底层走的是MODBUS-TCP协议，地址说明参见标记<br />
	/// Huichuan's serial communication protocol is suitable for H3U, XP and other series. 
	/// The bottom layer is MODBUS-TCP protocol. For the address description, please refer to the mark
	/// </summary>
	/// <remarks>
	/// <inheritdoc cref="T:HslCommunication.Profinet.Inovance.InovanceH3UTcp" path="remarks" />
	/// </remarks>
	public class InovanceH3USerial : ModbusRtu
	{
		/// <inheritdoc cref="M:HslCommunication.Profinet.Inovance.InovanceH3UTcp.#ctor" />
		public InovanceH3USerial()
		{
		}

		/// <summary>
		/// 指定客户端自己的站号来初始化<br />
		/// Specify the client's own station number to initialize
		/// </summary>
		/// <param name="station">客户端自身的站号</param>
		public InovanceH3USerial(byte station = 1)
			: base(station)
		{
		}

		/// <inheritdoc cref="M:HslCommunication.Profinet.Inovance.InovanceH3UTcp.Read(System.String,System.UInt16)" />
		public override OperateResult<byte[]> Read(string address, ushort length)
		{
			OperateResult<string> operateResult = InovanceHelper.PraseInovanceH3UAddress(address, 3);
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<byte[]>(operateResult);
			}
			return base.Read(operateResult.Content, length);
		}

		/// <inheritdoc cref="M:HslCommunication.Profinet.Inovance.InovanceH3UTcp.Write(System.String,System.Byte[])" />
		public override OperateResult Write(string address, byte[] value)
		{
			OperateResult<string> operateResult = InovanceHelper.PraseInovanceH3UAddress(address, 16);
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<byte[]>(operateResult);
			}
			return base.Write(operateResult.Content, value);
		}

		/// <inheritdoc cref="M:HslCommunication.Profinet.Inovance.InovanceH3UTcp.ReadBool(System.String,System.UInt16)" />
		public override OperateResult<bool[]> ReadBool(string address, ushort length)
		{
			OperateResult<string> operateResult = InovanceHelper.PraseInovanceH3UAddress(address, 1);
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<bool[]>(operateResult);
			}
			return base.ReadBool(operateResult.Content, length);
		}

		/// <inheritdoc cref="M:HslCommunication.Profinet.Inovance.InovanceH3UTcp.Write(System.String,System.Boolean[])" />
		public override OperateResult Write(string address, bool[] values)
		{
			OperateResult<string> operateResult = InovanceHelper.PraseInovanceH3UAddress(address, 15);
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<bool[]>(operateResult);
			}
			return base.Write(operateResult.Content, values);
		}

		/// <inheritdoc cref="M:HslCommunication.Profinet.Inovance.InovanceH3UTcp.Write(System.String,System.Boolean)" />
		public override OperateResult Write(string address, bool value)
		{
			OperateResult<string> operateResult = InovanceHelper.PraseInovanceH3UAddress(address, 5);
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<bool[]>(operateResult);
			}
			return base.Write(operateResult.Content, value);
		}

		/// <inheritdoc cref="M:HslCommunication.Profinet.Inovance.InovanceH3UTcp.Write(System.String,System.Int16)" />
		public override OperateResult Write(string address, short value)
		{
			OperateResult<string> operateResult = InovanceHelper.PraseInovanceH3UAddress(address, 6);
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<bool[]>(operateResult);
			}
			return base.Write(operateResult.Content, value);
		}

		/// <inheritdoc cref="M:HslCommunication.Profinet.Inovance.InovanceH3UTcp.Write(System.String,System.UInt16)" />
		public override OperateResult Write(string address, ushort value)
		{
			OperateResult<string> operateResult = InovanceHelper.PraseInovanceH3UAddress(address, 6);
			if (!operateResult.IsSuccess)
			{
				return OperateResult.CreateFailedResult<bool[]>(operateResult);
			}
			return base.Write(operateResult.Content, value);
		}

		/// <inheritdoc />
		public override string ToString()
		{
			return $"InovanceH3USerial[{base.PortName}:{base.BaudRate}]";
		}
	}
}
