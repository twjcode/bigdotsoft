using HslCommunication.Core;
using HslCommunication.Serial;

namespace HslCommunication.Profinet.Melsec
{
	/// <summary>
	/// 基于Qna 兼容3C帧的格式一的通讯，具体的地址需要参照三菱的基本地址<br />
	/// Based on Qna-compatible 3C frame format one communication, the specific address needs to refer to the basic address of Mitsubishi.
	/// </summary>
	/// <remarks>
	/// <inheritdoc cref="T:HslCommunication.Profinet.Melsec.MelsecA3CNet1OverTcp" path="remarks" />
	/// </remarks>
	public class MelsecA3CNet1 : SerialDeviceBase
	{
		private byte station = 0;

		/// <inheritdoc cref="P:HslCommunication.Profinet.Melsec.MelsecA3CNet1OverTcp.Station" />
		public byte Station
		{
			get
			{
				return station;
			}
			set
			{
				station = value;
			}
		}

		/// <inheritdoc cref="M:HslCommunication.Profinet.Melsec.MelsecA3CNet1OverTcp.#ctor" />
		public MelsecA3CNet1()
		{
			base.ByteTransform = new RegularByteTransform();
			base.WordLength = 1;
		}

		private OperateResult<byte[]> ReadWithPackCommand(byte[] command)
		{
			return ReadBase(MelsecA3CNet1OverTcp.PackCommand(command, station));
		}

		/// <inheritdoc cref="M:HslCommunication.Profinet.Melsec.MelsecA3CNet1OverTcp.Read(System.String,System.UInt16)" />
		public override OperateResult<byte[]> Read(string address, ushort length)
		{
			return MelsecA3CNet1OverTcp.ReadHelper(address, length, ReadWithPackCommand);
		}

		/// <inheritdoc cref="M:HslCommunication.Profinet.Melsec.MelsecA3CNet1OverTcp.Write(System.String,System.Byte[])" />
		public override OperateResult Write(string address, byte[] value)
		{
			return MelsecA3CNet1OverTcp.WriteHelper(address, value, ReadWithPackCommand);
		}

		/// <inheritdoc cref="M:HslCommunication.Profinet.Melsec.MelsecA3CNet1OverTcp.ReadBool(System.String,System.UInt16)" />
		public override OperateResult<bool[]> ReadBool(string address, ushort length)
		{
			return MelsecA3CNet1OverTcp.ReadBoolHelper(address, length, ReadWithPackCommand);
		}

		/// <inheritdoc cref="M:HslCommunication.Profinet.Melsec.MelsecA3CNet1OverTcp.Write(System.String,System.Boolean[])" />
		public override OperateResult Write(string address, bool[] value)
		{
			return MelsecA3CNet1OverTcp.WriteHelper(address, value, ReadWithPackCommand);
		}

		/// <inheritdoc cref="M:HslCommunication.Profinet.Melsec.MelsecA3CNet1OverTcp.RemoteRun" />
		public OperateResult RemoteRun()
		{
			return MelsecA3CNet1OverTcp.RemoteRunHelper(ReadWithPackCommand);
		}

		/// <inheritdoc cref="M:HslCommunication.Profinet.Melsec.MelsecA3CNet1OverTcp.RemoteStop" />
		public OperateResult RemoteStop()
		{
			return MelsecA3CNet1OverTcp.RemoteStopHelper(ReadWithPackCommand);
		}

		/// <inheritdoc cref="M:HslCommunication.Profinet.Melsec.MelsecA3CNet1OverTcp.ReadPlcType" />
		public OperateResult<string> ReadPlcType()
		{
			return MelsecA3CNet1OverTcp.ReadPlcTypeHelper(ReadWithPackCommand);
		}

		/// <inheritdoc />
		public override string ToString()
		{
			return $"MelsecA3CNet1[{base.PortName}:{base.BaudRate}]";
		}
	}
}
