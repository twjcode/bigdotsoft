using System.Threading.Tasks;
using HslCommunication.Core;
using HslCommunication.Serial;

namespace HslCommunication.Profinet.Melsec
{
	/// <summary>
	/// 三菱的串口通信的对象，适用于读取FX系列的串口数据，支持的类型参考文档说明<br />
	/// Mitsubishi's serial communication object is suitable for reading serial data of the FX series. Refer to the documentation for the supported types.
	/// </summary>
	/// <remarks>
	/// <inheritdoc cref="T:HslCommunication.Profinet.Melsec.MelsecFxSerialOverTcp" path="remarks" />
	/// </remarks>
	/// <example>
	/// <code lang="cs" source="HslCommunication_Net45.Test\Documentation\Samples\Profinet\MelsecFxSerial.cs" region="Usage" title="简单的使用" />
	/// </example>
	public class MelsecFxSerial : SerialDeviceBase
	{
		/// <summary>
		/// 实例化一个默认的对象
		/// </summary>
		public MelsecFxSerial()
		{
			base.ByteTransform = new RegularByteTransform();
			base.WordLength = 1;
		}

		/// <inheritdoc cref="M:HslCommunication.Profinet.Melsec.MelsecFxSerialOverTcp.Read(System.String,System.UInt16)" />
		public override OperateResult<byte[]> Read(string address, ushort length)
		{
			return MelsecFxSerialOverTcp.ReadHelper(address, length, base.ReadBase);
		}

		/// <inheritdoc cref="M:HslCommunication.Profinet.Melsec.MelsecFxSerialOverTcp.Write(System.String,System.Byte[])" />
		public override OperateResult Write(string address, byte[] value)
		{
			return MelsecFxSerialOverTcp.WriteHelper(address, value, base.ReadBase);
		}

		/// <inheritdoc cref="M:HslCommunication.Profinet.Melsec.MelsecFxSerialOverTcp.ReadBool(System.String,System.UInt16)" />
		public override OperateResult<bool[]> ReadBool(string address, ushort length)
		{
			return MelsecFxSerialOverTcp.ReadBoolHelper(address, length, base.ReadBase);
		}

		/// <inheritdoc cref="M:HslCommunication.Profinet.Melsec.MelsecFxSerialOverTcp.Write(System.String,System.Boolean)" />
		public override OperateResult Write(string address, bool value)
		{
			return MelsecFxSerialOverTcp.WriteHelper(address, value, base.ReadBase);
		}

		/// <inheritdoc />
		public override async Task<OperateResult> WriteAsync(string address, bool value)
		{
			return await Task.Run(() => Write(address, value));
		}

		/// <inheritdoc />
		public override string ToString()
		{
			return "MelsecFxSerial";
		}
	}
}
