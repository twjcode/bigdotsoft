using System.Runtime.CompilerServices;

namespace HslCommunication.Profinet.Melsec
{
	/// <summary>
	/// 在三菱的PLC通信的MC协议中，分为串行通信的报文和以太网接口的报文。<br />
	/// 在串口通信中，共有以下几种帧，其中1C,2C,3C帧支持格式1，2，3，4，在C帧里支持格式5通信<br />
	/// <list type="number">
	/// <item>4C帧，QnA系列串行通信模块专用协议（Qna扩展帧）</item>
	/// <item>3C帧，QnA系列串行通信模块专用协议（Qna帧）</item>
	/// <item>2C帧，QnA系列串行通信模块专用协议（Qna简易帧）</item>
	/// <item>1C帧，A系列计算机链接模块专用协议</item>
	/// </list>
	/// 在以太网通信中，共有以下几种帧，每种帧支持二进制和ASCII格式
	/// <list type="number">
	/// <item>4E帧，是3E帧上附加了“序列号”。</item>
	/// <item>3E帧，QnA系列以太网接口模块的报文格式，兼容SLMP的报文格式</item>
	/// <item>1E帧，A系列以太网接口模块的报文格式</item>
	/// </list>
	/// 在以太网通信里，HSL主要针对1E帧协议和3E帧协议进行实现，
	/// </summary>
	[CompilerGenerated]
	public class NamespaceDoc
	{
	}
}
